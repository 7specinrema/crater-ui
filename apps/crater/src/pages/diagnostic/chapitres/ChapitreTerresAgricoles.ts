import '../../commun/TimeLine.js';
import '../charts/ChartBarres.js';
import './composants/CommentAgir.js';
import './composants/IndicateurDetaille.js';
import './composants/IndicateurResume.js';
import './composants/PourquoiCestImportant.js';
import './composants/VoirEgalement.js';
import './SyntheseMaillon.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import {
    calculerQualificatifEvolution,
    calculerSigneNombre,
    formaterNombreEnNDecimalesString,
    formaterNombreSelonValeurString,
    QualificatifEvolution
} from '@lga/commun/build/outils/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { Rapport, SEUIL_SAU_PAR_HAB_M2_REGIME_ACTUEL, TerresAgricoles } from '../../../domaines/diagnostics';
import { DonneesPageDiagnostic } from '../../../domaines/pages/donnees-pages.js';
import { construireUrlPageDiagnosticMaillon, construireUrlPageDiagnosticTerritoire } from '../../../domaines/pages/pages-utils';
import { ICONES_SVG } from '../../../domaines/systeme-alimentaire/configuration/icones_svg';
import { IDS_INDICATEURS } from '../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { IDS_MAILLONS } from '../../../domaines/systeme-alimentaire/configuration/maillons';
import { SA } from '../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire';
import { Maillon } from '../../../domaines/systeme-alimentaire/index.js';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import { creerOptionsSyntheseMaillon } from './chapitres-outils';
import { OptionsIndicateurResume } from './composants/IndicateurResume';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-terres-agricoles': ChapitreTerresAgricoles;
    }
}

@customElement('c-chapitre-terres-agricoles')
export class ChapitreTerresAgricoles extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_CHAPITRES_DIAGNOSTIC];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly maillon: Maillon = SA.getMaillon(IDS_MAILLONS.terresAgricoles)!;

    render() {
        if (!this.rapport) return;

        const diagTA = this.rapport.getDiagnosticActif()!.terresAgricoles;

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.maillon?.nom}</h1>

                <c-synthese-maillon
                    .options=${creerOptionsSyntheseMaillon(
                        this.rapport,
                        this.rapport.getDiagnosticActif()!.terresAgricoles,
                        IDS_MAILLONS.terresAgricoles,
                        IDS_INDICATEURS.noteTerresAgricoles
                    )}
                >
                </c-synthese-maillon>

                <h2 id="pourquoi-cest-important">Pourquoi c'est important ?</h2>
                <c-pourquoi-cest-important .maillon=${this.maillon}></c-pourquoi-cest-important>

                <h2 id="etat-lieux">Quel est l'état des lieux ?</h2>
                <div class="indicateurs-resumes">
                    <p>Y a-t-il assez de terres agricoles pour nourrir les habitants ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-indicateurs-resumes">
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumeSauParHabitant(diagTA, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.sauParHabitant}"
                        ></c-indicateur-resume>
                    </div>
                    <p>Les terres agricoles, naturelles et forestières sont-elles artificialisées ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-indicateurs-resumes">
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumeRythmeArtificialisation(diagTA, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.rythmeArtificialisation}"
                        ></c-indicateur-resume>
                    </div>
                    <p>Est-ce justifié ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-indicateurs-resumes">
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumePolitiqueAmenagement(diagTA, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.politiqueAmenagement}"
                        ></c-indicateur-resume>
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumePartLogementsVacants(diagTA, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.partLogementsVacants}"
                        ></c-indicateur-resume>
                    </div>
                </div>

                <c-voir-egalement .liensIndicateurs=${this.maillon.indicateursAutresMaillons} .donneesPage=${this.donneesPage}></c-voir-egalement>

                <h2 id="leviers-action">Comment ma collectivité peut-elle s&#39;améliorer ?</h2>
                <c-comment-agir
                    .maillon=${this.maillon}
                    idTerritoireParcel=${ifDefined(this.rapport.getDiagnosticActif()?.idTerritoireParcel || undefined)}
                ></c-comment-agir>

                <c-encart-pages-precedente-suivante
                    libellePagePrecedente="Présentation du territoire"
                    hrefPagePrecedente=${construireUrlPageDiagnosticTerritoire(
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale ?? ''
                    )}
                    libellePageSuivante=${ifDefined(SA.getMaillon(IDS_MAILLONS.agriculteursExploitations)?.nom)}
                    hrefPageSuivante=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.agriculteursExploitations,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsIndicateurResumeSauParHabitant(
        diagTA: TerresAgricoles,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.sauParHabitant)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '',
                    chiffre: formaterNombreEnNDecimalesString(diagTA.sauParHabitantM2, 0) + ' m²',
                    libelleApresChiffre: 'de surface agricole utile productive par habitant'
                },
                {
                    libelleAvantChiffre: '',
                    chiffre: formaterNombreEnNDecimalesString(SEUIL_SAU_PAR_HAB_M2_REGIME_ACTUEL, 0) + ' m²',
                    libelleApresChiffre: 'nécéssaires pour le régime alimentaire actuel'
                }
            ],
            motSeparateurChiffresCles: 'contre',
            donneesPage: donneesPage
        };
    }
    private calculerOptionsIndicateurResumeRythmeArtificialisation(
        diagTA: TerresAgricoles,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.rythmeArtificialisation)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: 'l’équivalent de',
                    chiffre: formaterNombreSelonValeurString(diagTA.rythmeArtificialisationSauPourcent!) + ' %',
                    libelleApresChiffre: 'de la surface agricole a été artificialisé en 5 ans'
                },
                {
                    libelleAvantChiffre: '&nbsp',
                    chiffre: diagTA.chiffreRythmeArtificialisation.nombre + ' ',
                    icone: ICONES_SVG.terrainFootball,
                    libelleApresChiffre: 'terrains de football ' + diagTA.chiffreRythmeArtificialisation.periode
                }
            ],
            motSeparateurChiffresCles: 'soit',
            donneesPage: donneesPage
        };
    }
    private calculerOptionsIndicateurResumePolitiqueAmenagement(
        diagTA: TerresAgricoles,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.politiqueAmenagement)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '',
                    chiffre: `${formaterNombreSelonValeurString(diagTA.artificialisation5ansHa)}`,
                    libelleApresChiffre: 'hectares artificialisés entre 2011 et 2016',
                    evolutionQualificatif: diagTA.rythmeArtificialisationSauPourcent! > 0 ? QualificatifEvolution.rate : QualificatifEvolution.valide,
                    evolutionDescription: `Objectif Zéro Artificialisation ${diagTA.rythmeArtificialisationSauPourcent! > 0 ? ' raté' : ' atteint'}`
                },
                {
                    libelleAvantChiffre: '',
                    chiffre: `${formaterNombreSelonValeurString(diagTA.evolutionMenagesEmplois5ans, true)}`,
                    libelleApresChiffre: 'ménages + emplois sur la même période'
                }
            ],
            motSeparateurChiffresCles: 'vs',
            donneesPage: donneesPage
        };
    }
    private calculerOptionsIndicateurResumePartLogementsVacants(
        diagTA: TerresAgricoles,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partLogementsVacants)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '',
                    chiffre: formaterNombreEnNDecimalesString(diagTA.partLogementsVacants2018Pourcent!, 1) + ' %',
                    libelleApresChiffre: 'des logements',
                    evolutionSigne: calculerSigneNombre(diagTA.evolutionPartLogementsVacants2013a2018Points),
                    evolutionQualificatif: calculerQualificatifEvolution(
                        calculerSigneNombre(diagTA.evolutionPartLogementsVacants2013a2018Points),
                        'negative'
                    ),
                    evolutionDescription: `${formaterNombreSelonValeurString(
                        diagTA.evolutionPartLogementsVacants2013a2018Points,
                        true
                    )} pts entre 2011 et 2016`
                }
            ],
            donneesPage: donneesPage
        };
    }
}
