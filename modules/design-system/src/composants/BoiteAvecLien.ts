import './Boite.js';

import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { ICONES_DESIGN_SYSTEM } from '../styles/icones-design-system.js';
import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-boite-avec-lien': BoiteAvecLien;
    }
}

@customElement('c-boite-avec-lien')
export class BoiteAvecLien extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100%;
                height: 100%;
            }

            a {
                text-decoration: none;
            }

            #contenu {
                display: flex;
                padding: var(--dsem);
            }

            #trait-separation {
                height: 1px;
                width: 80%;
                background: var(--couleur-neutre-20);
                margin: auto;
                display: var(--displayPDF, block);
            }

            #lien {
                display: var(--displayPDF, flex);
                color: var(--couleur-primaire, black);
                text-decoration: none;
                justify-content: center;
                align-items: center;
                padding: calc(2 * var(--dsem));
            }

            #lien svg {
                width: 1.5rem;
                height: 1.5rem;
                padding-right: 0.2rem;
            }

            #lien svg * {
                fill: var(--couleur-primaire, black);
                stroke: var(--couleur-primaire, black);
            }

            a:hover #lien,
            a:hover #lien svg * {
                color: var(--couleur-accent);
                fill: var(--couleur-accent);
                stroke: var(--couleur-accent);
            }
        `
    ];

    @property()
    libelleLien = '';

    @property()
    href = '';

    @property({ type: Boolean })
    ombre?: boolean;

    @property({ type: Boolean })
    encadrement?: boolean;

    @property()
    arrondi?: string;

    render() {
        return html`
            <c-boite ?encadrement=${this.encadrement} arrondi=${ifDefined(this.arrondi)} ?ombre=${this.ombre}>
                <a href=${this.href}>
                    <slot name="contenu"></slot>
                    <div id="trait-separation"></div>
                    <div id="lien" class="texte-alternatif">${ICONES_DESIGN_SYSTEM.flecheDroite} ${this.libelleLien}</div>
                </a>
            </c-boite>
        `;
    }
}
