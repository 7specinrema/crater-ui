import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/chapitres/MenuNavigationChapitres.js';

import { html } from 'lit';

import { DonneesPageDiagnostic } from '../../../src/domaines/pages/donnees-pages';

const diagnostic = {
    terresAgricoles: { note: undefined },
    agriculteursExploitations: { note: null },
    intrants: { note: 0 },
    production: { note: 3 },
    transformationDistribution: { note: 6 },
    consommation: { note: 9 }
};

describe('Test MenuNavigationChapitres', () => {
    it('Test affichage', () => {
        // given
        cy.viewport(500, 500);
        const donneesPage: DonneesPageDiagnostic = {
            idTerritoirePrincipal: 'idTerritoirePrincipal',
            chapitre: 'territoire'
        };
        cy.mount<'c-menu-navigation-chapitres'>(
            html` <c-menu-navigation-chapitres .diagnostic=${diagnostic} .donneesPage=${donneesPage}></c-menu-navigation-chapitres> `
        );
        // then
        cy.get('c-menu-item').eq(1).find('a').should('have.class', 'selectionne');
        cy.get('c-menu-item').eq(2).invoke('attr', 'sousLibelle').should('eq', 'Score : -/10');
        cy.get('c-menu-item').eq(2).find('circle').should('have.css', 'fill', 'rgb(215, 215, 215)');
        cy.get('c-menu-item').eq(3).invoke('attr', 'sousLibelle').should('eq', 'Score : -/10');
        cy.get('c-menu-item').eq(3).find('circle').should('have.css', 'fill', 'rgb(215, 215, 215)');
        cy.get('c-menu-item').eq(4).invoke('attr', 'sousLibelle').should('eq', 'Score : 0/10');
        cy.get('c-menu-item').eq(4).find('circle').should('have.css', 'fill', 'rgb(155, 0, 0)');
        cy.get('c-menu-item').eq(5).invoke('attr', 'sousLibelle').should('eq', 'Score : 3/10');
        cy.get('c-menu-item').eq(5).find('circle').should('have.css', 'fill', 'rgb(208, 92, 45)');
        cy.get('c-menu-item').eq(6).invoke('attr', 'sousLibelle').should('eq', 'Score : 6/10');
        cy.get('c-menu-item').eq(6).find('circle').should('have.css', 'fill', 'rgb(201, 153, 76)');
        cy.get('c-menu-item').eq(7).invoke('attr', 'sousLibelle').should('eq', 'Score : 9/10');
        cy.get('c-menu-item').eq(7).find('circle').should('have.css', 'fill', 'rgb(75, 151, 82)');
    });
});
