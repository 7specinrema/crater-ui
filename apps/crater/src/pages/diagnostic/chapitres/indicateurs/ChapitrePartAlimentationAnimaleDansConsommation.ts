import '../../charts/ChartTreemapConsommation.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { OptionsTreemapConsommation } from '../../charts/ChartTreemapConsommation.js';
import { creerDonneesTreemapConsommation } from '../chapitres-outils.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-alimentation-animale-dans-consommation': ChapitrePartAlimentationAnimaleDansConsommation;
    }
}

@customElement('c-chapitre-part-alimentation-animale-dans-consommation')
export class ChapitrePartAlimentationAnimaleDansConsommation extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html` <c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.partAlimentationAnimaleDansConsommation}"
                .options=${this.calculerOptionsIndicateurDetailleEmpreinteSurfaceHumainsAnimaux(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="5"
            >
                <c-chart-treemap-consommation
                    .options=${this.calculerOptionsChartTreemapConsommation(this.rapport)}
                    slot="chart-indicateur-detaille"
                ></c-chart-treemap-consommation>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetailleEmpreinteSurfaceHumainsAnimaux(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partAlimentationAnimaleDansConsommation)!,
            message: `<p>
                    Les aliments d'origine animale (viande, oeufs, produits laitiers) requièrent davantage de ressources et de terres cultivées que
                    les produits végétaux pour atteindre une valeur nutritive similaire.
                </p>
            ${rapport.getDiagnosticActif()?.consommation.messagePartAlimentationAnimaleDansConsommation}
            `,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }

    private calculerOptionsChartTreemapConsommation(rapport: Rapport): OptionsTreemapConsommation {
        const donneesTreemapConsommationAnimaux = creerDonneesTreemapConsommation(
            rapport.getDiagnosticActif()!.consommation!.indicateurConsommation!.consommationAnimauxParGroupeCulture
        );
        const donneesTreemapConsommationHumains = creerDonneesTreemapConsommation(
            rapport.getDiagnosticActif()!.consommation!.indicateurConsommation!.consommationHumainsParGroupeCulture
        );

        return {
            consommationAnimauxHa: rapport.getDiagnosticActif()!.consommation!.indicateurConsommation!.consommationAnimauxHa,
            consommationHumainsHa: rapport.getDiagnosticActif()!.consommation!.indicateurConsommation!.consommationHumainsHa,
            donneesTreemapConsommationAnimaux: donneesTreemapConsommationAnimaux,
            donneesTreemapConsommationHumains: donneesTreemapConsommationHumains
        };
    }
}
