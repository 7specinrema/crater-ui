import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/commun/Sablier.js';

import { html } from 'lit';

describe('Test Sablier', () => {
    it('Desktop + Mobile', () => {
        cy.mount<'c-sablier'>(html`<c-sablier></c-sablier>`);
        cy.get('c-sablier').get('#logo').should('be.visible');
    });
});
