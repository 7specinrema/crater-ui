import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../src/pages/commun/MenuPiedPage.js';

import { html } from 'lit';

import { PAGES_PRINCIPALES } from '../../src/domaines/pages/configuration/declaration-pages';
describe('Test MenuPiedPage', () => {
    it('Test affichage menu pied page mobile avec bouton sommaire', () => {
        // given
        cy.viewport(500, 500);
        cy.mount<'c-menu-pied-page'>(
            html` <c-menu-pied-page idItemActif=${PAGES_PRINCIPALES.carte.getId()}>
                <div slot="contenu-sommaire" style="height:100%; background-color: red;padding-top: 5rem">
                    <p id="id-contenu">Contenu panneau</p>
                </div>
            </c-menu-pied-page>`
        );
        // then
        cy.get('c-bouton-carre').eq(0).find('div').should('have.not.class', 'selectionne');
        cy.get('c-bouton-carre').eq(1).find('div').should('have.class', 'selectionne');
        cy.get('c-bouton-carre[libelle=Menu]').should('exist').click();
        cy.get('#id-contenu').should('be.visible');
    });

    it('Test affichage menu pied page mobile sans bouton sommaire', () => {
        // given
        cy.viewport(500, 500);
        cy.mount<'c-menu-pied-page'>(
            html` <c-menu-pied-page idItemActif=${PAGES_PRINCIPALES.carte.getId()} desactiverBoutonSommaire> </c-menu-pied-page>`
        );
        // then
        cy.get('c-bouton-carre').eq(0).find('div').should('have.not.class', 'selectionne');
        cy.get('c-bouton-carre').eq(1).find('div').should('have.class', 'selectionne');
        cy.get('c-bouton-carre[libelle=Menu]').should('have.attr', 'desactive');
    });
});
