import { describe, expect, it } from 'vitest';

import { PolitiqueAmenagement } from './PolitiqueAmenagement';

describe("Tests de l'énumération PolitiqueAmenagement", () => {
    it('Vérifier les valeur créées par défaut', () => {
        const listeValeursEnum = PolitiqueAmenagement.listerToutes;

        expect(listeValeursEnum.map((v) => v.code)).toEqual([
            'ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE',
            'ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE',
            'ARTIF_QUASI_NULLE_OU_NEGATIVE',
            'DONNEES_NON_DISPONIBLES'
        ]);
    });

    it("Vérifier la récupération à partir d'une string", () => {
        expect(PolitiqueAmenagement.fromString('ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE')).toEqual(
            PolitiqueAmenagement.ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE
        );
        expect(PolitiqueAmenagement.fromString('ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE')).toEqual(
            PolitiqueAmenagement.ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE
        );
        expect(PolitiqueAmenagement.fromString('ARTIF_QUASI_NULLE_OU_NEGATIVE')).toEqual(PolitiqueAmenagement.ARTIF_QUASI_NULLE_OU_NEGATIVE);
        expect(PolitiqueAmenagement.fromString('DONNEES_NON_DISPONIBLES')).toEqual(PolitiqueAmenagement.DONNEES_NON_DISPONIBLES);
    });
});
