import { supprimerEspacesRedondantsEtRetoursLignes } from '@lga/commun/build/outils/base';
import { describe, expect, it } from 'vitest';

import {
    calculerMessagePartPopulationDependanteVoiture,
    calculerMessagePartTerritoireDependantVoiture,
    calculerMessageSynthese
} from './messages-transformation-distribution';

describe('Tests des fonctions de calcul des messages de Proxmité aux commerces', () => {
    it('Calculer les messages de synthese', () => {
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessageSynthese(null, 10, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes(`Données non disponibles (voir les échelles supérieures telle que l'EPCI)`)
        );
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessageSynthese(1, 10, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes(
                `<strong>10&nbsp;% de la population</strong> est théoriquement dépendante de la voiture pour ses achats alimentaires.`
            )
        );
    });

    it('Calculer les messages PartPopulationDependanteVoiture', () => {
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessagePartPopulationDependanteVoiture('NomEPCI', null, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes('Les données sont indisponibles pour ce territoire.')
        );
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessagePartPopulationDependanteVoiture('NomEPCI', 10, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes(
                `
            Sur le territoire <em>NomEPCI</em>, 
            <strong>10&nbsp;% de la population</strong> est théoriquement dépendante de la voiture pour ses achats alimentaires.
            <br/>En France 🇫🇷, c'est 20&nbsp;% de la population.
            `
            )
        );
    });

    it('Calculer les messages PartTerritoireDependantVoiture', () => {
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessagePartTerritoireDependantVoiture('NomEPCI', 'EPCI', null, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes(
                `
                Les données sont indisponibles pour ce territoire.
                `
            )
        );
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessagePartTerritoireDependantVoiture('NomEPCI', 'EPCI', 10, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes(
                `
                Dans <strong>10&nbsp;% des communes du territoire</strong>, 
                plus de la moitié de la population est théoriquement dépendante de la voiture pour ses achats alimentaires. 
                <br/>En France 🇫🇷, c'est 20&nbsp;% des communes qui sont dans cette situation.
            `
            )
        );
    });
});
