import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/commun/ReseauxSociaux';

import { html } from 'lit';

describe('Test ReseauxSociaux', () => {
    it('affiche les reseaux sociaux', () => {
        cy.mount<'c-reseaux-sociaux'>(html`<c-reseaux-sociaux></c-reseaux-sociaux>`);
        cy.get('a').siblings().should('have.length.at.least', 1);
        cy.get('svg').should('be.visible');
        cy.get('path').should('be.visible');
    });
});
