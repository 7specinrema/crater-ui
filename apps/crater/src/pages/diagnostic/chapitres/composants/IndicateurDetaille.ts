import '@lga/design-system/build/composants/Bouton';
import './TitreIndicateur.js';
import './MessageIndicateur.js';
import './Figure.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { construireUrlPageCarteTerritoire, construireUrlPageDiagnosticMaillon } from '../../../../domaines/pages/pages-utils';
import { Indicateur } from '../../../../domaines/systeme-alimentaire';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire';

export interface OptionsIndicateurDetaille {
    indicateur: Indicateur;
    message?: string;
    echelleEstDisponibleDansCarte: boolean;
    donneesPage?: DonneesPageDiagnostic;
    masquerLienMethodologie?: boolean;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-indicateur-detaille': IndicateurDetaille;
    }
}

@customElement('c-indicateur-detaille')
export class IndicateurDetaille extends LitElement {
    static styles = css`
        :host {
            width: 100%;
        }

        c-bouton {
            padding: calc(2 * var(--dsem)) 0 0 0;
        }
    `;

    @property({ type: Number })
    nbLignesMessageMinimum = 0;

    @property({ attribute: false })
    options?: OptionsIndicateurDetaille;

    @property({ type: Boolean })
    contenuAdhoc = false;

    render() {
        if (!this.options) return '';
        return html`
            <main>
                <c-bouton
                    id="bouton-retour"
                    href=${construireUrlPageDiagnosticMaillon(
                        this.options.indicateur.maillon.id,
                        this.options.donneesPage?.idTerritoirePrincipal ?? '',
                        this.options.donneesPage?.idEchelleTerritoriale,
                        `indicateur-resume-${this.options.indicateur?.id}`
                    )}
                    libelle="Retour"
                    libelleTooltip="Retour"
                    type="${Bouton.TYPE.plat}"
                    tailleReduite
                    >${ICONES_DESIGN_SYSTEM.flecheGauche}</c-bouton
                >
                <c-titre-indicateur
                    .indicateur=${this.options?.indicateur}
                    lienCarte=${this.estVisibleLienCarte(this.options)
                        ? construireUrlPageCarteTerritoire(this.options?.indicateur.id, this.options.donneesPage?.idTerritoireActif ?? '')
                        : ''}
                    ?masquerLienMethodologie=${this.options?.masquerLienMethodologie}
                >
                </c-titre-indicateur>
                <c-message-indicateur
                    message=${ifDefined(this.options?.message)}
                    nbLignesMinimum=${this.nbLignesMessageMinimum}
                ></c-message-indicateur>
                ${!this.contenuAdhoc
                    ? html` <c-figure titre=${ifDefined(this.options?.indicateur.legende)}>
                          <slot name="chart-indicateur-detaille" slot="chart-figure"></slot>
                      </c-figure>`
                    : html`<slot name="contenu"></slot>`}
            </main>
        `;
    }

    private estVisibleLienCarte(options: OptionsIndicateurDetaille) {
        return this.options?.echelleEstDisponibleDansCarte && SA.getIndicateurCarte(options.indicateur.id) !== undefined;
    }
}
