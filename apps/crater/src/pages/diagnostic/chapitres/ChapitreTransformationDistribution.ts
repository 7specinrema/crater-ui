import '../../commun/TimeLine.js';
import '../charts/ChartBarres.js';
import '../charts/ChartDistancesAuxCommerces.js';
import './composants/CommentAgir.js';
import './composants/IndicateurDetaille.js';
import './composants/IndicateurResume.js';
import './composants/PourquoiCestImportant.js';
import './composants/VoirEgalement.js';
import './SyntheseMaillon.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import { arrondirANDecimales } from '@lga/commun/build/outils/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { Rapport, TransformationDistribution } from '../../../domaines/diagnostics';
import { DonneesPageDiagnostic } from '../../../domaines/pages/donnees-pages.js';
import { construireUrlPageDiagnosticMaillon } from '../../../domaines/pages/pages-utils';
import { IDS_INDICATEURS } from '../../../domaines/systeme-alimentaire/configuration/indicateurs';
import { IDS_MAILLONS } from '../../../domaines/systeme-alimentaire/configuration/maillons';
import { SA } from '../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import { creerOptionsSyntheseMaillon } from './chapitres-outils';
import { OptionsIndicateurResume } from './composants/IndicateurResume';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-transformation-distribution': ChapitreTransformationDistribution;
    }
}

@customElement('c-chapitre-transformation-distribution')
export class ChapitreTransformationDistribution extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_CHAPITRES_DIAGNOSTIC];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly maillon = SA.getMaillon(IDS_MAILLONS.transformationDistribution);

    render() {
        if (!this.rapport) return;

        const diagTD = this.rapport.getDiagnosticActif()!.transformationDistribution;

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.maillon?.nom}</h1>

                <c-synthese-maillon
                    .options=${creerOptionsSyntheseMaillon(
                        this.rapport,
                        this.rapport!.getDiagnosticActif()!.transformationDistribution,
                        IDS_MAILLONS.transformationDistribution,
                        IDS_INDICATEURS.noteTransformationDistribution
                    )}
                ></c-synthese-maillon>

                <h2 id="pourquoi-cest-important">Pourquoi c'est important ?</h2>
                <c-pourquoi-cest-important .maillon=${this.maillon}></c-pourquoi-cest-important>

                <h2 id="etat-lieux">Quel est l'état des lieux ?</h2>
                <div class="indicateurs-resumes">
                    <p>
                        <span>Quelle dépendance au pétrole de la population pour ses achats alimentaires ?</span>
                    </p>
                    <c-timeline></c-timeline>
                    <div class="bloc-indicateurs-resumes">
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumePartPopulationDependanteVoiture(diagTD, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.partPopulationDependanteVoiture}"
                        ></c-indicateur-resume>
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumePartTerritoireDependanteVoiture(diagTD, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.partTerritoireDependantVoiture}"
                        ></c-indicateur-resume>
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumeDistancesAuxCommerces(diagTD, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.distancesPlusProchesCommerces}"
                        ></c-indicateur-resume>
                    </div>
                </div>

                <h2 id="leviers-action">Comment ma collectivité peut-elle s&#39;améliorer ?</h2>
                <c-comment-agir
                    .maillon=${this.maillon}
                    idTerritoireParcel=${ifDefined(this.rapport.getDiagnosticActif()?.idTerritoireParcel || undefined)}
                ></c-comment-agir>

                <c-encart-pages-precedente-suivante
                    libellePagePrecedente=${ifDefined(SA.getMaillon(IDS_MAILLONS.production)?.nom)}
                    hrefPagePrecedente=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.production,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                    libellePageSuivante=${ifDefined(SA.getMaillon(IDS_MAILLONS.consommation)?.nom)}
                    hrefPageSuivante=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.consommation,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsIndicateurResumePartPopulationDependanteVoiture(
        diagTD: TransformationDistribution,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partPopulationDependanteVoiture)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '',
                    chiffre: `${diagTD.partPopulationDependanteVoiturePourcent!} %`,
                    libelleApresChiffre: 'de la population est théoriquement dépendante de la voiture pour ses achats alimentaires'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsIndicateurResumePartTerritoireDependanteVoiture(
        diagTD: TransformationDistribution,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partTerritoireDependantVoiture)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: 'sur',
                    chiffre: `${diagTD.partTerritoireDependantVoiturePourcent!} %`,
                    libelleApresChiffre:
                        'du territoire, plus de la moitié de la population est théoriquement dépendante de la voiture pour ses achats alimentaires'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsIndicateurResumeDistancesAuxCommerces(
        diagTD: TransformationDistribution,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.distancesPlusProchesCommerces)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '',
                    chiffre: `${arrondirANDecimales(diagTD.indicateursProximiteCommerceGeneraliste.distancePlusProcheCommerceMetres! / 1000, 1)} km`,
                    libelleApresChiffre: `distance moyenne à vol d'oiseau entre le domicile et le plus proche commerce <strong>généraliste</strong>`
                }
            ],
            donneesPage: donneesPage
        };
    }
}
