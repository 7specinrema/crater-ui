import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-production-exportee': ChapitrePartProductionExportee;
    }
}

@customElement('c-chapitre-part-production-exportee')
export class ChapitrePartProductionExportee extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.partProductionExportee}"
                .options=${<OptionsIndicateurDetaille>{
                    indicateur: SA.getIndicateur(IDS_INDICATEURS.partProductionExportee)
                }}
                contenuAdhoc
            >
                <p slot="contenu">
                    A l'échelle d'un bassin de vie plus de 90% des produits agricoles locaux sont exportés, et dans le même temps plus de 90% de
                    l'alimentation est composée de produits agricoles importés.
                </p>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }
}
