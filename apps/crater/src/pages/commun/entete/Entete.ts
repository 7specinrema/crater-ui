import './IdentiteSite.js';
import './MenuGlissantMobile.js';
import '@lga/commun/build/composants/champ-recherche-territoire/ChampRechercheTerritoire.js';
import '@lga/design-system/build/composants/Bouton.js';
import '@lga/design-system/build/composants/BarreOnglets.js';
import '@lga/design-system/build/composants/PanneauGlissant.js';

import { OptionsBarreOnglets } from '@lga/design-system/build/composants/BarreOnglets.js';
import { BarreOngletsItem } from '@lga/design-system/build/composants/BarreOngletsItem.js';
import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';
import { ICONES_SVG } from '../../../domaines/systeme-alimentaire/configuration/icones_svg';

declare global {
    interface HTMLElementTagNameMap {
        'c-entete': Entete;
    }
}

@customElement('c-entete')
export class Entete extends LitElement {
    static styles = [
        css`
            :host {
                display: block;
                width: 100%;
                height: 100%;
            }

            main {
                background-color: var(--couleur-blanc);
                border-bottom: 1px solid var(--couleur-neutre-clair);
                width: 100%;
                height: 100%;
                display: flex;
                align-items: center;
                justify-content: center;
            }

            nav {
                height: var(--hauteur-entete);
                width: 100%;
                display: flex;
                align-items: center;
                justify-content: space-between;
            }

            section {
                padding: 0;
                margin: 0;
            }

            #zone-milieu-entete {
                flex: 999;
                display: flex;
                justify-content: center;
                align-items: center;
            }

            #zone-menu-principal {
                display: flex;
                align-items: center;
                justify-content: center;
            }

            #zone-menu-principal c-bouton {
                padding: 0 calc(2 * var(--dsem));
            }

            #burger-menu ::slotted(svg) {
                stroke: var(--couleur-neutre);
                width: 40px;
                height: 40px;
            }

            #zone-menu-mobile {
                margin-left: 0.4rem;
                margin-right: 0.4rem;
                align-items: center;
                display: none;
                justify-content: center;
            }

            #bouton-don {
                background-color: var(--couleur-accent);
                border-radius: 0.2rem;
            }

            #bouton-don a {
                color: var(--couleur-blanc) !important;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                #bouton-contact,
                #bouton-don {
                    display: none;
                }

                #zone-menu-mobile {
                    display: flex;
                }

                #zone-menu-principal {
                    display: none;
                }
            }
        `
    ];

    @property()
    idItemActif?: string;

    render() {
        const options: OptionsBarreOnglets = {
            onglets: [
                {
                    id: PAGES_PRINCIPALES.accueil.getId(),
                    libelle: PAGES_PRINCIPALES.accueil.getTitreCourt(),
                    href: PAGES_PRINCIPALES.accueil.getUrl()
                },
                {
                    id: PAGES_PRINCIPALES.diagnostic.getId(),
                    libelle: PAGES_PRINCIPALES.diagnostic.getTitreCourt(),
                    href: PAGES_PRINCIPALES.diagnostic.getUrl()
                },
                { id: PAGES_PRINCIPALES.carte.getId(), libelle: PAGES_PRINCIPALES.carte.getTitreCourt(), href: PAGES_PRINCIPALES.carte.getUrl() },
                { id: PAGES_PRINCIPALES.aide.getId(), libelle: PAGES_PRINCIPALES.aide.getTitreCourt(), href: PAGES_PRINCIPALES.aide.getUrl() },
                { id: PAGES_PRINCIPALES.projet.getId(), libelle: PAGES_PRINCIPALES.projet.getTitreCourt(), href: PAGES_PRINCIPALES.projet.getUrl() }
            ],
            idOngletSelectionne: this.idItemActif,
            theme: BarreOngletsItem.THEME.simple
        };
        return html`
            <main>
                <nav>
                    <section id="zone-identite-site">
                        <c-identite-site></c-identite-site>
                    </section>
                    <section id="zone-menu-principal">
                        <c-barre-onglets .options="${options}"></c-barre-onglets>
                        <c-bouton
                            libelle="Nous contacter"
                            libelleTooltip="Nous contacter"
                            themeCouleur="${Bouton.THEME_COULEUR.neutre}"
                            type="${Bouton.TYPE.relief}"
                            href="${PAGES_PRINCIPALES.contact.getUrl()}"
                        ></c-bouton>
                    </section>
                    <section id="zone-menu-mobile">
                        <c-panneau-glissant>
                            <c-bouton
                                id="burger-menu"
                                data-cy="bouton-ouvrir-menu-glissant"
                                slot="bouton-ouvrir"
                                .type=${Bouton.TYPE.plat}
                                .themeCouleur=${Bouton.THEME_COULEUR.primaire}
                            >
                                ${unsafeSVG(ICONES_SVG.burgerMenu)}
                            </c-bouton>
                            <c-menu-glissant-mobile slot="panneau"> </c-menu-glissant-mobile>
                        </c-panneau-glissant>
                    </section>
                </nav>
            </main>
        `;
    }
}
