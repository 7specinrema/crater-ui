// Voir https://vitejs.dev/guide/env-and-mode.html#intellisense-for-typescript
/// <reference types="vite/client" />
interface ImportMetaEnv {
    readonly VITE_API_BASE_URL: string;
    readonly VITE_MATOMO_SITE_ID: string;
}
interface ImportMeta {
    readonly env: ImportMetaEnv;
}
