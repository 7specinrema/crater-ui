import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-aide-alimentaire': ChapitreAideAlimentaire;
    }
}

@customElement('c-chapitre-aide-alimentaire')
export class ChapitreAideAlimentaire extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.aideAlimentaire}"
                .options=${<OptionsIndicateurDetaille>{
                    indicateur: SA.getIndicateur(IDS_INDICATEURS.aideAlimentaire),
                    donneesPage: this.donneesPage,
                    masquerLienMethodologie: true
                }}
                contenuAdhoc
            >
                <div slot="contenu">
                    <p>
                        En France, le recours à l'aide alimentaire, un dispositif censé pallier les situations d'urgence, a quasiment doublé en dix
                        ans et concerne aujourd'hui plus de cinq millions de personnes.
                    </p>
                    <p>
                        Pour en savoir plus, voir le rapport
                        <c-lien
                            href="https://publications.resiliencealimentaire.org/partie1_un-systeme-defaillant/#_une_malnutrition_omnipr%C3%A9sente_au_nord_comme_au_sud"
                            >Qui veille grain?</c-lien
                        >.
                    </p>
                </div>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }
}
