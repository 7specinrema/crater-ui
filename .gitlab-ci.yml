stages:
  - build
  - test
  - deploy

# to cache both npm modules and Cypress binary we use environment variables
# to point at the folders we can list as paths in "cache" job settings
variables:
  CYPRESS_CACHE_FOLDER: "$CI_PROJECT_DIR/cache/Cypress" # Variable d'environnement utilisée par cypress
  DOSSIER_CACHE_NPM: "$CI_PROJECT_DIR/.npm"  # Paramètre passé lors du npm ci
  CRATER_DATA_RESULTATS_GIT_URL: framagit.org/lga/crater-data-resultats.git

default:
  image: node:16.20.0-alpine


## Créer un pipeline uniquement pour une branche sans Mr, ou pour une MR, voir https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines
#workflow:
#  rules:
#    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
#    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
#      when: never
#    - if: $CI_COMMIT_BRANCH

#workflow:
#  rules:
#    - if: '$CI_MERGE_REQUEST_IID' # Run pipelines on Merge Requests
#    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH' # Run pipelines on main branch

#######################################
# Jobs de récupération des données cdr
#######################################

#copy-crater-data-resultats:
#  image:
#    name: alpine/git
#    entrypoint: [""]
#  stage: prebuild
##  rules:
##    - changes:
##        - api/**/*
#  cache: {}
#  script:
#    - cd api
#    - ./scripts/copy-cdr-gitlab.sh ${CI_COMMIT_BRANCH} ${CRATER_DATA_RESULTATS_GIT_URL} ${CI_JOB_TOKEN}
#  artifacts:
#    untracked: false
#    expire_in: 1h
#    paths:
#      - "api/prebuild/crater-data-resultats"

#######################################
# Jobs de build
#######################################


# YAML Anchor pour definir les param de cache partagés par les jobs de build
.node_modules-cache: &node_modules-cache
  key:
    files:
      - package-lock.json
  paths:
    - node_modules
    - $DOSSIER_CACHE_NPM
    - $CYPRESS_CACHE_FOLDER
  policy: pull

npm-install:
  stage: build
  cache:
    - <<: *node_modules-cache
      policy: pull-push # Surcharge de la policy => c'est ce job qui renseigne le cache, les autres sont en lecture seule
  script:
    - npm ci --cache $DOSSIER_CACHE_NPM  --prefer-offline


build-modules:
  stage: build
  cache:
    - <<: *node_modules-cache
  needs:
    - job: npm-install
  script:
    - npm run build -w modules
  artifacts:
    paths:
      - modules/**/build/

build-api:
  stage: build
  image: timbru31/java-node:8-alpine-jre-gallium  #image différente car l'api a besoin de java a cause d'openapi tools
  cache:
    - <<: *node_modules-cache
  needs:
    - job: npm-install
    - job: build-modules
  dependencies:
  #    - "copy-crater-data-resultats"
    - build-modules
  script:
    - export CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH
    - export CRATER_DATA_RESULTATS_GIT_URL=$CRATER_DATA_RESULTATS_GIT_URL
    - export CI_JOB_TOKEN=$CI_JOB_TOKEN
    - apk add git
    - npm run build:ci -w api
    - npm run integration-tests -w api
  artifacts:
    paths:
      - api/build/

build-apps-dev:
  stage: build
  except:
    - master
  cache:
    - <<: *node_modules-cache
  needs:
    - job: npm-install
    - job: build-modules
  dependencies:
    #    - "copy-crater-data-resultats"
    - build-modules
  script:
    - npm run build -w apps/crater/ --env=dev
    - npm run build -w apps/transition-alimentaire/ --env=dev
  artifacts:
    paths:
      - apps/**/build/

build-apps-prod:
  stage: build
  only:
    - master
  cache:
    - <<: *node_modules-cache
  needs:
    - job: npm-install
    - job: build-modules
  dependencies:
    #    - "copy-crater-data-resultats"
    - build-modules
  script:
    - npm run build -w apps/crater/ --env=prod
    - npm run build -w apps/transition-alimentaire/ --env=prod
  artifacts:
    paths:
      - apps/**/build/


#######################################
# Jobs de test
#######################################

cypress-tests:
  image: cypress/browsers:node16.16.0-chrome107-ff107
  stage: test
  cache:
    - <<: *node_modules-cache
  dependencies:
    - build-modules
    - build-apps-prod
    - build-apps-dev
  script:
    - npm run cypress:components -ws
    # Bug cypress dans gitlab ci : il faut cibler directement le script qui execute cypress, sinon le job ne se termine jamais
    - npm run cypress:e2e:run:firefox -w apps/crater/
  artifacts:
    when: always
    paths:
      - apps/**/cypress/screenshots/**/*.png
      - modules/**/cypress/screenshots/**/*.png
    expire_in: 1 day


#######################################
# Jobs de déploiement
#######################################

# YAML Anchor pour définir le setup avant déploiement sur le serveur api ovh
.before_script-deploiement_ovh: &before_script-deploiement_ovh
  before_script:
    - apk --no-cache add lftp ca-certificates openssh
    # config ssh pour pouvoir accéder au VPS OVH
    # config clé ssh, voir https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
    - 'command -v ssh-agent >/dev/null || ( apk update && apk add openssh-client )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    # Complément pour désactiver la vérification de clé du serveur distant OVH (Voir https://docs.gitlab.com/ee/ci/ssh_keys/)
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config'
#
#.before_script_deploy-api:
#  before_script:
#    # installer lftp
#    - apk --no-cache add lftp ca-certificates openssh
#    # config ssh pour pouvoir accéder au VPS OVH
#    # config clé ssh, voir https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
#    - 'command -v ssh-agent >/dev/null || ( apk update && apk add openssh-client )'
#    - eval $(ssh-agent -s)
#    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
#    - mkdir -p ~/.ssh
#    - chmod 700 ~/.ssh
#    # Complément pour désactiver la vérification de clé du serveur distant OVH (Voir https://docs.gitlab.com/ee/ci/ssh_keys/)
#    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config'

deploy-api-prod:
  image: alpine
  stage: deploy
  dependencies:
    - build-api
  only:
    - master
  <<: *before_script-deploiement_ovh
  script:
    - lftp -c "set ftp:ssl-allow no; open -u $O2S_FTP_USERNAME,$O2S_FTP_PASSWORD $O2S_FTP_HOST; mirror -Rev api/build ./crater/app/crater-api --parallel=10 --exclude-glob .git* --exclude .git/"
    # lftp veut un MDP, mais il n'est pas utilisé (clé ssh à la place), on met donc un MDP bidon
    - lftp -c "open -u $OVH_FTP_USERNAME,MDP_BIDON sftp://$OVH_FTP_HOST; mirror -Rev api/build ./crater/prod/crater-api --parallel=10 --exclude-glob .git* --exclude .git/"
    - lftp -c "open -u $OVH_FTP_USERNAME,MDP_BIDON sftp://$OVH_FTP_HOST; mirror -Rev api/ovh ./crater/ovh --parallel=10 --exclude-glob .git* --exclude .git/"
    - ssh $OVH_FTP_USERNAME@$OVH_FTP_HOST "cd crater/ovh/scripts && ./installer-prod.sh"

deploy-api-dev:
  image: alpine
  stage: deploy
  dependencies:
    - build-api
  only:
    - develop
  <<: *before_script-deploiement_ovh
  script:
    - lftp -c "set ftp:ssl-allow no; open -u $O2S_FTP_USERNAME,$O2S_FTP_PASSWORD $O2S_FTP_HOST; mirror -Rev api/build ./crater/dev/crater-api --parallel=10 --exclude-glob .git* --exclude .git/"
    # lftp veut un MDP, mais il n'est pas utilisé (clé ssh à la place), on met donc un MDP bidon
    - lftp -c "open -u $OVH_FTP_USERNAME,MDP_BIDON sftp://$OVH_FTP_HOST; mirror -Rev api/build ./crater/dev/crater-api --parallel=10 --exclude-glob .git* --exclude .git/"
    - lftp -c "open -u $OVH_FTP_USERNAME,MDP_BIDON sftp://$OVH_FTP_HOST; mirror -Rev api/ovh ./crater/ovh --parallel=10 --exclude-glob .git* --exclude .git/"
    - ssh $OVH_FTP_USERNAME@$OVH_FTP_HOST "cd crater/ovh/scripts && ./installer-dev.sh"

deploy-ui-prod:
  image: alpine
  stage: deploy
  dependencies:
    - build-apps-prod
  only:
    - master
  script:
    - apk --no-cache add lftp ca-certificates openssh
    - lftp -c "set ftp:ssl-allow no; open -u $O2S_FTP_USERNAME,$O2S_FTP_PASSWORD $O2S_FTP_HOST; mirror -Rev apps/crater/build ./crater/app/crater-ui  --parallel=10"
    - lftp -c "set ftp:ssl-allow no; open -u $O2S_FTP_USERNAME,$O2S_FTP_PASSWORD $O2S_FTP_HOST; mirror -Rev apps/transition-alimentaire/build ./transition-alimentaire/prod  --parallel=10"

deploy-ui-dev:
  image: alpine
  stage: deploy
  dependencies:
    - build-apps-dev
  only:
    - develop
  script:
    - apk --no-cache add lftp ca-certificates openssh
    - lftp -c "set ftp:ssl-allow no; open -u $O2S_FTP_USERNAME,$O2S_FTP_PASSWORD $O2S_FTP_HOST; mirror -Rev apps/crater/build ./crater/dev/crater-ui  --parallel=10"
    - lftp -c "set ftp:ssl-allow no; open -u $O2S_FTP_USERNAME,$O2S_FTP_PASSWORD $O2S_FTP_HOST; mirror -Rev apps/transition-alimentaire/build ./transition-alimentaire/dev  --parallel=10"
