import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-timeline': TimeLine;
    }
}

@customElement('c-timeline')
export class TimeLine extends LitElement {
    static styles = css`
        div {
            height: 100%;
            margin: auto;
        }
        .point {
            content: '';
            display: block;
            grid-column: 2;
            width: 20px;
            height: 20px;
            margin: auto;
            border-radius: 50%;
            border: 2px solid var(--couleur-primaire);
            background-color: var(--couleur-blanc);
        }

        .ligne {
            content: '';
            display: block;
            border: 3px solid var(--couleur-primaire);
            height: calc(50% - 15px);
            width: 0px;
            margin: auto;
        }
    `;

    render() {
        return html` <div><span class="ligne"></span><span class="point"></span><span class="ligne"></span></div> `;
    }
}
