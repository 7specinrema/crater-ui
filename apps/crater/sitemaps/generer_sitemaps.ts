import { existsSync, mkdirSync, writeFileSync } from 'fs';

import { EchelleTerritoriale } from '../src/domaines/carte/EchelleTerritoriale';
import { PAGES_AIDE, PAGES_CARTE, PAGES_COMPRENDRE_LE_SA, PAGES_PRINCIPALES } from '../src/domaines/pages/configuration/declaration-pages';
import { SA } from '../src/domaines/systeme-alimentaire/configuration/systeme-alimentaire';

export function genererSitemap() {
    let urlsCartes = ``;
    for (const i of SA.indicateursCarte) {
        // Pour l'instant on se limite aux seuls départements
        for (const e of EchelleTerritoriale.listerToutes.filter((e) => e === EchelleTerritoriale.Departements)) {
            {
                const urlRelativeCarte = PAGES_CARTE.indicateurs.getUrlCanonique({ idIndicateur: i.id, idEchelleTerritoriale: e.id });
                urlsCartes = urlsCartes + `\n\t\t<url><loc>https://crater.resiliencealimentaire.org${urlRelativeCarte}</loc></url>`;
            }
        }
    }

    const contenu = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    	<url><loc>https://crater.resiliencealimentaire.org${PAGES_PRINCIPALES.accueil.getUrlCanonique()}</loc></url>
    	<url><loc>https://crater.resiliencealimentaire.org${PAGES_PRINCIPALES.diagnostic.getUrlCanonique()}</loc></url>
    	<url><loc>https://crater.resiliencealimentaire.org${PAGES_PRINCIPALES.carte.getUrlCanonique()}</loc></url>
    	<url><loc>https://crater.resiliencealimentaire.org${PAGES_PRINCIPALES.aide.getUrlCanonique()}</loc></url>
    	<url><loc>https://crater.resiliencealimentaire.org${PAGES_COMPRENDRE_LE_SA.systemeActuel.getUrlCanonique()}</loc></url>
    	<url><loc>https://crater.resiliencealimentaire.org${PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getUrlCanonique()}</loc></url>
    	<url><loc>https://crater.resiliencealimentaire.org${PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire.getUrlCanonique()}</loc></url>
    	<url><loc>https://crater.resiliencealimentaire.org${PAGES_AIDE.methodologie.getUrlCanonique()}</loc></url>
    	<url><loc>https://crater.resiliencealimentaire.org${PAGES_AIDE.faq.getUrlCanonique()}</loc></url>
    	<url><loc>https://crater.resiliencealimentaire.org${PAGES_AIDE.glossaire.getUrlCanonique()}</loc></url>
    	<url><loc>https://crater.resiliencealimentaire.org${PAGES_PRINCIPALES.projet.getUrlCanonique()}</loc></url>
    	${urlsCartes}
    </urlset>`;

    if (!existsSync('./build')) mkdirSync('./build');
    writeFileSync('./build/sitemap-ui.xml', contenu, {
        flag: 'w'
    });
}

genererSitemap();
