import { describe, expect, it } from 'vitest';

import { DonneesPageCarte, DonneesPageSimple } from './donnees-pages';
import { ModelePage } from './ModelePage';

describe('Test de ModelePage', () => {
    it('ModelePage avec définition minimaliste et valeurs par défaut', () => {
        const modelePage = new ModelePage<DonneesPageSimple>({
            url: () => `/accueil`,
            titreCourt: 'Titre court'
        });
        expect(modelePage.getUrl({})).toEqual('/accueil');
        expect(modelePage.getId({})).toEqual('/accueil');
        expect(modelePage.getTitreCourt()).toEqual('Titre court');
        expect(modelePage.getTitreLong({})).toEqual('Titre court');
        expect(modelePage.getUrlCanonique({})).toEqual('/accueil');
        expect(modelePage.getMetaDescription({})).toBeUndefined();
    });

    it('ModelePage avec définition exhaustive', () => {
        const modelePage = new ModelePage<DonneesPageSimple>({
            url: () => `/accueil`,
            titreCourt: 'Titre court',
            titreLong: () => 'Titre long',
            urlCanonique: () => `/urlcanonique`,
            metaDescription: () => `Meta description`
        });
        expect(modelePage.getUrl({})).toEqual('/accueil');
        expect(modelePage.getId({})).toEqual('/accueil');
        expect(modelePage.getTitreCourt()).toEqual('Titre court');
        expect(modelePage.getTitreLong({})).toEqual('Titre long');
        expect(modelePage.getUrlCanonique({})).toEqual('/urlcanonique');
        expect(modelePage.getMetaDescription({})).toEqual('Meta description');
    });

    it('ModelePage avec query param dans url', () => {
        const modelePage = new ModelePage<DonneesPageSimple>({
            url: () => `/accueil?param1=1`,
            titreCourt: 'Titre court'
        });
        expect(modelePage.getId({})).toEqual('/accueil');
        expect(modelePage.getUrl({})).toEqual('/accueil?param1=1');
    });

    it('ModelePage avec hash dans url', () => {
        const modelePage = new ModelePage<DonneesPageSimple>({
            url: () => `/accueil`,
            titreCourt: 'Titre court'
        });
        const donneesPage = { idElementCibleScroll: 'hashid' };

        expect(modelePage.getId(donneesPage)).toEqual('/accueil');
        expect(modelePage.getUrl(donneesPage)).toEqual('/accueil#hashid');
        expect(modelePage.getUrlCanonique(donneesPage)).toEqual('/accueil');
    });

    it('ModelePage avec plusieurs paramètres et hash', () => {
        const modelePageCarteIndicateur = new ModelePage<DonneesPageCarte>({
            url: (c) => `/carte/${c.idIndicateur}/${c.idEchelleTerritoriale}?idTerritoire=${c.idTerritoirePrincipal}`,
            titreCourt: 'Page Carte',
            titreLong: (c) => `Page Carte, ${c.nomIndicateur}`,
            urlCanonique: (c) => `/carte/${c.idIndicateur}`,
            metaDescription: (c) => `Carte pour ${c.idIndicateur}`
        });
        const donneesPage: DonneesPageCarte = {
            idIndicateur: 'I1',
            nomIndicateur: 'NomIndicateur',
            idEchelleTerritoriale: 'REGION',
            idElementCibleScroll: 'idHash'
        };
        expect(modelePageCarteIndicateur.getUrl(donneesPage)).toEqual('/carte/i1/region#idhash');
        expect(modelePageCarteIndicateur.getTitreLong(donneesPage)).toEqual('Page Carte, NomIndicateur');
        expect(modelePageCarteIndicateur.getId(donneesPage)).toEqual('/carte/i1/region');
        expect(modelePageCarteIndicateur.getUrlCanonique(donneesPage)).toEqual('/carte/i1');
        expect(modelePageCarteIndicateur.getMetaDescription(donneesPage)).toEqual('Carte pour I1');
    });
});
