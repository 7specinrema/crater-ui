import { PAGES_METHODOLOGIE } from '../../pages/configuration/declaration-pages';
import { IDS_MAILLONS } from '../../systeme-alimentaire/configuration/maillons';
import { SA } from '../../systeme-alimentaire/configuration/systeme-alimentaire';
import { CategorieTerritoire } from '../../territoires/CategorieTerritoire';
import { Territoire } from '../../territoires/Territoire';
import { Note } from './IndicateurSynthese';
import { NbCommunesOtex } from './Otex';

export function calculerMessageTerritoire(
    categorieTerritoire: CategorieTerritoire,
    densitePopulationHabParKM2: number | null,
    pourcentageSauTotaleSurSuperficie: number | null,
    nbCommunesOtexMajoritaire5Postes: NbCommunesOtex
): string {
    if (densitePopulationHabParKM2 === null || pourcentageSauTotaleSurSuperficie === null) {
        return '';
    }
    let messageDensite = '';
    if (densitePopulationHabParKM2 < 100) {
        messageDensite = ' peu dense';
    } else if (densitePopulationHabParKM2 < 200) {
        messageDensite = ' de densité moyenne';
    } else {
        if ([CategorieTerritoire.Commune, CategorieTerritoire.Epci, CategorieTerritoire.Region].includes(categorieTerritoire)) {
            messageDensite = ' densément peuplée';
        } else {
            messageDensite = ' densément peuplé';
        }
    }

    let messageSau = '';
    if (pourcentageSauTotaleSurSuperficie < 15) {
        messageSau = ' avec une faible part de surface agricole';
    } else if (pourcentageSauTotaleSurSuperficie < 50) {
        messageSau = ' avec une part minoritaire de surface agricole';
    } else {
        messageSau = ' avec une part majoritaire de surface agricole';
    }

    const messageOtex = ` et une spécialisation agricole ${nbCommunesOtexMajoritaire5Postes.otex.libelle}`;

    return categorieTerritoire.libelleCategorie + messageDensite + messageSau + messageOtex + '.';
}

export function calculerMessageOtex(territoire: Territoire, nbCommunesOtexMajoritaire5Postes: NbCommunesOtex) {
    let motTerritoire = '';
    if (territoire.categorie === CategorieTerritoire.Commune) {
        motTerritoire = 'de la commune';
    } else {
        motTerritoire = 'du territoire';
    }
    return `La <c-lien href="${PAGES_METHODOLOGIE.presentationGenerale.getUrl()}#otex">spécialisation territoriale de la production agricole ${motTerritoire}</c-lien> est ${
        nbCommunesOtexMajoritaire5Postes.otex.libelle
    }.`;
}

export function calculerMessageMetaDescriptionDiagnostic(
    nomTerritoire: string,
    noteTerresAgricoles: Note,
    noteAgriculteursExploitations: Note,
    noteIntrants: Note,
    noteProduction: Note,
    noteTransformationDistribution: Note,
    noteConsommation: Note
): string {
    type DictMaillonNote = { idMaillon: string; note: Note };
    const maillonsNotes: DictMaillonNote[] = [
        { idMaillon: IDS_MAILLONS.terresAgricoles, note: noteTerresAgricoles },
        { idMaillon: IDS_MAILLONS.agriculteursExploitations, note: noteAgriculteursExploitations },
        { idMaillon: IDS_MAILLONS.intrants, note: noteIntrants },
        { idMaillon: IDS_MAILLONS.production, note: noteProduction },
        { idMaillon: IDS_MAILLONS.transformationDistribution, note: noteTransformationDistribution },
        { idMaillon: IDS_MAILLONS.consommation, note: noteConsommation }
    ];
    type DictMaillonAvecValeurNumerique = { idMaillon: string; note: number };
    const noteMaillonsAvecValeurNumerique: DictMaillonAvecValeurNumerique[] = maillonsNotes.filter((maillon) =>
        Number.isFinite(maillon.note)
    ) as DictMaillonAvecValeurNumerique[];
    if (noteMaillonsAvecValeurNumerique.length === 0) {
        return `Évaluation pour ${nomTerritoire} : aucun score disponible`;
    } else {
        return `Évaluation pour ${nomTerritoire} : ${noteMaillonsAvecValeurNumerique
            .map((e) => `Score ${SA.getMaillon(e.idMaillon)?.nom}=${e.note}/10`)
            .join(', ')}`;
    }
}
