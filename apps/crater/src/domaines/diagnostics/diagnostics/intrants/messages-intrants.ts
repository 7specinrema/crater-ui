import { formaterNombreEnNDecimalesString } from '@lga/commun/build/outils/base';

export function calculerMessageSyntheseIntrants(noduNormalise: number | null) {
    if (noduNormalise === null) {
        return `Données non disponibles (voir les échelles supérieures telle que l'EPCI).`;
    } else {
        let adjectif = '';

        if (noduNormalise < 0.1) {
            adjectif = 'nul';
        } else if (noduNormalise < 0.5) {
            adjectif = 'modéré';
        } else if (noduNormalise < 1.5) {
            adjectif = 'élevé';
        } else if (noduNormalise < 2.5) {
            adjectif = 'très élevé';
        } else if (noduNormalise >= 2.5) {
            adjectif = 'extrêment élevé';
        }

        return `Usage de pesticides <strong>${adjectif}</strong>${
            noduNormalise > 0.1
                ? ` (${formaterNombreEnNDecimalesString(noduNormalise, 1)} fois la dose annuelle maximale autorisée pour une substance donnée)`
                : ``
        }.`;
    }
}
