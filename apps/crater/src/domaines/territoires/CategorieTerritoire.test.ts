import { describe, expect, it } from 'vitest';

import { CategorieTerritoire } from './CategorieTerritoire';

describe("Test de l'énumération CategorieTerritoire", () => {
    it('Vérification des catégories créées par défaut', () => {
        expect(CategorieTerritoire.Region.codeCategorie).toEqual('REGION');
        expect(CategorieTerritoire.listerToutes).toEqual([
            CategorieTerritoire.Commune,
            CategorieTerritoire.Epci,
            CategorieTerritoire.RegroupementCommunes.AutreTerritoire,
            CategorieTerritoire.RegroupementCommunes.BassinDeVie,
            CategorieTerritoire.RegroupementCommunes.PaysPetr,
            CategorieTerritoire.RegroupementCommunes.Pn,
            CategorieTerritoire.RegroupementCommunes.Pnr,
            CategorieTerritoire.RegroupementCommunes.Scot,
            CategorieTerritoire.RegroupementCommunes.Pat,
            CategorieTerritoire.RegroupementCommunes.Nd,
            CategorieTerritoire.Departement,
            CategorieTerritoire.Region,
            CategorieTerritoire.Pays
        ]);
    });

    it('Vérification de la comparaison entre catégories', () => {
        expect(CategorieTerritoire.Region.comparer(CategorieTerritoire.Commune)).toBeGreaterThan(0);
        expect(CategorieTerritoire.Region.comparer(CategorieTerritoire.Region)).toEqual(0);
    });

    it("Vérification de la récupération d'une catégorie à partir d'une string", () => {
        expect(CategorieTerritoire.fromString('COMMUNE')).toEqual(CategorieTerritoire.Commune);
        expect(CategorieTerritoire.fromString('commune')).toEqual(CategorieTerritoire.Commune);
        expect(CategorieTerritoire.fromString('commune', null)).toEqual(CategorieTerritoire.Commune);
        expect(CategorieTerritoire.fromString('commune', '')).toEqual(CategorieTerritoire.Commune);
        expect(CategorieTerritoire.fromString('commune', 'SOUS_CATEGORIE_A_IGNORER')).toEqual(CategorieTerritoire.Commune);
        expect(CategorieTerritoire.fromString('EPCI', 'SOUS_CATEGORIE_A_IGNORER')).toEqual(CategorieTerritoire.Epci);
        expect(CategorieTerritoire.fromString('REGROUPEMENT_COMMUNES', 'SCHEMA_COHERENCE_TERRITORIAL')).toEqual(
            CategorieTerritoire.RegroupementCommunes.Scot
        );
        expect(CategorieTerritoire.fromString('REGROUPEMENT_COMMUNES', 'PARC_NATUREL_REGIONAL')).toEqual(
            CategorieTerritoire.RegroupementCommunes.Pnr
        );
        expect(CategorieTerritoire.fromString('REGROUPEMENT_COMMUNES', 'PROJET_ALIMENTAIRE_TERRITORIAL')).toEqual(
            CategorieTerritoire.RegroupementCommunes.Pat
        );
        expect(CategorieTerritoire.fromString('REGROUPEMENT_COMMUNES', 'NOUVEAU_DEPARTEMENT')).toEqual(CategorieTerritoire.RegroupementCommunes.Nd);
    });
});
