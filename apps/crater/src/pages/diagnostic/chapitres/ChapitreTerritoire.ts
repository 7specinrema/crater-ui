import '@lga/design-system/build/composants/Tableau.js';
import '@lga/design-system/build/composants/Lien.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';
import '../charts/ChartBarreEmpileeHorizontale';

import { formaterNombreEnEntierHtml, formaterNombreEnEntierString, formaterNombreSelonValeurString } from '@lga/commun/build/outils/base';
import { OptionsTableau } from '@lga/design-system/build/composants/Tableau';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { Rapport } from '../../../domaines/diagnostics';
import { PAGES_METHODOLOGIE } from '../../../domaines/pages/configuration/declaration-pages';
import { DonneesPageDiagnostic } from '../../../domaines/pages/donnees-pages.js';
import { construireUrlPageDiagnosticMaillon } from '../../../domaines/pages/pages-utils';
import { IDS_MAILLONS } from '../../../domaines/systeme-alimentaire/configuration/maillons';
import { sourcesDonnees } from '../../../domaines/systeme-alimentaire/configuration/sources-donnees';
import { SA } from '../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire';
import { CategorieTerritoire } from '../../../domaines/territoires/CategorieTerritoire';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { ItemBarreEmpileeHorizontale } from '../charts/ChartBarreEmpileeHorizontale';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-territoire': ChapitreTerritoire;
    }
}

@customElement('c-chapitre-territoire')
export class ChapitreTerritoire extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        STYLES_CHAPITRES_DIAGNOSTIC,
        css`
            h1 {
                color: var(--couleur-primaire-sombre);
            }

            section.PDF c-encart-pages-precedente-suivante {
                display: none;
            }

            #legende-otex {
                text-align: center;
                padding-bottom: 30px;
            }
        `
    ];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly nomChapitre = 'Présentation du territoire';

    render() {
        if (!this.rapport) return;

        const nomTerritoire = this.rapport.getTerritoireActif()?.nom ?? '';
        const informationNbCommunes =
            this.rapport.getDiagnosticActif()!.nbCommunes > 1
                ? `est composé de ${formaterNombreEnEntierString(this.rapport.getDiagnosticActif()?.nbCommunes)} communes. Il `
                : '';
        const superficie = formaterNombreEnEntierString(this.rapport.getDiagnosticActif()?.superficieHa ?? null);
        const population = formaterNombreEnEntierString(this.rapport.getDiagnosticActif()?.population ?? null);
        const densitePopulation = formaterNombreSelonValeurString(this.rapport.getDiagnosticActif()?.densitePopulationHabParKM2 ?? null);
        const rapportDensitePopulationSurPays = formaterNombreSelonValeurString(
            this.rapport.getDiagnosticActif()?.rapportDensitePopulationHabParM2SurPays ?? null
        );
        const sauTotale = formaterNombreEnEntierString(this.rapport.getDiagnosticActif()?.surfaceAgricoleUtile?.sauTotaleHa ?? null);
        const partSauSurSuperficie = formaterNombreEnEntierString(this.rapport.getDiagnosticActif()?.pourcentageSauTotaleSurSuperficie ?? null);

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.nomChapitre}</h1>
                <p>
                    Le territoire <strong style="color: var(--couleur-accent)">${nomTerritoire}</strong> ${informationNbCommunes} est peuplé de
                    ${population} habitants sur une surface de ${superficie} hectares soit une densité de population de ${densitePopulation} hab/km²
                    (= ${rapportDensitePopulationSurPays} fois la densité de la France métropolitaine).
                </p>
                <p>${unsafeHTML(this.rapport.getDiagnosticActif()?.messageOtex)}</p>
                ${this.rapport.getTerritoireActif()!.categorie !== CategorieTerritoire.Commune
                    ? html` <c-chart-barre-empilee-horizontale
                              id="barre-otex"
                              .items=${this.calculerOptionsBarreOtex(this.rapport)}
                              unite="commune(s)"
                          ></c-chart-barre-empilee-horizontale>
                          <p id="legende-otex" class="texte-petit">
                              Répartition des communes du territoire par OTEX (12 postes), d'après les données de
                              ${unsafeHTML(sourcesDonnees.otex.source + ', ' + sourcesDonnees.otex.annees)}
                          </p>`
                    : ''}
                <p>
                    La surface agricole utile totale est de ${sauTotale} hectares (soit ${partSauSurSuperficie} % de la superficie totale) et se
                    répartit de la façon suivante entre surfaces productive et peu productive :
                </p>
                <c-tableau .options=${this.calculerOptionsTableauPresentationTerritoire(this.rapport)}></c-tableau>
                <p>
                    Les surfaces agricoles sont issues du
                    <c-lien href="${PAGES_METHODOLOGIE.sourcesDonnees.getUrl()}#${sourcesDonnees.rpg.id}">RPG</c-lien> dont certaines informations et
                    limites sont données dans la
                    <c-lien href="${PAGES_METHODOLOGIE.presentationGenerale.getUrl()}#nomenclature-surfaces-agricoles"
                        >nomenclature des surfaces agricoles</c-lien
                    >.
                </p>
                <p>
                    Les surfaces agricoles peu productives rassemblent les jachères, les estives & landes et diverses cultures particulières. Cette
                    distinction est faite dans le but de tenir compte du fait que certaines terres agricoles ne participent pas (par exemple les
                    jachères) ou peu (par exemple les alpages de haute altitude) à la production agricole annuelle d'un territoire.
                </p>
                <p><strong>Ces surfaces dites peu productives sont exclues de tous les calculs de CRATer.</strong></p>

                <c-encart-pages-precedente-suivante
                    libellePageSuivante=${ifDefined(SA.getMaillon(IDS_MAILLONS.terresAgricoles)?.nom)}
                    hrefPageSuivante=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.terresAgricoles,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsTableauPresentationTerritoire(rapport: Rapport): OptionsTableau {
        return {
            entetes: ['', 'Superficie', 'Part'],
            contenuHtml: [
                [
                    'Surface productive',
                    formaterNombreEnEntierHtml(rapport.getDiagnosticActif()!.surfaceAgricoleUtile!.sauProductiveHa) + ' ha',
                    formaterNombreEnEntierHtml(rapport.getDiagnosticActif()!.surfaceAgricoleUtile!.pourcentageSauProductive) + ' %'
                ],
                [
                    'Surface peu productive',
                    formaterNombreEnEntierHtml(rapport.getDiagnosticActif()?.surfaceAgricoleUtile.sauPeuProductiveHa) + ' ha',
                    formaterNombreEnEntierHtml(rapport.getDiagnosticActif()?.surfaceAgricoleUtile.pourcentageSauPeuProductive) + ' %'
                ]
            ],
            ligneFin: [
                'Total',
                formaterNombreEnEntierHtml(rapport.getDiagnosticActif()?.surfaceAgricoleUtile.sauTotaleHa) + ' ha',
                formaterNombreEnEntierHtml(100) + ' %'
            ]
        };
    }

    private calculerOptionsBarreOtex(rapport: Rapport): ItemBarreEmpileeHorizontale[] {
        const nbCommunesOtex12Postes = rapport.getDiagnosticActif()?.otex!.nbCommunesOtex12Postes;
        if (!nbCommunesOtex12Postes) return [];
        return nbCommunesOtex12Postes.map(
            (i) =>
                <ItemBarreEmpileeHorizontale>{
                    valeur: i.valeur,
                    libelle: i.otex.libelle,
                    couleur: i.otex.couleur
                }
        );
    }
}
