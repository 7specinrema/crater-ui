import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../src/pages/diagnostic/chapitres/SyntheseMaillon.js';

import { html } from 'lit';

import { OptionsSyntheseMaillon } from '../../src/pages/diagnostic/chapitres/SyntheseMaillon';

describe('c-synthese-maillon', () => {
    const maillon = {
        id: '1'
    };
    const options = <OptionsSyntheseMaillon>{
        note: 10,
        messageSynthese: "je suis un composant synthese maillon avec un message super intéressant à donner à qui veut l'entendre !",
        idMaillon: '1'
    };
    const couleur = '#219653';

    it.only('Affichage mode desktop', () => {
        cy.viewport(1000, 600);
        cy.mount(html` <c-synthese-maillon .options=${options} couleur=${couleur}></c-synthese-maillon> `);
        cy.get('p').should('contain', 'je suis un composant synthese maillon');
        cy.get('c-jauge-note-sur-10').should('have.attr', 'note', '10');
    });

    it('Affichage mode mobile', () => {
        cy.viewport(320, 600);
        cy.mount(html` <c-synthese-maillon .options=${options} couleur=${couleur}></c-synthese-maillon> `);
        cy.get('p').should('contain', 'je suis un composant synthese maillon');
        cy.get('c-jauge-note-sur-10').should('have.attr', 'note', '10');
    });
});
