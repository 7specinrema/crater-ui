// Déclaration de type pour pouvoir importer un fichier geojson avec typescript (voir requetes-api-carte.ts)
declare module '*.geojson' {
    const src: string;
    export default src;
}
