function verifierPoductionEnHectares(nombreAttendu) {
    cy.get('[data-cy="synthese-valeur-sauProductiveHa"]')
        .invoke('text')
        .then((text) => parseInt(text.replace(/\u202F/g, ''), 10))
        .should('eq', nombreAttendu);
}

function verifierNoteEtMessage(cyId: string, note: string, message: string) {
    cy.get(`[data-cy="${cyId}"]`)
        .find('c-jauge-note-sur-10')
        .invoke('attr', 'note')
        .then((n) => {
            expect(n).to.equal(note);
        });
    cy.get(`[data-cy="${cyId}"]`).find('#message').contains(message);
}

function interceptTerritoires() {
    cy.intercept('GET', 'territoires/occitanie', { fixture: 'occitanieDetail.json' });
    cy.intercept('GET', 'diagnostics/occitanie', { fixture: 'diagnosticOccitanie.json' });
    cy.intercept('GET', '/territoires?critere=*', { fixture: 'listeSuggestionsFrance.json' });
    cy.intercept('GET', 'diagnostics/france', { fixture: 'diagnosticFrance.json' });
}

describe('Tests end2end page diagnostic', () => {
    it('Vérifier le contenu de la page de synthèse en mode desktop', () => {
        cy.viewport(1400, 800);
        interceptTerritoires();
        cy.visit('/diagnostic/occitanie');
        cy.verifierAffichagePage('Diagnostic du système alimentaire');
        verifierPoductionEnHectares(2568467);
        cy.get('c-barre-onglets-item#PAYS').find('a').click();
        verifierPoductionEnHectares(25018039);
        verifierNoteEtMessage(
            'message-cle-terres-agricoles',
            '5',
            'La surface agricole par habitant peut convenir pour un régime alimentaire moins carné mais l’objectif Zéro Artificialisation n’a pas été atteint entre 2011 et 2016.'
        );
        verifierNoteEtMessage(
            'message-cle-agriculteurs-exploitations',
            '5',
            "Part d'actifs agricoles permanents proche de la moyenne française et en déclin."
        );
        verifierNoteEtMessage(
            'message-cle-intrants',
            '0',
            'Usage de pesticides extrêment élevé (3,5 fois la dose annuelle maximale autorisée pour une substance donnée).'
        );
        verifierNoteEtMessage(
            'message-cle-production',
            '7',
            'Production élevée mais trop spécialisée pour couvrir la consommation et pratiques agricoles très préjudiciables à la biodiversité.'
        );
        verifierNoteEtMessage(
            'message-cle-transformation-distribution',
            '8',
            '24 % de la population est théoriquement dépendante de la voiture pour ses achats alimentaires.'
        );
        verifierNoteEtMessage(
            'message-cle-consommation',
            '!',
            'Régime alimentaire riche et très carné qui induit un besoin important de surfaces agricoles. Précarité alimentaire importante et en hausse.'
        );
    });

    it('Vérifier les liens sortants en mode desktop', () => {
        cy.viewport(1400, 800);
        interceptTerritoires();
        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-terres-agricoles"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Terres agricoles');

        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-agriculteurs-exploitations"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Agriculteurs & Exploitations');

        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-intrants"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Intrants');

        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-production"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Production');

        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-transformation-distribution"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Transformation & Distribution');

        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-consommation"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Consommation');
    });

    it('Vérifier le contenu de la page de synthèse en mode mobile', () => {
        cy.viewport(320, 500);
        interceptTerritoires();
        cy.visit('/diagnostic/occitanie');
        cy.get('h1').contains('Occitanie');
        cy.get('c-menu-pied-page').should('exist');
        cy.get('c-panneau-glissant').find('a[title="Menu"]').click({ force: true });
        cy.get('c-bouton[libelle="Modifier"]').click({ force: true });
        cy.get('c-menu-mobile-diagnostic-modifier-territoire').find('input[type="text"]').eq(0).type('france');
        cy.get('menu article').contains('France').click({ force: true });
        cy.verifierAffichagePage('Diagnostic du système alimentaire');
    });
});
