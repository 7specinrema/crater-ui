import { describe, expect, it } from 'vitest';

import { calculerMessageSyntheseIntrants } from './messages-intrants';

describe(`Tests des fonctions de calcul des messages du maillon Agricuteurs et Exploitations`, () => {
    it('Calculer le message de synthese', () => {
        expect(calculerMessageSyntheseIntrants(null)).toEqual(`Données non disponibles (voir les échelles supérieures telle que l'EPCI).`);
        expect(calculerMessageSyntheseIntrants(0)).toEqual(`Usage de pesticides <strong>nul</strong>.`);
        expect(calculerMessageSyntheseIntrants(0.3)).toEqual(
            `Usage de pesticides <strong>modéré</strong> (0,3 fois la dose annuelle maximale autorisée pour une substance donnée).`
        );
        expect(calculerMessageSyntheseIntrants(1)).toEqual(
            `Usage de pesticides <strong>élevé</strong> (1 fois la dose annuelle maximale autorisée pour une substance donnée).`
        );
        expect(calculerMessageSyntheseIntrants(2)).toEqual(
            `Usage de pesticides <strong>très élevé</strong> (2 fois la dose annuelle maximale autorisée pour une substance donnée).`
        );
        expect(calculerMessageSyntheseIntrants(5)).toEqual(
            `Usage de pesticides <strong>extrêment élevé</strong> (5 fois la dose annuelle maximale autorisée pour une substance donnée).`
        );
    });
});
