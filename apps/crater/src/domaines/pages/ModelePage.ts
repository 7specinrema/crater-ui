import { DonneesPage } from './donnees-pages';
import { nettoyerParamSansValeur } from './url-utils';

export type OptionsModelePage<D extends DonneesPage> = {
    url: (donneesPage: D) => string;
    urlCanonique?: (donneesPage: D) => string;
    titreCourt: string;
    titreLong?: (donneesPage: D) => string;
    metaDescription?: (donneesPage: D) => string;
};

export class ModelePage<D extends DonneesPage> {
    constructor(private readonly options: OptionsModelePage<D>) {}

    private getUrlSansHash(donneesPage: D) {
        return nettoyerParamSansValeur(this.options.url(donneesPage)).toLowerCase();
    }
    getUrl(donneesPage: D): string {
        return donneesPage.idElementCibleScroll
            ? `${this.getUrlSansHash(donneesPage)}#${donneesPage.idElementCibleScroll}`.toLowerCase()
            : this.getUrlSansHash(donneesPage);
    }

    private getUrlPath(donneesPage: D): string {
        return this.getUrlSansHash(donneesPage).split(/\?|#/g)[0];
    }
    getId(donneesPage: D): string {
        return this.getUrlPath(donneesPage);
    }

    getTitreCourt(): string {
        return this.options.titreCourt;
    }
    getTitreLong(donneesPage: D): string {
        return this.options.titreLong ? this.options.titreLong(donneesPage) : this.getTitreCourt();
    }
    getUrlCanonique(donneesPage: D): string {
        return this.options.urlCanonique ? this.options.urlCanonique(donneesPage).toLowerCase() : this.getUrlSansHash(donneesPage);
    }
    getMetaDescription(donneesPage: D): string | undefined {
        return this.options.metaDescription ? this.options.metaDescription(donneesPage) : undefined;
    }
}
