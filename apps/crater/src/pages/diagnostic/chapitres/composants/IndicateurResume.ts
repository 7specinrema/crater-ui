import '@lga/design-system/build/composants/BoiteAvecLien.js';

import { QualificatifEvolution, Signe } from '@lga/commun/build/outils/base';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, TemplateResult, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { construireUrlPageDiagnosticIndicateur } from '../../../../domaines/pages/pages-utils';
import { Indicateur } from '../../../../domaines/systeme-alimentaire/Indicateur.js';
import { STYLES_CRATER } from '../../../commun/pages-styles';

export interface OptionsIndicateurResume {
    indicateur: Indicateur;
    chiffresCles: ChiffreCle[];
    motSeparateurChiffresCles?: string;
    donneesPage?: DonneesPageDiagnostic;
}

interface ChiffreCle {
    libelleAvantChiffre?: string;
    chiffre: string;
    icone?: string;
    libelleApresChiffre?: string;
    evolutionSigne?: Signe;
    evolutionQualificatif?: QualificatifEvolution;
    evolutionDescription?: string;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-indicateur-resume': IndicateurResume;
    }
}

@customElement('c-indicateur-resume')
export class IndicateurResume extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            :host {
                display: block;
                width: 100%;
                margin-top: calc(2 * var(--dsem));
                margin-bottom: calc(2 * var(--dsem));
                --couleur-icone: var(--couleur-secondaire);
            }

            div {
                color: var(--couleur-neutre);
            }

            c-boite-avec-lien {
                max-width: 600px;
            }

            .titre-indicateur {
                margin-left: calc(2 * var(--dsem));
                margin-top: calc(2 * var(--dsem));
            }

            #evolution {
                display: inline-flex;
                align-items: center;
                gap: calc(var(--dsem) / 2);
                padding-left: calc(6 * var(--dsem));
                margin-bottom: calc(2 * var(--dsem));
            }

            #evolution p {
                margin: 0;
            }

            .succes > svg,
            .succes {
                color: var(--couleur-succes);
                stroke: var(--couleur-succes);
            }

            .danger > svg,
            .danger {
                color: var(--couleur-danger);
                stroke: var(--couleur-danger);
            }

            .neutre > svg,
            .neutre {
                color: var(--couleur-neutre);
                stroke: var(--couleur-neutre);
            }

            .conteneur-deux-items-chiffres-cles {
                display: grid;
                grid-template-columns: auto 1fr 20% 1fr;
                align-items: center;
                margin: calc(2 * var(--dsem));
            }

            .conteneur-un-item-chiffres-cles {
                display: grid;
                grid-template-columns: auto 1fr;
                align-items: center;
                margin: calc(2 * var(--dsem));
                gap: calc(1.5 * var(--dsem));
            }

            .mot-separateur {
                width: fit-content;
                margin-right: auto;
                margin-left: auto;
            }

            #icone-indicateur svg {
                width: calc(9 * var(--dsem));
                height: calc(9 * var(--dsem));
            }

            .chiffre-cle {
                padding: var(--dsem);
                padding-top: 0;
            }

            .valeur {
                display: flex;
                align-items: center;
                color: var(--couleur-primaire);
                max-width: 300px;
            }

            .valeur svg {
                fill: var(--couleur-primaire);
                padding-left: var(--dsem);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                #icone-indicateur {
                    display: none;
                }
                .titre-indicateur {
                    margin-left: var(--dsem);
                    margin-top: var(--dsem);
                }
                .conteneur-deux-items-chiffres-cles {
                    grid-template-columns: 1fr auto 1fr;
                }
                .mot-separateur {
                    margin-right: auto;
                    margin-left: auto;
                }
            }
        `
    ];

    @property({ attribute: false })
    options?: OptionsIndicateurResume;

    render() {
        if (!this.options) return;

        const dispositionChiffresCles =
            this.options.chiffresCles.length > 1 ? 'conteneur-deux-items-chiffres-cles' : 'conteneur-un-item-chiffres-cles';

        return html`
            <c-boite-avec-lien
                libelleLien="Voir l'indicateur"
                href=${construireUrlPageDiagnosticIndicateur(
                    this.options.indicateur.id,
                    this.options.donneesPage?.idTerritoirePrincipal ?? '',
                    this.options.donneesPage?.idEchelleTerritoriale
                )}
                encadrement
                arrondi="moyen"
            >
                <div slot="contenu">
                    <h2 class="titre-petit titre-indicateur">${unsafeHTML(this.options.indicateur.libelle)}</h2>
                    <div class=${dispositionChiffresCles}>
                        <div id="icone-indicateur">${unsafeHTML(this.options.indicateur.icone)}</div>
                        ${this.renderChiffresCles(this.options.chiffresCles)}
                    </div>
                    ${this.renderEvolution(this.options.chiffresCles[0])}
                </div>
            </c-boite-avec-lien>
        `;
    }

    private renderChiffresCles(chiffresCles: ChiffreCle[]) {
        return chiffresCles!.map(
            (c, i) => html`
                ${i !== 0 ? html`<div class="titre-moyen mot-separateur">${unsafeHTML(this.options!.motSeparateurChiffresCles)}</div> ` : ``}
                <div class="chiffre-cle">
                    <div class="texte-petit-majuscule">${unsafeHTML(c.libelleAvantChiffre)}</div>
                    <div class="valeur titre-moyen">${unsafeHTML(c.chiffre)}${unsafeHTML(c.icone)}</div>
                    <div class="texte-petit-majuscule">${unsafeHTML(c.libelleApresChiffre)}</div>
                </div>
            `
        );
    }
    private renderEvolution(chiffreCle: ChiffreCle) {
        let icone: TemplateResult = html``;
        let couleur = 'neutre';
        if (chiffreCle.evolutionSigne === Signe.POSITIF) {
            icone = ICONES_DESIGN_SYSTEM.flecheHautDroit;
        } else if (chiffreCle.evolutionSigne === Signe.NEGATIF) {
            icone = ICONES_DESIGN_SYSTEM.flecheBasDroit;
        }
        if (chiffreCle.evolutionQualificatif === QualificatifEvolution.valide) {
            icone = ICONES_DESIGN_SYSTEM.check;
        } else if (chiffreCle.evolutionQualificatif === QualificatifEvolution.rate) {
            icone = ICONES_DESIGN_SYSTEM.croix;
        }
        if (chiffreCle.evolutionQualificatif === QualificatifEvolution.progres || chiffreCle.evolutionQualificatif === QualificatifEvolution.valide) {
            couleur = 'succes';
        } else if (
            chiffreCle.evolutionQualificatif === QualificatifEvolution.declin ||
            chiffreCle.evolutionQualificatif === QualificatifEvolution.rate
        ) {
            couleur = 'danger';
        }
        return chiffreCle.evolutionQualificatif
            ? html`
                  <div id="evolution" class="${couleur}">
                      ${icone}
                      <p class="texte-petit-majuscule">${unsafeHTML(chiffreCle.evolutionDescription)}</p>
                  </div>
              `
            : ``;
    }
}
