import '../commun/template/TemplatePageAccueil.js';
import './DescriptionCrater.js';
import './ComprendreLesEnjeux.js';
import './ZoomSur.js';
import './RechercherUnTerritoire.js';

import { EvenementSelectionnerTerritoire } from '@lga/commun/build/composants/champ-recherche-territoire/EvenementSelectionnerTerritoire';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_PRINCIPALES } from '../../domaines/pages/configuration/declaration-pages';
import { construireUrlPageDiagnosticSynthese } from '../../domaines/pages/pages-utils';
import { EvenementNaviguer } from '../../evenements/EvenementNaviguer.js';
import { STYLES_CRATER } from '../commun/pages-styles';
declare global {
    interface HTMLElementTagNameMap {
        'c-page-accueil': PageAccueil;
    }
}

@customElement('c-page-accueil')
export class PageAccueil extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            c-template-page-accueil {
                --couleur-fond-accueil: var(--couleur-secondaire);
            }

            main {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                gap: calc(var(--dsem) * 12);
                padding-bottom: calc(var(--dsem) * 8);
            }

            c-description-crater,
            c-comprendre-les-enjeux,
            c-zoom-sur {
                max-width: var(--largeur-maximum-contenu);
                margin: 0 auto;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                main {
                    gap: calc(var(--dsem) * 10);
                    padding-bottom: calc(var(--dsem) * 4);
                }
            }
        `
    ];

    @property()
    idElementCibleScroll = '';

    render() {
        return html`
            <c-template-page-accueil idItemActifMenuPrincipal=${PAGES_PRINCIPALES.accueil.getId()} idElementCibleScroll=${this.idElementCibleScroll}>
                <main slot="contenu">
                    <c-rechercher-un-territoire @selectionnerTerritoire=${this.actionSelectionnerTerritoire}></c-rechercher-un-territoire>
                    <c-description-crater></c-description-crater>
                    <c-comprendre-les-enjeux></c-comprendre-les-enjeux>
                    <c-zoom-sur></c-zoom-sur>
                </main>
            </c-template-page-accueil>
        `;
    }

    private actionSelectionnerTerritoire(evt: Event) {
        this.dispatchEvent(
            new EvenementNaviguer(construireUrlPageDiagnosticSynthese((<EvenementSelectionnerTerritoire>evt).detail.idTerritoireCrater))
        );
    }
}
