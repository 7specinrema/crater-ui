import { css } from 'lit';

export const STYLES_BADGES = css`
    .badge-accent {
        background-color: var(--couleur-accent-clair);
        color: var(--couleur-accent-sombre);
        border-radius: var(--dsem);
        width: fit-content;
        padding: calc(0.5 * var(--dsem)) calc(1 * var(--dsem));
    }
`;
