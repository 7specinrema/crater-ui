export const VALEURS_COULEURS = {
    couleur_primaire_clair: '#BFE3D6',
    couleur_primaire: '#27987C',
    couleur_primaire_sombre: '#0F4A3B',

    couleur_accent_clair: '#D96C5B',
    couleur_accent: '#D23219',
    couleur_accent_sombre: '#571208',

    couleur_neutre_clair: '#F7F6F4',
    couleur_neutre: '#162A41',
    couleur_neutre_sombre: '#09111B',
    couleur_neutre_80: '#435365',
    couleur_neutre_60: '#6F7B89',
    couleur_neutre_40: '#9CA4AD',
    couleur_neutre_20: '#C8CCD1',

    couleur_blanc: '#ffffff',
    couleur_fond: '#FEFDFB',

    couleur_succes: '#B1CC7E',
    couleur_danger: '#EF5C3F'
};
