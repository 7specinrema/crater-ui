import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { STYLES_CRATER } from '../../commun/pages-styles';

export interface ItemLegende {
    couleur: string;
    motif: string;
    libelle: string;
}

export const MOTIF_RAYURE = 'repeating-linear-gradient(90deg, rgba(255, 255, 255, 1) 0 2px, rgba(0, 0, 0, 0) 1px 5px)';

declare global {
    interface HTMLElementTagNameMap {
        'c-legende-chart': LegendeChart;
    }
}
@customElement('c-legende-chart')
export class LegendeChart extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            .dot {
                position: relative;
                top: 2px;
                height: 1rem;
                width: 1rem;
                background-color: #bbb;
                border-radius: 50%;
                display: inline-block;
                margin-right: 0.3rem;
            }

            ul {
                display: grid;
                grid-template-columns: 1fr 1fr 1fr;
                justify-items: start;
                padding: 1rem 0 2rem 0;
                margin: auto;
            }
            li {
                list-style: none;
                display: inline-block;
                padding: 0 0.4rem;
            }
        `
    ];

    @property({ attribute: false })
    itemsLegende: ItemLegende[] = [];

    render() {
        return html` <ul>
            ${this.itemsLegende.map(
                (item) =>
                    html`<li class="texte-moyen">
                        <span
                            class="dot"
                            style="border: solid 1px ${item.couleur}; background-color: ${item.couleur}; background-image: ${item.motif}"
                        ></span
                        >${item.libelle}
                    </li>`
            )}
        </ul>`;
    }
}
