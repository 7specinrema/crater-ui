export interface SourceDonnee {
    id: string;
    nom: string;
    definition: string;
    annees: string;
    anneeLaPlusRecente?: string;
    perimetre_geographique: string;
    source: string;
    sourceCourt?: string;
    legende: string;
    limites?: string;
}

interface SourcesDonnees {
    decoupage_communal: SourceDonnee;
    codes_postaux: SourceDonnee;
    epcis: SourceDonnee;
    codes_officiels_departements_et_regions: SourceDonnee;
    communes_appartenance_bassins_de_vie: SourceDonnee;
    communes_appartenance_pn: SourceDonnee;
    communes_appartenance_pnr: SourceDonnee;
    communes_appartenance_pays_et_petr: SourceDonnee;
    communes_appartenance_pat: SourceDonnee;
    communes_appartenance_scot: SourceDonnee;
    geographie_des_communes: SourceDonnee;
    geographie_des_epcis: SourceDonnee;
    geographie_des_departements_et_regions: SourceDonnee;
    otex: SourceDonnee;
    rpg: SourceDonnee;
    sau_ra: SourceDonnee;
    population_totale: SourceDonnee;
    population_totale_historique: SourceDonnee;
    actifs_agricoles_permanents: SourceDonnee;
    exploitations_selon_age_chef_exploitation: SourceDonnee;
    exploitations_selon_taille_exploitation: SourceDonnee;
    artificialisation_sols: SourceDonnee;
    logements_totaux_et_vacants: SourceDonnee;
    quantites_substances_actives: SourceDonnee;
    doses_unites_substances_actives: SourceDonnee;
    hvn: SourceDonnee;
    surfaces_agricoles_bio: SourceDonnee;
    consommation_sau: SourceDonnee;
    base_permanente_equipements: SourceDonnee;
    commerces_osm: SourceDonnee;
    carroyage_insee: SourceDonnee;
    taux_pauvrete: SourceDonnee;
}

export const sourcesDonnees: SourcesDonnees = {
    decoupage_communal: {
        id: 'decoupage_communal',
        nom: 'Découpage communal de la France',
        definition: 'Découpage communal de la France, et appartenances géographiques',
        annees: '1er janvier 2022',
        perimetre_geographique: 'France métropolitaine + DROM',
        source: `<c-lien href="https://www.insee.fr/fr/information/2028028" target="_blank">INSEE</c-lien>`,
        legende: 'INSEE 2022'
    },

    codes_officiels_departements_et_regions: {
        id: 'codes_officiels_departements_et_regions',
        nom: 'Codes INSEE des départements & régions',
        definition: 'Code officiel géographique',
        annees: '1er janvier 2022',
        perimetre_geographique: 'France métropolitaine + DROM',
        source: `<c-lien href="https://www.insee.fr/fr/information/6051727" target="_blank">INSEE</c-lien>`,
        legende: 'INSEE 2022'
    },

    codes_postaux: {
        id: 'codes_postaux',
        nom: 'Codes postaux',
        definition: 'Base officielle des codes postaux (correspondance code INSEE ⇔ code postal)',
        annees: '1er janvier 2022',
        perimetre_geographique: 'France (métropole et DOM), TOM et MONACO',
        source: `<c-lien
                href="https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/export/?disjunctive.code_commune_insee&amp;disjunctive.nom_de_la_commune&amp;disjunctive.code_postal&amp;disjunctive.ligne_5"
                target="_blank"
                >La Poste</c-lien>`,
        legende: 'La Poste 2022'
    },

    epcis: {
        id: 'epcis',
        nom: 'EPCI',
        definition: 'Définition des EPCI',
        annees: '1er janvier 2022',
        perimetre_geographique: 'France métropolitaine + DROM',
        source: `<c-lien href="https://www.insee.fr/fr/information/2510634" target="_blank">INSEE</c-lien>`,
        legende: 'INSEE 2022'
    },

    communes_appartenance_bassins_de_vie: {
        id: 'communes_appartenance_bassins_de_vie',
        nom: "Communes d'appartenance des bassins de vie 2012",
        definition: 'Liste des communes des bassins de vie 2012 (BV2012)',
        annees: '1er janvier 2022',
        perimetre_geographique: 'France métropolitaine + DROM',
        source: `<c-lien href="https://www.insee.fr/fr/information/2115016" target="_blank">
                INSEE</c-lien>`,
        legende: 'INSEE 2022'
    },

    communes_appartenance_pn: {
        id: 'communes_appartenance_pn',
        nom: "Communes d'appartenance des parcs nationaux (PN)",
        definition: 'Liste des communes des parcs nationaux (PN)',
        annees: '1er janvier 2021',
        perimetre_geographique: 'France métropolitaine + DROM',
        source: `<c-lien href="https://www.observatoire-des-territoires.gouv.fr/communes-des-parcs-nationaux" target="_blank">
                Observatoire des territoires</c-lien>`,
        legende: 'Observatoire des territoires 2021'
    },

    communes_appartenance_pnr: {
        id: 'communes_appartenance_pnr',
        nom: "Communes d'appartenance des parcs naturels régionaux (PNR)",
        definition: 'Liste des communes des parcs naturels régionaux (PNR)',
        annees: '1er janvier 2021',
        perimetre_geographique: 'France métropolitaine + DROM',
        source: `<c-lien href="https://www.observatoire-des-territoires.gouv.fr/communes-des-parcs-naturels-regionaux-pnr" target="_blank">
                Observatoire des territoires</c-lien>`,
        legende: 'Observatoire des territoires 2021'
    },

    communes_appartenance_pays_et_petr: {
        id: 'communes_appartenance_pays_et_petr',
        nom: "Communes d'appartenance des PAYS et PETR",
        definition: 'Liste des communes des PAYS et PETR',
        annees: '1er janvier 2021',
        perimetre_geographique: 'France métropolitaine + DROM',
        source: `<c-lien href="https://www.observatoire-des-territoires.gouv.fr/perimetre-des-pays-et-petr" target="_blank">
                Observatoire des territoires</c-lien>`,
        legende: 'Observatoire des territoires 2021'
    },

    communes_appartenance_pat: {
        id: 'communes_appartenance_pat',
        nom: "Communes d'appartenance des Projets Alimentaires Territoriaux (PAT)",
        definition: 'Liste des communes des Projets Alimentaires Territoriaux (PAT)',
        annees: '1er janvier 2021',
        perimetre_geographique: 'France métropolitaine',
        source: `Contributeurs CRATer via demandes d'ajout de territoire`,
        legende: 'Contributeurs CRATer'
    },

    communes_appartenance_scot: {
        id: 'communes_appartenance_scot',
        nom: "Communes d'appartenance des Schémas de Cohérence Territoriale (SCoT)",
        definition: 'Liste des communes des Schémas de Cohérence Territoriale (SCoT)',
        annees: '1er janvier 2021',
        perimetre_geographique: 'France métropolitaine',
        source: `Contributeurs CRATer via demandes d'ajout de territoire`,
        legende: 'Contributeurs CRATer'
    },

    geographie_des_communes: {
        id: 'geographie_des_communes',
        nom: 'Géographie des communes',
        definition: 'Géographie des communes',
        annees: '1er janvier 2022',
        perimetre_geographique: 'France métropolitaine y compris les DROM',
        source: `<c-lien
                href="https://www.data.gouv.fr/fr/datasets/decoupage-administratif-communal-francais-issu-d-openstreetmap"
                target="_blank"
                >OpenStreetMap</c-lien>`,
        legende: 'OSM 2022'
    },

    geographie_des_epcis: {
        id: 'geographie_des_epcis',
        nom: 'Géographie des EPCI',
        definition: 'Géographie des EPCI',
        annees: '1er janvier 2022',
        perimetre_geographique: 'France métropolitaine y compris les DROM',
        source: `<c-lien
                href="https://www.banatic.interieur.gouv.fr/V5/cartographie/cartographie.php"
                target="_blank"
                >BANATIC, la base nationale sur l'intercommunalité</c-lien>`,
        legende: 'BANATIC 2022'
    },

    geographie_des_departements_et_regions: {
        id: 'geographie_des_departements_et_regions',
        nom: 'Géographie des départements et régions',
        definition: 'Géographie des départements et régions',
        annees: '2018',
        perimetre_geographique: 'France métropolitaine y compris les DROM',
        source: `<c-lien
                href="https://www.data.gouv.fr/fr/datasets/contours-des-departements-francais-issus-d-openstreetmap/"
                target="_blank"
                >OpenStreetMap</c-lien>`,
        legende: 'OSM 2018'
    },

    otex: {
        id: 'otex',
        nom: 'Spécialisation territoriale de la production agricole en 2020',
        definition: 'Spécialisation territoriale de la production agricole en 2020 (OTEX en 12 postes)',
        annees: '2020',
        perimetre_geographique: 'Communes au 01/01/2020 - France métropolitaine + DROM',
        source: `<c-lien
                href="https://stats.agriculture.gouv.fr/cartostat/#c=indicator&i=otex_2020_1.otefda20&t=A02&view=map15" target="_blank">
                Cartostat
                </c-lien>`,
        legende: 'RA (2020)'
    },

    rpg: {
        id: 'rpg',
        nom: 'Registre Parcellaire Graphique',
        definition: 'Surfaces, géométries des parcelles et types de cultures',
        annees: '2017',
        perimetre_geographique: 'France (métropole et DROM)',
        source: `<c-lien href="https://geoservices.ign.fr/rpg" target="_blank">IGN</c-lien>`,
        limites: `<c-lien href="https://www.geoportail.gouv.fr/actualites/les-usages-du-registre-parcellaire-graphique#!" target="_blank">
                Seules les parcelles appartenant à un exploitant les ayant déclarées dans sa demande de subventions à la PAC
                (Politique Agricole Communes) sont recensées.</c-lien>`,
        legende: 'RPG 2017'
    },

    sau_ra: {
        id: 'sau_ra',
        nom: 'Surfaces agricoles utiles issues du Recensement Agricole',
        definition: 'Surface agricole utile de chaque commune',
        annees: '2020',
        perimetre_geographique: 'Communes au 01/01/2020 - France métropolitaine + DROM',
        source: `Indicateur SAU Moyenne en 2020 pour les communes,
            <c-lien href="https://stats.agriculture.gouv.fr/cartostat" target="_blank">Cartostat</c-lien>`,
        sourceCourt: `<c-lien href="https://stats.agriculture.gouv.fr/cartostat" target="_blank">2020</c-lien>`,
        legende: 'RA (2020)'
    },

    population_totale: {
        id: 'population_totale',
        nom: 'Population totale',
        definition: 'Populations légales',
        annees: '1er janvier 2017',
        perimetre_geographique: 'Communes au 01/01/2019 - France métropolitaine + DROM hors Mayotte',
        source: `<c-lien href="https://www.insee.fr/fr/statistiques/4265429?sommaire=4265511" target="_blank">INSEE</c-lien>`,
        legende: 'INSEE 2017'
    },

    population_totale_historique: {
        id: 'population_totale_historique',
        nom: 'Populations totales, historique',
        definition: `Recensement de la population - Base des principaux indicateurs. Résultats sur les naissances, les décès, 
        la population, les résidences principales, les résidences secondaires et logements occasionnels, les caractéristiques 
        des personnes des ménages`,
        annees: '1er janvier 1968, 1975, 1982, 1990, 1999, 2010 et 2015',
        perimetre_geographique: 'Communes au 01/01/2017 - France métropolitaine + DROM hors Mayotte',
        source: `<c-lien href="https://www.insee.fr/fr/statistiques/3565661" target="_blank">INSEE</c-lien>`,
        legende: 'INSEE 2017'
    },

    actifs_agricoles_permanents: {
        id: 'actifs_agricoles_permanents',
        nom: 'Actifs agricoles permanents',
        definition: `Actifs agricoles permanents par commune. Cela inclut les chefs d'exploitations, conjoints et salariés permanents
            mais pas les salariés temporaires à l'échelle française.`,
        annees: 'Recensements de 1970, 1979, 1988, 2000 et 2010',
        anneeLaPlusRecente: 'Recensement 2010',
        perimetre_geographique: `Communes, Départements, Régions (nouvelles) au 1e janvier 2011 vraisemblablement - France métropolitaine + DROM`,
        source: `<c-lien href="https://agreste.agriculture.gouv.fr/agreste-web/disaron/G_2005/detail/" target="_blank">AGRESTE</c-lien>`,
        limites: `
            La donnée sur le nombre d’actifs agricoles inclut les chefs d&#39;exploitations, conjoints et salariés permanents mais
            pas les salariés temporaires à l&#39;échelle française (“En 2018, en France métropolitaine, le travail agricole,
            mesuré en unité de travail annuel (UTA) , est assuré pour 57,3 % par les dirigeants actifs, pour 26,4 % par les autres
            actifs permanents (non salariés ou salariés) et pour 16,3 % par les salariés saisonniers ou prestataires extérieurs à
            l’exploitation. La diminution de l’emploi de l’ensemble des actifs agricoles se poursuit (– 1,0 % en moyenne annuelle
            entre 2010 et 2018).&quot; selon l’<c-lien
                href="https://www.insee.fr/fr/statistiques/4277860?sommaire=4318291"
                target="_blank"
                >INSEE</c-lien
            >). De plus, les données sont assez anciennes et parfois sous secret statistique. Elles devraient être mise à jour en
            2022 (mise à disposition au printemps 2022 des données du recensement agricole réalisé en 2020).`,
        legende: 'RA 2010'
    },

    exploitations_selon_age_chef_exploitation: {
        id: 'exploitations_selon_age_chef_exploitation',
        nom: "Exploitations selon l'âge du chef d'exploitation",
        definition: "Nombre et superficie des exploitations par classes d'âge du chef d'exploitation (e.g. moins de 40 ans)",
        annees: 'Recensements de 1970, 1979, 1988, 2000 et 2010',
        perimetre_geographique: 'Communes, Départements, Régions (nouvelles) au 1e janvier 2011 vraisemblablement - France métropolitaine + DROM',
        source: `<c-lien href="https://agreste.agriculture.gouv.fr/agreste-web/disaron/G_2004/detail/" target="_blank">AGRESTE</c-lien>`,
        legende: 'RA 2010',
        limites: `Les données sont assez anciennes et parfois sous secret statistique. Elles devraient être mise à jour en 2022 (mise à disposition 
            au printemps 2022 des données du recensement agricole réalisé en 2020).`
    },

    exploitations_selon_taille_exploitation: {
        id: 'exploitations_selon_taille_exploitation',
        nom: 'Exploitations selon la classe de superficie',
        definition: 'Nombre et superficie des exploitations par classes de superficie (e.g. de 20 à 50 ha)',
        annees: 'Recensements de 1970, 1979, 1988, 2000 et 2010',
        anneeLaPlusRecente: 'Recensement 2010',
        perimetre_geographique: 'Communes, Départements, Régions (nouvelles) au 1e janvier 2011 vraisemblablement - France métropolitaine + DROM',
        source: `<c-lien href="https://agreste.agriculture.gouv.fr/agreste-web/disaron/G_2003/detail/" target="_blank">AGRESTE</c-lien>`,
        legende: 'RA 2010',
        limites: `
            Les données sont assez anciennes et parfois sous secret statistique. Elles devraient être mise à jour en 2022 (mise à
            disposition au printemps 2022 des données du recensement agricole réalisé en 2020).`
    },

    artificialisation_sols: {
        id: 'artificialisation_sols',
        nom: 'Artificialisation des sols',
        definition: 'Surfaces artificialisées pour l’habitat, l’activité, le mix, et autre.',
        annees: '2011 à 2016',
        perimetre_geographique:
            'Communes au 01/01/2019 (erreur vraisemblablement dans la note explicative du CEREMA qui indique 2017) - France métropolitaine + DROM hors Mayotte',
        source: `<c-lien href="https://artificialisation.biodiversitetousvivants.fr/les-donnees-au-1er-janvier-2018" target="_blank"
                >CEREMA</c-lien>`,
        legende: 'CEREMA 2018',
        limites: `La méthodologie de calcul est expliquée
            <c-lien
                href="https://artificialisation.biodiversitetousvivants.fr/sites/artificialisation/files/inline-files/definition%20artificialisation%20FF%20V3.pdf"
                target="_blank"
                >ici</c-lien
            >. Des limites y sont présentées telles que le fait que les fichiers fonciers ne traitent que les parcelles cadastrées
            (les routes ne sont ainsi en principe pas prises en compte bien qu’en pratique, certains espaces théoriquement dans le
            domaine public static readonly, y compris de grande ampleur (périphériques, routes nationales, certaines autoroutes,etc.) sont encore
            cadastrés). De même l’impact des bâtiments public static readonlys, des terrains militaires et des golfs est mal pris en compte.`
    },

    logements_totaux_et_vacants: {
        id: 'logements_totaux_et_vacants',
        nom: 'Nombre de logements totaux et vacants',
        definition: `
            Nombre de logements
            <c-lien href="https://www.insee.fr/fr/metadonnees/definition/c1702" target="_blank">totaux</c-lien>
            et
            <c-lien href="https://www.insee.fr/fr/metadonnees/definition/c1059" target="_blank">vacants</c-lien>`,
        annees: '2013 et 2018',
        anneeLaPlusRecente: '2018',
        perimetre_geographique: 'Communes au 01/01/2021 - France métropolitaine + DROM hors Mayotte',
        source: `Observatoires des territoires,
        <c-lien href="https://www.observatoire-des-territoires.gouv.fr/nombre-de-logements" target="_blank">logements</c-lien>
        et
        <c-lien href="https://www.observatoire-des-territoires.gouv.fr/nombre-de-logements-vacants" target="_blank">
        logements vacants</c-lien>`,
        legende: 'Observatoire des territoires 2021'
    },

    quantites_substances_actives: {
        id: 'qsa',
        nom: 'Quantités de Substances Actives achetées',
        definition: `La quantité de substance active (QSA) achetée, pour une substance donnée, sur une année, par code postal (ou département) de l'acheteur. 
            Voir la definition détaillée dans la
            <c-lien
                href="https://agriculture.gouv.fr/telecharger/106547?token=90fe2c9e64650e3f9164076be113b1307327a103934e774ea1d818adacaf12ac"
                target="_blank"
                >méthodologie de calcul du <abbr title="NOmbre de Doses Unités">NODU</abbr></c-lien>.`,
        annees: '2015 à 2020',
        perimetre_geographique: 'Codes postaux ou départements France métropolitaine + DROM',
        source: `
            Les données sont produites par l'<c-lien href="https://www.ofb.gouv.fr/" target="_blank"
                >Office français de la biodiversité</c-lien
            >. Elles sont disponibles sur le site
            <c-lien href="https://www.data.gouv.fr/fr/datasets/achats-de-pesticides-par-code-postal/" target="_blank">data.gouv.fr</c-lien>.`,
        sourceCourt: `<c-lien href="https://www.data.gouv.fr/fr/datasets/achats-de-pesticides-par-code-postal/" target="_blank">BNVd 2021</c-lien>`,
        legende: 'OFB 2020',
        limites: `
        La
        <abbr title="Banque Nationale des Ventes de produits phytopharmaceutiques par les Distributeurs agréés">BNVD</abbr>
        renseigne sur les achats des produits phytosanitaires pour un code postal acheteur (ou département) et une annee donnée. 
        Les données par code postal acheteur ne reflètent ni le périmètre géographique réel de l'utilisation (produits utilisés sur un territoire différent que
        le code postal d’achat renseigné, qui est lié à la domiciliation de l’acheteur ; exemple des centrales d'achat, des
        exploitations étendues sur plusieurs communes ou EPCIs), ni l'année effective d'utilisation (achat puis stockage). Les
        données incluent également les achats de produits à usage non agricole (services de voiries des mairies – avant la loi Labbé – ou de la SNCF)
        qu'il conviendrait d'exclure (pour l'instant la qualité des données ne permet pas de facilement isoler ces usages).`
    },

    doses_unites_substances_actives: {
        id: 'du',
        nom: 'Doses unités des substances actives',
        definition: `
            La dose unité (DU) représente la dose homologuée pour une substance donnée. Voir la definition:  détaillée dans la
            <c-lien
                href="https://agriculture.gouv.fr/telecharger/106547?token=90fe2c9e64650e3f9164076be113b1307327a103934e774ea1d818adacaf12ac"
                target="_blank"
                >méthodologie de calcul du <abbr title="NOmbre de Doses Unités">NODU</abbr></c-lien>.`,
        annees: '2017 et 2019',
        perimetre_geographique: '',
        source: `
            <ul>
                <li>
                    <c-lien
                        href="https://info.agriculture.gouv.fr/gedei/site/bo-agri/document_administratif-fbafa36b-8218-40fc-a461-1b47df670dbe"
                        target="_blank"
                        >Arrêté du 27/04/2017</c-lien
                    >
                    définissant la méthodologie de calcul et la valeur des doses unité de référence des substances actives
                    phytopharmaceutiques
                </li>
                <li>
                    <c-lien
                        href="https://info.agriculture.gouv.fr/gedei/site/bo-agri/document_administratif-6e692406-4700-4623-81f9-77c929399b61"
                        target="_blank"
                        >Arrêté du 18/12/2019</c-lien
                    >
                    portant modification de l'arrêté 27 avril 2017 définissant la méthodologie de calcul et la valeur des doses
                    unités de référence des substances actives phytopharmaceutiques. Contient une mise à jour des valeurs de
                    <abbr title="Dose Unité">DU</abbr>
                    par rapport à l’arrêté de 2017, jusqu’à l’annee:  2018.
                </li>
            </ul>`,
        sourceCourt: `<c-lien href="https://info.agriculture.gouv.fr/gedei/site/bo-agri/document_administratif-fbafa36b-8218-40fc-a461-1b47df670dbe" target="_blank">2017</c-lien> 
        et <c-lien href="https://info.agriculture.gouv.fr/gedei/site/bo-agri/document_administratif-6e692406-4700-4623-81f9-77c929399b61" target="_blank">2019</c-lien>`,
        legende: 'MAA arrêtés 2017 et 2019',
        limites: `
            Même en combinant les années 2017 et 2019, des Doses Unités sont manquantes pour certaines substances actives.
            `
    },

    hvn: {
        id: 'hvn',
        nom: 'Indicateur HVN',
        definition: 'Indicateur Haute Valeur Naturelle de SOLAGRO',
        annees: '2017',
        perimetre_geographique: 'Communes au 1e janvier 2018 - France métropolitaine',
        source: `<c-lien href="https://solagro.org/nos-domaines-d-intervention/agroecologie/haute-valeur-naturelle" target="_blank">SOLAGRO</c-lien>`,
        legende: 'SOLAGRO 2017'
    },

    surfaces_agricoles_bio: {
        id: 'sau_bio',
        nom: 'Surfaces biologiques',
        definition: 'Surfaces agricoles labellisées AB ou en cours de conversion',
        annees: '2019',
        perimetre_geographique: 'Communes, EPCIs, Départements, Régions au 1e janvier 2020 - France métropolitaine',
        source: `<c-lien href="https://www.agencebio.org/vos-outils/les-chiffres-cles/" target="_blank">Agence Bio</c-lien>`,
        legende: 'Agence Bio 2019',
        limites: `
            Pour des raisons de confidentialité, les données ne sont pas disponibles pour les territoires pour lesquels il y a
            moins de 3 exploitants en bio.`
    },

    consommation_sau: {
        id: 'consommation_sau',
        nom: 'Surfaces agricoles nécessaires pour couvrir la consommation de la population',
        definition: 'Surfaces agricoles nécessaires pour couvrir la consommation de la population',
        annees: '2019',
        perimetre_geographique: 'Communes, EPCIs, Départements, Régions, France métropolitaine au 1e janvier 2019',
        source: `<c-lien href="https://parcel-app.org/" target="_blank">PARCEL</c-lien>`,
        legende: 'PARCEL 2019'
    },

    base_permanente_equipements: {
        id: 'bpe',
        nom: 'Base permanente des équipements',
        definition:
            "La base permanente des équipements (BPE) est une source statistique qui fournit le niveau d'équipements et de services rendus à la population sur un territoire.",
        annees: '2020',
        perimetre_geographique: 'France entière (base géolocalisée)',
        source: `<c-lien href="https://www.insee.fr/fr/statistiques/3568638?sommaire=3568656" target="_blank">INSEE</c-lien>`,
        legende: 'INSEE 2020'
    },

    commerces_osm: {
        id: 'commerces_osm',
        nom: 'Base des commerces',
        definition: 'Base de données ouverte et collaborative OpenStreetMap des commerces',
        annees: '2021',
        perimetre_geographique: 'France entière (base géolocalisée)',
        source: `<c-lien href="https://babel.opendatasoft.com/explore/dataset/osm-shop-fr/information/?disjunctive.type&disjunctive.region&disjunctive.departement&disjunctive.commune" target="_blank">OpenStreetMap</c-lien>`,
        legende: 'OSM 2021'
    },

    carroyage_insee: {
        id: 'carroyage_insee',
        nom: 'Données carroyées ',
        definition: "Informations socio-économiques sur l'ensemble de la France discrétisée en carreaux de 200 mètres de côté.",
        annees: '2015',
        perimetre_geographique: 'Carroyage INSEE 200m',
        source: `<c-lien href="https://www.insee.fr/fr/statistiques/4176290?sommaire=4176305" target="_blank">INSEE</c-lien>`,
        legende: 'Données carroyées INSEE 2015'
    },

    taux_pauvrete: {
        id: 'taux_pauvrete',
        nom: 'Taux de pauvreté (seuil à 60% du revenu médian)',
        definition:
            "Le taux de pauvreté correspond à la proportion d'individus appartenant à des ménages dont le niveau de vie (après transferts, impôts et prestations sociales) est inférieur au seuil de 60 % du niveau de vie médian de l'ensemble de la population.",
        annees: '2019',
        perimetre_geographique: 'Communes, EPCIs, Départements, Régions, France métropolitaine au 1e janvier 2020',
        source: `<c-lien href="https://www.insee.fr/fr/statistiques/6036907" target="_blank">INSEE</c-lien>`,
        legende: 'INSEE 2019'
    }
};
