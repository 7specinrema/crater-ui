import { fusionner } from '@lga/commun/build/outils/base';
import { ApexOptions } from 'apexcharts';
import ApexCharts from 'apexcharts';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

import { APEX_OPTIONS_GLOBALES } from './chart-config';
import { formaterLabelNombreEleve, getVariableCss, ModeFiltrageSeries, SerieMultiple } from './chart-outils';

export interface OptionsChartBarresHorizontal {
    series: SerieMultiple;
    nomAxeX: string;
    modeFiltrage?: ModeFiltrageSeries;
    empile?: boolean;
    minY?: number;
    maxY?: number;
    largeur?: number;
    masquerAxeY?: boolean;
    renverserAxeY?: boolean;
    couleur?: string[];
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-barres-horizontal': ChartBarresHorizontal;
    }
}

@customElement('c-chart-barres-horizontal')
export class ChartBarresHorizontal extends LitElement {
    static styles = css`
        figure {
            margin: auto;
            max-width: 600px;
        }
    `;

    private chart?: ApexCharts;

    @property({ attribute: false })
    options?: OptionsChartBarresHorizontal;

    @query('figure')
    private figure?: HTMLElement;

    render() {
        return html`
            <div>
                <figure></figure>
            </div>
        `;
    }

    async updated() {
        if (this.chart) {
            this.chart?.updateOptions(this.calculerApexOptions(this.options), true, true);
        } else {
            this.chart = new ApexCharts(this.figure, this.calculerApexOptions(this.options));
            await this.chart?.render();
            // Le premier render d'apex ne fonctionne pas dans lit (diagramme vide). Contournement : on attend la fin du render, et on fait un update
            this.chart?.updateOptions(this.calculerApexOptions(this.options), true, true);
        }
    }

    private calculerApexOptions(options: OptionsChartBarresHorizontal | undefined): ApexOptions {
        if (!options) return APEX_OPTIONS_GLOBALES;

        const apexOptionsLocales = {
            series: options.series
                .filtrerValeursNull(options.modeFiltrage ?? ModeFiltrageSeries.SUPPRIMER_SERIES_AVEC_AU_MOINS_UNE_VALEUR_NULL)
                .positionnerNomSiVide(options.nomAxeX)
                .formaterLabels(9)
                .versSerieApexXY(),
            chart: {
                type: 'bar',
                height: 300,
                width: options.largeur ?? 300
            },
            colors: options.couleur ?? [getVariableCss('--c-barre-serie-1')],
            plotOptions: {
                bar: {
                    horizontal: true
                }
            },
            xaxis: {
                title: {
                    text: options.nomAxeX
                },
                tickAmount: 3,
                labels: {
                    formatter: (labelInitial: string) => formaterLabelNombreEleve(labelInitial)
                }
            },
            yaxis: {
                show: !(options.masquerAxeY ?? false),
                reversed: options.renverserAxeY ?? false,
                labels: {
                    offsetX: -8,
                    offsetY: 8,
                    maxWidth: 120,
                    align: 'center'
                }
            },
            tooltip: {
                y: {
                    formatter: (labelInitial: string) => formaterLabelNombreEleve(labelInitial)
                }
            }
        };

        return fusionner(APEX_OPTIONS_GLOBALES, apexOptionsLocales);
    }
}
