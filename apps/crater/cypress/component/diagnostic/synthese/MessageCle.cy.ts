import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../../src/pages/diagnostic/synthese/MessageCle.js';

import { html } from 'lit';

import { OptionsMessageCle } from '../../../../src/pages/diagnostic/synthese/MessageCle';

describe('Test composant MessageCle', () => {
    const options: OptionsMessageCle = {
        titre: 'Titre message clé',
        sousTitre: '(Sous titre message clé)',
        note: 10,
        icone: html`<svg fill="#000000" width="800px" height="800px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <title>square</title>
            <path d="M1.25 1.25v29.5h29.5v-29.5zM29.25 29.25h-26.5v-26.5h26.5z"></path>
        </svg>`,
        message: html`Ceci est un message clé un peu long pour <strong>tester</strong> l'affichage sur plusieurs lignes. Ceci est un message clé un
            peu long pour <strong>tester</strong> l'affichage sur plusieurs lignes. Ceci est un message clé un peu long pour
            <strong>tester</strong> l'affichage sur plusieurs lignes`,
        href: 'http://idMaillon'
    };

    it.only('Affichage mode desktop', () => {
        cy.viewport(1000, 600);
        cy.mount<'c-message-cle'>(html` <c-message-cle .options=${options}></c-message-cle> `);
        cy.get('c-message-cle').invoke('width').should('be.lt', 900);
    });

    it('Affichage mode mobile', () => {
        cy.viewport(320, 600);
        cy.mount<'c-message-cle'>(html` <c-message-cle .options=${options}></c-message-cle> `);
        cy.get('c-message-cle').invoke('height').should('be.gt', 350);
    });
});
