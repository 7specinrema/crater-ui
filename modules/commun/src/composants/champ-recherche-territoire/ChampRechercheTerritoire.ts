import '@lga/design-system/build/composants/Lien.js';

import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system.js';
import { STYLES_BADGES } from '@lga/design-system/build/styles/styles-composants';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { EvenementReinitialiserTerritoire } from './EvenementReinitialiserTerritoire';
import { EvenementSelectionnerTerritoire } from './EvenementSelectionnerTerritoire';
import { chargerListeSuggestionsTerritoires } from './requete-api-suggestions-territoires';
import { SuggestionTerritoire } from './SuggestionTerritoire';

declare global {
    interface HTMLElementTagNameMap {
        'c-champ-recherche-territoire': ChampRechercheTerritoire;
    }
}

@customElement('c-champ-recherche-territoire')
export class ChampRechercheTerritoire extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_BADGES,
        css`
            :host {
                display: block;
            }

            main {
                position: relative;
            }

            section {
                position: relative;
                display: flex;
                align-items: center;
                justify-content: center;
                gap: var(--dsem);
                padding: calc(1.5 * var(--dsem));
                border-radius: var(--dsem);
                box-shadow: 0 0 0 1px var(--couleur-neutre-20) inset;
                background-color: var(--couleur-blanc);
            }
            div {
                display: flex;
            }
            #icone-loupe svg * {
                stroke: var(--couleur-primaire);
            }

            #icone-croix {
                cursor: pointer;
            }

            #icone-croix svg * {
                stroke: var(--couleur-neutre);
            }

            input {
                width: 100%;
                border: none;
            }
            input:focus {
                outline: none;
                box-shadow: none;
                border: none;
            }

            section:focus-within {
                box-shadow: 0 0 0 2px var(--couleur-primaire) inset;
            }

            menu {
                z-index: 1;
                position: absolute;
                border: 1px solid var(--couleur-neutre-40);
                border-radius: var(--dsem);
                margin: 8px 0 0;
                padding: 0 0;
                width: 100%;
                background-color: var(--couleur-blanc);
            }

            menu > article {
                cursor: pointer;
                margin: 0;
            }

            .boite-suggestion {
                padding: calc(0.5 * var(--dsem)) calc(1.5 * var(--dsem));
                text-align: left;
                display: flex;
                flex-direction: column;
                align-items: flex-start;
                justify-content: center;
            }

            menu > article:first-child {
                border-top-left-radius: var(--dsem);
                border-top-right-radius: var(--dsem);
            }
            menu > article:last-child {
                border-bottom-left-radius: var(--dsem);
                border-bottom-right-radius: var(--dsem);
            }
            menu > article:hover {
                background-color: var(--couleur-neutre-20);
            }
            .suggestion {
                display: block;
                color: var(--couleur-neutre);
                max-width: 100%;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }
            .suggestion mark {
                color: var(--couleur-primaire);
                font-weight: bold;
                background-color: transparent;
            }
            .selected {
                background-color: var(--couleur-neutre-clair);
            }

            p {
                padding: 0.1rem;
                text-align: center;
            }

            #message-erreur {
                background-color: var(--couleur-warning);
            }
        `
    ];

    @query('input[type=text]')
    private input?: HTMLInputElement;

    @query('menu')
    private elementListeSuggestions?: HTMLElement;

    private valeurChampInputDerniereRequete = '';

    @property()
    apiBaseUrl = '';

    @property()
    codesCategorieTerritoire?: string;

    @property()
    lienFormulaireDemandeAjoutTerritoire = '';

    @state()
    private idTerritoireSelectionne = '';

    @state()
    private idTerritoireParcelSelectionne = '';

    @state()
    private listeSuggestions: SuggestionTerritoire[] = [];

    @state()
    private indexItemSelectionne = 0;

    @state()
    private erreurReseau = false;

    render() {
        return html`
            <main class="texte-moyen">
                <section>
                    <div id="icone-loupe">${ICONES_DESIGN_SYSTEM.loupe}</div>
                    <input
                        type="text"
                        class="texte-moyen"
                        autofocus
                        autocomplete="off"
                        placeholder="Rechercher un territoire"
                        @keyup="${this.traiterSaisie}"
                    />
                    <div id="icone-croix" @click="${this.viderChampInput}">${ICONES_DESIGN_SYSTEM.croix}</div>
                </section>
                ${this.input?.value ? this.renderResultatRecherche() : ''}
            </main>
        `;
    }

    private renderResultatRecherche() {
        if (this.idTerritoireSelectionne) {
            return html``;
        } else if (this.erreurReseau) {
            return html` <menu id="message-erreur">
                <p><strong>Problème technique, merci de réessayer dans quelques instants.</strong></p>
            </menu>`;
        } else if (this.listeSuggestions.length > 0) {
            return html` <menu> ${this.listeSuggestions.map((s, i) => this.renderLigneSuggestion(i, s))} </menu>`;
        } else if (this.input!.value.length >= 3) {
            return html` <menu id="message-territoire-inconnu">
                <p><strong>Ce territoire n’est pas encore référencé.</strong></p>
                <p>
                    Demandez l’ajout de votre territoire via ce
                    <c-lien href=${this.lienFormulaireDemandeAjoutTerritoire}>formulaire</c-lien>.
                </p>
            </menu>`;
        } else return;
    }

    private renderLigneSuggestion(i: number, s: SuggestionTerritoire) {
        return html` <article @click="${() => this.clickSelectionTerritoire(i)}" class=${this.indexItemSelectionne === i ? 'selected' : ''}>
            <div class="boite-suggestion">
                ${s.estSuggere
                    ? html`<div class="suggestion">${this.calculerLibelleHtml(s.libelleSecondaire, this.input!.value)}</div>
                          <div class="suggestion texte-petit">${this.calculerLibelleHtml(s.libellePrincipal, this.input!.value)}</div>
                          <div class="badge-accent texte-petit">Suggéré</div>`
                    : html`<div class="suggestion">${this.calculerLibelleHtml(s.libellePrincipal, this.input!.value)}</div>
                          <div class="suggestion texte-petit">${this.calculerLibelleHtml(s.libelleSecondaire, this.input!.value)}</div>`}
            </div>
        </article>`;
    }

    private traiterSaisie(event: KeyboardEvent) {
        switch (event.key) {
            case 'Enter':
                this.actionSelectionnerTerritoire();
                break;
            case 'ArrowDown':
                this.indexItemSelectionne = this.indexItemSelectionne === this.listeSuggestions.length - 1 ? 0 : this.indexItemSelectionne + 1;
                break;
            case 'ArrowUp':
                this.indexItemSelectionne = this.indexItemSelectionne === 0 ? this.listeSuggestions.length - 1 : this.indexItemSelectionne - 1;
                break;
            case 'Escape':
                this.viderChampInput();
                break;
            default:
                this.afficherListeSuggestions();
        }
    }

    private afficherListeSuggestions() {
        if (this.champInputModifie()) {
            this.idTerritoireSelectionne = '';
            this.idTerritoireParcelSelectionne = '';
            this.collecterContenuListeSuggestion(this.input!.value);
            this.valeurChampInputDerniereRequete = this.input!.value;
            this.inscrireListenerMasquerListeSuggestion();
        }
    }

    private collecterContenuListeSuggestion = (texteSaisi: string) => {
        if (this.codesCategorieTerritoire) texteSaisi += ` ${this.codesCategorieTerritoire}`;
        return chargerListeSuggestionsTerritoires(this.apiBaseUrl, texteSaisi)
            .then((listeSuggestionsTerritoires: SuggestionTerritoire[]) => {
                this.erreurReseau = false;
                this.listeSuggestions = listeSuggestionsTerritoires;
            })
            .catch(() => {
                this.erreurReseau = true;
            });
    };

    private champInputModifie() {
        return this.valeurChampInputDerniereRequete !== this.input!.value;
    }

    private actionSelectionnerTerritoire() {
        if (this.listeSuggestions.length > 0) {
            this.idTerritoireSelectionne = this.listeSuggestions[this.indexItemSelectionne].id;
            this.idTerritoireParcelSelectionne = this.listeSuggestions[this.indexItemSelectionne].idParcel;
            this.input!.value = this.listeSuggestions[this.indexItemSelectionne].nom;
            this.indexItemSelectionne = 0;
            this.masquerListeSuggestion();
        }
        if (this.idTerritoireSelectionne !== '') {
            this.emettreEvtSelectionTerritoire(this.idTerritoireSelectionne, this.idTerritoireParcelSelectionne);
        }
    }

    private clickSelectionTerritoire = (index: number) => {
        this.indexItemSelectionne = index;
        this.actionSelectionnerTerritoire();
    };

    private calculerLibelleHtml = (libelle: string, contenuChampInput: string) => {
        return html` ${unsafeHTML(libelle.replace(' ', '&nbsp;').replace(new RegExp('(' + contenuChampInput + ')', 'i'), '<mark>$1</mark>'))}`;
    };

    private masquerListeSuggestion() {
        this.listeSuggestions = [];
        this.supprimerListenerMasquerListeSuggestion();
    }

    private viderChampInput = () => {
        this.idTerritoireSelectionne = '';
        this.idTerritoireParcelSelectionne = '';
        this.input!.focus();
        this.input!.value = '';
        this.masquerListeSuggestion();
        this.emettreEvtReinitialiser();
    };

    private emettreEvtReinitialiser() {
        this.dispatchEvent(new EvenementReinitialiserTerritoire());
    }

    private emettreEvtSelectionTerritoire(idTerritoire: string, idTerritoireParcel: string) {
        this.input?.blur();
        this.dispatchEvent(new EvenementSelectionnerTerritoire(idTerritoire, idTerritoireParcel));
    }

    // arrow function car utilisée comme callback dans un listener
    private gererClickExterieur = (event: Event) => {
        const estTypeClicEtExterieurMenu: boolean =
            event.type === 'click' && this.elementListeSuggestions !== undefined && !event.composedPath().includes(this);

        if (estTypeClicEtExterieurMenu) {
            this.viderChampInput();
        }
    };

    private inscrireListenerMasquerListeSuggestion() {
        window.addEventListener('click', this.gererClickExterieur, { capture: true });
    }

    private supprimerListenerMasquerListeSuggestion() {
        window.removeEventListener('click', this.gererClickExterieur, { capture: true });
    }
}
