import { FeatureCollection } from 'geojson';

export type ValeurIndicateurParIdTerritoire = {
    idTerritoire: string;
    valeur: number | null;
};

export class TerritoiresVisiblesCarte {
    public contoursGeojson?: FeatureCollection;
    private valeursIndicateurs: ValeurIndicateurParIdTerritoire[] = [];

    ajouterContours(contoursGeojson: FeatureCollection) {
        this.contoursGeojson ? this.contoursGeojson.features.push(...contoursGeojson.features) : (this.contoursGeojson = contoursGeojson);
    }

    ajouterValeursIndicateurs(valeursIndicateurs: ValeurIndicateurParIdTerritoire[]) {
        this.valeursIndicateurs.push(...valeursIndicateurs);
    }

    getValeursIndicateur(idTerritoire: string): number | null {
        return this.valeursIndicateurs.find((i) => i.idTerritoire === idTerritoire)?.valeur ?? null;
    }

    reset() {
        this.contoursGeojson = undefined;
        this.valeursIndicateurs = [];
    }
}
