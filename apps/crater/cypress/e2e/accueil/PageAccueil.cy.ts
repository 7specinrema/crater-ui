describe('Tests end2end page accueil  ', () => {
    beforeEach(() => {
        cy.intercept('GET', 'territoires?critere=*', { fixture: 'listeSuggestionOccitanie.json' });
        cy.intercept('GET', 'territoires/occitanie', { fixture: 'occitanieDetail.json' });
        cy.intercept('GET', 'diagnostics/occitanie', { fixture: 'diagnosticOccitanie.json' });
        cy.intercept('GET', 'diagnostics/france', { fixture: 'diagnosticFrance.json' });
    });

    it('afficher la liste des suggestions à partir du Occi', () => {
        cy.visit('/');
        cy.get('input[type="text"]').type('Occi');
        cy.get('c-champ-recherche-territoire').within(() => {
            cy.get('menu').children().should('have.length', 2);
        });
    });

    it('la récupération du détail du territoire Occitanie ', () => {
        cy.visit('/');
        cy.get('input[type="text"]').type('occitanie{enter}');
        cy.verifierAffichagePage('Diagnostic du système alimentaire');
    });

    it('afficher page accueil et tester le contenu de h1', () => {
        cy.viewport(1366, 768);
        cy.visit('/');
        cy.verifierAffichagePage('Mon territoire peut-il garantir une alimentation saine et durable à ses habitants ?');
        cy.window().scrollTo('right');
        cy.window().its('scrollX').should('be.equal', 0);
    });

    it('test en mode mobile pour vérifier le lien vers page projet et affichée un h1 unique et égale à une valeur', () => {
        cy.viewport(320, 500);
        cy.visit('/');
        cy.get('c-description-crater').find('a[href="/projet"]').click();
        cy.verifierAffichagePage('Le projet CRATer');
    });

    it('test en mode mobile pour vérifier le lien vers page aide et affichée un h1 unique et égale à une valeur', () => {
        cy.viewport(320, 500);
        cy.visit('/');
        cy.get('c-encart-lien-vers-page').find('a[href="/aide/comprendre-le-systeme-alimentaire/le-systeme-actuel"]').click();
        cy.verifierAffichagePage('Un système fait pour produire, pas pour nourrir');
    });

    it('test en mode mobile pour vérifier le lien vers page aide et affichée un h1 unique et égale à une valeur', () => {
        cy.viewport(320, 500);
        cy.visit('/');
        cy.get('c-encart-lien-vers-page').find('a[href="/aide/comprendre-le-systeme-alimentaire/defaillances-et-vulnerabilites"]').click();
        cy.verifierAffichagePage('Nuages à l’horizon');
    });

    it('test en mode mobile pour vérifier le lien vers page aide et affichée un h1 unique et égale à une valeur', () => {
        cy.viewport(320, 500);
        cy.visit('/');
        cy.get('c-encart-lien-vers-page').find('a[href="/aide/comprendre-le-systeme-alimentaire/la-transition-agricole-et-alimentaire"]').click();
        cy.verifierAffichagePage('Changement de cap !');
    });

    it('test en mode mobile pour vérifier le lien vers page carte et affichée un h1 unique et égale à une valeur', () => {
        cy.viewport(320, 500);
        cy.visit('/');
        cy.get('c-encart-lien-vers-page').find('a[href="/carte/pesticides-intensite-usage/epci"]').click();
        cy.get('c-carte').should('exist');
    });

    it('test en mode mobile pour vérifier le lien  entete vers projet', () => {
        cy.viewport(320, 500);
        cy.visit('/');
        cy.get('c-entete').find('c-panneau-glissant').click().find('a[href="/projet"]').click({ force: true });
        cy.url().should('include', '/projet');
    });

    it('test en mode mobile pour vérifier le lien entete vers contact', () => {
        cy.viewport(320, 500);
        cy.visit('/');
        cy.get('c-entete').find('c-panneau-glissant').click().find('c-bouton[href="/contact"]').click();
        cy.url().should('include', '/contact');
        cy.verifierAffichagePage('Formulaire de contact');
    });
});
