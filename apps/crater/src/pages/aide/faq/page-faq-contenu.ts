import '@lga/design-system/build/composants/Lien.js';

import { html, TemplateResult } from 'lit';

export interface SectionPageFAQ {
    id: string;
    libelle: string;
    definition: TemplateResult;
}

export const PAGE_FAQ_CONTENU: SectionPageFAQ[] = [
    {
        id: 'difference-avec-parcel',
        libelle: 'Quelle est la différence entre CRATer et l’outil PARCEL ?',
        definition: html`CRATer a pour objectif de donner un diagnostic – ou état des lieux – du système alimentaire du territoire étudié de façon à
            montrer ses forces et faiblesses ; lorsque PARCEL permet de comprendre quels sont les effets associés à une relocalisation de la
            production agricole, un changement de régimes des consommateurs, une augmentation de la part de surfaces agricoles en bio et une réduction
            du gaspillage sur les surfaces et emplois à mobiliser et sur l’environnement. Les deux outils sont donc complémentaires ! Nous sommes
            travaillons d’ailleurs à une meilleure articulation entre les deux, notamment au travers d’un site internet commun :
            <c-lien href="https://transition-alimentaire.fr/">transition-alimentaire.fr </c-lien> `
    },
    {
        id: 'pourquoi-les-drom',
        libelle: 'Pourquoi les DROM ne sont-il pas présents ? Avez-vous prévu de les intégrer ?',
        definition: html`Les DROM ne sont pas présents car certaines données comme les besoins en surfaces agricoles issues de PARCEL et l’indicateur
        HVN de SOLAGRO ne couvrent pas ces territoires. Il est prévu de les intégrer en deux temps : d’abord en proposant un diagnostic, incomplet,
        basé sur les données disponibles ; ensuite en venant compléter ce diagnostic à l’aide d’autres sources de données.`
    },
    {
        id: 'certains-indicateurs-biais',
        libelle: 'Certains indicateurs ont des biais ou ne s’appliquent pas à tous les contextes, comment traitez-vous ce problème ?',
        definition: html`Nous sommes conscients que les indicateurs calculés ne sont sûrement pas adaptés à tous les territoires. Cependant, nous
        avons fait le choix d’appliquer les mêmes calculs à tous les territoires par souci d’homogénéité et de cohérence. Ainsi, comme expliqué dans
        notre méthodologie, le diagnostic proposé est à considérer comme un pré-diagnostic qui ne se substitue pas à un diagnostic terrain plus
        poussé. Cela étant, nous sommes preneurs de vos remarques pour améliorer notre méthodologie afin de la rendre toujours plus robuste ou pour
        nous signaler quels avertissements nous devrions ajouter !`
    },
    {
        id: 'pourquoi-des-notes',
        libelle: 'Pourquoi avoir utilisé des notes pour évaluer les composantes du système alimentaire ?',
        definition: html`Nous avons longuement hésité avant de décider d’utiliser des notes pour évaluer les composantes du système alimentaire.
        Conscients des limites d’utiliser un système de notation, nous avons cependant trouvé que c’était le moyen le plus simple et le plus
        intelligible pour présenter de manière synthétique les éléments apportés par les différents indicateurs. Nous nous sommes en revanche interdit
        de présenter une note globale – moyenne des différentes notes – car la résilience alimentaire d’un territoire ne peut se résumer en une seule
        phrase mais est au contraire à aborder sur toutes ses facettes.`
    },
    {
        id: 'certaines-donnees-2010',
        libelle: 'Certaines données datent de 2010 : pourquoi ne sont-elles pas plus récentes ?',
        definition: html`A priori, pour quasiment chaque source de données, le jeu le plus récent a été utilisé. Une mise à jour régulière est prévue
        au fur et à mesure. Cela étant, il est possible que nous n’ayons pas eu le temps, ou bien que nous n’ayons pas encore repéré une mise à jour.
        Dans ce cas-là, n’hésitez pas à nous le signaler en nous contactant !`
    },
    {
        id: 'donnees-calcul-actualiser',
        libelle: 'Les données ou modes de calcul sont-ils faciles à actualiser ?',
        definition: html`Pour les données, le travail est relativement simple si le format des données n’a pas changé entre deux publications et ne
        demande donc pas d’ajustement du code informatique. Pour les modes de calcul, cela dépend de l’actualisation à faire : s’il s’agit d’un
        changement à la marge (changement de la valeur d’un paramètre, changement de la formule de calcul en se basant sur les mêmes données...) cela
        est relativement facile ; si le changement est plus profond (prise en compte d’une nouvelle donnée, changement radical du mode de calcul...),
        cela peut demander des efforts plus importants.`
    }
];
