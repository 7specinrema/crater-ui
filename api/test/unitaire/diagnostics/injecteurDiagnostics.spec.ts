import { ServicesDiagnostics } from '../../../src/diagnostics/domaines/ServicesDiagnostics';
import { InjecteursDiagnostics } from '../../../src/diagnostics/InjecteursDiagnostics';

const CHEMIN_DONNEES_TESTS_UNITAIRES_INPUT = 'test/unitaire/data/input';

describe('Test unitaire injecteurs diagnostic', () => {
    let servicesDiagnostics: ServicesDiagnostics;
    let injecteursDiagnostics: InjecteursDiagnostics;

    beforeEach(() => {
        servicesDiagnostics = new ServicesDiagnostics();
        injecteursDiagnostics = new InjecteursDiagnostics(servicesDiagnostics);
    });

    it('Charger tout les diagnostics et vérifier le taux de consommation', async () => {
        await injecteursDiagnostics.injecterDonneesDiagnostics(CHEMIN_DONNEES_TESTS_UNITAIRES_INPUT);
        // then
        expect(servicesDiagnostics.rechercherTous().length).toEqual(14);
        let diagnostic = servicesDiagnostics.rechercherOuCreerDiagnostic('ile-de-france', 'Île-de-France', 'REGION');
        expect(diagnostic.consommation).toEqual({
            tauxPauvrete60Pourcent: 15.5
        });
    });

    it('Charger tout les diagnostics et vérifier le nb communes otex ', async () => {
        await injecteursDiagnostics.injecterDonneesDiagnostics(CHEMIN_DONNEES_TESTS_UNITAIRES_INPUT);
        // then
        expect(servicesDiagnostics.rechercherTous().length).toEqual(14);
        let diagnostic = servicesDiagnostics.rechercherOuCreerDiagnostic('ile-de-france', 'Île-de-France', 'REGION');
        expect(diagnostic.nbCommunesParOtex).toEqual({
            grandesCultures: 761,
            maraichageHorticulture: 58,
            viticulture: 3,
            fruits: 10,
            bovinLait: 3,
            bovinViande: 1,
            bovinMixte: 0,
            ovinsCaprinsAutresHerbivores: 12,
            porcinsVolailles: 3,
            polyculturePolyelevage: 157,
            nonClassees: 1,
            sansExploitations: 259,
            nonRenseigne: 0
        });
    });
});
