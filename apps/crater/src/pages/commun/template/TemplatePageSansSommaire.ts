import '../MenuPiedPage.js';
import './TemplatePage.js';

import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { STYLES_CRATER } from '../pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-template-page-sans-sommaire': TemplatePageSansSommaire;
    }
}

@customElement('c-template-page-sans-sommaire')
export class TemplatePageSansSommaire extends LitElement {
    static styles = [
        STYLES_CRATER,
        css`
            main {
                padding-top: calc(2 * var(--dsem));
                padding-left: max((100vw - var(--largeur-maximum-contenu)) / 2, calc(2 * var(--dsem)));
                padding-right: max((100vw - var(--largeur-maximum-contenu)) / 2, calc(2 * var(--dsem)));
            }
        `
    ];

    @property()
    idItemActifMenuPrincipal = '';

    @property()
    idElementCibleScroll = '';

    render() {
        return html`
            <c-template-page
                idItemActifMenuPrincipal="${this.idItemActifMenuPrincipal}"
                idElementCibleScroll=${this.idElementCibleScroll}
                desactiverBoutonSommaire
            >
                <main class="page" slot="contenu">
                    <slot name="contenu"></slot>
                </main>
            </c-template-page>
        `;
    }
}
