import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

export interface IntervalleLegende {
    couleur: string;
    libelleBorneInf: string;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-legende-carte': LegendeCarte;
    }
}

@customElement('c-legende-carte')
export class LegendeCarte extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                --largeur-barre: 200px;
            }

            .barre-coloree {
                display: flex;
                justify-content: left;
                align-items: end;
                height: 50px;
                padding-bottom: 10px;
            }

            .rond {
                box-sizing: content-box;
                width: 5px;
                height: 5px;
                background-color: #2c3e50;
                border-radius: 50%;
                border: solid 3px var(--couleur-blanc);
            }

            .rond.debut {
                transform: translate(5px, 3.5px);
            }

            .rond.fin {
                transform: translate(-5px, 3.5px);
            }

            .delimitation {
                box-sizing: content-box;
                width: 1px;
                height: 5px;
                background-color: #2c3e50;
            }

            .trait {
                width: 50px;
                height: 3px;
            }

            span {
                position: absolute;
                transform: translate(-50%, -25px);
                color: var(--couleur-neutre);
            }
        `
    ];

    @property({ attribute: false })
    intervalles?: IntervalleLegende[];

    render() {
        if (!this.intervalles) return ``;
        if (this.intervalles.length == 0) return '';
        else
            return html` <div class="barre-coloree texte-petit">
                <div class="rond debut" style="background-color: ${this.intervalles[0].couleur}"></div>
                ${this.intervalles.slice(0, -1).map(
                    (i, index) => html`
                        <div class="${index !== 100 ? 'delimitation' : ''}">
                            <span>${i.libelleBorneInf}</span>
                        </div>
                        <div
                            class="trait"
                            style="
                            background-image: linear-gradient(0.25turn, ${i.couleur}, ${this.intervalles![index + 1].couleur});
                            width: calc(var(--largeur-barre) / ${this.intervalles!.length - 1});"
                        ></div>
                    `
                )}
                <div class="rond fin" style="background-color: ${this.intervalles.slice(-1)[0].couleur}">
                    <span>${this.intervalles.slice(-1)[0].libelleBorneInf}</span>
                </div>
            </div>`;
    }
}
