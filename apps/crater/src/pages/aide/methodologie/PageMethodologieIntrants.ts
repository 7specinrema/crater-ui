import '../../commun/template/TemplatePageAvecSommaire.js';
import '@lga/design-system/build/composants/Lien.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_METHODOLOGIE, PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';
import { IDS_INDICATEURS } from '../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { construireLiensFilArianePagesMethodologie, ITEMS_MENU_METHODOLOGIE, STYLES_METHODOLOGIE } from './methodologie-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-methodologie-intrants': PageMethodologieIntrants;
    }
}

@customElement('c-page-methodologie-intrants')
export class PageMethodologieIntrants extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_METHODOLOGIE];

    render() {
        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${PAGES_METHODOLOGIE.intrants.getId()}"
                .itemsMenuSommaire="${ITEMS_MENU_METHODOLOGIE}"
                .liensFilAriane="${construireLiensFilArianePagesMethodologie(
                    PAGES_METHODOLOGIE.intrants.getTitreCourt(),
                    PAGES_METHODOLOGIE.intrants.getUrl()
                )}"
                idElementCibleScroll=${this.idElementCibleScroll}
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    @property()
    idElementCibleScroll = '';

    private renderCoeurPage() {
        return html`
            <section class="texte-moyen">
                <h1>Règles de gestion pour le maillon Intrants</h1>

                <h2>
                    Les indicateurs d'usage des pesticides : <abbr title="Quantité de Substances Actives">QSA</abbr>,
                    <abbr title="NOmbre de Doses Unités">NODU</abbr> et <abbr title="NOmbre de Doses Unités">NODU</abbr>
                    normalisé
                </h2>

                <p>
                    Note : pour de plus amples détails sur la méthodologie utilisée, vous pouvez consulter
                    <c-lien href="public/pdf/Methodologie_indicateurs_produits_phytosanitaires_v2.1.pdf">
                        le document d'élaboration de cet méthodologie</c-lien
                    >.
                </p>

                <h2>Pourquoi des indicateurs sur l’utilisation des pesticides ?</h2>
                <p>
                    Symboles de l’industrialisation des pratiques, les pesticides sont devenus indispensables à l’agriculture en France aujourd’hui.
                    D’un point de vue agronomique, la grande homogénéité des systèmes agricoles et les faibles niveaux de biodiversité sauvage
                    favorisent les « bioagresseurs » (insectes, champignons pathogènes, etc.) et limitent leur régulation naturelle. D’un point de vue
                    économique, la concurrence internationale, le faible prix des marchandises agricoles et la non prise en compte des coûts
                    sanitaires et environnementaux incitent mécaniquement les exploitations à maximiser les volumes de production à l’aide des
                    pesticides. Si les pesticides, en tant qu’outils à disposition des agriculteurs pour faire face à la propagation brutale d’un
                    bioagresseur, pourraient être perçus comme un facteur de résilience, la dépendance structurelle de notre modèle agricole à leur
                    utilisation est au contraire source de vulnérabilité. Leur usage massif et systématique entretient un cercle vicieux qui les rend
                    toujours plus indispensables : les bioagresseurs résistants sont naturellement sélectionnés et la toxicité des substances dégrade
                    les fonctions de régulation remplies par les espèces sauvages. De plus, leur fabrication dépend de ressources fossiles en voie
                    d’épuisement et est contrôlée par une poignée de multinationales en situation d’oligopole, plongeant les agriculteurs dans une
                    situation de dépendance risquée au vu de l’augmentation des tensions économiques et politiques à venir.
                </p>

                <h2 id="${IDS_INDICATEURS.qsaNodu}">Quantité de Substances Actives (QSA)</h2>
                <p>La Quantité de Substances Actives correspond à la quantité totale, en kg, de substances actives.</p>

                <p>
                    Les substances actives contenues dans les pesticides vendus sont classées selon leur niveau de toxicité. Avant 2015 la
                    classification était la suivante :
                </p>
                <ul>
                    <li>T, T+, CMR : substance toxique, très toxique, cancérogène mutagène reprotoxique</li>
                    <li>N minéral : substance minérale dangereuse pour l’environnement</li>
                    <li>N organique : substance organique dangereuse pour l’environnement</li>
                    <li>Autre : autre substance</li>
                </ul>
                <p>
                    A partir de 2015 : les substances classées CMR 2 (supposés) sont intégrées dans la catégorie « T, T+, CMR » alors qu’elles étaient
                    auparavant catégorisées «N organique »
                </p>
                <p>A partir de 2019 la nomenclature de la classification est modifiée :</p>
                <ul>
                    <li>CMR: substance toxique, très toxique, cancérogène mutagène reprotoxique</li>
                    <li>
                        Santé A: toxicité aiguë de catégorie 1, 2 ou 3 ou toxicité spécifique pour certains organes cibles, de catégorie 1, à la suite
                        d’une exposition unique ou après une exposition répétée, soit en raison de leurs effets sur ou via l’allaitement
                    </li>
                    <li>
                        Env A : toxicité aiguë pour le milieu aquatique de catégorie 1 ou toxicité chronique pour le milieu aquatique de catégorie 1
                        ou 2
                    </li>
                    <li>Env B : toxicité chronique pour le milieu aquatique de catégorie 3 ou 4</li>
                    <li>Autre : autre substance</li>
                </ul>

                <h3>Dose Unité (DU)</h3>
                <p>
                    La <abbr title="Dose Unité">DU</abbr>, en kg/ha, correspond à la dose maximale applicable sur un hectare. Elle reflète en ce sens
                    la puissance des substances actives.
                </p>
                <p>Remarque : certaines substances n'ont pas de <abbr title="Dose Unité">DU</abbr>.</p>

                <h3>Nombre de Doses Unités (NODU)</h3>
                <p>
                    Le <abbr title="NOmbre de Doses Unités">NODU</abbr>, en hectares, est calculé pour chaque subtsance active en faisant le ratio de
                    la <abbr title="Quantité de Substances Actives">QSA</abbr> avec la <abbr title="Dose Unité">DU</abbr> de la substance active. Le
                    calcul du <abbr title="NOmbre de Doses Unités">NODU</abbr> permet donc de comparer et d'additionner des substances actives qui
                    n'ont pas le même impact à quantité égale utilisée.
                </p>
                <p>
                    <strong
                        >NB : les substances sans <abbr title="Dose Unité">DU</abbr> n'ont de facto pas de
                        <abbr title="NOmbre de Doses Unités">NODU</abbr>.</strong
                    >
                </p>

                <h2 id="${IDS_INDICATEURS.noduNormalise}">Nombre de Doses Unités normalisé (NODU normalisé)</h2>
                <p>
                    Le <abbr title="NOmbre de Doses Unités">NODU</abbr> normalisé, grandeur adimensionnelle, est calculé en faisant le ratio entre le
                    <abbr title="NOmbre de Doses Unités">NODU</abbr> et la surface agricole utile (SAU) totale du territoire. Il peut s'interpréter
                    comme le nombre moyen de traitements par pesticides que reçoivent les terres agricoles du territoire, en tenant compte de la
                    toxicité des produits employés. Il permet de faire des comparaisons entre territoires et/ou périodes temporelles différent(e)s.
                </p>
                <p>
                    <strong
                        >NB : cet indicateur étant calculé à partir du <abbr title="NOmbre de Doses Unités">NODU</abbr> présenté plus haut, il ne
                        tient compte que de certaines substances actives. De plus, étant calculé sur base de la SAU totale, il peut cacher des
                        sur-dosages locaux.</strong
                    >
                </p>

                <h3>Données d'entrée</h3>

                <p>
                    Les indicateurs utilisent la base de donnée
                    <abbr title="Banque Nationale des Ventes de produits phytopharmaceutiques par les Distributeurs agréés">BNVD</abbr>
                    de l’Office Français de la Biodiversité (voir
                    <c-lien href=${PAGES_METHODOLOGIE.sourcesDonnees.getUrl()}>sources de données</c-lien>) donnant les achats de produits
                    phytosanitaires (et les substances actives qu'ils contiennent) par code postal de l'acheteur ; les doses unités issus des arrêtés
                    ministériels (voir <c-lien href=${PAGES_METHODOLOGIE.sourcesDonnees.getUrl()}>sources de données</c-lien>) ; et les surfaces
                    agricoles utiles totales de l'Agreste issues du recensement agricole 2020 (voir
                    <c-lien href=${PAGES_METHODOLOGIE.sourcesDonnees.getUrl()}>sources de données</c-lien>)
                </p>

                <h3>Méthode de calcul</h3>
                <p>Les étapes du calcul sont les suivantes :</p>
                <blockquote>
                    <ol>
                        <li>
                            import des doses unitaires : utilisation des <abbr title="Dose Unité">DU</abbr> décrites dans l’arrêté de 2019, complétées
                            par celles décrites dans l’arrêté de 2017 et non présentes dans celui de 2019.
                        </li>
                        <li>
                            import des <abbr title="Quantité de Substances Actives">QSA</abbr> par code postal acheteur et département sur l’ensemble
                            des années considérées (2015 à 2019)
                            <strong>en incluant toutes les substances sauf celles appartenant à la classification <i>Autre</i>.</strong>
                        </li>
                        <li>
                            calcul des <abbr title="NOmbre de Doses Unités">NODU</abbr> par substance via la formule
                            <abbr title="NOmbre de Doses Unités">NODU</abbr> = <abbr title="Quantité de Substances Actives">QSA</abbr> /
                            <abbr title="Dose Unité">DU</abbr>.
                            <strong>Il n’est donc disponible que pour les substances disposant d’une <abbr title="Dose Unité">DU</abbr>.</strong>
                        </li>
                        <li>
                            calcul du QSA_avec_DU, QSA_sans_DU et <abbr title="NOmbre de Doses Unités">NODU</abbr> total (toutes substances confondues
                            sauf classées <i>Autre</i>) :
                            <ol>
                                <li>
                                    au niveau des communes : la répartition depuis le code postal vers les communes est faite au prorata de la SAU de
                                    chaque commune (cas des codes postaux couvrants plusieurs communes) ;
                                </li>
                                <li>
                                    au niveau des EPCI et des regroupements de communes (ex: PAT) : le calcul est fait par somme des résultats obtenus
                                    au niveau des communes ;
                                </li>
                                <li>au niveau des départements : les résultats sont directement importés des données départementales ;</li>
                                <li>
                                    au niveau des régions et de la France métropolitaine : le calcul est fait par somme des résultats obtenus au
                                    niveau des départements.
                                </li>
                            </ol>
                        </li>
                        <li>
                            calcul du <abbr title="NOmbre de Doses Unités normalisé">NODU_normalisé</abbr> pour chaque année et chaque territoire
                            (communs, EPCI, regroupements de communes, département, région, pays), via la formule :
                            <abbr title="NOmbre de Doses Unités normalisé">NODU_normalisé</abbr> = <abbr title="NOmbre de Doses Unités">NODU</abbr> /
                            SAU.
                        </li>
                        <li>
                            calcul du QSA_avec_DU, QSA_sans_DU, du <abbr title="NOmbre de Doses Unités">NODU</abbr> et du
                            <abbr title="NOmbre de Doses Unités normalisé">NODU_normalisé</abbr> de l’année n par moyenne sur les années n, n-1 et
                            n-2. On obtient donc des valeurs pour les années 2017, 2018, 2019 et 2020.
                        </li>
                        <li>suppression des données à l’échelle de la commune, utilisées uniquement pour les calculs.</li>
                    </ol>
                </blockquote>

                <p>Les résulats sont donc donnés :</p>
                <ul>
                    <li>pour les EPCI, autres regroupements de communes (PAT, PNR..), départements, régions, France entière ; le tout hors DROM.</li>
                    <li>
                        en moyenne triennale pour les années 2017, 2018, 2019 et 2020 (valeur année n = moyenne des années n, n-1 et n-2; e.g. 2019
                        correspond à la moyenne 2017, 2018 et 2019) ;
                    </li>
                    <li>toutes substances non classées <i>Autre</i> confondues.</li>
                </ul>

                <h3>Limites</h3>
                <p>
                    La <abbr title="Banque Nationale des Ventes de produits phytopharmaceutiques par les Distributeurs agréés">BNVD</abbr>
                    renseigne sur les achats de produits phytosanitaires. Les produits peuvent donc être utilisés ailleurs (que le code postal d’achat
                    renseigné, qui est lié à la domiciliation de l’acheteur). Les calculs sont donnés à l’échelle de l’EPCI et non du code postal ou
                    de la commune pour limiter ce problème (notamment celui des exploitations qui sont sur plusieurs communes). Cela ne corrige
                    cependant pas les cas suivants :
                </p>

                <ul>
                    <li>exploitation sur plusieurs EPCI (dont les achats sont attribués au siège de l’exploitation) ;</li>
                    <li>achats de produits de traitement des récoltes avant entreposage (produits non épandus dans les champs) ;</li>
                    <li>
                        achats de produits à usage non agricole (services de voiries des mairies ou de la SNCF). L’usage des produits donné dans la
                        base ANSES n’a pas pu être utilisé pour isoler ces cas pour plusieurs raisons : 1. l’absence de données d’usage pour de
                        nombreux produits ; 2. l’usage mixte de nombreux produits (eg TOUCHDOWN FORET contenant du glyphosate utilisé par la SNCF :
                        sur 22 usages, seuls 2 ont pour cible les voies ferrées) avec très peu de produits utilisé.
                    </li>
                </ul>
                <p>Les produits peuvent également être utilisés après l'année d'achat (stockage). La moyenne triennale permet de lisser cet effet</p>
                <p>Limitation sur le périmètre des substances actives prises en compte :</p>
                <ul>
                    <li>
                        les substances actives provenant de produits importés (semences, fourrages, aliments...) et qui finissent in fine dans
                        l’environnement sont absents.
                    </li>
                    <li>
                        Les arrêtés ne sont pas exhaustifs : les <abbr title="Dose Unité">DU</abbr> de certaines substances actives sont inconnues.
                        Aussi, ils référencent les substances via leur nom sans leur numéro CAS . Si l’orthographe est différente de celle utilisée
                        dans la <abbr title="Banque Nationale des Ventes de produits phytopharmaceutiques par les Distributeurs agréés">BNVD</abbr>,
                        la correspondance ne peut pas être réalisée hormis via un travail manuel chronophage. L’utilisation de la base de données des
                        substances actives de l’ANSES (e-phy) qui contient pour chaque substance, un numéro CAS et des variants de noms ne peut être
                        utilisée car pour certaines substances, les variants proposés ne correspondent parfois pas à la même molécule mais à une
                        molécule de la même famille (e.g. glyphosate associé aux variants glyphosate sel monosodium | glyphosate sel de diméthylamine
                        | glyphosate sel d'ammonium [...]) qui n’a peut être pas forcément la même <abbr title="Dose Unité">DU</abbr>.
                    </li>
                    <li>
                        pour pallier à ces limites, le choix a été fait de se limiter aux substances non classées dans Autre, ce dernier étant le
                        groupe pour lequel il manque le plus de <abbr title="Dose Unité">DU</abbr> . Une partie des achats n’est donc pas prise en
                        compte, ce qui a pour conséquence de sous-estimer les valeurs de <abbr title="NOmbre de Doses Unités">NODU</abbr>.
                    </li>
                </ul>
                <p>
                    Limitation sur la mesure de la SAU : La SAU utilisée inclut les prairies permanentes qui sont pourtant les surfaces sur lesquelles
                    sont peu épandus de pesticides. Cependant, aucune donnée fiable et complète n’est disponible : le recensement de 2010 ne donne pas
                    les valeurs pour toutes les communes du fait du secret statistique ; le RPG sous-estime les surfaces agricoles en général,
                    notamment les zones viticoles fortes consommatrices de pesticides.
                </p>
                <p>
                    Le choix a été fait d’utiliser la SAU totale issue du recensement agricole 2020 qui est exhaustive pour les communes. Quand les
                    données plus détaillées du RA 2020 seront publiée, il sera possible de retirer la SAU toujours en herbe pour les communes non
                    soumises au secret statistique.
                </p>

                <h2 id="${IDS_INDICATEURS.noteIntrants}">Évaluation globale du maillon Intrants</h2>
                <p>
                    La note et le message de synthèse sont basés sur l'indicateur i "Nombre de Doses Unités normalisé". La note est calculée de la
                    façon suivante :
                </p>
                <blockquote>N = max(0, 10 * (1 - i))</blockquote>
            </section>
        `;
    }
}
