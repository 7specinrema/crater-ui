import { GroupeCulture } from '../../systeme-alimentaire';

export class SurfaceAgricoleUtile {
    constructor(
        public readonly sauTotaleHa: number,
        public readonly sauPeuProductiveHa: number,
        public readonly sauProductiveHa: number,
        public readonly sauBioHa: number,
        public readonly listeSauParGroupeCulture: SauParGroupeCulture[]
    ) {}

    public get pourcentageSauProductive() {
        return (this.sauProductiveHa / this.sauTotaleHa) * 100;
    }

    public get pourcentageSauPeuProductive() {
        return (this.sauPeuProductiveHa / this.sauTotaleHa) * 100;
    }

    public get sauParGroupeCultureHorsSNC(): SauParGroupeCulture[] {
        return this.listeSauParGroupeCulture
            .filter((sau) => GroupeCulture.codes.includes(sau.codeGroupeCulture))
            .sort((sau1, sau2) => GroupeCulture.fromString(sau1.codeGroupeCulture).comparer(GroupeCulture.fromString(sau2.codeGroupeCulture)));
    }
}

export class SauParGroupeCulture {
    constructor(
        public readonly codeGroupeCulture: string,
        public readonly nomGroupeCulture: string,
        public readonly sauHa: number,
        public readonly sauParCulture: SauParCulture[]
    ) {}

    get groupeCulture(): GroupeCulture {
        return GroupeCulture.fromString(this.codeGroupeCulture);
    }
}

export class SauParCulture {
    constructor(public readonly codeCulture: string, public readonly nomCulture: string, public readonly sauHa: number) {}
}
