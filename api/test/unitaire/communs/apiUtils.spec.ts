import { extrairePremiereValeurSelonFiltre, extraireSousObjet, extraireValeurDepuisSousObjet, jsonToCsv } from '../../../src/communs/apiUtils';

describe('Test conversion json vers csv', () => {
    it('Convertir objet ou tableau vide', () => {
        expect(jsonToCsv({})).toEqual([]);
        expect(jsonToCsv([])).toEqual([]);
    });

    it('Convertir objet simple', () => {
        const json = {
            territoire: 'Lyon',
            population: 10
        };
        const csv = ['territoire;Lyon', 'population;10'];
        expect(jsonToCsv(json)).toEqual(csv);
    });

    it('Convertir objet avec objets imbriqués', () => {
        const json = {
            territoire: 'Lyon',
            sau: {
                sauTotale: 3,
                detail: {
                    sauProductive: 1,
                    sauBio: 2
                }
            }
        };
        const csv = ['territoire;Lyon', 'sau.sauTotale;3', 'sau.detail.sauProductive;1', 'sau.detail.sauBio;2'];
        expect(jsonToCsv(json)).toEqual(csv);
    });

    it("Convertir objet avec tableaux d'objets", () => {
        const json = {
            territoire: 'Lyon',
            indicateurs: [
                { note: 1, message: 'message1' },
                { note: 2, message: 'message2' }
            ]
        };
        const csv = ['territoire;Lyon', 'indicateurs.note;1', 'indicateurs.message;message1', 'indicateurs.note;2', 'indicateurs.message;message2'];
        expect(jsonToCsv(json)).toEqual(csv);
    });

    it('Convertir objet avec tableau de valeurs', () => {
        const json = {
            territoire: 'Lyon',
            indicateurs: [{ annee: [2019, 2020], note: [null, 3], message: ['message1', 'message2'] }]
        };
        const csv = ['territoire;Lyon', 'indicateurs.annee;2019;2020', 'indicateurs.note;;3', 'indicateurs.message;message1;message2'];
        expect(jsonToCsv(json)).toEqual(csv);
    });
});

describe('Test extraction valeur depuis objet ou tableau', () => {
    const objet = {
        territoire: 'Lyon',
        population: 10,
        indicateurs: [
            { note: 1, typeCommerce: 'typeCommerce1', annee: 2019 },
            { note: 2, typeCommerce: 'typeCommerce2', annee: 2020 },
            { note: 3, typeCommerce: 'typeCommerce3', annee: 2020 }
        ],
        sau: {
            sauTotale: 3,
            detail: {
                sauProductive: 1,
                sauBio: 2
            }
        }
    };
    it('Extraire depuis un objet vide retourne vide quelquesoit la clé', () => {
        expect(extraireValeurDepuisSousObjet({}, 'attributInconnu')).toEqual({});
    });

    it('Extraire champ simple au premier niveau', () => {
        expect(extraireValeurDepuisSousObjet(objet, 'population')).toEqual(10);
        expect(extraireValeurDepuisSousObjet(objet, 'territoire')).toEqual('Lyon');
    });

    it('Extraire champ sous objet au premier niveau', () => {
        expect(extraireValeurDepuisSousObjet(objet, 'sau')).toEqual(objet.sau);
    });
    it('Extraire champ sous objet au 2eme niveau', () => {
        expect(extraireValeurDepuisSousObjet(objet, 'sau.detail')).toEqual(objet.sau.detail);
    });
    it('Extraire champ de type tableau en retournant tout le tableau', () => {
        expect(extraireValeurDepuisSousObjet(objet, 'indicateurs')).toEqual(objet.indicateurs);
    });
    it('Extraire champ inconnu retourne un objet vide', () => {
        expect(extraireValeurDepuisSousObjet(objet, 'attributInconnu')).toEqual({});
        expect(extraireValeurDepuisSousObjet(objet, 'sau.detail.attributInconnu')).toEqual({});
    });
    it('Extraire une valeur depuis un tableau avec un filtre sur annee', () => {
        expect(extraireValeurDepuisSousObjet(objet, 'indicateurs.note', '2020')).toEqual(2);
    });
    it('Extraire une valeur depuis un tableau avec champ a extraire inconnu retourne null', () => {
        expect(extraireValeurDepuisSousObjet(objet, 'indicateurs.inconnu', '')).toEqual(null);
    });
    it('Extraire une valeur depuis un tableau sans filtre retourne une liste de valeurs', () => {
        expect(extraireValeurDepuisSousObjet(objet, 'indicateurs.note', '')).toEqual([1, 2, 3]);
    });
});
