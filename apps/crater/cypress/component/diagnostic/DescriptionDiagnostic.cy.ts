import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/composants/DescriptionDiagnostic.js';

import { html } from 'lit';

describe('Test DescriptionDiagnostic', () => {
    it('Test affichage desktop', () => {
        // given
        cy.viewport(1100, 500);
        cy.mount<'c-description-diagnostic'>(html` <c-description-diagnostic></c-description-diagnostic> `);
        // then
        cy.get('c-description-diagnostic').should('exist');
        cy.get('c-description-diagnostic').find('li').should('exist');
    });
    it('Test affichage mobile', () => {
        // given
        cy.viewport(350, 500);
        cy.mount<'c-description-diagnostic'>(html` <c-description-diagnostic></c-description-diagnostic> `);
        // then
        cy.get('c-description-diagnostic').should('exist');
        cy.get('c-description-diagnostic').find('li').should('exist');
        cy.get('c-description-diagnostic').find('main').should('have.css', 'flex-direction', 'column');
    });
});
