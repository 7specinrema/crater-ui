module.exports = {
    roots: ['<rootDir>/test'],
    testMatch: ['**/integration/**/?(*.)+(spec|test).+(ts|tsx|js)'],
    transform: {
        '^.+\\.(ts|tsx)$': 'ts-jest'
    }
};
