import './composants/widget-carte/WidgetCarte.js';
import './composants/ListeChoixIndicateursCarte.js';
import './composants/ContenuPanneauSommaireCarte.js';
import './composants/widget-carte/LegendeCarte.js';
import '../commun/template/TemplatePageAvecMenuTerritoire.js';
import '@lga/design-system/build/composants/Lien.js';
import './composants/MenuGlissantMobileDescriptionIndicateurActifCarte';
import './composants/MenuGlissantDesktopChangerIndicateurCarte';
import '../commun/Sablier.js';

import { EvenementSelectionnerTerritoire } from '@lga/commun/build/composants/champ-recherche-territoire/EvenementSelectionnerTerritoire';
import { OptionsRadioBouton } from '@lga/design-system/build/composants/RadioBouton';
import { EvenementSelectionner } from '@lga/design-system/build/evenements/EvenementSelectionner';
import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { DomaineCarte, EtatCarte } from '../../domaines/carte/DomaineCarte';
import { EchelleTerritoriale } from '../../domaines/carte/EchelleTerritoriale';
import { PAGES_PRINCIPALES } from '../../domaines/pages/configuration/declaration-pages';
import { DonneesPageCarte, majDonneesPage } from '../../domaines/pages/donnees-pages.js';
import { construireUrlPageMethodologie } from '../../domaines/pages/pages-utils';
import { SA } from '../../domaines/systeme-alimentaire/configuration/systeme-alimentaire';
import { EvenementErreur } from '../../evenements/EvenementErreur';
import { EvenementMajDonneesPage } from '../../evenements/EvenementMajDonneesPage';
import { EvenementSelectionnerCategorieTerritoire } from '../../evenements/EvenementSelectionnerCategorieTerritoire';
import { OptionsBarreMenuTerritoires } from '../commun/BarreMenuTerritoires.js';
import { STYLES_CRATER } from '../commun/pages-styles';
import { EvenementDeplacerCarte } from './composants/widget-carte/EvenementDeplacerCarte';
import { IntervalleLegende } from './composants/widget-carte/LegendeCarte';
import { WidgetCarte } from './composants/widget-carte/WidgetCarte.js';
import { ControleurCarte } from './ControleurCarte';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-carte': PageCarte;
    }
}

@customElement('c-page-carte')
export class PageCarte extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                background-color: var(--couleur-blanc);
                display: grid;
                grid-template-columns: 40% 60%;
                height: calc(100vh - var(--hauteur-totale-entete));
            }

            #section-choix-indicateur {
                background-color: var(--couleur-blanc);
                border-right: 1px solid var(--couleur-neutre-clair);
            }

            #description-indicateur-actif {
                padding: calc(2 * var(--dsem));
                background-color: var(--couleur-blanc);
            }

            #description-indicateur-actif h3 {
                margin-top: 0;
            }

            #titre-indicateur-actif {
                display: flex;
                align-items: center;
                justify-content: space-between;
            }

            h2 {
                margin: 0;
            }

            #section-carte {
                position: relative;
                height: 100%;
                z-index: 0;
            }

            #bandeau-haut-carte {
                position: absolute;
                top: 0;
                z-index: 501;
                width: 100%;
                margin: auto;
                padding: calc(2 * var(--dsem));
                text-align: center;
                background-color: rgba(243, 245, 246, 0.9);
            }

            #titre-carte h1 {
                margin: 0;
            }

            #indication-titre-carte {
                margin-bottom: 0;
            }

            #texte-indicateur-actif {
                padding-top: calc(2 * var(--dsem));
            }

            #nom-indicateur-actif {
                padding-right: var(--dsem);
            }

            c-menu-glissant-mobile-description-indicateur-actif-carte {
                display: none;
            }

            #legende-et-source-carte {
                position: absolute;
                bottom: 0;
                left: 0;
                z-index: 500;
                display: flex;
                justify-content: center;
                vertical-align: baseline;
                gap: calc(5 * var(--dsem));
                background-color: rgba(243, 245, 246, 0.8);
                width: 100%;
                min-height: 80px;
                padding: var(--dsem) var(--dsem) 0 var(--dsem);
            }

            #source-carte {
                margin: auto;
                text-align: center;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                main {
                    display: block;
                    height: calc(100vh - var(--hauteur-totale-entete) - var(--hauteur-menu-pied-page));
                }

                #section-choix-indicateur {
                    display: none;
                }

                #source-carte {
                    display: none;
                }

                #titre-carte {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    gap: var(--dsem);
                }

                #indication-titre-carte {
                    display: none;
                }

                c-menu-glissant-mobile-description-indicateur-actif-carte {
                    display: block;
                }
            }
        `
    ];

    private controleurCarte = new ControleurCarte();

    private domaineCarte = new DomaineCarte();

    @property({ attribute: false })
    donneesPage: DonneesPageCarte = {
        idIndicateur: SA.indicateursCarte[0].id
    };

    @query('c-carte') private carte?: WidgetCarte;

    willUpdate() {
        this.verifierDonneesPage();
        if (this.estNecessaireMajCarte(this.domaineCarte, this.donneesPage)) {
            this.domaineCarte.setEtatVide();
            this.domaineCarte.majIndicateurActif(this.donneesPage.idIndicateur);
            if (this.donneesPage.idTerritoirePrincipal) {
                this.controleurCarte
                    .chargerTerritoirePrincipal(this.donneesPage.idTerritoirePrincipal, this.domaineCarte)
                    .then((domaineCarte) => {
                        this.domaineCarte = domaineCarte;
                        this.domaineCarte.setEtatPret();
                        this.donneesPage = majDonneesPage(this.donneesPage, {
                            nomIndicateur: this.domaineCarte.getIndicateurActif().libelle,
                            idEchelleTerritoriale: undefined,
                            libelleEchelleTerritoriale: this.domaineCarte.getEchelleTerritorialeActive().libelle
                        });
                        this.dispatchEvent(new EvenementMajDonneesPage(this.donneesPage));
                    })
                    .catch((e: Error) => {
                        this.actionErreur('CARTE-01', 'ERREUR lors du chargement de la carte', e);
                    });
            } else {
                // param echelleTerritoriale pris en compte uniquement si pas de territoire dans l'url
                this.domaineCarte.majEchelleTerritorialeActive(this.donneesPage.idEchelleTerritoriale!);
                this.domaineCarte.setEtatPret();
            }
        }
    }

    render() {
        if (this.domaineCarte.getEtat() === EtatCarte.PRET) {
            const indicateurActif = this.domaineCarte.getIndicateurActif();

            return html`
                <c-template-page-avec-menu-territoire
                    idItemActifMenuPrincipal=${PAGES_PRINCIPALES.carte.getId()}
                    .optionsBarreMenuTerritoires=${this.calculerOptionsBarreMenuTerritoires()}
                    desactiverPiedPage
                    @selectionnerCategorieTerritoire=${this.actionModifierEchelleTerritorialeCarte}
                    @selectionnerTerritoire=${this.actionNouveauTerritoire}
                >
                    <main slot="contenu">
                        <section id="section-choix-indicateur">
                            <div id="description-indicateur-actif">
                                <div id="titre-indicateur-actif">
                                    <h2 id="nom-indicateur-actif" class="titre-petit">
                                        Carte des ${this.domaineCarte.getEchelleTerritorialeActive().libelle}<br />${indicateurActif?.libelle}
                                    </h2>
                                    <c-menu-glissant-desktop-changer-indicateur-carte
                                        .idIndicateurActif=${this.domaineCarte.getIndicateurActif().id}
                                        @selectionner=${this.actionModifierIndicateurActif}
                                    ></c-menu-glissant-desktop-changer-indicateur-carte>
                                </div>
                                <div id="texte-indicateur-actif">
                                    <p class="texte-moyen">${unsafeHTML(indicateurActif?.description)}</p>
                                    <c-lien
                                        class="texte-moyen"
                                        href=${construireUrlPageMethodologie(indicateurActif?.maillon.id, indicateurActif?.id)}
                                        >→ voir la méthodologie</c-lien
                                    >
                                </div>
                            </div>
                        </section>
                        <section id="section-carte">
                            <div id="bandeau-haut-carte">
                                <div id="titre-carte">
                                    <h1 class="texte-titre">
                                        ${this.domaineCarte.getIndicateurActif().libelle +
                                        (this.domaineCarte.getIndicateurActif().unite
                                            ? ' [' + this.domaineCarte.getIndicateurActif().unite + ']'
                                            : '')}
                                    </h1>
                                    <c-menu-glissant-mobile-description-indicateur-actif-carte
                                        idIndicateurActif=${this.domaineCarte.getIndicateurActif().id}
                                    ></c-menu-glissant-mobile-description-indicateur-actif-carte>
                                </div>
                                <p id="indication-titre-carte" class="texte-petit">Cliquez sur un territoire pour aller sur son diagnostic</p>
                            </div>
                            <c-carte
                                autoriserZoom
                                autoriserMouvements
                                @deplacerCarte=${(e: Event) => this.actionMouvementCarte((<EvenementDeplacerCarte>e).detail.zoom)}
                            ></c-carte>
                            <div id="legende-et-source-carte">
                                <c-legende-carte
                                    .intervalles=${this.domaineCarte.getIndicateurActif().style.seuilsLegende.map((i) => {
                                        return <IntervalleLegende>{ couleur: i.couleur, libelleBorneInf: i.libelleBorneInf };
                                    })}
                                ></c-legende-carte>
                                <h3 id="source-carte" class="texte-petit">
                                    ${this.domaineCarte.getIndicateurActif().legende
                                        ? unsafeHTML(this.domaineCarte.getIndicateurActif().legende)
                                        : html`Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>`}
                                </h3>
                            </div>
                        </section>
                    </main>
                    <c-contenu-panneau-sommaire-carte
                        slot="contenu-sommaire"
                        .idIndicateurActif=${this.domaineCarte.getIndicateurActif().id}
                        .optionsBoutonsTerritoires=${{
                            boutons: EchelleTerritoriale.listerToutes.map((echelle: EchelleTerritoriale) => ({
                                id: echelle.id,
                                libelle: echelle.libelle
                            })),
                            idBoutonActif: this.domaineCarte.getEchelleTerritorialeActive().id
                        } as OptionsRadioBouton}
                        @selectionner=${this.actionModifierIndicateurActif}
                        @selectionnerCategorieTerritoire=${this.actionModifierEchelleTerritorialeCarte}
                        @selectionnerTerritoire=${this.actionNouveauTerritoire}
                    ></c-contenu-panneau-sommaire-carte>
                </c-template-page-avec-menu-territoire>
            `;
        } else {
            return html`<c-sablier></c-sablier>`;
        }
    }

    private calculerOptionsBarreMenuTerritoires() {
        return <OptionsBarreMenuTerritoires>{
            onglets: EchelleTerritoriale.listerToutes.map((echelle: EchelleTerritoriale) => ({
                id: echelle.id,
                libelle: echelle.libelle
            })),
            idOngletSelectionne: this.domaineCarte.getEchelleTerritorialeActive().id,
            filtrerRecherchePourCarte: true
        };
    }

    updated() {
        if (this.domaineCarte.getEtat() === EtatCarte.PRET) {
            this.carte?.maj(
                this.domaineCarte.getEchelleTerritorialeActive(),
                this.domaineCarte.getIndicateurActif().id,
                this.domaineCarte.getIndicateurActif().unite,
                this.domaineCarte.getIndicateurActif().nomIndicateurRequeteApi,
                this.domaineCarte.getIndicateurActif().style,
                this.domaineCarte.getIdTerritoireZoom(),
                this.domaineCarte.getIdDepartementInitialCommune()
            );
            this.domaineCarte.desactiverZoomAutoTerritoire();
        }
    }

    private actionModifierIndicateurActif = (e: Event) => {
        const idIndicateur = (<EvenementSelectionner>e).detail.idItemSelectionne;
        this.donneesPage = majDonneesPage(this.donneesPage, {
            idIndicateur: idIndicateur,
            nomIndicateur: this.domaineCarte.getIndicateurActif().libelle,
            idEchelleTerritoriale: this.domaineCarte.getEchelleTerritorialeActive().id,
            libelleEchelleTerritoriale: this.domaineCarte.getEchelleTerritorialeActive().libelle,
            idTerritoirePrincipal: undefined
        });
        this.dispatchEvent(new EvenementMajDonneesPage(this.donneesPage));
    };

    private actionModifierEchelleTerritorialeCarte = (e: Event) => {
        const idEchelleTerritoriale = (<EvenementSelectionnerCategorieTerritoire>e).detail.codeCategorieTerritoire;
        this.modifierEchelleTerritoriale(idEchelleTerritoriale);
    };

    private modifierEchelleTerritoriale(idEchelleTerritoriale: string) {
        this.donneesPage = majDonneesPage(this.donneesPage, {
            idEchelleTerritoriale: idEchelleTerritoriale,
            libelleEchelleTerritoriale: this.domaineCarte.getEchelleTerritorialeActive().libelle,
            idTerritoirePrincipal: undefined
        });
        this.dispatchEvent(new EvenementMajDonneesPage(this.donneesPage));
    }

    private actionMouvementCarte(zoom: number) {
        if (this.domaineCarte.getEchelleTerritorialeActive().id === EchelleTerritoriale.Communes.id) {
            if (zoom < EchelleTerritoriale.Communes.minZoom) {
                this.modifierEchelleTerritoriale(EchelleTerritoriale.Epcis.id);
            }
        }
    }

    private actionNouveauTerritoire = (e: Event) => {
        this.donneesPage = majDonneesPage(this.donneesPage, {
            idTerritoirePrincipal: (<EvenementSelectionnerTerritoire>e).detail.idTerritoireCrater
        });
        // Pas de dispatchEvent car il est fait dans willUpdate
    };

    private actionErreur = (codeErreur: string, messageErreur: string, erreur?: Error) => {
        console.warn(`Erreur : ${codeErreur} ${messageErreur}`, erreur);
        this.dispatchEvent(new EvenementErreur(codeErreur, messageErreur));
    };

    private estNecessaireMajCarte(domaineCarte: DomaineCarte, donneesPage: DonneesPageCarte): boolean {
        const estModifieIdIndicateur = donneesPage.idIndicateur !== domaineCarte.getIndicateurActif().id;

        const estModifieIdTerritoirePrincipal =
            donneesPage.idTerritoirePrincipal !== undefined && donneesPage.idTerritoirePrincipal !== domaineCarte.getIdTerritoirePrincipal();

        const estModifieeEchelleTerritoriale =
            donneesPage.idTerritoirePrincipal === undefined && donneesPage.idEchelleTerritoriale !== domaineCarte.getEchelleTerritorialeActive().id;

        return (
            domaineCarte.getEtat() === EtatCarte.VIDE ||
            (domaineCarte.getEtat() === EtatCarte.PRET &&
                (estModifieIdIndicateur || estModifieIdTerritoirePrincipal || estModifieeEchelleTerritoriale))
        );
    }

    private verifierDonneesPage() {
        if (!SA.getIndicateurCarte(this.donneesPage.idIndicateur)) {
            this.dispatchEvent(
                new EvenementErreur('CARTE-01', `Erreur dans l'adresse saisie, le nom de l'indicateur est absent ou inconnu: ${location.toString()}`)
            );
        } else if (
            this.donneesPage.idEchelleTerritoriale !== undefined &&
            EchelleTerritoriale.fromId(this.donneesPage.idEchelleTerritoriale) === undefined
        ) {
            this.dispatchEvent(
                new EvenementErreur(
                    'CARTE-02',
                    `Erreur dans l'adresse saisie, la valeur du paramètre echelleTerritoriale est incorrecte : ${location.toString()}`
                )
            );
        }
    }
}
