import { css, unsafeCSS } from 'lit';

import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from './styles-breakpoints';

export const STYLES_DESIGN_SYSTEM = css`
    *,
    *:before,
    *:after {
        box-sizing: border-box;
    }

    .texte-petit {
        font-family: var(--font-family-texte-gras);
        font-size: 12px;
        line-height: 20px;
        letter-spacing: 0.02em;
    }

    .texte-petit-majuscule {
        font-family: var(--font-family-texte-gras);
        font-size: 12px;
        line-height: 20px;
        letter-spacing: 0.02em;
        text-transform: uppercase;
    }

    .texte-moyen,
    p,
    li {
        font-family: var(--font-family-texte);
        font-size: 16px;
        line-height: 24px;
        letter-spacing: 0.02em;
    }

    .texte-titre,
    h6 {
        font-family: var(--font-family-texte-gras);
        font-size: 16px;
        line-height: 24px;
        letter-spacing: 0.02em;
    }

    .texte-alternatif {
        font-family: var(--font-family-texte-gras);
        text-transform: uppercase;
        font-size: 16px;
        line-height: 24px;
        letter-spacing: 0.02em;
    }

    .titre-petit,
    h3 {
        font-family: var(--font-family-titre);
        font-size: 16px;
        line-height: 24px;
        letter-spacing: 0.04em;
    }

    .titre-moyen,
    h2 {
        font-family: var(--font-family-titre);
        font-size: 24px;
        line-height: 32px;
        letter-spacing: 0.04em;
    }

    .titre-large,
    h1 {
        font-family: var(--font-family-titre);
        font-size: 32px;
        line-height: 40px;
        letter-spacing: 0.04em;
    }

    @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
        .titre-moyen,
        h2 {
            font-size: 16px;
            line-height: 24px;
        }

        .titre-large,
        h1 {
            font-size: 20px;
            line-height: 28px;
        }
    }
`;
