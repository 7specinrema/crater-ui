const { resolve } = require('path');
const { defineConfig } = require('vite');

module.exports = defineConfig({
    root: resolve(__dirname, 'src'),
    envDir: resolve(__dirname, 'config'),
    build: {
        outDir: resolve(__dirname, 'build'),
        target: 'es2019',
        rollupOptions: {
            input: {
                transitionalimentaire: resolve(__dirname, 'src/index.html'),
                mentionslegales: resolve(__dirname, 'src/mentions-legales.html')
            },
            output: {
                // preserveModule: true,
                manualChunks: {
                    lit: ['lit']
                }
            }
        }
    }
});
