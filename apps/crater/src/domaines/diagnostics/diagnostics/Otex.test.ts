import { sommeArray } from '@lga/commun/src/outils/base';
import { describe, expect, it } from 'vitest';

import { Otex } from './Otex';

describe('Tests de la classe Otex', () => {
    it('Calculer proprietés otex', () => {
        const otex = new Otex({
            grandesCultures: 1,
            maraichageHorticulture: 2,
            viticulture: 0,
            fruits: 0,
            bovinLait: 0,
            bovinViande: 0,
            bovinMixte: 0,
            ovinsCaprinsAutresHerbivores: 0,
            porcinsVolailles: 0,
            polyculturePolyelevage: 0,
            nonClassees: 0,
            sansExploitations: 0,
            nonRenseigne: 0
        });

        expect(sommeArray(otex.nbCommunesOtex12Postes.map((e) => e.valeur))!).toEqual(3);
        expect(otex.nbCommunesOtexMajoritaire5Postes.otex.libelle).toEqual('Maraîchage Horticulture, Viticulture ou Fruits');
    });
});
