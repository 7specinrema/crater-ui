import '../../src/composants/Boite.js';
import '../../public/theme-defaut/theme-defaut.css';

import chaiColors from 'chai-colors';
import { html } from 'lit';
import { Boite } from '../../src/composants/Boite.js';

chai.use(chaiColors);

describe('Test composant Boite', () => {
    it('Arrondi ', () => {
        cy.mount<'c-boite'>(html` <c-boite id="boite-petit" arrondi="${Boite.ARRONDI.petit}"> <p>Boite avec de l'arrondi petit</p></c-boite> `);
        cy.mount('<br>');
        cy.mount<'c-boite'>(html` <c-boite id="boite-moyen" arrondi="${Boite.ARRONDI.moyen}"> <p>Boite avec de l'arrondi moyen</p></c-boite> `);
        cy.mount('<br>');
        cy.mount<'c-boite'>(html`
            <c-boite id="boite-large" arrondi="${Boite.ARRONDI.large}">
                <p>Boite avec de l'arrondi large</p>
            </c-boite>
        `);

        cy.get('#boite-petit').find('slot').should('exist');
        cy.get('#boite-petit').find('div').should('have.css', 'border-radius', '4px');
        cy.get('#boite-moyen').find('slot').should('exist');
        cy.get('#boite-moyen').find('div').should('have.css', 'border-radius', '8px');
        cy.get('#boite-large').find('slot').should('exist');
        cy.get('#boite-large').find('div').should('have.css', 'border-radius', '12px');
    });
    it('Encadrement de la boite', () => {
        cy.mount<'c-boite'>(html`
            <c-boite id="boite-encadree" encadrement> <p>C'est une boite encadrée</p></c-boite>
            <br />
            <c-boite id="boite-nonencadree"> <p>C'est une boite sans cadre</p></c-boite>
        `);
        //test css + test element fils
        cy.get('#boite-encadree').find('div').should('have.css', 'border-style', 'solid');
        cy.get('#boite-encadree').find('slot').should('exist');
        cy.get('#boite-nonencadree').find('div').should('have.css', 'border-style', 'none');
        cy.get('#boite-nonencadree').find('slot').should('exist');
    });
    it('Ombre de la boite', () => {
        cy.mount<'c-boite'>(html`
            <c-boite id="boite-ombree" ombre>
                <p>Boite avec de l'ombre</p>
            </c-boite>
            <br />
            <c-boite id="boite-nonombree">
                <p>Boite sans ombre</p>
            </c-boite>
        `);
        cy.get('#boite-nonombree').find('slot').should('exist');
        cy.get('#boite-ombree').find('p').should('exist');

        cy.get('#boite-nonombree').find('slot').should('exist');
        cy.get('#boite-nonombree').find('p').should('exist');
    });
});
