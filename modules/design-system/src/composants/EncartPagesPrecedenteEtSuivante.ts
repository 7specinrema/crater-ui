import './Bouton.js';

import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { ICONES_DESIGN_SYSTEM } from '../styles/icones-design-system.js';
import { Bouton } from './Bouton.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-encart-pages-precedente-suivante': EncartPagesPrecedenteEtSuivante;
    }
}

@customElement('c-encart-pages-precedente-suivante')
export class EncartPagesPrecedenteEtSuivante extends LitElement {
    static styles? = [
        css`
            :host {
                display: block;
                width: 100%;
                border-top: var(--couleur-neutre-20, gray) solid 2px;
                margin-top: calc(6 * var(--dsem));
                margin-bottom: 7rem;
            }

            .bouton-droit {
                float: right;
            }

            .bouton-gauche {
                float: left;
            }
        `
    ];

    @property()
    libellePagePrecedente?: string;

    @property()
    libellePageSuivante?: string;

    @property()
    hrefPagePrecedente?: string;

    @property()
    hrefPageSuivante?: string;

    render() {
        return html`
            ${this.hrefPagePrecedente
                ? html` <c-bouton
                      class="bouton-gauche"
                      libelle=${ifDefined(this.libellePagePrecedente)}
                      libelleCourt="Précédent"
                      href=${this.hrefPagePrecedente}
                      positionIcone=${Bouton.POSITION_ICONE.gauche}
                      type=${Bouton.TYPE.plat}
                      themeCouleur=${Bouton.THEME_COULEUR.neutre}
                  >
                      ${ICONES_DESIGN_SYSTEM.flecheGauche}
                  </c-bouton>`
                : ''}
            ${this.hrefPageSuivante
                ? html`<c-bouton
                      class="bouton-droit"
                      libelle=${ifDefined(this.libellePageSuivante)}
                      libelleCourt="Suivant"
                      href=${this.hrefPageSuivante}
                      positionIcone=${Bouton.POSITION_ICONE.droite}
                      type=${Bouton.TYPE.plat}
                      themeCouleur=${Bouton.THEME_COULEUR.neutre}
                  >
                      ${ICONES_DESIGN_SYSTEM.flecheDroite}
                  </c-bouton>`
                : ''}
        `;
    }
}
