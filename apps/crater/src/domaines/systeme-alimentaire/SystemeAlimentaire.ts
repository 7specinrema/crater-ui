import { Indicateur, IndicateurCarte } from './Indicateur';
import { Maillon } from './Maillon';
import { OutilExterne } from './OutilExterne';
import { VoieResilience } from './VoieResilience';

export class SystemeAlimentaire {
    public voiesResilience: VoieResilience[] = [];
    public outilsExternes: OutilExterne[] = [];
    public maillons: Maillon[] = [];
    public indicateurs: Indicateur[] = [];

    public getVoieResilience(id: string): VoieResilience | undefined {
        return this.voiesResilience.find((l) => l.id === id);
    }

    public getOutilExterne(id: string): OutilExterne | undefined {
        return this.outilsExternes.find((l) => l.id === id);
    }

    public getMaillon(id: string): Maillon | undefined {
        return this.maillons.find((m) => m.id === id);
    }

    public getIndicateur(id: string): Indicateur | undefined {
        return this.indicateurs.find((i) => i.id === id);
    }

    get indicateursCarte(): IndicateurCarte[] {
        return this.indicateurs.filter((i) => i.indicateurCarte).map((i) => i.indicateurCarte!);
    }

    public getIndicateurCarte(id: string): IndicateurCarte | undefined {
        return this.indicateursCarte.find((i) => i.id === id);
    }
}
