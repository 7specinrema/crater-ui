import '../../commun/template/TemplatePageAvecSommaire.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_AIDE, PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { PAGE_FAQ_CONTENU } from './page-faq-contenu.js';
import { construireLiensFilArianePagesFAQ, ITEMS_MENU_FAQ } from './page-faq-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-faq': PageFAQ;
    }
}

@customElement('c-page-faq')
export class PageFAQ extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER];

    @property()
    idElementCibleScroll = '';

    render() {
        const sectionFAQ =
            PAGE_FAQ_CONTENU.find((section) => {
                return section.id === this.idElementCibleScroll;
            }) ?? PAGE_FAQ_CONTENU[0];

        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getUrl()}"
                idItemActifMenuSommaire="${sectionFAQ.id}"
                .itemsMenuSommaire="${ITEMS_MENU_FAQ}"
                .liensFilAriane="${construireLiensFilArianePagesFAQ(sectionFAQ.libelle, PAGES_AIDE.faq.getUrl() + '#' + sectionFAQ.id)}"
                idElementCibleScroll="${this.idElementCibleScroll}"
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    renderCoeurPage() {
        return html`
            <main>
                <section>
                    <h1>${PAGES_AIDE.faq.getTitreCourt()}</h1>
                    ${PAGE_FAQ_CONTENU.map(
                        (section) =>
                            html`
                                <h3 id="${section.id}">${section.libelle}</h3>
                                <p>${section.definition}</p>
                            `
                    )}
                </section>
            </main>
        `;
    }
}
