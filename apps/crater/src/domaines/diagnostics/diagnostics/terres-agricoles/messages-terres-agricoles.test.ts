import { describe, expect, it } from 'vitest';

import {
    calculerMessageDetaillePolitiqueAmenagement,
    calculerMessagePartLogementsVacants,
    calculerMessageSyntheseTerresAgricoles,
    MessageRythmeArtificialisation,
    MessageSauParHabitant
} from './messages-terres-agricoles';
import { PolitiqueAmenagement } from './PolitiqueAmenagement';
import {
    SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE,
    SEUIL_SAU_PAR_HAB_M2_REGIME_ACTUEL,
    SEUIL_SAU_PAR_HAB_M2_REGIME_MOINS_CARNE,
    SEUIL_SAU_PAR_HAB_M2_REGIME_TRES_VEGETAL
} from './TerresAgricoles';

describe('Tests des fonctions de calcul des messages de Terres agricoles', () => {
    it('Calculer le message de synthese', () => {
        expect(
            calculerMessageSyntheseTerresAgricoles(
                null,
                SEUIL_SAU_PAR_HAB_M2_REGIME_TRES_VEGETAL - 1,
                SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE
            )
        ).toEqual(`Données non disponibles (voir les échelles supérieures telle que l'EPCI).`);
        expect(
            calculerMessageSyntheseTerresAgricoles(
                1,
                SEUIL_SAU_PAR_HAB_M2_REGIME_TRES_VEGETAL - 1,
                SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE
            )
        ).toEqual(
            `La surface agricole par habitant <strong>est trop faible</strong> mais l’objectif <c-lien href="/aide/glossaire#zero-artificialisation">Zéro Artificialisation</c-lien> <strong>a été atteint entre 2011 et 2016</strong>.`
        );
        expect(
            calculerMessageSyntheseTerresAgricoles(1, SEUIL_SAU_PAR_HAB_M2_REGIME_MOINS_CARNE - 1, SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE)
        ).toEqual(
            `La surface agricole par habitant <strong>peut convenir pour un régime alimentaire très végétal</strong> et l’objectif <c-lien href="/aide/glossaire#zero-artificialisation">Zéro Artificialisation</c-lien> <strong>a été atteint entre 2011 et 2016</strong>.`
        );
        expect(
            calculerMessageSyntheseTerresAgricoles(1, SEUIL_SAU_PAR_HAB_M2_REGIME_ACTUEL - 1, SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE)
        ).toEqual(
            `La surface agricole par habitant <strong>peut convenir pour un régime alimentaire moins carné</strong> et l’objectif <c-lien href="/aide/glossaire#zero-artificialisation">Zéro Artificialisation</c-lien> <strong>a été atteint entre 2011 et 2016</strong>.`
        );
        expect(
            calculerMessageSyntheseTerresAgricoles(1, SEUIL_SAU_PAR_HAB_M2_REGIME_ACTUEL + 1, SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE)
        ).toEqual(
            `La surface agricole par habitant <strong>est suffisante pour le régime alimentaire actuel</strong> et l’objectif <c-lien href="/aide/glossaire#zero-artificialisation">Zéro Artificialisation</c-lien> <strong>a été atteint entre 2011 et 2016</strong>.`
        );
        expect(
            calculerMessageSyntheseTerresAgricoles(
                1,
                SEUIL_SAU_PAR_HAB_M2_REGIME_ACTUEL + 1,
                SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE + 1000
            )
        ).toEqual(
            `La surface agricole par habitant <strong>est suffisante pour le régime alimentaire actuel</strong> mais l’objectif <c-lien href="/aide/glossaire#zero-artificialisation">Zéro Artificialisation</c-lien> <strong>n’a pas été atteint entre 2011 et 2016</strong>.`
        );
    });

    it("Calculer le message détaillé pour l'indicateur SAU par habitants", () => {
        expect(MessageSauParHabitant.construireMessage('NomCommune', null)).toEqual(
            `Sur le territoire <em>NomCommune</em>, l'indicateur est indisponible.`
        );
        expect(MessageSauParHabitant.construireMessage('NomCommune', 99)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageSauParHabitant.messageInferieur100
        );
        expect(MessageSauParHabitant.construireMessage('NomCommune', 1500)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageSauParHabitant.messageEntre100Et1700
        );
        expect(MessageSauParHabitant.construireMessage('NomCommune', 2000)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageSauParHabitant.messageEntre1700et2500
        );
        expect(MessageSauParHabitant.construireMessage('NomCommune', 3000)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageSauParHabitant.messageEntre2500Et4000
        );
        expect(MessageSauParHabitant.construireMessage('NomCommune', 4500)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageSauParHabitant.messageSuperieur4000
        );
    });

    it("Calculer le message détaillé pour l'indicateur rythme d`artificialisation", () => {
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', null, null)).toEqual(
            `Sur le territoire <em>NomCommune</em>, l'indicateur est indisponible.`
        );
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', 0, 0.1)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageRythmeArtificialisation.messageRythmeArtificialisation0
        );
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', 0.05, 0.1)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageRythmeArtificialisation.messageRythmeArtificialisation0_80
        );
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', 0.09, 0.1)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageRythmeArtificialisation.messageRythmeArtificialisation80_120
        );
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', 0.13, 0.1)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageRythmeArtificialisation.messageRythmeArtificialisation120_1
        );
        expect(MessageRythmeArtificialisation.construireMessage('NomCommune', 1.1, 0.1)).toEqual(
            `Sur le territoire <em>NomCommune</em>` + MessageRythmeArtificialisation.messageRythmeArtificialisationSup1
        );
    });

    it(`Calculer le message Politique d'aménagement`, () => {
        expect(calculerMessageDetaillePolitiqueAmenagement('NomCommune', PolitiqueAmenagement.DONNEES_NON_DISPONIBLES, null, null, null)).toEqual(
            "Sur le territoire <em>NomCommune</em>, l'indicateur est indisponible."
        );
        expect(
            calculerMessageDetaillePolitiqueAmenagement('NomCommune', PolitiqueAmenagement.ARTIF_QUASI_NULLE_OU_NEGATIVE, null, null, null)
        ).toEqual("Sur le territoire <em>NomCommune</em>, l'objectif Zéro Artificialisation Nette a été atteint entre 2011 et 2016.");
        expect(
            calculerMessageDetaillePolitiqueAmenagement(
                'NomCommune',
                PolitiqueAmenagement.ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE,
                100,
                null,
                9
            )
        ).toEqual(
            "Sur le territoire <em>NomCommune</em>, l'objectif Zéro Artificialisation Nette n'a pas été atteint entre 2011 et 2016 puisque 100 ha ont été artificialisés soit 9 % de la superficie totale du territoire."
        );
        expect(
            calculerMessageDetaillePolitiqueAmenagement(
                'NomCommune',
                PolitiqueAmenagement.ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE,
                100,
                null,
                9
            )
        ).toEqual(
            "Sur le territoire <em>NomCommune</em>, l'objectif Zéro Artificialisation Nette n'a pas été atteint entre 2011 et 2016 puisque 100 ha ont été artificialisés soit 9 % de la superficie totale du territoire."
        );
        expect(
            calculerMessageDetaillePolitiqueAmenagement(
                'NomCommune',
                PolitiqueAmenagement.ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE,
                100,
                -20,
                9
            )
        ).toEqual(
            "Sur le territoire <em>NomCommune</em>, l'objectif Zéro Artificialisation Nette n'a pas été atteint entre 2011 et 2016 puisque 100 ha ont été artificialisés soit 9 % de la superficie totale du territoire, alors que le territoire a perdu 20 ménages et emplois."
        );
    });

    it('Calculer le message Politique Artificialisation', () => {
        expect(calculerMessagePartLogementsVacants('NomCommune', null, 1)).toEqual(
            "Sur le territoire <em>NomCommune</em>, l'indicateur est indisponible."
        );
        expect(calculerMessagePartLogementsVacants('NomCommune', 1.1, 2)).toEqual(
            'Sur le territoire <em>NomCommune</em>, la part de logements vacants était de 2 % en 2018 (1,1 % en 2013).'
        );
    });
});
