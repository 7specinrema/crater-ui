import { describe, expect, it } from 'vitest';

import {
    arrondirANDecimales,
    calculerEtFormaterEvolutionString,
    calculerEvolutionParPasDeTemps,
    calculerPourcentageString,
    calculerQualificatifEvolution,
    calculerSigneNombre,
    calculerValeurEntiereNonNulle,
    clip,
    formaterEvolutionPourcentString,
    formaterNombreEnEntierHtml,
    formaterNombreEnEntierString,
    formaterNombreEnNDecimalesString,
    formaterNombreSelonValeurString,
    QualificatifEvolution,
    Signe,
    sommeArray,
    supprimerEspacesRedondantsEtRetoursLignes
} from './base';

describe('Tests des fonctions de base.ts', () => {
    it("Vérifier que la locale fr-FR est présente dans l'environnement de test", () => {
        // Selon l'installation nodejs utilisé pour lancer ces tests, la locale fr-FR peut manquer (par ex selon l'image docker choisie sur framagit)
        // Voir https://nodejs.org/api/intl.html
        const locales = ['fr-FR', 'locale-inconnue'];
        expect(Intl.DateTimeFormat.supportedLocalesOf(locales).join(', ')).toEqual('fr-FR');
    });

    it('Fonction arrondirANDecimales', () => {
        expect(arrondirANDecimales(123.456, -1)).toEqual(120);
        expect(arrondirANDecimales(123.456, 0)).toEqual(123);
        expect(arrondirANDecimales(123.5, 0)).toEqual(124);
        expect(arrondirANDecimales(123.456, 1)).toEqual(123.5);
        expect(arrondirANDecimales(123.456, 2)).toEqual(123.46);
    });
    it('Fonction calculerValeurEntiereNonNulle', () => {
        expect(calculerValeurEntiereNonNulle(null)).toEqual(0);
        expect(calculerValeurEntiereNonNulle(10)).toEqual(10);
        expect(calculerValeurEntiereNonNulle(10.1)).toEqual(10);
        expect(calculerValeurEntiereNonNulle(9.9)).toEqual(10);
    });
    it('Fonction clip', () => {
        expect(clip(5, 0, 10)).toEqual(5);
        expect(clip(-1, 0, 10)).toEqual(0);
        expect(clip(11, 0, 10)).toEqual(10);
    });
    it('Fonction sommeArray', () => {
        expect(sommeArray([])).toEqual(null);
        expect(sommeArray([null])).toEqual(null);
        expect(sommeArray([null, 0])).toEqual(0);
        expect(sommeArray([null, 1, 2])).toEqual(3);
        expect(sommeArray([1, 2, 3])).toEqual(6);
    });
    it('Fonction formaterNombreEnEntierString', () => {
        expect(formaterNombreEnEntierString(null)).toEqual('-');
        expect(formaterNombreEnEntierString(NaN)).toEqual('-');
        expect(formaterNombreEnEntierString(undefined)).toEqual('-');
        expect(formaterNombreEnEntierString(5)).toEqual('5');
        expect(formaterNombreEnEntierString(-5)).toEqual('-5');
        expect(formaterNombreEnEntierString(-5, true)).toEqual('5');
        expect(formaterNombreEnEntierString(5.1)).toEqual('5');
        expect(formaterNombreEnEntierString(1000)).toEqual('1 000');
    });
    it('Fonction formaterNombreEnNDecimalesString', () => {
        expect(formaterNombreEnNDecimalesString(null)).toEqual('-');
        expect(formaterNombreEnNDecimalesString(NaN)).toEqual('-');
        expect(formaterNombreEnNDecimalesString(undefined)).toEqual('-');
        expect(formaterNombreEnNDecimalesString(5)).toEqual('5');
        expect(formaterNombreEnNDecimalesString(-5)).toEqual('-5');
        expect(formaterNombreEnNDecimalesString(-5, 1, true)).toEqual('5');
        expect(formaterNombreEnNDecimalesString(5.12, 0)).toEqual('5');
        expect(formaterNombreEnNDecimalesString(5.12)).toEqual('5,1');
        expect(formaterNombreEnNDecimalesString(5.12, 1)).toEqual('5,1');
        expect(formaterNombreEnNDecimalesString(5.99, 1)).toEqual('6');
        expect(formaterNombreEnNDecimalesString(5.99, 2)).toEqual('5,99');
        expect(formaterNombreEnNDecimalesString(1000.2, 1)).toEqual('1 000,2');
    });
    it('Fonction formaterNombreSelonValeurString', () => {
        expect(formaterNombreSelonValeurString(null)).toEqual('-');
        expect(formaterNombreSelonValeurString(NaN)).toEqual('-');
        expect(formaterNombreSelonValeurString(undefined)).toEqual('-');
        expect(formaterNombreSelonValeurString(10.1)).toEqual('10');
        expect(formaterNombreSelonValeurString(10.1, true)).toEqual('+10');
        expect(formaterNombreSelonValeurString(-10.1)).toEqual('-10');
        expect(formaterNombreSelonValeurString(1.11)).toEqual('1,1');
        expect(formaterNombreSelonValeurString(0.89)).toEqual('0,9');
        expect(formaterNombreSelonValeurString(0.011)).toEqual('0,01');
        expect(formaterNombreSelonValeurString(0.001)).toEqual('0');
    });
    it('Fonction formaterEvolutionPourcentString', () => {
        expect(formaterEvolutionPourcentString(null)).toEqual('-%');
        expect(formaterEvolutionPourcentString(NaN)).toEqual('-%');
        expect(formaterEvolutionPourcentString(undefined)).toEqual('-%');
        expect(formaterEvolutionPourcentString(0)).toEqual('+0%');
        expect(formaterEvolutionPourcentString(50)).toEqual('+50%');
        expect(formaterEvolutionPourcentString(150)).toEqual('x2,5');
        expect(formaterEvolutionPourcentString(-0.33)).toEqual('-0,3%');
        expect(formaterEvolutionPourcentString(-33.33)).toEqual('-33%');
    });
    it('Fonction calculerEtFormaterEvolutionString', () => {
        expect(calculerEtFormaterEvolutionString(null, 10)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(5, null)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(NaN, 10)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(5, NaN)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(undefined, 10)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(5, undefined)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(10, 15)).toEqual('+50%');
        expect(calculerEtFormaterEvolutionString(10, 25)).toEqual('x2,5');
        expect(calculerEtFormaterEvolutionString(15, 10)).toEqual('-33%');
    });
    it('Fonction formaterNombreEnEntierHtml', () => {
        expect(formaterNombreEnEntierHtml(null)).toEqual(`-`);
        expect(formaterNombreEnEntierHtml(undefined)).toEqual(`-`);
        expect(formaterNombreEnEntierHtml(5)).toEqual(`<span class="chiffre">${'5'}</span>`);
    });
    it('Fonction calculerSigneNombre', () => {
        expect(calculerSigneNombre(null)).toEqual(Signe.NULL);
        expect(calculerSigneNombre(0)).toEqual(Signe.ZERO);
        expect(calculerSigneNombre(1)).toEqual(Signe.POSITIF);
        expect(calculerSigneNombre(-1)).toEqual(Signe.NEGATIF);
    });
    it('Fonction calculerQualificatifEvolution', () => {
        expect(calculerQualificatifEvolution(Signe.NULL)).toEqual(QualificatifEvolution.neutre);
        expect(calculerQualificatifEvolution(Signe.ZERO)).toEqual(QualificatifEvolution.neutre);
        expect(calculerQualificatifEvolution(Signe.POSITIF)).toEqual(QualificatifEvolution.progres);
        expect(calculerQualificatifEvolution(Signe.NEGATIF)).toEqual(QualificatifEvolution.declin);
        expect(calculerQualificatifEvolution(Signe.NULL, 'negative')).toEqual(QualificatifEvolution.neutre);
        expect(calculerQualificatifEvolution(Signe.ZERO, 'negative')).toEqual(QualificatifEvolution.neutre);
        expect(calculerQualificatifEvolution(Signe.POSITIF, 'negative')).toEqual(QualificatifEvolution.declin);
        expect(calculerQualificatifEvolution(Signe.NEGATIF, 'negative')).toEqual(QualificatifEvolution.progres);
    });
    it('Fonction calculerPourcentageString', () => {
        expect(calculerPourcentageString(null, null)).toEqual('-');
        expect(calculerPourcentageString(null, 0)).toEqual('-');
        expect(calculerPourcentageString(0, null)).toEqual('-');
        expect(calculerPourcentageString(1, 2)).toEqual('50');
    });
    it('Fonction supprimerEspacesEtRetoursChariotsMultiple', () => {
        expect(supprimerEspacesRedondantsEtRetoursLignes('a b')).toEqual('a b');
        expect(supprimerEspacesRedondantsEtRetoursLignes('a  b')).toEqual('a b');
        expect(
            supprimerEspacesRedondantsEtRetoursLignes(
                `
            a   
            b
            `
            )
        ).toEqual(`a b`);
        expect(
            supprimerEspacesRedondantsEtRetoursLignes(
                `

                a  

                b

                `
            )
        ).toEqual(`a b`);
    });
    it('Fonction calculerEvolutionParPasDeTemps', () => {
        expect(calculerEvolutionParPasDeTemps(null)).toEqual({ nombre: null, periode: null });
        expect(calculerEvolutionParPasDeTemps(0)).toEqual({ nombre: 0, periode: 'tous les dix ans' });
        expect(calculerEvolutionParPasDeTemps(1)).toEqual({ nombre: 10, periode: 'tous les dix ans' });
        expect(calculerEvolutionParPasDeTemps(-1)).toEqual({ nombre: -10, periode: 'tous les dix ans' });
        expect(calculerEvolutionParPasDeTemps(1 * 2)).toEqual({ nombre: 10, periode: 'tous les cinq ans' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5)).toEqual({ nombre: 10, periode: 'par an' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 12)).toEqual({ nombre: 10, periode: 'par mois' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 52)).toEqual({ nombre: 10, periode: 'par semaine' });
        expect(calculerEvolutionParPasDeTemps(-1 * 2 * 5 * 52)).toEqual({ nombre: -10, periode: 'par semaine' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 52 * 7)).toEqual({ nombre: 10, periode: 'par jour' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 52 * 7 * 24)).toEqual({ nombre: 10, periode: 'par heure' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 52 * 7 * 24 * 60)).toEqual({ nombre: 10, periode: 'par minute' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 52 * 7 * 24 * 60 * 60)).toEqual({ nombre: 10, periode: 'par seconde' });
    });
});
