import { EvenementPersonnalisable } from './EvenementPersonnalisable';

export class EvenementSelectionner extends EvenementPersonnalisable<{ idItemSelectionne: string }> {
    static ID = 'selectionner';
    constructor(idItemSelectionne: string) {
        super(EvenementSelectionner.ID, { idItemSelectionne: idItemSelectionne });
    }
}
