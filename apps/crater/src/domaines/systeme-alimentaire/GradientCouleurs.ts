import { clip } from '@lga/commun/build/outils/base';

export interface ValeurCouleur {
    valeur: number;
    couleur: string;
}

export const COULEUR_VALEUR_NULL = '#d7d7d7';

export class GradientCouleurs {
    constructor(private seuilsCouleurs: ValeurCouleur[]) {
        this.seuilsCouleurs = seuilsCouleurs.map((couleur) => {
            return { valeur: couleur.valeur, couleur: this.nettoyerCodeCouleur(couleur.couleur) };
        });
    }
    calculerCouleur(valeur: number | null) {
        if (valeur != null) {
            let indiceSeuilBas = 0;
            while (indiceSeuilBas < this.seuilsCouleurs.length - 2 && valeur >= this.seuilsCouleurs[indiceSeuilBas + 1].valeur) {
                indiceSeuilBas++;
            }
            return this.calculerGradientDeuxCouleurs(
                valeur,
                this.seuilsCouleurs[indiceSeuilBas].valeur,
                this.seuilsCouleurs[indiceSeuilBas + 1].valeur,
                this.seuilsCouleurs[indiceSeuilBas].couleur,
                this.seuilsCouleurs[indiceSeuilBas + 1].couleur
            );
        }
        return COULEUR_VALEUR_NULL;
    }

    get couleurBasse() {
        return this.calculerCouleur(this.seuilsCouleurs[0].valeur);
    }
    get couleurHaute() {
        return this.calculerCouleur(this.seuilsCouleurs[this.seuilsCouleurs.length - 1].valeur);
    }

    private nettoyerCodeCouleur(codeCouleur: string) {
        return codeCouleur.replace(/#| /g, '');
    }

    private calculerCouleurHex(codeCouleurInt: number): string {
        const codeCouleurHexa = codeCouleurInt.toString(16);
        return codeCouleurHexa.length === 1 ? '0' + codeCouleurHexa : codeCouleurHexa;
    }

    private calculerGradientDeuxCouleurs(valeur: number, valeurMin: number, valeurMax: number, couleurValeurMin: string, couleurValeurMax: string) {
        valeur = clip(valeur, valeurMin, valeurMax);
        const ratio = (valeur - valeurMin) / (valeurMax - valeurMin);

        const r = Math.ceil(parseInt(couleurValeurMax.substring(0, 2), 16) * ratio + parseInt(couleurValeurMin.substring(0, 2), 16) * (1 - ratio));
        const g = Math.ceil(parseInt(couleurValeurMax.substring(2, 4), 16) * ratio + parseInt(couleurValeurMin.substring(2, 4), 16) * (1 - ratio));
        const b = Math.ceil(parseInt(couleurValeurMax.substring(4, 6), 16) * ratio + parseInt(couleurValeurMin.substring(4, 6), 16) * (1 - ratio));

        return '#' + this.calculerCouleurHex(r) + this.calculerCouleurHex(g) + this.calculerCouleurHex(b);
    }
}
