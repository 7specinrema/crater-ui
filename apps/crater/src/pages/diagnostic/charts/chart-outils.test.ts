import { describe, expect, it } from 'vitest';

import {
    decouperLabelEnPlusieursLignes,
    formaterLabelNombreEleve,
    ModeFiltrageSeries,
    Serie,
    SerieMultiple,
    transformerEnLabelMultiligne
} from './chart-outils';

describe('Tests des outils de chart', () => {
    it('Formatter nombre grands', () => {
        expect(formaterLabelNombreEleve('0,15')).toBe('0,15');
        expect(formaterLabelNombreEleve('0.15')).toBe('0,15');
        expect(formaterLabelNombreEleve('0,33333339')).toBe('0,33');
        expect(formaterLabelNombreEleve('99,6')).toBe('99,6');
        expect(formaterLabelNombreEleve('100')).toBe('100');
        expect(formaterLabelNombreEleve('1000')).toBe('1 000');
        expect(formaterLabelNombreEleve('10000')).toBe('10 000');
        expect(formaterLabelNombreEleve('100000')).toBe('100k');
        expect(formaterLabelNombreEleve('1000000')).toBe('1 000k');
        expect(formaterLabelNombreEleve('10000000')).toBe('10 000k');
        expect(formaterLabelNombreEleve('100000000')).toBe('100M');
        expect(formaterLabelNombreEleve('1000000000')).toBe('1 000M');
        expect(formaterLabelNombreEleve('5000000000')).toBe('5 000M');
    });

    it('Traiter les labels long', () => {
        expect(transformerEnLabelMultiligne('Commerce spécialisé', 8)).toStrictEqual(['Commerce', 'spécialisé']);
        expect(transformerEnLabelMultiligne('Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson', 15)).toStrictEqual([
            'Saint-Remy-en',
            '-Bouzemont',
            '-Saint-Genest',
            '-et-Isson'
        ]);
        expect(transformerEnLabelMultiligne('Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson', 16)).toStrictEqual([
            'Saint-Remy-en',
            '-Bouzemont-Saint',
            '-Genest-et-Isson'
        ]);
        expect(transformerEnLabelMultiligne('CA Concarneau Cornouaille', 1)).toStrictEqual(['CA', 'Concarneau', 'Cornouaille']);
        expect(transformerEnLabelMultiligne('CA Concarneau Cornouaille', 12)).toStrictEqual(['CA', 'Concarneau', 'Cornouaille']);
        expect(transformerEnLabelMultiligne('CA Concarneau Cornouaille', 13)).toStrictEqual(['CA Concarneau', 'Cornouaille']);
        expect(decouperLabelEnPlusieursLignes(['CA Concarneau Cornouaille', 'Agglomération'], 13)).toStrictEqual([
            'CA Concarneau',
            'Cornouaille',
            'Agglomération'
        ]);
        expect(transformerEnLabelMultiligne('Entre 100 et 200 ha', 9)).toStrictEqual(['Entre 100', 'et 200 ha']);
        expect(transformerEnLabelMultiligne('De 20 à 50 ha', 9)).toStrictEqual(['De 20 à', '50 ha']);
    });

    it('Filtrer les valeurs null dans les SerieMultiple selon les différents modes de filtrage', () => {
        const seriesAvecNull = new SerieMultiple([new Serie([1, 5, 20], 'serie1'), new Serie([null, 10, 30], 'serie2')], ['A', 'B', 'C']);

        expect(seriesAvecNull.filtrerValeursNull(ModeFiltrageSeries.SUPPRIMER_VALEURS_EN_POSITION_X_SI_AU_MOINS_UN_NULL)).toEqual(
            new SerieMultiple([new Serie([5, 20], 'serie1'), new Serie([10, 30], 'serie2')], ['B', 'C'])
        );
        expect(seriesAvecNull.filtrerValeursNull(ModeFiltrageSeries.SUPPRIMER_SERIES_AVEC_AU_MOINS_UNE_VALEUR_NULL)).toEqual(
            new SerieMultiple([new Serie([1, 5, 20], 'serie1')], ['A', 'B', 'C'])
        );

        const seriesSansNull = new SerieMultiple([new Serie([1, 5, 7], 'serie1'), new Serie([5, 10, 25], 'serie2')], ['A', 'B', 'C']);
        expect(seriesSansNull.filtrerValeursNull(ModeFiltrageSeries.SUPPRIMER_SERIES_AVEC_AU_MOINS_UNE_VALEUR_NULL)).toEqual(seriesSansNull);
        expect(seriesSansNull.filtrerValeursNull(ModeFiltrageSeries.SUPPRIMER_VALEURS_EN_POSITION_X_SI_AU_MOINS_UN_NULL)).toEqual(seriesSansNull);
    });
});
