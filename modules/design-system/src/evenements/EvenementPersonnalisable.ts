export class EvenementPersonnalisable<T> extends CustomEvent<T> {
    // composed=true car on veut publier les evt en dehors du shadow dom
    // bubbles=true car on veut qu'ils soient visibles par tous les noeuds intermédiaires
    constructor(type: string, detail: T) {
        super(type, {
            composed: true,
            bubbles: true,
            detail: detail
        });
    }
}
