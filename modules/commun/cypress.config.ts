import { defineConfig } from 'cypress';

export default defineConfig({
    includeShadowDom: true,
    experimentalWebKitSupport: true,
    video: false,

    component: {
        supportFile: 'cypress/support/component.ts',
        // Ignorer les erreurs car on ne renseigne pas la prop "framework" de devServer (framework "lit" non disponible, on utilise cypress-lit en alternative)
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        devServer: {
            bundler: 'vite',
            viteConfig: {
                server: {
                    fs: {
                        // Autorise le server de dev de vite à faire l'import dynamique des modules dans ../* (donc dans design-system)
                        allow: ['../']
                    }
                }
            }
        },
        indexHtmlFile: 'cypress/support/component-index.html'
    }
});
