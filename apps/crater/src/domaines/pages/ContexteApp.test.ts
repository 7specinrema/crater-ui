import { describe, expect, it } from 'vitest';

import { ContexteApp } from './ContexteApp';
import { ModelePageStatique } from './ModelePageStatique';
import { Page } from './Page';

describe('Test de ContexteApp', () => {
    it("Gestion de l'état Erreur", () => {
        // when
        const modelePage = new ModelePageStatique({
            url: 'accueil',
            titreCourt: 'Titre court'
        });
        const contexteApp = new ContexteApp(new Page(modelePage));
        // then
        expect(contexteApp.estEnErreur()).toBeFalsy();
        // when
        contexteApp.mettreEnErreur('CODE', 'Message');
        // then
        expect(contexteApp.estEnErreur()).toBeTruthy();
        expect(contexteApp.erreur?.code).toEqual('CODE');
        expect(contexteApp.erreur?.message).toEqual('Message');
        // when
        contexteApp.annulerErreur();
        // then
        expect(contexteApp.estEnErreur()).toBeFalsy();
    });
});
