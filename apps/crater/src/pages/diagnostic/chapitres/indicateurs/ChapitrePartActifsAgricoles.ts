import '../../charts/ChartBarres.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import { OptionsChartBarres } from '../../charts/ChartBarres.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-actifs-agricoles': ChapitrePartActifsAgricoles;
    }
}

@customElement('c-chapitre-part-actifs-agricoles')
export class ChapitrePartActifsAgricoles extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.partActifsAgricoles}"
                .options=${this.calculerOptionsIndicateurDetaillePartActifsAgricoles(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="7"
            >
                <c-chart-barres
                    .options=${this.calculerOptionsChartPartActifsAgricoles(this.rapport)}
                    slot="chart-indicateur-detaille"
                ></c-chart-barres>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetaillePartActifsAgricoles(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partActifsAgricoles)!,
            message: `<p>
                En France métropolitaine, le nombre d'actifs agricoles permanents est passé de <strong>2 039 000</strong> en 1988 à
                <strong>966 000</strong> en 2010 (-53%) et <strong>759 000</strong> en 2020 (-21%) selon les résultats provisoires du
                <c-lien
                    href="https://agriculture.gouv.fr/recensement-agricole-2020-julien-denormandie-presente-les-premiers-resultats-du-recensement-decennal"
                    target="_blank"
                    >dernier recensement de 2020</c-lien
                >.
            </p>
            <br>
            ${rapport.getDiagnosticActif()?.agriculteursExploitations.messageActifsAgricoles}
            `,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }
    private calculerOptionsChartPartActifsAgricoles(rapport: Rapport): OptionsChartBarres {
        return {
            series: new SerieMultiple(
                [
                    new Serie(
                        rapport.getListeDiagnostics().map((d) => d!.agriculteursExploitations.partActifsAgricoles1988Pourcent),
                        '1988'
                    ),
                    new Serie(
                        rapport.getListeDiagnostics().map((d) => d!.agriculteursExploitations.partActifsAgricoles2010Pourcent),
                        '2010'
                    )
                ],
                rapport.getListeDiagnostics().map((d) => d!.territoire.nom)
            ),
            nomAxeOrdonnees: 'Actifs agricoles permanents / pop. totale (%)'
        };
    }
}
