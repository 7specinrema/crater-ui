import { LienFilAriane } from '@lga/design-system/build/composants/FilAriane';
import { MenuAccordeonItem, MenuAccordeonSousItem } from '@lga/design-system/build/composants/MenuAccordeon';

import { PAGES_AIDE, PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';
import { PAGE_GLOSSAIRE_CONTENU } from './page-glossaire-contenu';

const construireSousItems = () => {
    const sousItems: MenuAccordeonSousItem[] = [];

    PAGE_GLOSSAIRE_CONTENU.forEach((section) => {
        sousItems.push({
            id: section.id,
            libelle: section.libelle,
            href: PAGES_AIDE.glossaire.getUrl() + '#' + section.id
        });
    });

    return sousItems;
};

export const ITEMS_MENU_GLOSSAIRE: MenuAccordeonItem[] = [
    {
        id: 'glossaire',
        libelle: PAGES_AIDE.glossaire.getTitreCourt(),
        sousItems: construireSousItems()
    }
];

export function construireLiensFilArianePagesGlossaire(libellePage: string, hrefPage: string): LienFilAriane[] {
    return [
        {
            libelle: PAGES_PRINCIPALES.aide.getTitreCourt(),
            href: PAGES_PRINCIPALES.aide.getUrl()
        },
        {
            libelle: PAGES_AIDE.glossaire.getTitreCourt(),
            href: PAGES_AIDE.glossaire.getUrl()
        },
        {
            libelle: libellePage,
            href: hrefPage
        }
    ];
}
