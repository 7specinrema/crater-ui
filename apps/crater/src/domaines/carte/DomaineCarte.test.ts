import { beforeEach, describe, expect, it } from 'vitest';

import { CategorieTerritoire, Territoire } from '../territoires';
import { DomaineCarte } from './DomaineCarte';
import { EchelleTerritoriale } from './EchelleTerritoriale';

describe('Tests de la classe Carte', () => {
    let carte: DomaineCarte;

    beforeEach(() => {
        carte = new DomaineCarte();
    });

    it("Modifier l'indicateur actif", () => {
        //when
        carte.majIndicateurActif('score-maillon-production');
        //then
        expect(carte.getIndicateurActif().id).toEqual('score-maillon-production');
    });

    it("Ne pas changer l'indicateur actif si l'id est inconnu", () => {
        //then (valeur par defaut)
        expect(carte.getIndicateurActif().id).toEqual('score-maillon-terres-agricoles');
        //when
        carte.majIndicateurActif('id-inconnu');
        //then
        expect(carte.getIndicateurActif().id).toEqual('score-maillon-terres-agricoles');
    });

    it("Modifier l'échelle territoriale active", () => {
        //then
        expect(carte.getEchelleTerritorialeActive()).toEqual(EchelleTerritoriale.Departements);
        //when
        carte.majEchelleTerritorialeActive(EchelleTerritoriale.Epcis.id);
        //then
        expect(carte.getEchelleTerritorialeActive()).toEqual(EchelleTerritoriale.Epcis);
        expect(carte.getIdTerritoirePrincipal()).toBeUndefined();
        expect(carte.getIdTerritoireZoom()).toBeUndefined();
        expect(carte.getIdDepartementInitialCommune()).toBeUndefined();
    });

    it('Positionner le territoire principal', () => {
        // when
        const territoire = new Territoire('idTerritoire', 'nomTerritoire', CategorieTerritoire.Commune);
        carte.positionnerTerritoirePrincipal(territoire, 'idDepartementInitialCommune');
        //then
        expect(carte.getEchelleTerritorialeActive()).toEqual(EchelleTerritoriale.Communes);
        expect(carte.getIdTerritoirePrincipal()).toEqual('idTerritoire');
        expect(carte.getIdTerritoireZoom()).toEqual('idTerritoire');
        expect(carte.getIdDepartementInitialCommune()).toEqual('idDepartementInitialCommune');
        //when
        carte.desactiverZoomAutoTerritoire();
        //then
        expect(carte.getEchelleTerritorialeActive()).toEqual(EchelleTerritoriale.Communes);
        expect(carte.getIdTerritoirePrincipal()).toEqual('idTerritoire');
        expect(carte.getIdTerritoireZoom()).toBeUndefined();
    });

    it("Modifier l'échelle territorial vers la valeur par défaut si l'idEchelleTerritorial est inconnu", () => {
        //when
        carte.majEchelleTerritorialeActive(EchelleTerritoriale.Epcis.id);
        carte.majEchelleTerritorialeActive('idInconnu');
        //then
        expect(carte.getEchelleTerritorialeActive()).toEqual(EchelleTerritoriale.Departements);
    });
});
