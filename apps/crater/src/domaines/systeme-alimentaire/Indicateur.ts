import { Maillon } from './Maillon';
import { StyleIndicateur } from './StyleIndicateur';
import { SystemeAlimentaire } from './SystemeAlimentaire';

export interface ParametresIndicateur {
    id: string;
    idMaillon: string;
    libelle: string;
    description: string;
    icone?: string;
    legende?: string;
    indicateurCarte?: ParametresIndicateurCarte;
    indicateurNote?: boolean;
}

export interface ParametresIndicateurCarte {
    libelle?: string;
    description?: string;
    unite?: string;
    style: StyleIndicateur;
    nomIndicateurRequeteApi: string;
    legende?: string;
}

export class Indicateur {
    public readonly id: string;
    public readonly maillon: Maillon;
    public readonly libelle: string;
    public readonly description: string;
    public readonly icone?: string;
    public readonly legende?: string;
    public readonly indicateurCarte?: IndicateurCarte;
    public readonly indicateurNote?: boolean;
    constructor(parametresIndicateur: ParametresIndicateur, systemeAlimentaire: SystemeAlimentaire) {
        this.id = parametresIndicateur.id;
        this.maillon = systemeAlimentaire.getMaillon(parametresIndicateur.idMaillon)!;
        this.libelle = parametresIndicateur.libelle;
        this.description = parametresIndicateur.description;
        this.icone = parametresIndicateur.icone;
        this.legende = parametresIndicateur.legende;
        if (parametresIndicateur.indicateurCarte) this.indicateurCarte = new IndicateurCarte(this, parametresIndicateur.indicateurCarte);
        parametresIndicateur.indicateurNote ? (this.indicateurNote = true) : false;
    }
}

export class IndicateurCarte {
    public readonly id: string;
    public readonly maillon: Maillon;
    public readonly icone?: string;
    public readonly libelle: string;
    public readonly description: string;
    public readonly unite: string;
    public readonly style: StyleIndicateur;
    public readonly nomIndicateurRequeteApi: string;
    public readonly legende: string;
    constructor(private readonly indicateur: Indicateur, parametresIndicateurCarte: ParametresIndicateurCarte) {
        this.id = indicateur.id;
        this.maillon = indicateur.maillon;
        this.icone = indicateur.icone;
        this.libelle = parametresIndicateurCarte.libelle ?? this.indicateur.libelle;
        this.description = parametresIndicateurCarte.description ?? this.indicateur.description;
        this.unite = parametresIndicateurCarte.unite ?? '';
        this.style = parametresIndicateurCarte.style;
        this.nomIndicateurRequeteApi = parametresIndicateurCarte.nomIndicateurRequeteApi;
        this.legende = parametresIndicateurCarte.legende ?? this.indicateur.legende ?? '';
    }
}
