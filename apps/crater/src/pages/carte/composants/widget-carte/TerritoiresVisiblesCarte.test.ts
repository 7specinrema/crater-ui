import { FeatureCollection } from 'geojson';
import { beforeEach, describe, expect, it } from 'vitest';

import { TerritoiresVisiblesCarte, ValeurIndicateurParIdTerritoire } from './TerritoiresVisiblesCarte';

describe("Test de l'objet TerritoiresVisiblesCarte", () => {
    let territoiresVisiblesCarte: TerritoiresVisiblesCarte;
    const contoursRegions: FeatureCollection = {
        type: 'FeatureCollection',
        features: [
            {
                type: 'Feature',
                properties: {
                    id_territoire: 'regions'
                },
                geometry: {
                    type: 'Polygon',
                    coordinates: [[[0, 1]]]
                }
            }
        ]
    };
    const valeursIndicateurs: ValeurIndicateurParIdTerritoire[] = [
        { idTerritoire: 'test', valeur: 7 },
        { idTerritoire: 'test2', valeur: 2 }
    ];

    beforeEach(() => {
        territoiresVisiblesCarte = new TerritoiresVisiblesCarte();
    });

    it('Un nouvel objet doit etre initialisé avec des attributs valorisés à undefined pour les contours', () => {
        // when
        //then
        expect(territoiresVisiblesCarte.contoursGeojson).toEqual(undefined);
    });

    it("Réinitialiser l'objet TerritoiresVisiblesCarte", () => {
        // when
        territoiresVisiblesCarte.ajouterContours(contoursRegions);
        territoiresVisiblesCarte.ajouterValeursIndicateurs(valeursIndicateurs);
        territoiresVisiblesCarte.reset();
        //then
        expect(territoiresVisiblesCarte.contoursGeojson).toEqual(undefined);
    });

    describe("Test d'ajout des contours", () => {
        const contoursCommunesDuDepartement01: FeatureCollection = {
            type: 'FeatureCollection',
            features: [
                {
                    type: 'Feature',
                    properties: {
                        id_territoire: 'departement01'
                    },
                    geometry: {
                        type: 'Polygon',
                        coordinates: [[[0, 1]]]
                    }
                }
            ]
        };
        const contoursCommunesDuDepartement02: FeatureCollection = {
            type: 'FeatureCollection',
            features: [
                {
                    type: 'Feature',
                    properties: {
                        id_territoire: 'departement02'
                    },
                    geometry: {
                        type: 'Polygon',
                        coordinates: [[[0, 1]]]
                    }
                }
            ]
        };
        const contoursCommunesDuDepartement01et02: FeatureCollection = {
            type: 'FeatureCollection',
            features: [
                {
                    type: 'Feature',
                    properties: {
                        id_territoire: 'departement01'
                    },
                    geometry: {
                        type: 'Polygon',
                        coordinates: [[[0, 1]]]
                    }
                },
                {
                    type: 'Feature',
                    properties: {
                        id_territoire: 'departement02'
                    },
                    geometry: {
                        type: 'Polygon',
                        coordinates: [[[0, 1]]]
                    }
                }
            ]
        };

        it('Ajouter les contours geojson en une seule fois', () => {
            // when
            territoiresVisiblesCarte.ajouterContours(contoursRegions);
            //then
            expect(territoiresVisiblesCarte.contoursGeojson).toEqual(contoursRegions);
        });

        it('Ajouter les contours geojson en plusieurs fois', () => {
            // when
            territoiresVisiblesCarte.ajouterContours(contoursCommunesDuDepartement01);
            territoiresVisiblesCarte.ajouterContours(contoursCommunesDuDepartement02);
            //then
            expect(territoiresVisiblesCarte.contoursGeojson).toEqual(contoursCommunesDuDepartement01et02);
        });
    });

    describe("Test d'ajout des indicateurs", () => {
        const valeursIndicateursAvecNull: ValeurIndicateurParIdTerritoire[] = [
            { idTerritoire: 'test3', valeur: 7 },
            { idTerritoire: 'test4', valeur: null }
        ];

        it('Ajouter les valeurs indicateurs geojson en une seule fois', () => {
            // when
            territoiresVisiblesCarte.ajouterValeursIndicateurs(valeursIndicateurs);
            //then
            expect(territoiresVisiblesCarte.getValeursIndicateur('test')).toEqual(valeursIndicateurs[0].valeur);
            expect(territoiresVisiblesCarte.getValeursIndicateur('test2')).toEqual(valeursIndicateurs[1].valeur);
        });

        it('Ajouter les valeurs indicateurs geojson même si une valeur est null', () => {
            // when
            territoiresVisiblesCarte.ajouterValeursIndicateurs(valeursIndicateursAvecNull);
            //then
            expect(territoiresVisiblesCarte.getValeursIndicateur('test3')).toEqual(valeursIndicateursAvecNull[0].valeur);
            expect(territoiresVisiblesCarte.getValeursIndicateur('test4')).toEqual(valeursIndicateursAvecNull[1].valeur);
        });

        it('Ajouter les valeurs indicateurs geojson en plusieurs fois', () => {
            // when
            territoiresVisiblesCarte.ajouterValeursIndicateurs(valeursIndicateurs);
            territoiresVisiblesCarte.ajouterValeursIndicateurs(valeursIndicateursAvecNull);
            //then
            expect(territoiresVisiblesCarte.getValeursIndicateur('test')).toEqual(valeursIndicateurs[0].valeur);
            expect(territoiresVisiblesCarte.getValeursIndicateur('test2')).toEqual(valeursIndicateurs[1].valeur);
            expect(territoiresVisiblesCarte.getValeursIndicateur('test3')).toEqual(valeursIndicateursAvecNull[0].valeur);
            expect(territoiresVisiblesCarte.getValeursIndicateur('test4')).toEqual(valeursIndicateursAvecNull[1].valeur);
        });
    });
});
