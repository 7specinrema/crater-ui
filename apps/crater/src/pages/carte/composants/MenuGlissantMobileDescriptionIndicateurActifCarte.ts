import './ListeChoixIndicateursCarte.js';
import '@lga/commun/build/composants/champ-recherche-territoire/ChampRechercheTerritoire.js';
import '@lga/design-system/build/composants/Bouton.js';
import '@lga/design-system/build/composants/PanneauGlissant.js';
import '../../commun/template/TemplatePanneau.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { EvenementFermer } from '@lga/design-system/build/evenements/EvenementFermer.js';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { construireUrlPageMethodologie } from '../../../domaines/pages/pages-utils';
import { SA } from '../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-glissant-mobile-description-indicateur-actif-carte': MenuGlissantMobileDescriptionIndicateurActifCarte;
    }
}

@customElement('c-menu-glissant-mobile-description-indicateur-actif-carte')
export class MenuGlissantMobileDescriptionIndicateurActifCarte extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                height: 10%;
            }

            main {
                padding: 0 calc(2 * var(--dsem));
            }

            div {
                text-align: left;
            }

            c-bouton {
                margin: auto;
                margin-right: 0;
            }
        `
    ];

    @property()
    idIndicateurActif?: string;

    @query('#bouton-retour')
    private boutonRetour?: Bouton;

    render() {
        const indicateurActif = SA.getIndicateur(this.idIndicateurActif!);
        return html`
            <c-panneau-glissant>
                <c-bouton id="bouton-description-indicateur" slot="bouton-ouvrir" .type=${Bouton.TYPE.relief}>
                    ${ICONES_DESIGN_SYSTEM.question}
                </c-bouton>
                <c-template-panneau slot="panneau">
                    <main slot="contenu-panneau">
                        <c-bouton
                            id="bouton-retour"
                            libelle="Retour"
                            libelleTooltip="Retour"
                            type="${Bouton.TYPE.plat}"
                            tailleReduite
                            @click="${this.fermer}"
                            >${ICONES_DESIGN_SYSTEM.flecheGauche}</c-bouton
                        >
                        <div>
                            <p class="titre-petit">${unsafeHTML(indicateurActif?.libelle)}</p>
                            <p class="texte-moyen">${unsafeHTML(indicateurActif?.description)}</p>
                            <c-lien class="texte-moyen" href=${construireUrlPageMethodologie(indicateurActif?.maillon.id, indicateurActif?.id)}
                                >→ voir la méthodologie</c-lien
                            >
                        </div>
                    </main>
                </c-template-panneau>
            </c-panneau-glissant>
        `;
    }

    private fermer() {
        this.boutonRetour?.dispatchEvent(new EvenementFermer());
    }
}
