context("Test de vérification de l'affichage de la page de FAQ dans son template", () => {
    describe('Test du contenu de la page FAQ et de la présence des ids', () => {
        it("La FAQ doit comporter un fil d'ariane comprenant les items 'aide' et 'foire aux questions' avec les hrefs correspondant", () => {
            // init
            cy.viewport(1500, 1500);
            cy.visit('aide/faq');

            cy.get('c-fil-ariane').find('a[href="/aide"]').should('be.visible');
            cy.get('c-fil-ariane').find('a[href="/aide/faq"]').should('be.visible');
        });

        it("La FAQ doit comporter un titre en h1 contenant le texte 'Foire aux questions'", () => {
            // init
            cy.viewport(1500, 1500);
            cy.visit('aide/faq');

            cy.window().then((win) => {
                cy.get('h1').contains('Foire aux questions');
            });
        });
    });
});
