import '@lga/design-system/build/composants/Bouton.js';
import '@lga/design-system/build/composants/PanneauGlissant.js';
import '../ReseauxSociaux.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { EvenementFermer } from '@lga/design-system/build/evenements/EvenementFermer.js';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

import { PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';
import imageLga from '../../../ressources/images/lga-200x80.png';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-glissant-mobile': MenuGlissantMobile;
    }
}

@customElement('c-menu-glissant-mobile')
export class MenuGlissantMobile extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            menu {
                height: 100vh;
                background-color: var(--couleur-blanc);
                margin: 0;
                padding: 0 0 2rem 0;
            }

            header {
                background-color: var(--c-header-arriereplan);
                display: flex;
                justify-content: end;
                align-items: center;
                height: var(--hauteur-c-entete);
                padding: 0 1rem;
                margin-bottom: 1rem;
            }

            main {
                margin: 0;
            }

            ul {
                padding-left: 0;
                padding-right: 0;
                padding-bottom: 0.5rem;
                margin-top: 0;
                text-align: center;
            }

            li {
                list-style: none;
                white-space: nowrap;
                padding: 0.6rem;
            }

            li > a {
                color: var(--couleur-neutre);
                text-decoration: none;
                cursor: pointer;
            }

            li > c-bouton {
                margin: auto;
            }

            li > a:hover,
            li > a:focus {
                color: var(--couleur-accent);
                text-decoration: none;
            }

            footer {
                display: flex;
                flex-direction: column;
                align-items: center;
                margin-top: 5rem;
            }

            footer img {
                width: 80px;
                height: auto;
            }

            c-reseaux-sociaux {
                --couleur: var(--couleur-neutre-sombre);
            }
        `
    ];

    render() {
        return html`
            <menu>
                <header>
                    <c-bouton @click="${this.fermerParents}" libelleTooltip="Fermer" .type=${Bouton.TYPE.plat} data-cy="bouton-fermer-menu-glissant">
                        ${ICONES_DESIGN_SYSTEM.croix}
                    </c-bouton>
                </header>
                <main>
                    <ul>
                        <li>
                            <a class="texte-alternatif" href="${PAGES_PRINCIPALES.accueil.getUrl()}" @click=${this.fermerParents}>Accueil</a>
                        </li>
                        <li>
                            <a class="texte-alternatif" href="${PAGES_PRINCIPALES.projet.getUrl()}" @click=${this.fermerParents}>Le projet</a>
                        </li>
                        <li>
                            <c-bouton
                                libelle="Nous contacter"
                                libelleTooltip="Nous contacter"
                                themeCouleur="${Bouton.THEME_COULEUR.primaire}"
                                href="${PAGES_PRINCIPALES.contact.getUrl()}"
                            ></c-bouton>
                        </li>
                    </ul>
                </main>
                <footer>
                    <section>
                        <a href="https://resiliencealimentaire.org" target="_blank">
                            <img src="${imageLga}" width="200" height="80" alt="Les Greniers d'Abondance"
                        /></a>
                    </section>
                    <section>
                        <c-reseaux-sociaux></c-reseaux-sociaux>
                    </section>
                </footer>
            </menu>
        `;
    }

    private fermerParents() {
        this.dispatchEvent(new EvenementFermer(true));
    }
}
