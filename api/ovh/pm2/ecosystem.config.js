module.exports = {
    apps: [
        {
            name: 'crater-api-dev',
            cwd: '../../dev/crater-api/api',
            script: 'server.js',
            // watch: '.',
            // ignore_watch: ['node_modules'],
            cron_restart: '0 2 * * *',
            env: {
                PORT: 3333
            }
        },
        {
            name: 'crater-api-prod',
            cwd: '../../prod/crater-api/api',
            script: 'server.js',
            // watch: '.',
            // ignore_watch: ['node_modules'],
            cron_restart: '0 3 * * *',
            env: {
                PORT: 3000
            }
        }
    ]
};
