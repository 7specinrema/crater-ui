import { fetchRetry } from '@lga/commun/build/outils/requetes-utils';

import { HierarchieTerritoires, Territoire } from '../domaines/territoires';

export function chargerHierarchieTerritoires(apiBaseUrl: string, idTerritoire: string): Promise<HierarchieTerritoires> {
    return fetchRetry(apiBaseUrl + 'crater/api/territoires/' + idTerritoire)
        .then((response) => response.json())
        .then((donneesTerritoire: unknown) => {
            return traduireJsonEnHierarchieTerritoires(donneesTerritoire);
        });
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function traduireJsonEnTerritoire(donneesTerritoireJson: any): Territoire {
    return new Territoire(donneesTerritoireJson.idTerritoire, donneesTerritoireJson.nomTerritoire, donneesTerritoireJson.categorieTerritoire);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function traduireJsonEnHierarchieTerritoires(donneesTerritoireJson: any): HierarchieTerritoires {
    return new HierarchieTerritoires(
        donneesTerritoireJson.idTerritoire,
        donneesTerritoireJson.nomTerritoire,
        donneesTerritoireJson.categorieTerritoire,
        donneesTerritoireJson.sousCategorieTerritoire ?? null,
        donneesTerritoireJson.commune?.id,
        donneesTerritoireJson.commune?.nom,
        donneesTerritoireJson.epci?.id,
        donneesTerritoireJson.epci?.nom,
        donneesTerritoireJson.departement?.id,
        donneesTerritoireJson.departement?.nom,
        donneesTerritoireJson.region?.id,
        donneesTerritoireJson.region?.nom,
        donneesTerritoireJson.pays?.id,
        donneesTerritoireJson.pays?.nom
    );
}
