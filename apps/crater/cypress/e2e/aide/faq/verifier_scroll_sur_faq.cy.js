const tailleArbitraireParagraphe = 150;

context('Test sur la page FAQ templatée', () => {
    describe("Test de l'intégration du menu accordeon et du bon fonctionnement du scrolling sur les éléments de la FAQ", () => {
        before(() => {
            cy.viewport(1500, 500);
            cy.visit('aide/faq');

            cy.get('c-menu-sous-item[href="/aide/faq#difference-avec-parcel"]').find('a[href="/aide/faq#difference-avec-parcel"]').eq(0).click();

            cy.window().its('scrollY').should('be.gt', 150);

            cy.url().should('include', '/aide/faq#difference-avec-parcel');
            cy.get('c-fil-ariane').find('a[href="/aide/faq#difference-avec-parcel"]').should('be.visible');
        });

        it("Le click sur l'élément #pourquoi-les-drom doit provoquer le scroll vers le texte correspondant", () => {
            cy.viewport(1500, 500);
            cy.visit('aide/faq');
            cy.get('c-menu-sous-item[href="/aide/faq#pourquoi-les-drom"]').find('a[href="/aide/faq#pourquoi-les-drom"]').eq(0).click();

            cy.window().its('scrollY').should('be.gt', 300);

            cy.url().should('include', '/aide/faq#pourquoi-les-drom');
            cy.get('c-fil-ariane').find('a[href="/aide/faq#pourquoi-les-drom"]').should('be.visible');
        });
    });
});
