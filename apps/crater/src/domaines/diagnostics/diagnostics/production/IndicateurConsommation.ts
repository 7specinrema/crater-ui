import { arrondirANDecimales } from '@lga/commun/build/outils/base';

import { GroupeCulture } from '../../../systeme-alimentaire';

export class ConsommationParCulture {
    constructor(
        public readonly codeCulture: string,
        public readonly nomCulture: string,
        public readonly alimentationAnimale: boolean,
        public readonly consommationHa: number
    ) {}
}

export class ConsommationParGroupeCulture {
    constructor(
        public readonly codeGroupeCulture: string,
        public readonly nomGroupeCulture: string,
        public readonly consommationHa: number,
        public readonly tauxAdequationBrutPourcent: number,
        // TODO : le champ besoinsTotauxTousGroupesCulturesHa est temporaire, a remplacer par partDansBesoinsTotaux quand ce dernier sera fourni par l'api
        public readonly consommationTotalTousGroupesCulturesHa: number,
        public readonly consommationParCulture: ConsommationParCulture[]
    ) {}

    get groupeCulture(): GroupeCulture {
        return GroupeCulture.fromString(this.codeGroupeCulture);
    }

    get partDansBesoinsTotaux() {
        return (this.consommationHa / this.consommationTotalTousGroupesCulturesHa) * 100;
    }

    get besoinsHumainsHa() {
        return this.consommationParCulture
            .filter((bc) => bc.alimentationAnimale === false)
            .map((bc) => bc.consommationHa)
            .reduce(this.ajouter, 0);
    }

    get besoinsAnimauxHa() {
        return this.consommationParCulture
            .filter((bc) => bc.alimentationAnimale === true)
            .map((bc) => bc.consommationHa)
            .reduce(this.ajouter, 0);
    }

    get partBesoinsHumainsHa() {
        return arrondirANDecimales((this.besoinsHumainsHa / this.consommationHa) * 100);
    }

    get partBesoinsAnimauxHa() {
        return arrondirANDecimales((this.besoinsAnimauxHa / this.consommationHa) * 100);
    }

    private ajouter(a: number, b: number) {
        return a + b;
    }
}

export class IndicateurConsommation {
    constructor(public readonly consommationHa: number, private _consommationsParGroupeCultures: ConsommationParGroupeCulture[]) {}

    public get consommationsParGroupeCultures(): ConsommationParGroupeCulture[] {
        return this._consommationsParGroupeCultures.sort((b1, b2) =>
            GroupeCulture.fromString(b1.codeGroupeCulture).comparer(GroupeCulture.fromString(b2.codeGroupeCulture))
        );
    }

    public get consommationHumainsParGroupeCulture() {
        return this._consommationsParGroupeCultures
            .map((bgc) => {
                const besoinsHumainsParCulture = bgc.consommationParCulture.filter((bc) => !bc.alimentationAnimale);
                const besoinsHumainsHa = besoinsHumainsParCulture.map((bc) => bc.consommationHa).reduce((a, b) => a + b, 0);
                return new ConsommationParGroupeCulture(
                    bgc.codeGroupeCulture,
                    bgc.nomGroupeCulture,
                    besoinsHumainsHa,
                    bgc.tauxAdequationBrutPourcent,
                    bgc.consommationTotalTousGroupesCulturesHa,
                    besoinsHumainsParCulture
                );
            })
            .filter((b) => b.besoinsHumainsHa != 0);
    }

    public get consommationAnimauxParGroupeCulture() {
        return this._consommationsParGroupeCultures
            .map((bgc) => {
                const besoinsAnimauxParCulture = bgc.consommationParCulture.filter((bc) => bc.alimentationAnimale);
                const besoinsAnimauxHa = besoinsAnimauxParCulture.map((bc) => bc.consommationHa).reduce((a, b) => a + b, 0);
                return new ConsommationParGroupeCulture(
                    bgc.codeGroupeCulture,
                    bgc.nomGroupeCulture,
                    besoinsAnimauxHa,
                    bgc.tauxAdequationBrutPourcent,
                    bgc.consommationTotalTousGroupesCulturesHa,
                    besoinsAnimauxParCulture
                );
            })
            .filter((b) => b.besoinsAnimauxHa != 0);
    }

    public get consommationAnimauxHa() {
        return this.consommationAnimauxParGroupeCulture.map((b) => b.besoinsAnimauxHa).reduce((a, b) => a + b, 0);
    }
    public get consommationHumainsHa() {
        return this.consommationHumainsParGroupeCulture.map((b) => b.besoinsHumainsHa).reduce((a, b) => a + b, 0);
    }
    public get pourcentageConsommationAnimauxSurConsommationTotale() {
        return arrondirANDecimales((this.consommationAnimauxHa / this.consommationHa) * 100);
    }
}
