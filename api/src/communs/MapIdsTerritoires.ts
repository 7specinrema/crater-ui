import { lireCelluleString, TraiterCellules } from './injecteursUtils';

export class MapIdsTerritoires {
    private correspondanceIdCodeVersIdNom = new Map<string, string>();

    private ajouterTerritoire(idCodeTerritoire: string, idNomTerritoire: string): void {
        this.correspondanceIdCodeVersIdNom.set(idCodeTerritoire, idNomTerritoire);
    }

    lireCelluleIdTerritoire(map: Map<string, string>, nomColonne = 'id_territoire'): string {
        const idCodeTerritoire = lireCelluleString(map, nomColonne);
        return this.correspondanceIdCodeVersIdNom.get(idCodeTerritoire!)!;
    }

    traiterCellulesCorrespondanceIdsTerritoires: TraiterCellules = (mapCellules) => {
        const idCodeTerritoire = lireCelluleString(mapCellules, 'id_territoire');
        const idNomTerritoire = lireCelluleString(mapCellules, 'id_nom_territoire');
        if (!idCodeTerritoire || !idNomTerritoire) {
            throw new Error(
                `Erreur critique lors de la construction de la table de correspondance des idTerritoires : au moins un des id est undefined id_territoire=${idCodeTerritoire}, id_nom_territoire=${idNomTerritoire}`
            );
        }
        this.ajouterTerritoire(idCodeTerritoire, idNomTerritoire);
    };
}
