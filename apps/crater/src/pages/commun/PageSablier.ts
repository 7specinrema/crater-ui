import './Sablier.js';

import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-sablier': PageSablier;
    }
}

@customElement('c-page-sablier')
export class PageSablier extends LitElement {
    static styles = css`
        :host {
            display: block;
            height: 100vh;
            background-color: var(--couleur-blanc);
        }
    `;

    render() {
        return html`
            <main>
                <c-sablier></c-sablier>
            </main>
        `;
    }
}
