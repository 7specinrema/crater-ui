import '../../charts/ChartTreemapDoubleProductionBesoins.js';
import '@lga/design-system/build/composants/Tableau.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { formaterNombreEnEntierHtml } from '@lga/commun/build/outils/base.js';
import { OptionsTableau } from '@lga/design-system/build/composants/Tableau.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { SauParGroupeCulture } from '../../../../domaines/diagnostics/index.js';
import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { DonneesTreemapCultures, DonneesTreemapDetailCultures } from '../../charts/ChartTreemapCultures.js';
import { OptionsChartTreemapDoubleProductionBesoins } from '../../charts/ChartTreemapDoubleProductionBesoins.js';
import { creerDonneesTreemapConsommation } from '../chapitres-outils.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-adequation-theorique-production-consommation': ChapitreAdequationTheoriqueProductionConsommation;
    }
}

@customElement('c-chapitre-adequation-theorique-production-consommation')
export class ChapitreAdequationTheoriqueProductionConsommation extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.adequationTheoriqueProductionConsommation}"
                .options=${<OptionsIndicateurDetaille>{
                    indicateur: SA.getIndicateur(IDS_INDICATEURS.adequationTheoriqueProductionConsommation),
                    donneesPage: this.donneesPage,
                    echelleEstDisponibleDansCarte: this.rapport.echelleEstDisponibleDansCarte()
                }}
                contenuAdhoc
            >
                <div slot="contenu">
                    <p>
                        Les estimations brutes de production et consommation sont présentées ci-dessous toutes cultures confondues, et sont exprimés
                        toutes deux en hectares de surface agricole :
                    </p>

                    <c-figure titre="${SA.getIndicateur(IDS_INDICATEURS.adequationTheoriqueProductionConsommation)!.legende!}">
                        <c-tableau .options=${this.calculerOptionsTableauProductionBesoinsGlobal(this.rapport)} slot="chart-figure"></c-tableau>
                    </c-figure>
                    <p>
                        L'analyse du ratio production / consommation sans prendre en compte les besoins par grande catégorie de culture peut masquer
                        des déséquilibres importants (manques ou production excessive pour certains secteurs). Le diagramme suivant illustre l'écart
                        entre production et consommation pour le territoire avec le détail par groupes de cultures :
                    </p>

                    <c-figure titre="${SA.getIndicateur(IDS_INDICATEURS.adequationTheoriqueProductionConsommation)!.legende!}">
                        <c-chart-treemap-production-besoins
                            .options=${this.calculerOptionsChartTreemapProductionBesoins(this.rapport!)}
                            slot="chart-figure"
                        ></c-chart-treemap-production-besoins>
                    </c-figure>

                    <p>
                        L'adéquation théorique global représente de façon plus fine l'adéquation théorique entre production et consommation puisque
                        tenant compte de la diversité des cultures nécessaire à une alimentation variée. Il est calculé en faisait la moyenne des
                        adéquations théoriques de chaque groupe de culture capées à 100% (pour ne pas tenir compte des surproductions) et pondérées
                        par leur part dans le total des consommations.
                    </p>
                    <c-figure titre="${SA.getIndicateur(IDS_INDICATEURS.adequationTheoriqueProductionConsommation)!.legende!}">
                        <c-tableau .options=${this.obtenirTableauProductionBesoinsDetail(this.rapport)} slot="chart-figure"></c-tableau>
                    </c-figure>
                </div>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsTableauProductionBesoinsGlobal(rapport: Rapport): OptionsTableau {
        return {
            entetes: ['', 'Production', 'Consommation', 'Ratio<br>production/<br>consommation'],
            contenuHtml: rapport
                .getListeDiagnostics()
                .map((d) => [
                    d!.territoire.nom,
                    formaterNombreEnEntierHtml(d!.surfaceAgricoleUtile.sauProductiveHa) + ' ha',
                    formaterNombreEnEntierHtml(d!.consommation.indicateurConsommation?.consommationHa) + ' ha',
                    formaterNombreEnEntierHtml(d!.production.tauxAdequationBrutPourcent) + ' %'
                ])!
        };
    }

    private calculerOptionsChartTreemapProductionBesoins(rapport: Rapport): OptionsChartTreemapDoubleProductionBesoins {
        const donneesTreemapProduction = this.creerDonneesTreemapProduction(
            rapport.getDiagnosticActif()!.surfaceAgricoleUtile!.sauParGroupeCultureHorsSNC
        );
        const donneesTreemapBesoins = creerDonneesTreemapConsommation(
            rapport.getDiagnosticActif()!.consommation!.indicateurConsommation!.consommationsParGroupeCultures
        );

        return {
            productionHa: rapport.getDiagnosticActif()!.surfaceAgricoleUtile!.sauProductiveHa,
            besoinsHa: rapport.getDiagnosticActif()!.consommation!.indicateurConsommation!.consommationHa,
            donneesTreemapProduction: donneesTreemapProduction,
            donneesTreemapBesoins: donneesTreemapBesoins
        };
    }

    private creerDonneesTreemapProduction(sauParGroupeCultures: SauParGroupeCulture[]) {
        return sauParGroupeCultures.map(
            (p) =>
                <DonneesTreemapCultures>{
                    code: p!.codeGroupeCulture,
                    labelCourt: p!.groupeCulture.nomCourt,
                    labelLong: p!.nomGroupeCulture,
                    couleur: p!.groupeCulture.couleur,
                    sauHa: p!.sauHa,
                    detailCultures: p!.sauParCulture.map(
                        (s) =>
                            <DonneesTreemapDetailCultures>{
                                codeCulture: s.codeCulture,
                                nomCulture: s.nomCulture,
                                sauHa: s.sauHa
                            }
                    )
                }
        );
    }

    private obtenirTableauProductionBesoinsDetail(rapport: Rapport): OptionsTableau {
        const ligneEntete = ['', '<span>Consommation</span>', '<span>Part dans le régime alimentaire</span>', '<span>Adéquation théorique</span>'];

        const formaterTauxAdequationBrut = (taux: number) => {
            if (taux <= 100) {
                return formaterNombreEnEntierHtml(taux) + ' %';
            } else {
                return `>${formaterNombreEnEntierHtml(100)}% (<small>${formaterNombreEnEntierHtml(taux)} %</small>)`;
            }
        };

        const contenuTableau = rapport
            .getDiagnosticActif()!
            .consommation.indicateurConsommation!.consommationsParGroupeCultures.map((b) => [
                b.nomGroupeCulture,
                formaterNombreEnEntierHtml(b.consommationHa) + ' ha',
                formaterNombreEnEntierHtml(b.partDansBesoinsTotaux) + ' %',
                formaterTauxAdequationBrut(b.tauxAdequationBrutPourcent)
            ])!;

        const ligneFin = [
            'Adéquation théorique globale',
            '',
            '',
            formaterNombreEnEntierHtml(rapport.getDiagnosticActif()?.production.tauxAdequationMoyenPonderePourcent) + ' %'
        ];
        return {
            entetes: ligneEntete,
            contenuHtml: contenuTableau,
            ligneFin: ligneFin
        };
    }
}
