import '../../charts/ChartBarres.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import { OptionsChartBarres } from '../../charts/ChartBarres.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-taux-pauvrete': ChapitreTauxPauvrete;
    }
}

@customElement('c-chapitre-taux-pauvrete')
export class ChapitreTauxPauvrete extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                .options=${this.calculerOptionsIndicateurDetailleTauxPauvrete(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="6"
            >
                <c-chart-barres .options=${this.calculerOptionsChartTauxPauvrete(this.rapport)} slot="chart-indicateur-detaille"></c-chart-barres>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetailleTauxPauvrete(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.tauxPauvrete)!,
            message: rapport.getDiagnosticActif()!.consommation.messageTauxPauvrete,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }

    private calculerOptionsChartTauxPauvrete(rapport: Rapport): OptionsChartBarres {
        const labels = rapport.getListeDiagnostics()!.map((d) => d!.territoire.nom);
        return {
            series: new SerieMultiple([new Serie(rapport.getListeDiagnostics()!.map((d) => d!.consommation.tauxPauvrete))], labels),
            nomAxeOrdonnees: 'Taux de pauvreté (%)'
        };
    }
}
