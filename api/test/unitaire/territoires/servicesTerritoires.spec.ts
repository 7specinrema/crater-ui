import { ServicesTerritoires } from '../../../src/territoires/domaines/ServicesTerritoires';
import { Commune, Departement, Epci, Pays, RegroupementCommunes } from '../../../src/territoires/domaines/entitesTerritoires';

describe('Test unitaire recherche de territoires par critere', () => {
    let servicesTerritoires: ServicesTerritoires;

    function initialiserServicesTerritoiresAvecCommunes(nomsCommunes: string[]) {
        let servicesTerritoires = new ServicesTerritoires();
        nomsCommunes.forEach((nom) => {
            servicesTerritoires.ajouterTerritoire(new Commune(nom, nom, '', '', '', '', []));
        });
        return servicesTerritoires;
    }

    it('Verifier les règles de normalisation des lettres et mots de liaisons', () => {
        expect(ServicesTerritoires.normaliser('aei')).toEqual('AEI');
        expect(ServicesTerritoires.normaliser('àéI')).toEqual('AEI');
        expect(ServicesTerritoires.normaliser('à â ä é è ê ë ï î ô ö ù û ü ÿ ç')).toEqual('A A A E E E E I I O O U U U Y C');
        expect(ServicesTerritoires.normaliser('œ æ')).toEqual('OE AE');
        expect(ServicesTerritoires.normaliser('à-à-à')).toEqual('A A A');
        expect(ServicesTerritoires.normaliser("a'a'a")).toEqual('A A A');
        expect(ServicesTerritoires.normaliser('CC du Perche')).toEqual('CC PERCHE');
        expect(ServicesTerritoires.normaliser('CC de Perche')).toEqual('CC PERCHE');
        expect(ServicesTerritoires.normaliser('CC des Perche')).toEqual('CC PERCHE');
        expect(ServicesTerritoires.normaliser('CC de la Perche')).toEqual('CC PERCHE');
        expect(ServicesTerritoires.normaliser('CC le les Perche')).toEqual('CC PERCHE');
        expect(ServicesTerritoires.normaliser("CC d'Perche")).toEqual('CC PERCHE');
    });

    it('Trouver des territoires par correspondance partielle du nom, avec majuscule et caractères spéciaux', () => {
        servicesTerritoires = initialiserServicesTerritoiresAvecCommunes(['Bordeaux', 'Bläye', 'Pau', 'Blanquefort']);
        let suggestions = servicesTerritoires.rechercherTerritoireParCritere('bla', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Bläye', 'Blanquefort']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('BLà', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Bläye', 'Blanquefort']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('àye', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Bläye']);
    });

    it('Trouver des territoires par le nom exact, avec majuscule et caractères spéciaux', () => {
        servicesTerritoires = initialiserServicesTerritoiresAvecCommunes(['Bordeaux', 'Bläye', 'Pau', 'Blanquefort']);
        let suggestions = servicesTerritoires.rechercherTerritoireParCritere('blaye', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Bläye']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('BLàYE', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['Bläye']);
    });

    it("Trouver des territoires par le nom partiel, avec - et '", () => {
        servicesTerritoires = initialiserServicesTerritoiresAvecCommunes(['Bordeaux', "l'aa-bb-ccc", 'Pau', 'Blanquefort']);
        let suggestions = servicesTerritoires.rechercherTerritoireParCritere('L AA B', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(["l'aa-bb-ccc"]);
    });

    it('Trouver des territoires par le code postal', () => {
        servicesTerritoires = new ServicesTerritoires();
        servicesTerritoires.ajouterTerritoire(new Commune('1', 'Commune1', '', '', '', '', ['01000', '01001']));
        servicesTerritoires.ajouterTerritoire(new Commune('2', 'CommuneC', '', '', '', '', ['02000']));
        servicesTerritoires.ajouterTerritoire(new Commune('3', 'CommuneA', '', '', '', '', ['02000', '02001']));
        servicesTerritoires.ajouterTerritoire(new Pays('4', 'Pays'));
        let suggestions = servicesTerritoires.rechercherTerritoireParCritere('02001', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['CommuneA']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('020', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['CommuneA', 'CommuneC']);
    });

    it('Ne rien trouver si aucun territoire ne correspond', () => {
        const recherche = new ServicesTerritoires().rechercherTerritoireParCritere('Rien', 5);
        expect(recherche).toEqual([]);
    });

    it('Trier les résultats par groupe: correspondance mot complet en premier, puis mot partiel, et dans chaque groupe tri par taille de nom et ordre alphabétique quand meme taille', () => {
        servicesTerritoires = initialiserServicesTerritoiresAvecCommunes(['celoi', 'beloi', 'aaeloi', "CU d'Eloi", "CC d'Eloi", 'eloi']);
        const suggestions = servicesTerritoires.rechercherTerritoireParCritere('éloi', 10);
        expect(suggestions.map((t) => t.nom)).toEqual(['eloi', "CC d'Eloi", "CU d'Eloi", 'beloi', 'celoi', 'aaeloi']);
    });

    it('Ne pas tenir compte des mots de liaison', () => {
        servicesTerritoires = initialiserServicesTerritoiresAvecCommunes(['autre', 'BLAYE', 'CC de Blaye', 'autre']);
        let suggestions = servicesTerritoires.rechercherTerritoireParCritere('CC du Blaye', 3);
        expect(suggestions.map((t) => t.nom)).toEqual(['CC de Blaye']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('CC le Blaye', 3);
        expect(suggestions.map((t) => t.nom)).toEqual(['CC de Blaye']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('CC Blaye', 3);
        expect(suggestions.map((t) => t.nom)).toEqual(['CC de Blaye']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('Blaye', 3);
        expect(suggestions.map((t) => t.nom)).toEqual(['BLAYE', 'CC de Blaye']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('CC de', 3);
        expect(suggestions.map((t) => t.nom)).toEqual(['CC de Blaye']);
    });

    it('Pour un critère de moins de 3 lettres, ne retourner que les territoires avec correspondance exacte du nom', () => {
        servicesTerritoires = initialiserServicesTerritoiresAvecCommunes(['y', 'ar', 'a', 'arras']);
        let suggestions = servicesTerritoires.rechercherTerritoireParCritere('a', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['a']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('ar', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['ar']);
    });

    it('Faire une recherche uniquement sur une categorie de territoires', () => {
        servicesTerritoires = new ServicesTerritoires();
        servicesTerritoires.ajouterTerritoire(new Commune('1', 'NomTerritoireC', '', '', '', '', ['01000', '01001']));
        servicesTerritoires.ajouterTerritoire(new Commune('2', 'Autre commune', '', '', '', '', ['02000']));
        servicesTerritoires.ajouterTerritoire(new Epci('3', 'NomTerritoireE', '', '', '', ''));
        servicesTerritoires.ajouterTerritoire(new Departement('4', 'NomTerritoireD', '', ''));
        servicesTerritoires.ajouterTerritoire(new RegroupementCommunes('5', 'SCOT NomTerritoireS', 'SCOT', '', '', ''));

        let suggestions = servicesTerritoires.rechercherTerritoireParCritere('epci Nom', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['NomTerritoireE']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('epci epci Nom', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['NomTerritoireE']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('epci departement Nom', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['NomTerritoireD', 'NomTerritoireE']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('scot Nom', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['SCOT NomTerritoireS']);

        suggestions = servicesTerritoires.rechercherTerritoireParCritere('Nom', 5);
        expect(suggestions.map((t) => t.nom)).toEqual(['NomTerritoireC', 'NomTerritoireD', 'NomTerritoireE', 'SCOT NomTerritoireS']);
    });
});
