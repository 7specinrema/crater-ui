import '@lga/design-system/build/composants/Lien.js';

import { getApiBaseUrl } from '@lga/commun/build/env/config-baseurl';
import { formaterNombreSelonValeurString } from '@lga/commun/build/outils/base';
import { CSS_BREAKPOINT_MAX_WIDTH_TABLETTE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import * as geojson from 'geojson';
import { Feature, FeatureCollection, Point } from 'geojson';
import L, { FeatureGroup, GeoJSON, GeoJSONOptions, LatLng, LatLngBounds, Layer, LayerGroup, LeafletMouseEvent, Map, PathOptions } from 'leaflet';
import leafletRawCss from 'leaflet/dist/leaflet.css';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';

import { EchelleTerritoriale } from '../../../../domaines/carte/EchelleTerritoriale';
import { PAGES_PRINCIPALES } from '../../../../domaines/pages/configuration/declaration-pages';
import { construireUrlPageDiagnosticIndicateur, construireUrlPageDiagnosticMaillon } from '../../../../domaines/pages/pages-utils';
import { StyleIndicateur } from '../../../../domaines/systeme-alimentaire';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire';
import { EvenementNaviguer } from '../../../../evenements/EvenementNaviguer';
import {
    chargerContoursTerritoires,
    chargerCoucheHotelsDepartements,
    chargerCoucheHotelsRegions,
    chargerIndicateurs
} from '../../../../requetes-api/requetes-api-carte';
import { EvenementDeplacerCarte } from './EvenementDeplacerCarte.js';
import { TerritoiresVisiblesCarte, ValeurIndicateurParIdTerritoire } from './TerritoiresVisiblesCarte';

//
// Config
//

// Calculé avec http://bboxfinder.com/
const BOUNDING_BOX_FRANCE = new LatLngBounds([
    [39.2, -5.4],
    [52.2, 10.1]
]);

const BOUDING_BOX_LARGE = new LatLngBounds([
    [35.5, -13.5],
    [55, 20]
]);

interface DonneesFeatureTerritoire {
    // Le nom de ces attributs fait référence aux champs des fichiers geojson, produits directement par crater-data
    // On utilise donc id_nom_territoire - et pas id_territoire - car c'est l'id_nom_territoire qui est exposé par l'API comme id de territoire
    id_nom_territoire: string;
    nom_territoire: string;
}

interface DonneesFeatureVille {
    Commune: string;
}

interface CouchesVilles {
    coucheHotelsDepartements: Layer | null;
    coucheHotelsRegions: Layer | null;
}

interface BboxDepartement {
    bbox: LatLngBounds;
    idDepartement: string;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-carte': WidgetCarte;
    }
}

@customElement('c-carte')
export class WidgetCarte extends LitElement {
    private carteLeaflet?: Map;
    private coucheContoursTerritoires?: GeoJSON<DonneesFeatureTerritoire>;
    private idIndicateur?: string;
    private unite?: string;
    private idEchelleTerritoriale?: string;
    private nomIndicateurRequeteApi?: string;
    private territoiresVisiblesCarte: TerritoiresVisiblesCarte = new TerritoiresVisiblesCarte();
    private styleIndicateur?: StyleIndicateur;
    private couchesVilles: CouchesVilles = {
        coucheHotelsDepartements: null,
        coucheHotelsRegions: null
    };
    private bboxDepartements?: BboxDepartement[];

    static styles = [
        unsafeCSS(leafletRawCss),
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                height: 100%;
            }

            * {
                box-sizing: initial;
            }

            #carte {
                height: 100%;
                background-color: var(--couleur-fond);
            }

            #conteneur-message-erreur {
                background-color: var(--couleur-neutre-clair);
                display: flex;
                position: absolute;
                z-index: 500; /* Leaflet utilise 400 pour le z-index de la carte */
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
            }

            #message-erreur {
                background-color: var(--couleur-warning);
                padding: 0.5rem;
                border-radius: 10px;
                text-align: center;
                margin: auto;
                max-width: 90%;
            }

            #titre-message-erreur {
                padding: 0.2rem;
                text-align: center;
            }

            .leaflet-left {
                width: 25%;
            }

            .leaflet-right {
                width: 65%;
            }

            .leaflet-control {
                background: transparent !important;
            }

            .leaflet-control-container > * {
                z-index: 401 !important; /* Leaflet utilise 400 pour le z-index de la carte */
            }

            .leaflet-popup-tip,
            .leaflet-popup-content-wrapper {
                box-shadow: none !important;
                background-color: lightgrey;
            }

            .leaflet-tooltip {
                box-shadow: none !important;
            }

            .icones-villes {
                background: black;
            }

            .icones-villes .libelle {
                white-space: nowrap;
                padding-left: 3px;
                color: black;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_TABLETTE + 'px')}) {
                .titre-carte h2 {
                    display: none !important;
                }
            }
        `
    ];

    @property({ type: Boolean })
    autoriserZoom = false;

    @property({ type: Boolean })
    autoriserMouvements = false;

    // TODO: comportement à implémenter
    @property({ type: Boolean })
    afficherBoutonPleinEcran = false;

    @state()
    estEnErreur = false;

    @query('#conteneur-message-erreur') private messageErreur!: HTMLElement;

    private async initialiserCarte() {
        await this.chargerBboxDepartements();
        this.carteLeaflet = this.initialiserCarteLeaflet();
        this.carteLeaflet!.on('moveend', this.gererDeplacementCarte, this);
        this.carteLeaflet!.on('zoomend', () => this.majCoucheVillesAfficheeSelonZoom());
    }

    disconnectedCallback() {
        this.carteLeaflet?.off();
        this.carteLeaflet?.remove();
    }

    render() {
        return html` <div id="carte">
            ${this.estEnErreur
                ? html` <div id="conteneur-message-erreur">
                      <div id="message-erreur">
                          <p id="titre-message-erreur">Problème...</p>
                          <p id="texte-message-erreur">
                              Merci d'essayer à nouveau dans quelques secondes.
                              <br />Si le problème persiste, vous pouvez nous envoyer un message via le
                              <c-lien href=${PAGES_PRINCIPALES.contact.getUrl()}>formulaire de contact</c-lien>.
                          </p>
                      </div>
                  </div>`
                : ''}
        </div>`;
    }
    //////////////////////////////////////
    // Init
    //////////////////////////////////////
    private initialiserCarteLeaflet(): Map {
        const carte = L.map(<HTMLElement>this.shadowRoot!.querySelector('#carte'), {
            zoomControl: false,
            attributionControl: false,
            minZoom: 5,
            maxZoom: 12,
            zoomSnap: 0.2,
            zoomDelta: 0.2,
            wheelPxPerZoomLevel: 150,
            maxBounds: BOUDING_BOX_LARGE
        }).fitBounds(BOUNDING_BOX_FRANCE);

        if (!this.autoriserZoom) {
            carte.touchZoom.disable();
            carte.doubleClickZoom.disable();
            carte.scrollWheelZoom.disable();
            carte.boxZoom.disable();
            carte.keyboard.disable();
        }

        if (!this.autoriserMouvements) {
            carte.dragging.disable();
        }

        return carte;
    }

    //////////////////////////////////////
    // Déclaration des fonctions publiques
    //////////////////////////////////////
    async maj(
        echelleTerritoriale: EchelleTerritoriale,
        idIndicateur: string,
        unite: string,
        nomIndicateurRequeteApi: string,
        styleIndicateur: StyleIndicateur,
        idTerritoireSelectionne?: string,
        idDepartementAppartenanceCommune?: string
    ) {
        // A faire au tout début, car si juste avant le setMaxZoom cela ne marche pas... pb de synchro leaflet ?
        if (!this.carteLeaflet) {
            await this.initialiserCarte();
        }
        this.estEnErreur = false;
        this.majCoucheVillesAfficheeSelonZoom();
        this.idIndicateur = idIndicateur;
        this.unite = unite;
        this.styleIndicateur = styleIndicateur;
        this.idEchelleTerritoriale = echelleTerritoriale.id;
        this.nomIndicateurRequeteApi = nomIndicateurRequeteApi;
        // setMaxZoom fait ici, car sinon il interfere avec les fitBounds ou panTo dans centreEtZoomer (voir https://github.com/Leaflet/Leaflet/issues/7019)
        this.carteLeaflet?.setMaxZoom(echelleTerritoriale.maxZoom);
        await this.chargementInitialTerritoiresVisibles(idDepartementAppartenanceCommune);
        this.majCoucheContoursTerritoires(this.territoiresVisiblesCarte.contoursGeojson!);
        this.centrerEtZoomer(echelleTerritoriale, idTerritoireSelectionne);
    }

    private naviguerVersPageIndicateur = (mouseEvent: LeafletMouseEvent) => {
        const properties: DonneesFeatureTerritoire = mouseEvent.target.feature.properties;
        const idTerritoire = properties.id_nom_territoire;
        if (SA.getIndicateur(this.idIndicateur!)?.indicateurNote) {
            this.dispatchEvent(
                new EvenementNaviguer(construireUrlPageDiagnosticMaillon(SA.getIndicateur(this.idIndicateur!)!.maillon.id, idTerritoire))
            );
        } else {
            this.dispatchEvent(new EvenementNaviguer(construireUrlPageDiagnosticIndicateur(this.idIndicateur!, idTerritoire)));
        }
    };

    //////////////////////////////////////
    // Fonctions privées : gestion styles, focus, etc...
    //////////////////////////////////////
    private optionsInitialesLayer(): GeoJSONOptions<DonneesFeatureTerritoire> {
        return {
            onEachFeature: this.configurerFeature
        };
    }
    private ajouterCoucheHotelsRegions() {
        this.supprimerCouche(this.carteLeaflet!, this.couchesVilles.coucheHotelsDepartements);
        if (this.couchesVilles.coucheHotelsRegions === null) {
            chargerCoucheHotelsRegions()
                .then((coucheHotelsRegions) => {
                    this.couchesVilles.coucheHotelsRegions = this.creerCoucheVilles(coucheHotelsRegions);
                    this.carteLeaflet!.addLayer(this.couchesVilles.coucheHotelsRegions);
                })
                .catch((e) => this.mettreEnErreur(e, 'erreur chargement couche hotels de régions'));
        } else {
            this.carteLeaflet!.addLayer(this.couchesVilles.coucheHotelsRegions);
        }
    }
    private ajouterCoucheHotelsDepartements() {
        this.supprimerCouche(this.carteLeaflet!, this.couchesVilles.coucheHotelsRegions);
        if (this.couchesVilles.coucheHotelsDepartements === null) {
            chargerCoucheHotelsDepartements()
                .then((coucheHotelsDepartements) => {
                    this.couchesVilles.coucheHotelsDepartements = this.creerCoucheVilles(coucheHotelsDepartements);
                    this.carteLeaflet!.addLayer(this.couchesVilles.coucheHotelsDepartements);
                })
                .catch((e) => this.mettreEnErreur(e, 'erreur chargement couche hotels de départements'));
        } else {
            this.carteLeaflet!.addLayer(this.couchesVilles.coucheHotelsDepartements);
        }
    }

    private creerCoucheVilles(coucheVilles: geojson.GeoJsonObject) {
        const groupeCouches: LayerGroup<DonneesFeatureVille> = new L.LayerGroup();
        L.geoJSON(coucheVilles, {
            pointToLayer: function (feature: Feature<Point, DonneesFeatureVille>, latlng) {
                return L.marker(latlng, {
                    icon: L.divIcon({
                        className: 'icones-villes',
                        html: '<div class="libelle texte-large">' + feature.properties.Commune + '</div>',
                        iconSize: [3, 3]
                    })
                });
            }
        }).addTo(groupeCouches);
        return groupeCouches;
    }

    private supprimerCouche(carte: Map, couche: Layer | null) {
        if (couche && carte.hasLayer(couche)) {
            carte.removeLayer(couche);
        }
    }

    private majCoucheVillesAfficheeSelonZoom() {
        if (this.carteLeaflet!.getZoom() > 7.5) {
            this.ajouterCoucheHotelsDepartements();
        } else if (this.carteLeaflet!.getZoom() > 5.5) {
            this.ajouterCoucheHotelsRegions();
        } else {
            this.supprimerCouche(this.carteLeaflet!, this.couchesVilles.coucheHotelsDepartements);
            this.supprimerCouche(this.carteLeaflet!, this.couchesVilles.coucheHotelsRegions);
        }
    }

    private majCoucheContoursTerritoires(contoursGeojson: geojson.FeatureCollection) {
        if (this.coucheContoursTerritoires) {
            this.carteLeaflet!.removeLayer(this.coucheContoursTerritoires);
        }
        this.coucheContoursTerritoires = L.geoJSON(contoursGeojson, this.optionsInitialesLayer());
        this.coucheContoursTerritoires!.setStyle(this.calculerStyle());
        this.coucheContoursTerritoires!.addTo(this.carteLeaflet!);
    }

    private calculerStyle(): (feature: geojson.Feature<geojson.GeometryObject, DonneesFeatureTerritoire> | undefined) => PathOptions {
        return (feature: geojson.Feature<geojson.GeometryObject, DonneesFeatureTerritoire> | undefined) => {
            const valeur = this.territoiresVisiblesCarte!.getValeursIndicateur(feature!.properties.id_nom_territoire);
            return {
                weight: 0.5,
                opacity: 1,
                color: this.styleIndicateur!.couleurTexte,
                fillOpacity: 1,
                fillColor: this.styleIndicateur!.calculerCouleur(valeur!),
                className: `carte__id-territoire__${feature!.properties.id_nom_territoire}`
            } as PathOptions;
        };
    }

    private construireMessageTooltip(
        nomTerritoire: string,
        idIndicateur: string | undefined,
        valeurIndicateur: number | null,
        unite: string | undefined
    ) {
        return `<strong>${nomTerritoire}</strong>
           <br>${SA.getIndicateur(idIndicateur!)?.indicateurNote ? 'Score : ' : 'Valeur: '}
            ${formaterNombreSelonValeurString(valeurIndicateur)}${unite === '%' ? '%' : unite !== '' ? ` ${unite}` : ''}`;
    }

    private creerPopupTerritoireSelectionne(idTerritoire: string, nomTerritoire: string, latlng: LatLng) {
        const popup = L.popup({ className: 'texte-moyen' })
            .setLatLng(latlng)
            .setContent(
                this.construireMessageTooltip(
                    nomTerritoire,
                    this.idIndicateur,
                    this.territoiresVisiblesCarte?.getValeursIndicateur(idTerritoire),
                    this.unite
                )
            )
            .openOn(this.carteLeaflet!);
        setTimeout(() => {
            popup.close();
        }, 3000);
    }
    private centrerEtZoomer(echelleTerritoriale: EchelleTerritoriale, idTerritoireSelectionne?: string) {
        if (idTerritoireSelectionne) {
            let donneesFeatureGeoJson: DonneesFeatureTerritoire;
            const fgTerritoireZoom: FeatureGroup = this.coucheContoursTerritoires?.getLayers().find((l: Layer) => {
                const feature = (l as FeatureGroup).feature;
                donneesFeatureGeoJson = (feature as geojson.Feature<geojson.GeometryObject, DonneesFeatureTerritoire>)?.properties;
                return donneesFeatureGeoJson?.id_nom_territoire === idTerritoireSelectionne;
            }) as FeatureGroup;
            // Animate=false permet d'éviter certaines interférences avec les setZoom (https://github.com/Leaflet/Leaflet/issues/7019)
            this.carteLeaflet!.setZoom(echelleTerritoriale.maxZoom, { animate: false }).panTo(fgTerritoireZoom.getBounds().getCenter(), {
                animate: false
            });
            this.creerPopupTerritoireSelectionne(
                idTerritoireSelectionne,
                donneesFeatureGeoJson!.nom_territoire,
                fgTerritoireZoom.getBounds().getCenter()
            );
        } else {
            if (echelleTerritoriale === EchelleTerritoriale.Communes) {
                this.carteLeaflet!.setZoom(Math.max(this.carteLeaflet!.getZoom(), EchelleTerritoriale.Communes.minZoom), { animate: false });
            }
        }
    }

    private configurerFeature = (feature: geojson.Feature<geojson.GeometryObject, DonneesFeatureTerritoire>, layer: Layer) => {
        layer.on({
            mouseover: this.focusTerritoire,
            mouseout: this.unfocusTerritoire,
            click: this.naviguerVersPageIndicateur
        });
        layer.bindTooltip(
            this.construireMessageTooltip(
                feature.properties.nom_territoire,
                this.idIndicateur,
                this.territoiresVisiblesCarte?.getValeursIndicateur(feature.properties.id_nom_territoire),
                this.unite
            ),
            { className: 'texte-moyen', direction: 'top', opacity: 1 }
        );
    };

    private focusTerritoire(mouseEvent: LeafletMouseEvent) {
        mouseEvent.target.setStyle({
            weight: 4
        });
    }

    private unfocusTerritoire(mouseEvent: LeafletMouseEvent) {
        mouseEvent.target.setStyle({
            weight: 1
        });
    }

    private async chargerBboxDepartements() {
        if (!this.bboxDepartements) {
            await chargerContoursTerritoires(getApiBaseUrl(), EchelleTerritoriale.Departements.nomGeojson)
                .then((contoursDepartements) => {
                    this.bboxDepartements = contoursDepartements.features.map(
                        (contourDepartement: geojson.Feature<geojson.GeometryObject, DonneesFeatureTerritoire>) => {
                            const contourDepartementGeoJSON = L.geoJSON(contourDepartement);
                            return { bbox: contourDepartementGeoJSON.getBounds(), idDepartement: contourDepartement.properties!.id_nom_territoire };
                        }
                    );
                })
                .catch((e) => this.mettreEnErreur(e, 'erreur chargement coutours territoires'));
        }
        return Promise.resolve();
    }

    private async chargementInitialTerritoiresVisibles(idDepartementInitialCommune?: string): Promise<void> {
        this.territoiresVisiblesCarte.reset();
        const promisesContours: Promise<FeatureCollection>[] = [];
        const promisesValeurs: Promise<ValeurIndicateurParIdTerritoire[]>[] = [];
        if (this.idEchelleTerritoriale) {
            if (this.idEchelleTerritoriale === EchelleTerritoriale.Communes.id && idDepartementInitialCommune !== undefined) {
                promisesContours.push(
                    chargerContoursTerritoires(
                        getApiBaseUrl(),
                        EchelleTerritoriale.fromId(this.idEchelleTerritoriale!)!.nomGeojson,
                        idDepartementInitialCommune
                    )
                );
                promisesValeurs.push(
                    chargerIndicateurs(getApiBaseUrl(), this.nomIndicateurRequeteApi!, this.idEchelleTerritoriale!, idDepartementInitialCommune)
                );
            } else if (this.idEchelleTerritoriale !== EchelleTerritoriale.Communes.id) {
                promisesContours.push(
                    chargerContoursTerritoires(getApiBaseUrl(), EchelleTerritoriale.fromId(this.idEchelleTerritoriale!)!.nomGeojson)
                );
                promisesValeurs.push(chargerIndicateurs(getApiBaseUrl(), this.nomIndicateurRequeteApi!, this.idEchelleTerritoriale!));
            }
        }
        return Promise.all([
            Promise.all(promisesContours).then((resultatsPromises: FeatureCollection[]) => {
                resultatsPromises.forEach((r) => this.territoiresVisiblesCarte!.ajouterContours(r));
            }),
            Promise.all(promisesValeurs).then((resultatsPromises: ValeurIndicateurParIdTerritoire[][]) => {
                resultatsPromises.forEach((r) => this.territoiresVisiblesCarte!.ajouterValeursIndicateurs(r));
            })
        ])
            .then(() => {
                return;
            })
            .catch((e) => this.mettreEnErreur(e, 'erreur chargement initale territoires visibles'));
    }

    private async chargementComplementTerritoiresVisiblesPourCommunes(): Promise<void> {
        if (this.idEchelleTerritoriale !== EchelleTerritoriale.Communes.id) {
            // Rien à faire pour EPCI, Departement et Region car tout est chargé lors de l'initialisation
            return Promise.resolve();
        }
        const promisesContours: Promise<FeatureCollection>[] = [];
        const promisesValeurs: Promise<ValeurIndicateurParIdTerritoire[]>[] = [];
        if (this.carteLeaflet!.getZoom() >= EchelleTerritoriale.Communes.minZoom) {
            this.territoiresVisiblesCarte.reset();
            const idsDepartementsVisibles: string[] = await this.calculerIdsDepartementsVisibles(this.carteLeaflet!.getBounds());
            idsDepartementsVisibles.map((idDepartement) => {
                promisesContours.push(
                    chargerContoursTerritoires(getApiBaseUrl(), EchelleTerritoriale.fromId(this.idEchelleTerritoriale!)!.nomGeojson, idDepartement)
                );
                promisesValeurs.push(chargerIndicateurs(getApiBaseUrl(), this.nomIndicateurRequeteApi!, this.idEchelleTerritoriale!, idDepartement));
            });
        } else {
            // cas Commune et zoom trop large => on ne charge rien, ne doit pas se produire
            console.warn(
                `Attention, pas de chargement des données, zoom trop large pour les communes : ${this.carteLeaflet!.getZoom()} et ${
                    EchelleTerritoriale.Communes.minZoom
                }`
            );
        }
        return Promise.all([
            Promise.all(promisesContours).then((resultatsPromises: FeatureCollection[]) => {
                resultatsPromises.forEach((r) => this.territoiresVisiblesCarte!.ajouterContours(r));
            }),
            Promise.all(promisesValeurs).then((resultatsPromises: ValeurIndicateurParIdTerritoire[][]) => {
                resultatsPromises.forEach((r) => this.territoiresVisiblesCarte!.ajouterValeursIndicateurs(r));
            })
        ])
            .then(() => {
                return;
            })
            .catch((e) => this.mettreEnErreur(e, 'erreur chargement complement territoires visibles pour commune'));
    }

    private async calculerIdsDepartementsVisibles(coordonneesViewBox: L.LatLngBounds) {
        await this.chargerBboxDepartements();
        return this.bboxDepartements!.filter((bboxDepartement) => {
            return coordonneesViewBox.intersects(bboxDepartement.bbox);
        }).map((c) => c.idDepartement);
    }

    private async gererDeplacementCarte() {
        const zoom = this.carteLeaflet!.getZoom();
        const coordonneesViewBox = this.carteLeaflet!.getBounds();

        this.dispatchEvent(new EvenementDeplacerCarte(await this.calculerIdsDepartementsVisibles(coordonneesViewBox), zoom));
        // Dans le cas des communes, une fois que la carte est centrée, on peut charger les contours et indicateurs des communes d'autres départements s'ils sont dans la viewbox
        await this.chargementComplementTerritoiresVisiblesPourCommunes();
        this.majCoucheContoursTerritoires(this.territoiresVisiblesCarte.contoursGeojson!);
    }

    private mettreEnErreur = (erreur: Error, messageErreur: string) => {
        console.warn('Widget carte :', messageErreur);
        this.estEnErreur = true;
    };
}
