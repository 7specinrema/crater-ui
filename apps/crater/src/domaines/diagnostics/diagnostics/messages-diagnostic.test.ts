import { describe, expect, it } from 'vitest';

import { CategorieTerritoire, Territoire } from '../../territoires';
import { NOTE_VALEUR_WARNING } from './IndicateurSynthese';
import { calculerMessageMetaDescriptionDiagnostic, calculerMessageOtex, calculerMessageTerritoire } from './messages-diagnostic';

describe("Tests des fonctions de calcul des messages au niveau du diagnostic d'un territoire", () => {
    it('Calculer le message information territoire', () => {
        const nbCommunesOtexMajoritaire5Postes = { otex: { libelle: 'Otex majoritaire 5 postes' }, valeur: 10 };
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 99, null, nbCommunesOtexMajoritaire5Postes)).toEqual(``);
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 99, 5, nbCommunesOtexMajoritaire5Postes)).toEqual(
            `Région peu dense avec une faible part de surface agricole et une spécialisation agricole Otex majoritaire 5 postes.`
        );
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 100, 5, nbCommunesOtexMajoritaire5Postes)).toEqual(
            `Région de densité moyenne avec une faible part de surface agricole et une spécialisation agricole Otex majoritaire 5 postes.`
        );
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 200, 5, nbCommunesOtexMajoritaire5Postes)).toEqual(
            `Région densément peuplée avec une faible part de surface agricole et une spécialisation agricole Otex majoritaire 5 postes.`
        );
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 200, 15, nbCommunesOtexMajoritaire5Postes)).toEqual(
            `Région densément peuplée avec une part minoritaire de surface agricole et une spécialisation agricole Otex majoritaire 5 postes.`
        );
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 200, 50, nbCommunesOtexMajoritaire5Postes)).toEqual(
            `Région densément peuplée avec une part majoritaire de surface agricole et une spécialisation agricole Otex majoritaire 5 postes.`
        );
    });

    it("Calculer le message sur l'OTEX", () => {
        const territoire = new Territoire('C-1', 'NomCommune', CategorieTerritoire.Commune);
        const territoirePays = new Territoire('P-FR', 'NomPays', CategorieTerritoire.Pays);
        const nbCommunesOtexMajoritaire5Postes = { otex: { libelle: 'otex majoritaire 5 postes' }, valeur: 10 };
        expect(calculerMessageOtex(territoire, nbCommunesOtexMajoritaire5Postes)).toEqual(
            `La <c-lien href="/aide/methodologie/presentation-generale#otex">spécialisation territoriale de la production agricole de la commune</c-lien> est otex majoritaire 5 postes.`
        );
        expect(calculerMessageOtex(territoirePays, nbCommunesOtexMajoritaire5Postes)).toEqual(
            `La <c-lien href="/aide/methodologie/presentation-generale#otex">spécialisation territoriale de la production agricole du territoire</c-lien> est otex majoritaire 5 postes.`
        );
    });

    it('Calculer le message meta description du diagnostic', () => {
        const messageTousMaillonsAvecNote = calculerMessageMetaDescriptionDiagnostic('NomTerritoire', 1, 2, 3, 4, 5, 6);
        expect(messageTousMaillonsAvecNote).toEqual(
            'Évaluation pour NomTerritoire : Score Terres agricoles=1/10, Score Agriculteurs & Exploitations=2/10, Score Intrants=3/10, Score Production=4/10, Score Transformation & Distribution=5/10, Score Consommation=6/10'
        );

        const messageMaillonsSansNote = calculerMessageMetaDescriptionDiagnostic('NomTerritoire', 5, null, 5, 5, 5, NOTE_VALEUR_WARNING);
        expect(messageMaillonsSansNote).toEqual(
            `Évaluation pour NomTerritoire : Score Terres agricoles=5/10, Score Intrants=5/10, Score Production=5/10, Score Transformation & Distribution=5/10`
        );

        const messageTousMaillonNoteNullOuWarning = calculerMessageMetaDescriptionDiagnostic(
            'NomTerritoire',
            null,
            null,
            NOTE_VALEUR_WARNING,
            null,
            null,
            NOTE_VALEUR_WARNING
        );
        expect(messageTousMaillonNoteNullOuWarning).toEqual(`Évaluation pour NomTerritoire : aucun score disponible`);
    });
});
