import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-revenu-agriculteurs': ChapitreRevenuAgriculteurs;
    }
}

@customElement('c-chapitre-revenu-agriculteurs')
export class ChapitreRevenuAgriculteurs extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.revenuAgriculteurs}"
                .options=${<OptionsIndicateurDetaille>{
                    indicateur: SA.getIndicateur(IDS_INDICATEURS.revenuAgriculteurs),
                    donneesPage: this.donneesPage,
                    masquerLienMethodologie: true
                }}
                contenuAdhoc
            >
                <div slot="contenu">
                    <p>
                        Sur la période 2017-2020, la moitié des moyennes et grandes exploitations agricoles françaises ont dégagé un revenu par
                        travailleur après cotisations sociales inférieur à 1 280 euros par mois.
                        <strong
                            >Ramené au temps de travail, la majorité des agriculteurs se rémunère donc à un taux horaire inférieur à 70 % du
                            SMIC</strong
                        >. Pour un quart des exploitations, le revenu dégagé par travailleur est inférieur à <strong>600 euros par mois</strong>, et
                        ce avant même d’éventuelles cotisations sociales. De l’autre côté de l’échelle, les 10 % d’exploitations les plus
                        rémunératrices parviennent à un revenu mensuel avant cotisations supérieur à 5 500 euros.
                    </p>
                    <p>
                        Pour en savoir plus, voir le rapport
                        <c-lien
                            href="https://publications.resiliencealimentaire.org/partie1_un-systeme-defaillant/#_des_agriculteurs_qui_ne_gagnent_pas_leur_vie"
                            >Qui veille grain?</c-lien
                        >.
                    </p>
                </div>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }
}
