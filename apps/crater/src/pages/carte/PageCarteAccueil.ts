import '../commun/template/TemplatePageAccueil.js';
import '../commun/BoiteLienVersPage.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { EchelleTerritoriale } from '../../domaines/carte/EchelleTerritoriale.js';
import { PAGES_PRINCIPALES } from '../../domaines/pages/configuration/declaration-pages.js';
import { construireUrlPageCarte } from '../../domaines/pages/pages-utils';
import { ICONES_SVG } from '../../domaines/systeme-alimentaire/configuration/icones_svg.js';
import { IDS_INDICATEURS } from '../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { STYLES_CRATER } from '../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-carte-accueil': PageCarteAccueil;
    }
}
@customElement('c-page-carte-accueil')
export class PageCarteAccueil extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(var(--dsem) * 10);
            }

            section {
                display: flex;
                flex-direction: column;
                gap: 5vh;
                max-width: var(--largeur-maximum-contenu);
            }

            h1 {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(var(--dsem) * 2);
                text-align: center;
                max-width: 700px;
            }

            h1 > svg {
                fill: var(--couleur-primaire);
            }

            c-boite-lien-vers-page {
                max-width: 800px;
            }

            div[slot='illustration'] svg {
                width: 100px;
                height: 100px;
                fill: var(--couleur-secondaire);
                padding: calc(var(--dsem));
            }
        `
    ];

    render() {
        return html`
            <c-template-page-accueil idItemActifMenuPrincipal=${PAGES_PRINCIPALES.carte.getId()}>
                <main slot="contenu">
                    <h1>Explorez les différents indicateurs de CRATer sous forme de cartes ${unsafeSVG(ICONES_SVG.soulignerTexte)}</h1>
                    <section>
                        <c-boite-lien-vers-page
                            titre="Pesticides"
                            libelleLien="EXPLORER LA CARTE"
                            href=${ifDefined(construireUrlPageCarte(IDS_INDICATEURS.noduNormalise, EchelleTerritoriale.Epcis.id))}
                            encadrement
                        >
                            <div slot="illustration">${unsafeSVG(SA.getIndicateur(IDS_INDICATEURS.noduNormalise)?.icone)}</div>
                            <p slot="texte">
                                On estime que moins de 0,1% des molécules toxiques employées à l'échelle mondiale atteignent effectivement leurs
                                cibles.
                            </p>
                        </c-boite-lien-vers-page>
                        <c-boite-lien-vers-page
                            titre="Haute Valeur Naturelle"
                            libelleLien="EXPLORER LA CARTE"
                            href=${ifDefined(construireUrlPageCarte(IDS_INDICATEURS.scoreHVN, EchelleTerritoriale.Epcis.id))}
                            encadrement
                        >
                            <div slot="illustration">${unsafeSVG(SA.getIndicateur(IDS_INDICATEURS.scoreHVN)?.icone)}</div>
                            <p slot="texte">
                                Un tiers des oiseaux à disparu des milieux agricoles en trente ans et deux tiers des insectes ont déserté les prairies
                                allemandes en seulement dix ans.
                            </p>
                        </c-boite-lien-vers-page>
                        <c-boite-lien-vers-page
                            titre="Superficie moyenne des exploitations"
                            libelleLien="EXPLORER LA CARTE"
                            href=${ifDefined(construireUrlPageCarte(IDS_INDICATEURS.superficiesExploitations, EchelleTerritoriale.Epcis.id))}
                            encadrement
                        >
                            <div slot="illustration">${unsafeSVG(SA.getIndicateur(IDS_INDICATEURS.superficiesExploitations)?.icone)}</div>
                            <p slot="texte">Plus de 50 fermes disparaissent chaque jour.</p>
                        </c-boite-lien-vers-page>
                        <c-boite-lien-vers-page
                            titre="Rythme d’artificialisation"
                            libelleLien="EXPLORER LA CARTE"
                            href=${ifDefined(construireUrlPageCarte(IDS_INDICATEURS.rythmeArtificialisation, EchelleTerritoriale.Epcis.id))}
                            encadrement
                        >
                            <div slot="illustration">${unsafeSVG(SA.getIndicateur(IDS_INDICATEURS.rythmeArtificialisation)?.icone)}</div>
                            <p slot="texte">L'équivalent d'un département moyen est artificialisé tous les 10 ans.</p>
                        </c-boite-lien-vers-page>
                    </section>
                </main>
            </c-template-page-accueil>
        `;
    }
}
