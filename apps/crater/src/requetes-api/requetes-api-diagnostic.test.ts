import { toJson } from '@lga/commun/build/outils/requetes-utils';
import * as fs from 'fs';
import { beforeEach, describe, expect, it } from 'vitest';

import { Diagnostic } from '../domaines/diagnostics';
import { Territoire } from '../domaines/territoires';
import { traduireJsonEnDiagnostic } from './requetes-api-diagnostic';
import { traduireJsonEnTerritoire } from './requetes-api-territoires';

const CARACTERE_SAUT_LIGNE = /\r/g;
const BASEPATH = __dirname;

describe('Test interface API Diagnostic...', () => {
    let territoire: Territoire;
    let diagnostic: Diagnostic;

    beforeEach(() => {
        // TODO : doublon avec le fichier dans crater-api...voir comment utiliser le même, ou bien baser ce test sur un vrai appel a l'api
        const territoireJson = JSON.parse(fs.readFileSync(`${BASEPATH}/__test__/input/territoire_P-FR.json`).toString());
        territoire = traduireJsonEnTerritoire(territoireJson);
        const diagnosticJson = JSON.parse(fs.readFileSync(`${BASEPATH}/__test__/input/diagnostic_P-FR.json`).toString());
        diagnostic = traduireJsonEnDiagnostic(territoire, diagnosticJson);
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.territoire", () => {
        expect(diagnostic.territoire.id).toEqual('P-FR');
        expect(diagnostic.territoire.nom).toEqual('France');
    });

    it("Mapper le retour JSON dans les champs simples de l'objet du domaine Diagnostic", () => {
        expect(diagnostic.superficieHa).toEqual(54873594);
        expect(diagnostic.population).toEqual(64616201);
        expect(diagnostic.idTerritoireParcel).toEqual('1');
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.surfaceAgricoleUtile", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.surfaceAgricoleUtile), `${BASEPATH}/__test__/`, 'diagnostic.surfaceAgricoleUtile.json');
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.terresAgricoles", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.terresAgricoles), `${BASEPATH}/__test__/`, 'diagnostic.terresAgricoles.json');
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.populationAgricole", () => {
        verifierEgaliteAvecFichierTexte(
            toJson(diagnostic.agriculteursExploitations),
            `${BASEPATH}/__test__/`,
            'diagnostic.agriculteursExploitations.json'
        );
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.intrants", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.intrants), `${BASEPATH}/__test__/`, 'diagnostic.intrants.json');
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.production", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.production), `${BASEPATH}/__test__/`, 'diagnostic.production.json');
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.transformationDistribution", () => {
        verifierEgaliteAvecFichierTexte(
            toJson(diagnostic.transformationDistribution),
            `${BASEPATH}/__test__/`,
            'diagnostic.transformationDistribution.json'
        );
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.consommation", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.consommation), `${BASEPATH}/__test__/`, 'diagnostic.consommation.json');
    });
});

function verifierEgaliteAvecFichierTexte(contenuFichierAValider: string, cheminDossierDonneesTest: string, nomFichierResultatAttendu: string) {
    if (!fs.existsSync(cheminDossierDonneesTest + 'output')) {
        fs.mkdirSync(cheminDossierDonneesTest + 'output');
    }
    // écriture d'un fichier avec le contenu pour faciliter les diff avec le resultat attendu en cas d'erreur
    const cheminFichierResultat = cheminDossierDonneesTest + 'output/' + nomFichierResultatAttendu;
    fs.writeFile(cheminFichierResultat, contenuFichierAValider, (error) => {
        if (error) throw error;
    });

    const cheminFichierAttendu = cheminDossierDonneesTest + 'expected/' + nomFichierResultatAttendu;
    const contenuFichierAttendu = fs.readFileSync(cheminFichierAttendu).toString().replace(CARACTERE_SAUT_LIGNE, '');

    if (contenuFichierAValider !== contenuFichierAttendu) {
        throw new Error(
            `Erreur : les données ne sont pas identiques entre le fichier resultat (${cheminFichierResultat}) et le fichier attendu ${cheminFichierAttendu}`
        );
    }
}
