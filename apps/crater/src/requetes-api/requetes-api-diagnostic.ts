import { fetchRetry } from '@lga/commun/build/outils/requetes-utils';

import {
    AgriculteursExploitations,
    CategorieCommerce,
    ClassesAges,
    ClassesSuperficies,
    Consommation,
    ConsommationParCulture,
    ConsommationParGroupeCulture,
    Diagnostic,
    IndicateurConsommation,
    IndicateurHvn,
    IndicateursPesticides,
    IndicateursPesticidesAnneeN,
    IndicateursPesticidesParAnnees,
    IndicateursProximiteCommerces,
    Intrants,
    NOTE_VALEUR_WARNING,
    PolitiqueAmenagement,
    Production,
    SauParCulture,
    SauParGroupeCulture,
    SurfaceAgricoleUtile,
    TerresAgricoles,
    TransformationDistribution
} from '../domaines/diagnostics';
import { Otex } from '../domaines/diagnostics/diagnostics/Otex';
import { HierarchieTerritoires, Territoire } from '../domaines/territoires';

export function chargerDiagnostics(apiBaseUrl: string, hierarchieTerritoires: HierarchieTerritoires): Promise<unknown[]> {
    return Promise.all(hierarchieTerritoires.getListeTerritoires().map((t) => chargerDiagnostic(apiBaseUrl, t.id)));
}

export function chargerDiagnostic(apiBaseUrl: string, idTerritoire: string) {
    return fetchRetry(apiBaseUrl + 'crater/api/diagnostics/' + idTerritoire).then((response) => response.json());
}

export function chargerDiagnosticCsv(apiBaseUrl: string, idTerritoire: string) {
    window.location.href = apiBaseUrl + 'crater/api/diagnostics/csv/' + idTerritoire;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function traduireJsonEnDiagnostic(territoire: Territoire, diagJson: any): Diagnostic {
    const surfaceAgricoleUtile = new SurfaceAgricoleUtile(
        diagJson.surfaceAgricoleUtile.sauTotaleHa,
        diagJson.surfaceAgricoleUtile.sauPeuProductiveHa,
        diagJson.surfaceAgricoleUtile.sauProductiveHa,
        diagJson.surfaceAgricoleUtile.sauBioHa,
        creerListeSauParGroupeCulture(diagJson.surfaceAgricoleUtile.sauParGroupeCulture)
    );
    return new Diagnostic(
        territoire,
        diagJson.population,
        diagJson.superficieHa,
        diagJson.idTerritoireParcel,
        diagJson.nbCommunes,
        new Otex(diagJson.nbCommunesParOtex),
        surfaceAgricoleUtile,
        new TerresAgricoles(
            territoire,
            diagJson.politiqueFonciere.note,
            diagJson.superficieHa,
            diagJson.politiqueFonciere.artificialisation5ansHa,
            diagJson.politiqueFonciere.evolutionMenagesEmplois5ans,
            diagJson.politiqueFonciere.sauParHabitantM2,
            diagJson.politiqueFonciere.rythmeArtificialisationSauPourcent,
            PolitiqueAmenagement.fromString(diagJson.politiqueFonciere.evaluationPolitiqueAmenagement),
            diagJson.politiqueFonciere.partLogementsVacants2013Pourcent,
            diagJson.politiqueFonciere.partLogementsVacants2018Pourcent
        ),
        new AgriculteursExploitations(
            territoire,
            diagJson.populationAgricole.note,
            diagJson.populationAgricole.populationAgricole1988,
            diagJson.populationAgricole.populationAgricole2010,
            diagJson.populationAgricole.partPopulationAgricole1988Pourcent,
            diagJson.populationAgricole.partPopulationAgricole2010Pourcent,
            diagJson.populationAgricole.nbExploitations1988,
            diagJson.populationAgricole.nbExploitations2010,
            diagJson.populationAgricole.sauHa1988,
            diagJson.populationAgricole.sauHa2010,
            diagJson.populationAgricole.sauMoyenneParExploitationHa1988,
            diagJson.populationAgricole.sauMoyenneParExploitationHa2010,
            new ClassesAges(
                diagJson.populationAgricole.nbExploitationsParClassesAgesChefExploitation.moins_40_ans,
                diagJson.populationAgricole.nbExploitationsParClassesAgesChefExploitation.de_40_a_49_ans,
                diagJson.populationAgricole.nbExploitationsParClassesAgesChefExploitation.de_50_a_59_ans,
                diagJson.populationAgricole.nbExploitationsParClassesAgesChefExploitation.plus_60_ans
            ),
            new ClassesSuperficies(
                diagJson.populationAgricole.nbExploitationsParClassesSuperficies.moins_20_ha,
                diagJson.populationAgricole.nbExploitationsParClassesSuperficies.de_20_a_50_ha,
                diagJson.populationAgricole.nbExploitationsParClassesSuperficies.de_50_a_100_ha,
                diagJson.populationAgricole.nbExploitationsParClassesSuperficies.de_100_a_200_ha,
                diagJson.populationAgricole.nbExploitationsParClassesSuperficies.plus_200_ha
            ),
            new ClassesSuperficies(
                diagJson.populationAgricole.sauHaParClassesSuperficies.moins_20_ha,
                diagJson.populationAgricole.sauHaParClassesSuperficies.de_20_a_50_ha,
                diagJson.populationAgricole.sauHaParClassesSuperficies.de_50_a_100_ha,
                diagJson.populationAgricole.sauHaParClassesSuperficies.de_100_a_200_ha,
                diagJson.populationAgricole.sauHaParClassesSuperficies.plus_200_ha
            )
        ),
        new Intrants(
            territoire,
            diagJson.intrants.note,
            new IndicateursPesticides(
                new IndicateursPesticidesParAnnees(creerListeIndicateursPesticidesAnneeN(diagJson.intrants.pesticides.indicateursMoyennesTriennales))
            )
        ),
        new Production(
            territoire,
            diagJson.production.note,
            diagJson.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent,
            diagJson.productionsBesoins.besoinsAssietteActuelle.tauxAdequationMoyenPonderePourcent,
            diagJson.production.noteTauxAdequationMoyenPondere,
            diagJson.production.notePartSauBio,
            diagJson.production.noteHvn,
            new IndicateurHvn(
                diagJson.pratiquesAgricoles.indicateurHvn.indice1,
                diagJson.pratiquesAgricoles.indicateurHvn.indice2,
                diagJson.pratiquesAgricoles.indicateurHvn.indice3,
                diagJson.pratiquesAgricoles.indicateurHvn.indiceTotal
            ),
            diagJson.surfaceAgricoleUtile.sauBioHa,
            diagJson.pratiquesAgricoles.partSauBioPourcent
        ),
        new TransformationDistribution(
            territoire,
            diagJson.proximiteCommerces.note,
            diagJson.proximiteCommerces.partPopulationDependanteVoiturePourcent,
            diagJson.proximiteCommerces.partTerritoireDependantVoiturePourcent,
            creerListeIndicateursProximitesCommerces(diagJson.proximiteCommerces.indicateursParTypesCommerces)
        ),
        new Consommation(
            territoire,
            NOTE_VALEUR_WARNING,
            diagJson.consommation.tauxPauvrete60Pourcent,
            new IndicateurConsommation(
                diagJson.productionsBesoins.besoinsAssietteActuelle.besoinsHa,
                creerListeConsommationParGroupeCulture(
                    diagJson.productionsBesoins.besoinsAssietteActuelle.besoinsParGroupeCulture,
                    diagJson.productionsBesoins.besoinsAssietteActuelle.besoinsHa
                )
            )
        )
    );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function creerListeSauParGroupeCulture(listeSauParGroupeCultureJson: any[]) {
    return listeSauParGroupeCultureJson.map(
        (sauParGroupeCultureJson) =>
            new SauParGroupeCulture(
                sauParGroupeCultureJson.codeGroupeCulture,
                sauParGroupeCultureJson.nomGroupeCulture,
                sauParGroupeCultureJson.sauHa,
                creerListeSauParCulture(sauParGroupeCultureJson.sauParCulture)
            )
    );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function creerListeSauParCulture(listeSauParCultureJson: any[]) {
    return listeSauParCultureJson.map((sauParCulture) => new SauParCulture(sauParCulture.codeCulture, sauParCulture.nomCulture, sauParCulture.sauHa));
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function creerListeConsommationParGroupeCulture(listeConsommationParGroupeCultureJson: any[], consommationTotaleTousGroupesCultures: number) {
    return listeConsommationParGroupeCultureJson.map(
        (consommationParGroupeCultureJson) =>
            new ConsommationParGroupeCulture(
                consommationParGroupeCultureJson.codeGroupeCulture,
                consommationParGroupeCultureJson.nomGroupeCulture,
                consommationParGroupeCultureJson.besoinsHa,
                consommationParGroupeCultureJson.tauxAdequationBrutPourcent,
                consommationTotaleTousGroupesCultures,
                creerListeConsommationParCulture(consommationParGroupeCultureJson.besoinsParCulture)
            )
    );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function creerListeConsommationParCulture(listeConsommationParCultureJson: any[]) {
    return listeConsommationParCultureJson.map(
        (consommationParCultureJson) =>
            new ConsommationParCulture(
                consommationParCultureJson.codeCulture,
                consommationParCultureJson.nomCulture,
                consommationParCultureJson.alimentationAnimale,
                consommationParCultureJson.besoinsHa
            )
    );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function creerListeIndicateursPesticidesAnneeN(listeIndicateursPesticidesAnneeNJson: any[]): IndicateursPesticidesAnneeN[] {
    return listeIndicateursPesticidesAnneeNJson.map(
        (i) => new IndicateursPesticidesAnneeN(i.annee, i.quantiteSubstanceSansDUKg, i.quantiteSubstanceAvecDUKg, i.noduHa, i.noduNormalise)
    );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function creerListeIndicateursProximitesCommerces(listeIndicateursJson: any[]): IndicateursProximiteCommerces[] {
    return listeIndicateursJson
        .filter((i) => CategorieCommerce.listerToutes.find((c) => c.code === i.typeCommerce))
        .map(
            (i) =>
                new IndicateursProximiteCommerces(
                    CategorieCommerce.fromString(i.typeCommerce),
                    i.distancePlusProcheCommerceMetres,
                    i.partPopulationAccesAVeloPourcent,
                    i.partPopulationDependanteVoiturePourcent
                )
        );
}
