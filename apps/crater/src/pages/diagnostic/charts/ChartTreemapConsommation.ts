import './ChartTreemapCultures.js';
import './LegendeChart.js';

import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

import { GroupeCulture } from '../../../domaines/systeme-alimentaire';
import { DonneesTreemapCultures } from './ChartTreemapCultures';
import { ItemLegende, LegendeChart, MOTIF_RAYURE } from './LegendeChart';

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-treemap-consommation': ChartTreemapConsommation;
    }
}

const HAUTEUR_MAX_PIXELS = 350;
const DECALAGE_VERTICAL_TREEMAP_CONSOMMATION_ANIMAUX = 35;

const ITEMS_LEGENDE_GROUPES_CULTURES_CONSOMMATION: ItemLegende[] = GroupeCulture.tous
    .filter((gc) => ['FOU', 'CER', 'OLP'].includes(gc.code))
    .map((g) => {
        return { libelle: `${g.nom} alim. animale (${g.nomCourt})`, couleur: g.couleur, motif: MOTIF_RAYURE };
    })
    .concat(
        GroupeCulture.tous.map((g) => {
            return { libelle: `${g.nom} (${g.nomCourt})`, couleur: g.couleur, motif: '' };
        })
    );

export interface OptionsTreemapConsommation {
    consommationAnimauxHa: number;
    consommationHumainsHa: number;
    donneesTreemapConsommationAnimaux: DonneesTreemapCultures[];
    donneesTreemapConsommationHumains: DonneesTreemapCultures[];
}

@customElement('c-chart-treemap-consommation')
export class ChartTreemapConsommation extends LitElement {
    static styles = css`
        figure {
            margin: auto;
            max-width: 600px;
            text-align: center;
        }

        #chart-treemap-consommation-animaux {
            display: block;
            transform: translate(0, ${DECALAGE_VERTICAL_TREEMAP_CONSOMMATION_ANIMAUX}px);
        }
    `;

    @query('c-legende-chart')
    private legende?: LegendeChart;

    @property({ attribute: false })
    options?: OptionsTreemapConsommation;

    render() {
        if (!this.options) return;

        const consommationHa = this.options.consommationAnimauxHa + this.options.consommationHumainsHa;
        return html`
            <figure>
                <div class="chart-treemap">
                    <c-chart-treemap-cultures
                        .options=${this.calculerOptionsTreemapConsommationAnimaux(this.options, consommationHa)}
                        id="chart-treemap-consommation-animaux"
                    ></c-chart-treemap-cultures>
                    <c-chart-treemap-cultures
                        .options=${this.calculerOptionsTreemapConsommationHumains(this.options, consommationHa)}
                        id="chart-treemap-consommation-humains"
                    ></c-chart-treemap-cultures>
                </div>
                <c-legende-chart .itemsLegende=${ITEMS_LEGENDE_GROUPES_CULTURES_CONSOMMATION}></c-legende-chart>
            </figure>
        `;
    }

    private calculerOptionsTreemapConsommationAnimaux(options: OptionsTreemapConsommation, consommationHa: number) {
        return {
            donnees: options.donneesTreemapConsommationAnimaux!,
            hauteur: this.calculerHauteurTreemapConsommation(options.consommationAnimauxHa, consommationHa),
            activerFillPattern: true
        };
    }

    private calculerOptionsTreemapConsommationHumains(options: OptionsTreemapConsommation, consommationHa: number) {
        return {
            donnees: options.donneesTreemapConsommationHumains,
            hauteur: this.calculerHauteurTreemapConsommation(options.consommationHumainsHa, consommationHa)
        };
    }

    private calculerHauteurTreemapConsommation(consommation: number, consommationHa: number) {
        const hauteurTreemapConsommation = HAUTEUR_MAX_PIXELS + DECALAGE_VERTICAL_TREEMAP_CONSOMMATION_ANIMAUX / 2 + 3;
        return (consommation * hauteurTreemapConsommation) / consommationHa;
    }
}
