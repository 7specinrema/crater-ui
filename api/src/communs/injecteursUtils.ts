import fs = require('fs');
import rd = require('readline');
import { logErreur, logInfo } from './logUtils';

export type TraiterCellules = (cellules: Map<string, string>) => void;

export function lireCelluleString(map: Map<string, string>, cle: string): string {
    const contenuCellule = map.get(cle);
    if (contenuCellule === undefined) {
        throw new Error(`Erreur critique : la colonne ${cle} ne correspond pas à un nom de colonne dans le fichier`);
    }
    return contenuCellule;
}

export function lireCelluleInt(map: Map<string, string>, cle: string): number {
    const contenuCellule = lireCelluleString(map, cle);
    return Math.round(parseFloat(contenuCellule));
}

export function lireCelluleIntOuNull(map: Map<string, string>, cle: string): number | null {
    const contenuCellule = lireCelluleString(map, cle);
    return contenuCellule === '' ? null : Math.round(parseFloat(contenuCellule));
}

export function lireCelluleFloatOuNull(map: Map<string, string>, cle: string): number | null {
    const contenuCellule = lireCelluleString(map, cle);
    return contenuCellule === '' ? null : Number.parseFloat(contenuCellule);
}

export class InjecteurCsv {
    public numLigneCourante = 0;
    public nomColonnes: Array<string> = [];
    private messageErreur?: string;

    public static readonly SEPARATEUR_POINT_VIRGULE = ';';
    public static readonly SEPARATEUR_VIRGULE = ',';

    async injecter(fichierDonnees: string, traiterCellules: TraiterCellules) {
        this.numLigneCourante = 0;

        return this.traiterFichier(fichierDonnees, traiterCellules)
            .then(() => {
                logInfo(`[${new Date().toUTCString()}'] Chargement fait pour le fichier ${fichierDonnees}, ${this.numLigneCourante} lignes lues`);
            })
            .catch((reason) => {
                logErreur(`ERREUR lors du chargement du fichier ${fichierDonnees}, ${this.numLigneCourante} lignes lues\n${reason}`);
                return Promise.reject(reason);
            });
    }

    private traiterFichier(fichierDonnees: string, traiterCellules: TraiterCellules): Promise<void> {
        const readStream = fs.createReadStream(fichierDonnees);

        const reader = rd.createInterface(readStream);
        readStream.on('error', (erreur: Error): void => {
            this.messageErreur = `Erreur lors de la lecture dans le fichier ${fichierDonnees}.\n${erreur.message}`;
            logErreur(this.messageErreur);
            reader.emit('close');
        });

        reader.on('line', (ligne: string): void => {
            if (!ligne || ligne == '') {
                return;
            }
            this.numLigneCourante = this.numLigneCourante + 1;
            const cellules = ligne.split(InjecteurCsv.SEPARATEUR_POINT_VIRGULE);

            if (this.numLigneCourante === 1) {
                this.nomColonnes = [];
                cellules.forEach((c) => this.nomColonnes.push(c));
            } else {
                const mapCellules: Map<string, string> = new Map<string, string>();
                for (let i = 0; i < cellules.length; i++) {
                    mapCellules.set(this.nomColonnes[i], cellules[i]);
                }
                traiterCellules(mapCellules);
            }
        });

        return new Promise((resolve, reject) => {
            reader
                .on('close', (): void => {
                    if (this.messageErreur) {
                        logErreur('Erreur callbackFinFichier :' + this.messageErreur);
                        reject(this.messageErreur);
                    } else {
                        resolve();
                    }
                })
                .on('error', (e): void => {
                    reject(e);
                });
        });
    }
}
