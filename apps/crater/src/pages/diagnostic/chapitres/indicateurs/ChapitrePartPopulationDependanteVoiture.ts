import '../../charts/ChartBarres.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import { OptionsChartBarres } from '../../charts/ChartBarres.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-population-dependante-voiture': ChapitrePartPopulationDependanteVoiture;
    }
}

@customElement('c-chapitre-part-population-dependante-voiture')
export class ChapitrePartPopulationDependanteVoiture extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                .options=${this.calculerOptionsIndicateurDetaillePartPopulationDependanteVoiture(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="6"
            >
                <c-chart-barres
                    .options=${this.calculerOptionsChartPartPopulationDependanteVoiture(this.rapport)}
                    slot="chart-indicateur-detaille"
                ></c-chart-barres>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetaillePartPopulationDependanteVoiture(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partPopulationDependanteVoiture)!,
            message:
                rapport.getDiagnosticActif()?.transformationDistribution.messagePartPopulationDependanteVoiture +
                `<br><br>Le graphique suivant présente la part de la population théoriquement dépendante de la voiture pour ses achats alimentaires pour chaque type de commerces :`,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }

    private calculerOptionsChartPartPopulationDependanteVoiture(rapport: Rapport): OptionsChartBarres {
        return {
            series: new SerieMultiple(
                [
                    new Serie(
                        rapport
                            .getDiagnosticActif()
                            ?.transformationDistribution.indicateursProximiteCommercesParTypesCommerces.map(
                                (e) => e.partPopulationDependanteVoiturePourcent
                            ),
                        rapport.getDiagnosticActif()?.territoire.nom
                    ),
                    new Serie(
                        rapport
                            .getDiagnosticActif()
                            ?.transformationDistribution.indicateursProximiteCommercesParTypesCommerces.map(
                                (e) => e.partPopulationDependanteVoiturePourcent
                            ),
                        rapport.getDiagnosticPays()?.territoire.nom
                    )
                ],
                rapport
                    .getDiagnosticActif()
                    ?.transformationDistribution.indicateursProximiteCommercesParTypesCommerces.map((e) => e.categorieCommerce.libelle)
            ),
            nomAxeOrdonnees: 'Part de la population [%]',
            maxY: 100
        };
    }
}
