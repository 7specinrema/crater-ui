import { Territoire } from '../../territoires';
import { AgriculteursExploitations } from './agriculteurs-exploitations';
import { Consommation } from './consommation';
import { Intrants } from './intrants';
import { calculerMessageMetaDescriptionDiagnostic, calculerMessageOtex, calculerMessageTerritoire } from './messages-diagnostic';
import { Otex } from './Otex';
import { Production } from './production';
import { SurfaceAgricoleUtile } from './SurfaceAgricoleUtile';
import { TerresAgricoles } from './terres-agricoles';
import { TransformationDistribution } from './transformation-distribution';

export class Diagnostic {
    public readonly territoire: Territoire;
    public readonly population: number;
    public readonly superficieHa: number;
    public readonly idTerritoireParcel: string | null;
    public readonly nbCommunes: number;
    public readonly otex: Otex;
    public readonly surfaceAgricoleUtile: SurfaceAgricoleUtile;
    public readonly terresAgricoles: TerresAgricoles;
    public readonly agriculteursExploitations: AgriculteursExploitations;
    public readonly intrants: Intrants;
    public readonly production: Production;
    public readonly transformationDistribution: TransformationDistribution;
    public readonly consommation: Consommation;
    private diagnosticPays?: Diagnostic;

    constructor(
        territoire: Territoire,
        population: number,
        superficieHa: number,
        idTerritoireParcel: string | null,
        nbCommunes: number,
        otex: Otex,
        surfaceAgricoleUtile: SurfaceAgricoleUtile,
        terresAgricoles: TerresAgricoles,
        agriculteursExploitations: AgriculteursExploitations,
        intrants: Intrants,
        production: Production,
        proximiteCommerces: TransformationDistribution,
        consommation: Consommation
    ) {
        this.territoire = territoire;
        this.population = population;
        this.superficieHa = superficieHa;
        this.idTerritoireParcel = idTerritoireParcel;
        this.nbCommunes = nbCommunes;
        this.otex = otex;
        this.surfaceAgricoleUtile = surfaceAgricoleUtile;
        this.terresAgricoles = terresAgricoles;
        this.agriculteursExploitations = agriculteursExploitations;
        this.intrants = intrants;
        this.production = production;
        this.transformationDistribution = proximiteCommerces;
        this.consommation = consommation;
    }

    get densitePopulationHabParKM2() {
        return (this.population * 100) / this.superficieHa;
    }

    // TODO : deplacer dans SurfaceAgricoleUtile, ou bien creer une classe SuperficieTerritoires pour rassembler superficieHa, sauTotale
    get pourcentageSauTotaleSurSuperficie() {
        return (this.surfaceAgricoleUtile.sauTotaleHa / this.superficieHa) * 100;
    }

    get messageTerritoire() {
        return calculerMessageTerritoire(
            this.territoire.categorie,
            this.densitePopulationHabParKM2,
            this.pourcentageSauTotaleSurSuperficie,
            this.otex.nbCommunesOtexMajoritaire5Postes
        );
    }

    get messageOtex() {
        return calculerMessageOtex(this.territoire, this.otex.nbCommunesOtexMajoritaire5Postes);
    }

    get messageMetaDescription() {
        return calculerMessageMetaDescriptionDiagnostic(
            this.territoire.nom,
            this.terresAgricoles.note,
            this.agriculteursExploitations.note,
            this.intrants.note,
            this.production.note,
            this.transformationDistribution.note,
            this.consommation.note
        );
    }

    setDiagnosticPays(diagnosticPays: Diagnostic | undefined) {
        if (diagnosticPays !== undefined) {
            this.diagnosticPays = diagnosticPays;
            this.agriculteursExploitations.setAgriculteursExploitationsPays(diagnosticPays.agriculteursExploitations);
            this.intrants.setIntrantsPays(diagnosticPays.intrants);
            this.production.setProductionPays(diagnosticPays.production);
            this.terresAgricoles.setTerresAgricolesPays(diagnosticPays.terresAgricoles);
            this.transformationDistribution.setTransformationDistributionPays(diagnosticPays.transformationDistribution);
            this.consommation.setConsommationPays(diagnosticPays.consommation);
        }
    }

    get rapportDensitePopulationHabParM2SurPays() {
        return this.densitePopulationHabParKM2 / this.diagnosticPays!.densitePopulationHabParKM2;
    }
}
