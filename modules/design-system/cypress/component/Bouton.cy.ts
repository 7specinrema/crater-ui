import '../../src/composants/Bouton.js';
import '../../public/theme-defaut/theme-defaut.css';

import chaiColors from 'chai-colors';
import { html } from 'lit';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { Bouton } from '../../src/composants/Bouton';
import { VALEURS_COULEURS } from './outils_tests';

// permet de comparer des couleurs hex rgb, etc...
chai.use(chaiColors);

//TODO : simplifier un peu et tester la propriété tailleReduite si encore là + icone seulement

const ICONE = `<svg slot="icone" viewbox="6 0 10 20" xmlns="http://www.w3.org/2000/svg" version="1.1" style="stroke-width: 0px;">
                    <path d="m17.08898,9.44945l-4.12,-4.40446c-0.28372,-0.30331 -0.74298,-0.30331 -1.01311,0c-0.28372,0.30331 -0.28372,0.79427 0,1.08307l2.90426,3.10478l-9.67188,0c-0.39169,0 -0.71594,0.34661 -0.71594,0.76536s0.32422,0.76536 0.71594,0.76536l9.65837,0l-2.89075,3.10478c-0.28372,0.30331 -0.28372,0.79427 0,1.08307c0.13512,0.14445 0.32422,0.23108 0.51334,0.23108c0.18911,0 0.36474,-0.07222 0.51334,-0.23108l4.13351,-4.4189c0.13512,-0.14445 0.21615,-0.34661 0.21615,-0.54878c-0.02704,-0.18766 -0.10808,-0.38983 -0.2432,-0.53428l-0.00003,0l0.00001,-0.00001z"/>
                  </svg>`;

describe('Test composant Bouton', () => {
    it('Position icone et texte', () => {
        // given
        cy.viewport(800, 800);
        cy.mount<'c-bouton'>(html`
            <c-bouton id="icone-a-gauche" libelle="Icone à gauche" positionIcone="${Bouton.POSITION_ICONE.gauche}"> ${unsafeSVG(ICONE)} </c-bouton>
        `);
        cy.mount('<br>');
        cy.mount<'c-bouton'>(html`
            <c-bouton id="icone-a-droite" libelle="Icône à droite" positionIcone="${Bouton.POSITION_ICONE.droite}"> ${unsafeSVG(ICONE)} </c-bouton>
        `);
        cy.mount('<br>');
        cy.mount<'c-bouton'>(html`<c-bouton id="icone-seule"> ${unsafeSVG(ICONE)} </c-bouton> `);

        // then
        cy.get('#icone-a-gauche').find('p').should('be.visible').should('contain.text', 'Icone à gauche');
        cy.get('#icone-a-gauche').find('svg').should('be.visible');
        cy.get('#icone-a-gauche').find('slot').next('p');

        cy.get('#icone-a-droite').find('p').next('slot');

        cy.get('#icone-seule').find('p').should('not.exist');
        cy.get('#icone-seule').find('svg').should('exist');
    });

    it('Etats bouton type relief', () => {
        // given
        cy.viewport(800, 800);
        cy.mount<'c-bouton'>(html`<c-bouton id="bouton-active" libelle="Bouton activé" type="${Bouton.TYPE.relief}"></c-bouton>`);
        cy.mount('<br>');
        cy.mount<'c-bouton'>(html`<c-bouton id="bouton-desactive" libelle="Bouton desactivé" type="${Bouton.TYPE.relief}" desactive></c-bouton>`);
        // then
        cy.get('#bouton-active').find('a').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_primaire);
        cy.get('#bouton-active').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_blanc);
        // rq : pas de moyen de tester facilement le css lors d'un hover ou d'un click (active) avec cypress

        cy.get('#bouton-desactive').find('a').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_neutre_clair);
        cy.get('#bouton-desactive').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_neutre);
    });

    it('Etats bouton type encadre', () => {
        // given
        cy.viewport(800, 800);
        cy.mount<'c-bouton'>(html`<c-bouton id="bouton-active" libelle="Bouton activé" type="${Bouton.TYPE.encadre}">${unsafeSVG(ICONE)}</c-bouton>`);
        cy.mount('<br>');
        cy.mount<'c-bouton'>(
            html`<c-bouton id="bouton-desactive" libelle="Bouton desactivé" type="${Bouton.TYPE.encadre}" desactive>${unsafeSVG(ICONE)}</c-bouton>`
        );
        // then
        cy.get('#bouton-active').find('a').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_blanc);
        cy.get('#bouton-active').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_primaire);
        cy.get('#bouton-active').find('a').should('have.css', 'border-color').and('be.colored', VALEURS_COULEURS.couleur_primaire);
        // rq : pas de moyen de tester facilement le css lors d'un hover ou d'un click (active) avec cypress

        cy.get('#bouton-desactive').find('a').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_blanc);
        cy.get('#bouton-desactive').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_neutre);
        cy.get('#bouton-desactive').find('a').should('have.css', 'border-color').and('be.colored', VALEURS_COULEURS.couleur_neutre);
    });

    it('Etats bouton type plat', () => {
        // given
        cy.viewport(800, 800);
        cy.mount<'c-bouton'>(html`<c-bouton id="bouton-active" libelle="Bouton activé" type="${Bouton.TYPE.plat}"></c-bouton>`);
        cy.mount('<br>');
        cy.mount<'c-bouton'>(html`<c-bouton id="bouton-desactive" libelle="Bouton desactivé" type="${Bouton.TYPE.plat}" desactive></c-bouton>`);
        // then
        // cy.get('#bouton-plat').find('a').should('not.have.css', 'background-color') On ne fait pas cette vérif, ne marche pas avec cypress
        cy.get('#bouton-active').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_primaire);
        cy.get('#bouton-active').find('a').should('have.css', 'border-width').and('equal', '2px');
        // rq : pas de moyen de tester facilement le css lors d'un hover ou d'un click (active) avec cypress

        cy.get('#bouton-desactive').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_neutre);
    });

    it('Themes de couleurs', () => {
        // given
        cy.viewport(800, 800);
        cy.mount<'c-bouton'>(
            html`<c-bouton
                id="relief-primaire"
                libelle="Relief primaire"
                href="/test"
                type="${Bouton.TYPE.relief}"
                themeCouleur="${Bouton.THEME_COULEUR.primaire}"
            ></c-bouton>`
        );
        cy.mount('<br>');
        cy.mount<'c-bouton'>(
            html`<c-bouton
                id="relief-neutre"
                libelle="Relief neutre"
                href="/test"
                type="${Bouton.TYPE.relief}"
                themeCouleur="${Bouton.THEME_COULEUR.neutre}"
            ></c-bouton>`
        );
        cy.mount('<br>');
        cy.mount<'c-bouton'>(
            html`<c-bouton
                id="relief-accent"
                libelle="Relief accent"
                href="/test"
                type="${Bouton.TYPE.relief}"
                themeCouleur="${Bouton.THEME_COULEUR.accent}"
            ></c-bouton>`
        );
        cy.mount('<br>');
        cy.mount('<br>');
        cy.mount<'c-bouton'>(
            html`<c-bouton
                id="encadre-primaire"
                libelle="Encadre primaire"
                href="/test"
                type="${Bouton.TYPE.encadre}"
                themeCouleur="${Bouton.THEME_COULEUR.primaire}"
            ></c-bouton>`
        );
        cy.mount('<br>');
        cy.mount<'c-bouton'>(
            html`<c-bouton
                id="encadre-neutre"
                libelle="Encadre neutre"
                href="/test"
                type="${Bouton.TYPE.encadre}"
                themeCouleur="${Bouton.THEME_COULEUR.neutre}"
            ></c-bouton>`
        );
        cy.mount('<br>');
        cy.mount<'c-bouton'>(
            html`<c-bouton
                id="encadre-accent"
                libelle="Encadre accent"
                href="/test"
                type="${Bouton.TYPE.encadre}"
                themeCouleur="${Bouton.THEME_COULEUR.accent}"
            ></c-bouton>`
        );
        cy.mount('<br>');
        cy.mount('<br>');
        cy.mount<'c-bouton'>(
            html`<c-bouton
                id="plat-primaire"
                libelle="Plat primaire"
                href="/test"
                type="${Bouton.TYPE.plat}"
                themeCouleur="${Bouton.THEME_COULEUR.primaire}"
            ></c-bouton>`
        );
        cy.mount('<br>');
        cy.mount<'c-bouton'>(
            html`<c-bouton
                id="plat-neutre"
                libelle="Plat neutre"
                href="/test"
                type="${Bouton.TYPE.plat}"
                themeCouleur="${Bouton.THEME_COULEUR.neutre}"
            ></c-bouton>`
        );
        cy.mount('<br>');
        cy.mount<'c-bouton'>(
            html`<c-bouton
                id="plat-accent"
                libelle="Plat accent"
                href="/test"
                type="${Bouton.TYPE.plat}"
                themeCouleur="${Bouton.THEME_COULEUR.accent}"
            ></c-bouton>`
        );

        // then
        cy.get('#relief-primaire').find('a').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_primaire);
        cy.get('#relief-neutre').find('a').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_neutre_80);
        cy.get('#relief-accent').find('a').should('have.css', 'background-color').and('be.colored', VALEURS_COULEURS.couleur_accent);
        cy.get('#encadre-primaire').find('a').should('have.css', 'border-color').and('be.colored', VALEURS_COULEURS.couleur_primaire);
        cy.get('#encadre-neutre').find('a').should('have.css', 'border-color').and('be.colored', VALEURS_COULEURS.couleur_neutre_80);
        cy.get('#encadre-accent').find('a').should('have.css', 'border-color').and('be.colored', VALEURS_COULEURS.couleur_accent);
        cy.get('#plat-primaire').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_primaire);
        cy.get('#plat-neutre').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_neutre_80);
        cy.get('#plat-accent').find('p').should('have.css', 'color').and('be.colored', VALEURS_COULEURS.couleur_accent);
    });

    it('Bouton sur mobile affiche le libelle court', () => {
        // given
        cy.viewport(800, 800);
        cy.mount<'c-bouton'>(html`<c-bouton libelle="Libellé long" libelleCourt="Libellé court"></c-bouton>`);
        // then
        cy.get('c-bouton').contains('Libellé long');

        // when
        cy.viewport(500, 800);
        // then
        cy.get('c-bouton').contains('Libellé court');
    });
});
