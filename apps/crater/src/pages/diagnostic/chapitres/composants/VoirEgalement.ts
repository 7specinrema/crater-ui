import '@lga/design-system/build/composants/Lien.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { construireUrlPageDiagnosticMaillon } from '../../../../domaines/pages/pages-utils';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire';

declare global {
    interface HTMLElementTagNameMap {
        'c-voir-egalement': VoirEgalement;
    }
}

export interface LienIndicateur {
    question: string;
    idIndicateur: string;
}

@customElement('c-voir-egalement')
export class VoirEgalement extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            #encart {
                padding-bottom: 0.5rem;
            }

            ul {
                padding-left: 2rem;
                list-style: none;
            }

            li:hover {
                color: var(--couleur-accent);
                text-decoration: underline;
            }
        `
    ];

    @property({ attribute: false })
    liensIndicateurs: LienIndicateur[] = [];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    render() {
        return html` <div id="encart">
            <p>Voir également :</p>
            <ul>
                ${this.liensIndicateurs.map(
                    (l) => html` <li>
                        <c-lien
                            href="${construireUrlPageDiagnosticMaillon(
                                SA.getIndicateur(l.idIndicateur)?.maillon.id ?? '',
                                this.donneesPage?.idTerritoirePrincipal ?? '',
                                this.donneesPage?.idEchelleTerritoriale,
                                `indicateur-resume-${l.idIndicateur}`
                            )}"
                        >
                            → ${unsafeHTML(l.question)}
                        </c-lien>
                    </li>`
                )}
            </ul>
        </div>`;
    }
}
