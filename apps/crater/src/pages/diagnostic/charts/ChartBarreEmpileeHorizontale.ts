import { arrondirANDecimales, formaterNombreEnEntierString, sommeArray } from '@lga/commun/build/outils/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/src/styles/styles-breakpoints';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { styleMap } from 'lit/directives/style-map.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-barre-empilee-horizontale': ChartBarreEmpileeHorizontale;
    }
}

export interface ItemBarreEmpileeHorizontale {
    valeur: number;
    libelle: string;
    couleur: string;
    part?: number;
}

@customElement('c-chart-barre-empilee-horizontale')
export class ChartBarreEmpileeHorizontale extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100%;
                height: 100%;
                padding: 30px 100px 15px 100px;
                position: relative;
            }

            #barre {
                display: inline-flex;
                width: 100%;
                border-radius: 5px;
                overflow: hidden;
            }

            .barre-item {
                width: 100%;
                display: block;
            }

            .tooltip {
                visibility: hidden;
                display: block;
                background-color: var(--couleur-neutre);
                color: var(--couleur-blanc);
                text-align: left;
                border-radius: 5px;
                padding: 2px 5px;
                position: absolute;
                top: 0;
                z-index: 1;
            }

            .barre-item:hover .tooltip {
                visibility: visible;
            }

            #legende {
                width: 100%;
                padding-top: 10px;
            }

            .legende-item {
                display: inline-flex;
                align-items: center;
                padding-right: 10px;
                position: relative;
            }

            .legende-item .tooltip {
                left: 0;
                width: 100%;
                text-align: center;
            }

            .legende-item:hover .tooltip {
                visibility: visible;
            }

            .legende-item-carre-couleur {
                width: 10px;
                height: 10px;
                border-radius: 1px;
                margin-right: 5px;
            }

            .legende-item-texte {
                display: inline-flex;
                align-items: baseline;
                gap: 5px;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                :host {
                    padding: 10px;
                }

                .tooltip {
                    display: none;
                }
            }
        `
    ];

    @property({ attribute: false })
    items: ItemBarreEmpileeHorizontale[] = [];

    @property()
    unite = '';

    parts: number[] = [];

    render() {
        const totalValeurs = sommeArray(this.items.map((i) => i.valeur))!;
        if (totalValeurs > 0) {
            this.items.forEach((item) => (item.part = arrondirANDecimales((item.valeur / totalValeurs) * 100)), 1);
            this.items[0].part = arrondirANDecimales(100 - sommeArray(this.items.slice(1).map((i) => i.part!))!, 1);
        }

        return html`
            <div id="barre">
                ${this.items?.map(
                    (item) =>
                        html`
                            <div class="barre-item" style=${styleMap({ width: `${item.part}%`, backgroundColor: `${item.couleur}` })}>
                                &nbsp;
                                <div class="tooltip texte-moyen">
                                    ${item.libelle} (${formaterNombreEnEntierString(item.valeur)}${this.unite !== '' ? ' ' + this.unite : ''})
                                </div>
                            </div>
                        `
                )}
            </div>
            <div id="legende">
                ${this.items?.map(
                    (item) =>
                        html`
                            <div class="legende-item">
                                <div class="legende-item-carre-couleur" style=${styleMap({ backgroundColor: `${item.couleur}` })}></div>
                                <div class="legende-item-texte">
                                    <div class="texte-moyen">${item.libelle}</div>
                                    <div class="texte-titre">${item.part}%</div>
                                </div>
                                <div class="tooltip texte-petit">
                                    ${formaterNombreEnEntierString(item.valeur)}${this.unite !== '' ? ' ' + this.unite : ''}
                                </div>
                            </div>
                        `
                )}
            </div>
        `;
    }
}
