import { GradientCouleurs, Indicateur, ParametresIndicateur, StyleIndicateur, SystemeAlimentaire } from '..';
import { ICONES_SVG } from './icones_svg';
import { IDS_MAILLONS } from './maillons';
import { sourcesDonnees } from './sources-donnees';

export const IDS_INDICATEURS = {
    noteTerresAgricoles: 'score-maillon-terres-agricoles',
    sauParHabitant: 'surface-agricole-utile-par-habitant',
    politiqueAmenagement: 'politique-amenagement',
    rythmeArtificialisation: 'rythme-artificialisation',

    noteAgriculteursExploitations: 'score-maillon-agriculteurs-exploitations',
    partActifsAgricoles: 'part-actifs-agricoles',
    revenuAgriculteurs: 'revenu-agriculteurs',
    agesChefsExploitation: 'ages-chefs-exploitations',
    superficiesExploitations: 'superficies-exploitations',
    partLogementsVacants: 'part-logements-vacants',

    noteProduction: 'score-maillon-production',
    adequationTheoriqueProductionConsommation: 'adequation-theorique-production-consommation',
    partProductionExportee: 'part-production-exportee',
    partSAUBio: 'part-surface-agricole-utile-en-bio',
    scoreHVN: 'score-haute-valeur-naturelle',

    noteIntrants: 'score-maillon-intrants',
    qsaNodu: 'pesticides-quantites-utilisees',
    noduNormalise: 'pesticides-intensite-usage',

    noteTransformationDistribution: 'score-maillon-transformation-distribution',
    partPopulationDependanteVoiture: 'part-population-dependante-voiture',
    partTerritoireDependantVoiture: 'part-territoire-dependant-voiture',
    distancesPlusProchesCommerces: 'distances-plus-proches-commerces',

    noteConsommation: 'score-maillon-consommation',
    partAlimentationAnimaleDansConsommation: 'part-alimentation-animale-dans-consommation',
    tauxPauvrete: 'taux-pauvrete',
    aideAlimentaire: 'aide-alimentaire',
    partPopulationObese: 'part-population-obese'
};

const BEIGE = 'fff4d7';
const MARRON = '#635452';
const BLEU = '#5f9ea0';
const VERT = '#1b4d3e';
const BLEU_PETROLE = '#014160';
const ORANGE = '#851605';

export const stylesIndicateurNotes = new StyleIndicateur({
    nbDecimales: 0,
    gradientCouleurs: new GradientCouleurs([
        { valeur: 0, couleur: '#9b0000' },
        { valeur: 5, couleur: '#f2994a' },
        { valeur: 10, couleur: '#219653' }
    ]),
    couleurTexte: 'white',
    labels: [
        { valeur: 0, label: '0' },
        { valeur: 2, label: '2' },
        { valeur: 4, label: '4' },
        { valeur: 6, label: '6' },
        { valeur: 8, label: '8' },
        { valeur: 10, label: '10' }
    ],
    typeJauge: 'NOTE'
});

export function creerIndicateurs(systemeAlimentaire: SystemeAlimentaire): Indicateur[] {
    const donneesIndicateurs: ParametresIndicateur[] = [
        {
            id: IDS_INDICATEURS.noteTerresAgricoles,
            idMaillon: IDS_MAILLONS.terresAgricoles,
            libelle: 'Score du maillon Terres agricoles',
            description: `Cette note évalue la capacité du territoire à préserver ses terres agricoles, en particulier face à l'artificialisation. Elle se base sur la surface agricole par habitant et le rythme d'artificialisation des sols`,
            icone: ICONES_SVG.note,
            indicateurCarte: {
                style: stylesIndicateurNotes,
                nomIndicateurRequeteApi: 'politiqueFonciere.note'
            },
            indicateurNote: true
        },
        {
            id: IDS_INDICATEURS.sauParHabitant,
            idMaillon: IDS_MAILLONS.terresAgricoles,
            libelle: 'Surface agricole utile par habitant',
            description: `Cet indicateur permet d'appréhender la capacité de production du territoire. Une surface d'environ 4000 m²/hab 
                est nécessaire pour le régime alimentaire actuel, 2500 m²/hab pour un régime alimentaire moins riche en protéines et produits animaux et 
                1700 m²/hab pour un régime alimentaire très végétal (voir figure 33 page 143 du <c-lien href="https://resiliencealimentaire.org/page-telechargement-guide/" target="_blank">guide</c-lien> basée sur la figure 2 page 31 de Solagro (2019), 
                Le revers de notre assiette. Changer d’alimentation pour préserver notre santé et notre environnement.).`,
            icone: ICONES_SVG.sauParHabitant,
            legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>,
            à partir du ${sourcesDonnees.rpg.nom} (${sourcesDonnees.rpg.source}, ${sourcesDonnees.rpg.annees})
             et de la ${sourcesDonnees.population_totale.nom} (${sourcesDonnees.population_totale.source}, ${sourcesDonnees.population_totale.annees})`,
            indicateurCarte: {
                unite: 'm²/hab',
                description: `Cet indicateur permet d'appréhender la capacité de production du territoire. Une surface d'environ 4000 m²/hab 
                est nécessaire pour le régime alimentaire actuel, 2500 m²/hab pour un régime alimentaire moins riche en protéines et produits animaux et 
                1700 m²/hab pour un régime alimentaire très végétal.`,
                style: new StyleIndicateur({
                    nbDecimales: 1,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: '#ffffff' },
                        { valeur: 4000, couleur: MARRON }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 1000, label: '1000' },
                        { valeur: 2000, label: '2000' },
                        { valeur: 3000, label: '3000' },
                        { valeur: 4000, label: '4000+' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'politiqueFonciere.sauParHabitantM2'
            }
        },
        {
            id: IDS_INDICATEURS.rythmeArtificialisation,
            idMaillon: IDS_MAILLONS.terresAgricoles,
            libelle: "Rythme d'artificialisation",
            description: `Cet indicateur correspond au ratio entre le nombre d'hectares agricoles, naturels et forestiers qui ont été 
                artificialisés en cinq ans et la surface agricole utile productive. 
                Cette valeur doit tendre vers zéro dans le cadre d'un objectif de zéro artificialisation.`,
            icone: ICONES_SVG.rythmeArtificialisation,
            legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
            du ${sourcesDonnees.rpg.nom} (${sourcesDonnees.rpg.source}, ${sourcesDonnees.rpg.annees})
             et des données d'${sourcesDonnees.artificialisation_sols.nom} (${sourcesDonnees.artificialisation_sols.source}, ${sourcesDonnees.artificialisation_sols.annees}).`,
            indicateurCarte: {
                unite: '%',
                style: new StyleIndicateur({
                    nbDecimales: 1,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: '#ffffff' },
                        { valeur: 5, couleur: MARRON }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 1, label: '1' },
                        { valeur: 2, label: '2' },
                        { valeur: 3, label: '3' },
                        { valeur: 4, label: '4' },
                        { valeur: 5, label: '5+' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'politiqueFonciere.rythmeArtificialisationSauPourcent'
            }
        },
        {
            id: IDS_INDICATEURS.politiqueAmenagement,
            idMaillon: IDS_MAILLONS.terresAgricoles,
            libelle: "Politique d'aménagement",
            description: `Cet indicateur, qualitatif, permet d'appréhender le respect de l'objectif national de 
                de <c-lien href="https://www.ecologie.gouv.fr/artificialisation-des-sols" target="_blank">Zéro Artificialisation Nette</c-lien>.
                Il se base sur le total d'hectares agricoles, naturels et forestiers qui ont été artificialisés sur une période donnée 
                au regard de l'évolution du nombre de ménages et d'emplois.`,
            icone: ICONES_SVG.politiqueAmenagement
        },
        {
            id: IDS_INDICATEURS.partLogementsVacants,
            idMaillon: IDS_MAILLONS.terresAgricoles,
            libelle: 'Part de logements vacants',
            description: `La part de logements vacants correspond au ratio entre le nombre de logements vacants et le nombre total
                de logements. La part de logements vacants peut notamment être mise en regard du rythme d'artificialisation car 
                il arrive souvent que des terres soient artificialisées alors que de nombreux logements sont vacants.`,
            icone: ICONES_SVG.partLogementsVacants,
            legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
            du ${sourcesDonnees.logements_totaux_et_vacants.nom} (${sourcesDonnees.logements_totaux_et_vacants.source}, ${sourcesDonnees.logements_totaux_et_vacants.annees}).`,
            indicateurCarte: {
                unite: '%',
                style: new StyleIndicateur({
                    nbDecimales: 0,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: '#ffffff' },
                        { valeur: 15, couleur: MARRON }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 5, label: '5' },
                        { valeur: 10, label: '10' },
                        { valeur: 15, label: '15+' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'politiqueFonciere.partLogementsVacants2018Pourcent',
                legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
                du ${sourcesDonnees.logements_totaux_et_vacants.nom} (${sourcesDonnees.logements_totaux_et_vacants.source}, ${sourcesDonnees.logements_totaux_et_vacants.anneeLaPlusRecente}).`
            }
        },

        {
            id: IDS_INDICATEURS.noteAgriculteursExploitations,
            idMaillon: IDS_MAILLONS.agriculteursExploitations,
            libelle: 'Score du maillon Agriculteurs et exploitations',
            description: `Cette note évalue l'effectif et la dynamique de renouvellement de la population agricole. Elle se base sur la part d'actifs agricoles dans la population et son évolution sur 30 années passées.`,
            icone: ICONES_SVG.note,
            indicateurCarte: {
                style: stylesIndicateurNotes,
                nomIndicateurRequeteApi: 'populationAgricole.note'
            },
            indicateurNote: true
        },
        {
            id: IDS_INDICATEURS.partActifsAgricoles,
            idMaillon: IDS_MAILLONS.agriculteursExploitations,
            libelle: 'Part des actifs agricoles permanents dans la population totale',
            description: 'Cet indicateur correspond au ratio entre le nombre de travailleurs agricoles permanents et la population totale.',
            icone: ICONES_SVG.partActifsAgricoles,
            legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
            des ${sourcesDonnees.actifs_agricoles_permanents.nom} (${sourcesDonnees.actifs_agricoles_permanents.source}, ${sourcesDonnees.actifs_agricoles_permanents.annees})
             et de la ${sourcesDonnees.population_totale.nom} (${sourcesDonnees.population_totale.source}, ${sourcesDonnees.population_totale.annees})`,
            indicateurCarte: {
                unite: '%',
                style: new StyleIndicateur({
                    nbDecimales: 0,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: '#ffffff' },
                        { valeur: 15, couleur: BLEU }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 5, label: '5' },
                        { valeur: 10, label: '10' },
                        { valeur: 15, label: '15+' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'populationAgricole.partPopulationAgricole2010Pourcent',
                legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
                des ${sourcesDonnees.actifs_agricoles_permanents.nom} (${sourcesDonnees.actifs_agricoles_permanents.source}, ${sourcesDonnees.actifs_agricoles_permanents.anneeLaPlusRecente})
                 et de la ${sourcesDonnees.population_totale.nom} (${sourcesDonnees.population_totale.source}, ${sourcesDonnees.population_totale.annees})`
            }
        },
        {
            id: IDS_INDICATEURS.revenuAgriculteurs,
            idMaillon: IDS_MAILLONS.agriculteursExploitations,
            libelle: `Revenu des agriculteurs`,
            description: `Cet indicateur donne le revenu maximal par travailleur du quart des exploitations françaises les moins rémunératrices.
            <br><br>Source : <c-lien href="https://publications.resiliencealimentaire.org/partie1_un-systeme-defaillant/#_des_agriculteurs_qui_ne_gagnent_pas_leur_vie">rapport "Qui veille au grain?"</c-lien>, Les Greniers d'Abondance, à partir
            des données du Réseau d’Information Comptable Agricole (RICA) pour les années 2017 à 2020`,
            icone: ICONES_SVG.revenu
        },
        {
            id: IDS_INDICATEURS.agesChefsExploitation,
            idMaillon: IDS_MAILLONS.agriculteursExploitations,
            libelle: `Âge des chefs d'exploitation`,
            description: `Cet indicateur donne le nombre de chefs d'exploitation par classes d'âge.`,
            icone: ICONES_SVG.agesChefsExploitation,
            legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
            des ${sourcesDonnees.exploitations_selon_age_chef_exploitation.nom} (${sourcesDonnees.exploitations_selon_age_chef_exploitation.source}, ${sourcesDonnees.exploitations_selon_age_chef_exploitation.annees})`
        },
        {
            id: IDS_INDICATEURS.superficiesExploitations,
            idMaillon: IDS_MAILLONS.agriculteursExploitations,
            libelle: 'Nombre et superficie des exploitations',
            description: `Cet indicateur donne le nombre totale et la superficie associée des exploitations par classes de superficie.`,
            icone: ICONES_SVG.superficiesExploitations,
            legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
            des ${sourcesDonnees.exploitations_selon_taille_exploitation.nom} (${sourcesDonnees.exploitations_selon_taille_exploitation.source}, ${sourcesDonnees.exploitations_selon_taille_exploitation.annees})`,
            indicateurCarte: {
                libelle: 'Superficie moyenne des exploitations',
                unite: 'ha',
                description: `Cet indicateur donne la superficie moyenne des exploitations.`,
                style: new StyleIndicateur({
                    nbDecimales: 0,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: '#ffffff' },
                        { valeur: 150, couleur: BLEU }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 50, label: '50' },
                        { valeur: 100, label: '100' },
                        { valeur: 150, label: '150+' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'populationAgricole.sauMoyenneParExploitationHa2010',
                legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
                des ${sourcesDonnees.exploitations_selon_taille_exploitation.nom} (${sourcesDonnees.exploitations_selon_taille_exploitation.source}, ${sourcesDonnees.exploitations_selon_taille_exploitation.anneeLaPlusRecente})`
            }
        },

        {
            id: IDS_INDICATEURS.noteIntrants,
            idMaillon: IDS_MAILLONS.intrants,
            libelle: 'Score du maillon Intrants',
            description: `Cette note évalue l'intensité d'usage d'intrants en se basant sur l'intensité en usage des <strong>pesticides</strong>.`,
            icone: ICONES_SVG.intrants,
            indicateurNote: true
        },
        {
            id: IDS_INDICATEURS.qsaNodu,
            idMaillon: IDS_MAILLONS.intrants,
            libelle: 'Quantités de substances actives achetées et Nombre de doses unités équivalent',
            description: `
            L'indicateur <abbr title="Quantité de Substances Actives">QSA</abbr>, exprimé en kg, correspond à la quantité totale de 
            substances actives qui ont été achetées sur le territoire.
            Seules les substances considérées comme cancérigènes, mutagène ou reprotoxiques, dangereuses pour la santé ou dangereuses 
            pour l'environnement sont prises en compte (ie toutes les classifications de toxicité sauf "Autre").
            <br><br>
            L'indicateur <abbr title="NOmbre de Doses Unités">NODU</abbr>, exprimé en hectares, est calculé pour chaque substance 
            active en faisant le ratio de la <abbr title="Quantité de Substances Actives">QSA</abbr> avec la <abbr title="Dose Unité">DU</abbr> de 
            la substance active. Cette DU correspond à la dose maximale applicable sur un hectare. Le calcul du NODU permet donc 
            de comparer et d'additionner des substances actives qui n'ont pas le même impact à quantité égale utilisée.
            Certaines substances n'ont pas de DU et sont donc exclues de facto du calcul. Aussi, comme il est calculé à partir du 
            <abbr title="Quantité de Substances Actives">QSA</abbr>, seules les substances prises en compte dans le calcul de ce dernier 
            le sont également ici.
            <br><br>
            Ces indicateurs sont calculés en réalisant une moyenne triennale (la moyenne triennale de l'année N est calculée par moyenne 
            des années N, N-1 et N-2).`,
            icone: ICONES_SVG.qsaNodu,
            legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
            des données de vente de la ${sourcesDonnees.quantites_substances_actives.sourceCourt}
            et des arrêtés ministériels ${sourcesDonnees.doses_unites_substances_actives.sourceCourt} fixant les <abbr title="Dose Unité">DU</abbr>.
            `
        },
        {
            id: IDS_INDICATEURS.noduNormalise,
            idMaillon: IDS_MAILLONS.intrants,
            libelle: `Intensité d'usage de pesticides`,
            description: `
            Cet indicateur peut s’interpréter comme le nombre moyen de traitements par pesticides que reçoivent les terres agricoles 
            du territoire, en tenant compte de la toxicité des produits employés. Il permet de faire des comparaisons entre territoires 
            et/ou périodes temporelles différent(e)s.`,
            icone: ICONES_SVG.noduNormalise,
            legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
            des données de vente de la ${sourcesDonnees.quantites_substances_actives.sourceCourt}
            , arrêtés ministériels ${sourcesDonnees.doses_unites_substances_actives.sourceCourt}
             et des surfaces agricoles du recensement agricole ${sourcesDonnees.sau_ra.sourceCourt}
            <br>NB: des valeurs supérieures à 10 peuvent signifier une surestimation du <abbr title="NOmbre de Doses Unités">NODU</abbr>`,
            indicateurCarte: {
                style: new StyleIndicateur({
                    nbDecimales: 1,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: '#ffffff' },
                        { valeur: 5, couleur: '#e2a582' },
                        { valeur: 10, couleur: '#c44b06' }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 2, label: '2' },
                        { valeur: 4, label: '4' },
                        { valeur: 6, label: '6' },
                        { valeur: 8, label: '8' },
                        { valeur: 10, label: '10+' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'intrants.noduNormalise'
            }
        },
        {
            id: IDS_INDICATEURS.noteProduction,
            idMaillon: IDS_MAILLONS.production,
            libelle: 'Score du maillon Production',
            description: `Cette note évalue la capacité du territoire à produire une alimentation répondant à la consommation des habitants du territoire tout en utilisant des pratiques agricoles qui soutiennent la biodiversité.`,
            icone: ICONES_SVG.note,
            indicateurCarte: {
                style: stylesIndicateurNotes,
                nomIndicateurRequeteApi: 'production.note'
            },
            indicateurNote: true
        },
        {
            id: IDS_INDICATEURS.adequationTheoriqueProductionConsommation,
            idMaillon: IDS_MAILLONS.production,
            libelle: 'Adéquation théorique entre production et consommation',
            description: `
                    Cet indicateur représente la part de la consommation du territoire qui pourrait en théorie être couverte par sa propre production. <br>
                    <strong>Attention</strong>, il ne s'agit pas de la part de la consommation réellement couverte
                    par la production locale. Les flux logistiques sont aujourd'hui totalement dissociés de la disponibilité locale, si bien qu'à
                    l'échelle d'un bassin de vie, presque toute la production est généralement exportée, et tous les biens consommés sont importés
                    depuis d'autres territoires.`,
            icone: ICONES_SVG.adequationTheoriqueProductionConsommation,
            legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
            du ${sourcesDonnees.rpg.nom} (${sourcesDonnees.rpg.source}, ${sourcesDonnees.rpg.annees})
             et des ${sourcesDonnees.consommation_sau.nom} (${sourcesDonnees.consommation_sau.source}, ${sourcesDonnees.consommation_sau.annees})`,
            indicateurCarte: {
                unite: '%',
                style: new StyleIndicateur({
                    nbDecimales: 0,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: '#ffffff' },
                        { valeur: 100, couleur: VERT }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 20, label: '20' },
                        { valeur: 40, label: '40' },
                        { valeur: 60, label: '60' },
                        { valeur: 80, label: '80' },
                        { valeur: 100, label: '100' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'production.tauxAdequationMoyenPonderePourcent'
            }
        },
        {
            id: IDS_INDICATEURS.partProductionExportee,
            idMaillon: IDS_MAILLONS.production,
            libelle: 'Part de la production exportée et part de la consommation importée',
            description: `Estimation obtenue après analyse de 100 aires urbaines françaises. Pas de détail par territoire. <br>Source : Utopies, <c-lien href="https://utopies.com/wp-content/uploads/2019/12/autonomie-alimentaire-des-villes-notedeposition12.pdf">Autonomie alimentaire des villes</c-lien>, 2017`,
            icone: ICONES_SVG.partProductionExportee,
            legende: `Source : Utopies, <c-lien href="https://utopies.com/wp-content/uploads/2019/12/autonomie-alimentaire-des-villes-notedeposition12.pdf">Autonomie alimentaire des villes</c-lien>, 2017`
        },
        {
            id: IDS_INDICATEURS.partSAUBio,
            idMaillon: IDS_MAILLONS.production,
            libelle: 'Part de surface agricole labellisée Agriculture Biologique',
            description: `Cet indicateur correspond à la part de la surface agricole labellisée agriculture biologique (ou en conversion) dans la surface agricole totale du territoire.
                <br>
                L'agriculture biologique répond à un cahier des charges qui incorpore plusieurs <strong>pratiques agroécologiques</strong> et fait
                l'objet d'un suivi régulier.`,
            icone: ICONES_SVG.partSAUBio,
            legende: `
            Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>,
            à partir des ${sourcesDonnees.surfaces_agricoles_bio.nom} (${sourcesDonnees.surfaces_agricoles_bio.source}, ${sourcesDonnees.surfaces_agricoles_bio.annees})
             et du ${sourcesDonnees.rpg.nom} (${sourcesDonnees.rpg.source}, ${sourcesDonnees.rpg.annees})
            `,
            indicateurCarte: {
                unite: '%',
                style: new StyleIndicateur({
                    nbDecimales: 0,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: '#ffffff' },
                        { valeur: 100, couleur: VERT }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 20, label: '20' },
                        { valeur: 40, label: '40' },
                        { valeur: 60, label: '60' },
                        { valeur: 80, label: '80' },
                        { valeur: 100, label: '100' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'production.partSauBioPourcent'
            }
        },
        {
            id: IDS_INDICATEURS.scoreHVN,
            idMaillon: IDS_MAILLONS.production,
            libelle: 'Score HVN (Haute Valeur Naturelle)',
            description: `Cet indicateur caractérise les <strong>systèmes agricoles</strong> qui maintiennent un <strong>haut niveau de biodiversité</strong>. Il est calculé à partir du score HVN de <c-lien href="https://solagro.org">Solagro</c-lien>.
                <br>
                <p>
                Trois dimensions, notées de
                1 à 10, sont prises en compte :
                </p>
                <ul>
                    <li>la diversité des assolements, qui indique la variété des cultures présentes sur les fermes ;</li>
                    <li>l'extensivité des pratiques (faible niveau d'intrants, pesticides et engrais chimiques) ;</li>
                    <li>la présence d’éléments du paysage à intérêt agroécologique, tels que des haies ou des prairies permanentes.</li>
                </ul>`,
            icone: ICONES_SVG.indiceHVN,
            legende: `
            Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>,
            à partir de l'${sourcesDonnees.hvn.nom} (${sourcesDonnees.hvn.source}, ${sourcesDonnees.hvn.annees})
            `,
            indicateurCarte: {
                style: new StyleIndicateur({
                    nbDecimales: 0,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: '#ffffff' },
                        { valeur: 30, couleur: VERT }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 10, label: '10' },
                        { valeur: 20, label: '20' },
                        { valeur: 30, label: '30' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'production.indiceHvn'
            }
        },
        {
            id: IDS_INDICATEURS.noteTransformationDistribution,
            idMaillon: IDS_MAILLONS.transformationDistribution,
            libelle: `Score du maillon Transformation et Distribution`,
            description: `Cette note évalue le niveau d'indépendance aux énergies fossiles des consommateurs pour leurs achats alimentaires. Elle se base sur la part de la population indépendante de la voiture pour ses achats alimentaires.`,
            icone: ICONES_SVG.note,
            indicateurCarte: {
                style: stylesIndicateurNotes,
                nomIndicateurRequeteApi: 'proximiteCommerces.note'
            },
            indicateurNote: true
        },
        {
            id: IDS_INDICATEURS.partPopulationDependanteVoiture,
            idMaillon: IDS_MAILLONS.transformationDistribution,
            libelle: 'Part de la population théoriquement dépendante de la voiture pour ses achats alimentaires',
            description: `Cet indicateur évalue la part de la population théoriquement dépendante de la voiture pour accéder à un ensemble représentatif de commerces alimentaires (ie ne disposant pas de 3 commerces de type différent accessibles à vélo depuis le domicile).`,
            icone: ICONES_SVG.partPopulationDependanteVoiturePourcent,
            legende: `Source : Les Greniers d'Abondance, à partir des 
            ${sourcesDonnees.base_permanente_equipements.nom} (${sourcesDonnees.base_permanente_equipements.source}, ${sourcesDonnees.base_permanente_equipements.annees}), 
            ${sourcesDonnees.commerces_osm.nom} (${sourcesDonnees.commerces_osm.source}, ${sourcesDonnees.commerces_osm.annees}) et 
            ${sourcesDonnees.carroyage_insee.nom} (${sourcesDonnees.carroyage_insee.source}, ${sourcesDonnees.carroyage_insee.annees})`,
            indicateurCarte: {
                unite: '%',
                style: new StyleIndicateur({
                    nbDecimales: 0,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: '#ffffff' },
                        { valeur: 100, couleur: BLEU_PETROLE }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 20, label: '20' },
                        { valeur: 40, label: '40' },
                        { valeur: 60, label: '60' },
                        { valeur: 80, label: '80' },
                        { valeur: 100, label: '100' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'proximiteCommerces.partPopulationDependanteVoiturePourcent'
            }
        },
        {
            id: IDS_INDICATEURS.partTerritoireDependantVoiture,
            idMaillon: IDS_MAILLONS.transformationDistribution,
            libelle: 'Part du territoire dont la population est en majorité théoriquement dépendante de la voiture pour ses achats alimentaires',
            description: `Cet indicateur évalue la part du territoire dont plus de la moitié de la population est théoriquement dépendante de la voiture pour accéder à un ensemble représentatif de commerces alimentaires (ie ne disposant pas de 3 commerces de type différent accessibles à vélo depuis le domicile).`,
            icone: ICONES_SVG.partTerritoireDependantVoiturePourcent,
            legende: `Source : Les Greniers d'Abondance, à partir des 
            ${sourcesDonnees.base_permanente_equipements.nom} (${sourcesDonnees.base_permanente_equipements.source}, ${sourcesDonnees.base_permanente_equipements.annees}), 
            ${sourcesDonnees.commerces_osm.nom} (${sourcesDonnees.commerces_osm.source}, ${sourcesDonnees.commerces_osm.annees}) et 
            ${sourcesDonnees.carroyage_insee.nom} (${sourcesDonnees.carroyage_insee.source}, ${sourcesDonnees.carroyage_insee.annees})`,
            indicateurCarte: {
                unite: '%',
                style: new StyleIndicateur({
                    nbDecimales: 0,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: '#ffffff' },
                        { valeur: 100, couleur: BLEU_PETROLE }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 20, label: '20' },
                        { valeur: 40, label: '40' },
                        { valeur: 60, label: '60' },
                        { valeur: 80, label: '80' },
                        { valeur: 100, label: '100' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'proximiteCommerces.partTerritoireDependantVoiturePourcent'
            }
        },
        {
            id: IDS_INDICATEURS.distancesPlusProchesCommerces,
            idMaillon: IDS_MAILLONS.transformationDistribution,
            libelle: 'Distances moyennes aux plus proches commerces par type de commerce alimentaire',
            description: "Cet indicateur mesure la distance moyenne à vol d'oiseau entre le domicile et le plus proche commerce d'un type donné.",
            icone: ICONES_SVG.distancesPlusProchesCommerces,
            legende: `Source : Les Greniers d'Abondance, à partir de la ${sourcesDonnees.base_permanente_equipements.source}, la ${sourcesDonnees.commerces_osm.source} et les ${sourcesDonnees.carroyage_insee.source}.`
        },
        {
            id: IDS_INDICATEURS.noteConsommation,
            idMaillon: IDS_MAILLONS.consommation,
            libelle: 'Score du maillon Consommation',
            description: `Aucune note n'est actuellement disponible mais un message, donné à l'échelle nationale, évalue le régime alimentaire actuel moyen des Français ainsi que la précarité alimentaire.`,
            icone: ICONES_SVG.note,
            indicateurNote: true
        },
        {
            id: IDS_INDICATEURS.partAlimentationAnimaleDansConsommation,
            idMaillon: IDS_MAILLONS.consommation,
            libelle: "Part de l'alimentation d'origine animale dans l'empreinte en surface de la consommation",
            description: `Cet indicateur mesure la part de l'alimentation d'origine animale dans l'empreinte en surface de la consommation totale des habitants (selon le régime alimentaire actuel moyen d'un Français).`,
            icone: ICONES_SVG.partAlimentationAnimaleDansConsommation,
            legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
            des ${sourcesDonnees.consommation_sau.nom} (${sourcesDonnees.consommation_sau.source}, ${sourcesDonnees.consommation_sau.annees})`
        },
        {
            id: IDS_INDICATEURS.tauxPauvrete,
            idMaillon: IDS_MAILLONS.consommation,
            libelle: 'Taux de pauvreté',
            description: `${sourcesDonnees.taux_pauvrete.definition}.<br>Source : ${sourcesDonnees.taux_pauvrete.source}`,
            icone: ICONES_SVG.tauxPauvrete,
            legende: `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir
            des ${sourcesDonnees.taux_pauvrete.nom} (${sourcesDonnees.taux_pauvrete.source}, ${sourcesDonnees.taux_pauvrete.annees})`,
            indicateurCarte: {
                unite: '%',
                style: new StyleIndicateur({
                    nbDecimales: 0,
                    gradientCouleurs: new GradientCouleurs([
                        { valeur: 0, couleur: BEIGE },
                        { valeur: 30, couleur: ORANGE }
                    ]),
                    couleurTexte: 'grey',
                    labels: [
                        { valeur: 0, label: '0' },
                        { valeur: 5, label: '5' },
                        { valeur: 10, label: '10' },
                        { valeur: 15, label: '15' },
                        { valeur: 20, label: '20' },
                        { valeur: 25, label: '25' },
                        { valeur: 30, label: '30+' }
                    ],
                    typeJauge: 'VALEUR'
                }),
                nomIndicateurRequeteApi: 'consommation.tauxPauvrete60Pourcent'
            }
        },
        {
            id: IDS_INDICATEURS.aideAlimentaire,
            idMaillon: IDS_MAILLONS.consommation,
            libelle: "Recours à l'aide alimentaire",
            description: `Statistique valable à l'échelle nationale, pas de détail par territoire.
            <br><br>Source : <c-lien href="https://publications.resiliencealimentaire.org/partie1_un-systeme-defaillant/#_une_malnutrition_omnipr%C3%A9sente_au_nord_comme_au_sud">rapport "Qui veille au grain?"</c-lien>, Les Greniers d'Abondance, à partir de Le Morvan F. et Wanecq T. (2019)`,
            icone: ICONES_SVG.aideAlimentaire
        },
        {
            id: IDS_INDICATEURS.partPopulationObese,
            idMaillon: IDS_MAILLONS.consommation,
            libelle: "Part de la population touchée par l'obésité",
            description: `Statistique valable à l'échelle nationale, pas de détail par territoire.<br>Source : Wikipédia, <c-lien href="https://fr.wikipedia.org/wiki/Obésité_en_France">Obésité en France</c-lien>, basé entre autres sur INSEE et Odoxa`,
            icone: ICONES_SVG.partPopulationObese,
            legende: `Source : Wikipédia, <c-lien href="https://fr.wikipedia.org/wiki/Obésité_en_France">Obésité en France</c-lien>, basé entre autres sur INSEE et Odoxa`
        }
    ];

    return donneesIndicateurs.map((i) => new Indicateur(i, systemeAlimentaire));
}
