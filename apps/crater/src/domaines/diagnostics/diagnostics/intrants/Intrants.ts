import { arrondirANDecimales, formaterNombreEnNDecimalesString } from '@lga/commun/build/outils/base';

import { Territoire } from '../../../territoires';
import { IndicateurSynthese, Note } from '../IndicateurSynthese';
import { MessageEvolutionPesticides } from './MessageEvolutionPesticides';
import { calculerMessageSyntheseIntrants } from './messages-intrants';

export class Intrants implements IndicateurSynthese {
    public intrantsPays?: Intrants;

    constructor(private territoire: Territoire, public readonly note: Note, public readonly pesticides: IndicateursPesticides) {}

    setIntrantsPays(intrantsPays: Intrants) {
        this.intrantsPays = intrantsPays;
    }

    get messageSynthese() {
        return calculerMessageSyntheseIntrants(this.pesticides.indicateursMoyennesTriennales.anneeFinale.noduNormalise);
    }

    get messagePesticides() {
        const indicateursAnneeInitiale = this.pesticides.indicateursMoyennesTriennales.anneeInitiale;
        const indicateursAnneeFinale = this.pesticides.indicateursMoyennesTriennales.anneeFinale;
        return MessageEvolutionPesticides.construireMessage(
            this.territoire.nom,
            indicateursAnneeInitiale.annee,
            indicateursAnneeFinale.annee,
            indicateursAnneeInitiale.quantiteSubstanceTotaleKg,
            indicateursAnneeFinale.quantiteSubstanceTotaleKg,
            indicateursAnneeInitiale.noduHa,
            indicateursAnneeFinale.noduHa
        );
    }

    get qsaTotalAnneeFinale() {
        const q = this.pesticides.indicateursMoyennesTriennales.anneeFinale.quantiteSubstanceTotaleKg;
        if (q === null) {
            return '- kg';
        } else if (q > 1e6) {
            return formaterNombreEnNDecimalesString(q / 1000, 0) + ' t';
        } else {
            return formaterNombreEnNDecimalesString(q, 0) + ' kg';
        }
    }

    get evolutionNodu() {
        if (
            this.pesticides.indicateursMoyennesTriennales.anneeFinale.noduHa === null ||
            this.pesticides.indicateursMoyennesTriennales.anneeInitiale.noduHa === null
        )
            return null;

        return (
            (this.pesticides.indicateursMoyennesTriennales.anneeFinale.noduHa / this.pesticides.indicateursMoyennesTriennales.anneeInitiale.noduHa -
                1) *
            100
        );
    }
}

export class IndicateursPesticides {
    constructor(public readonly indicateursMoyennesTriennales: IndicateursPesticidesParAnnees) {}
}

export class IndicateursPesticidesParAnnees {
    private listeIndicateursParAnnees: Array<IndicateursPesticidesAnneeN> = [];

    constructor(indicateursPesticidesParAnnees: IndicateursPesticidesAnneeN[]) {
        this.listeIndicateursParAnnees = indicateursPesticidesParAnnees;
        this.listeIndicateursParAnnees.sort((ia1, ia2) => {
            return ia1.annee - ia2.annee;
        });
    }

    get annees(): number[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.annee);
    }

    get quantitesSubstancesSansDUKg(): (number | null)[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.quantiteSubstanceSansDUKg);
    }

    get quantitesSubstancesAvecDUKg(): (number | null)[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.quantiteSubstanceAvecDUKg);
    }

    get nodusHa(): (number | null)[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.noduHa);
    }

    get nodusNormalises(): (number | null)[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.noduNormalise);
    }

    get anneeInitiale(): IndicateursPesticidesAnneeN {
        return this.listeIndicateursParAnnees.slice(0)[0];
    }

    get anneeFinale(): IndicateursPesticidesAnneeN {
        return this.listeIndicateursParAnnees.slice(-1)[0];
    }

    get quantitesSubstancesTotalesKg(): (number | null)[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.quantiteSubstanceTotaleKg);
    }
}

export class IndicateursPesticidesAnneeN {
    constructor(
        public annee: number,
        public quantiteSubstanceSansDUKg: number | null,
        public quantiteSubstanceAvecDUKg: number | null,
        public noduHa: number | null,
        public noduNormalise: number | null
    ) {}

    get quantiteSubstanceTotaleKg(): number | null {
        if (this.quantiteSubstanceSansDUKg && this.quantiteSubstanceAvecDUKg) {
            return arrondirANDecimales(this.quantiteSubstanceSansDUKg + this.quantiteSubstanceAvecDUKg, 2);
        } else {
            return null;
        }
    }
}
