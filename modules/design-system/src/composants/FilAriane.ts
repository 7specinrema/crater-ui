import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-fil-ariane': FilAriane;
    }
}

export interface LienFilAriane {
    libelle: string;
    href: string;
}

@customElement('c-fil-ariane')
export class FilAriane extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100%;
                height: fit-content;
            }

            div {
                display: flex;
                flex-wrap: wrap;
                flex-direction: row;
                align-items: center;
                column-gap: calc(var(--dsem) / 2);
            }

            a {
                display: grid;
                cursor: pointer;
                padding: calc(var(--dsem) / 2);
                color: var(--couleur-neutre);
                text-decoration: none;
            }

            a:hover {
                text-decoration: underline;
            }

            svg {
                width: 1.5em;
                height: 1.5em;
                fill: var(--couleur-neutre);
                margin: auto;
            }

            svg:hover {
                fill: var(--couleur-primaire);
            }

            .separateur-liens {
                color: var(--couleur-neutre);
            }

            .dernier-lien {
                color: var(--couleur-primaire);
            }
        `
    ];

    @property({ attribute: false })
    liens: LienFilAriane[] = [];

    @property()
    hrefAccueil = '/';

    render() {
        return html`
            <div>
                <a href=${this.hrefAccueil}>
                    <svg width="16" height="14" viewBox="0 0 16 14" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M15.255 6.39361L8.01663 0.115213C7.84386 -0.0384043 7.57506 -0.0384043 7.40216 0.115213L0.163761 6.39361C-0.162621 6.68156 0.0292965 7.21914 0.470993 7.21914H1.87256V13.7471H6.11576V9.13914H9.28369V13.7471H13.5269V7.21914H14.9285C15.3892 7.21914 15.6004 6.68154 15.2547 6.39361H15.255Z"
                        />
                    </svg>
                </a>
                ${this.liens?.map(
                    (l, index) =>
                        html` <div class="texte-petit-majuscule separateur-liens">></div>
                            <a class="texte-petit-majuscule ${index === this.liens.length! - 1 ? `dernier-lien` : ``}" href="${l.href}">
                                ${l.libelle}</a
                            >`
                )}
            </div>
        `;
    }
}
