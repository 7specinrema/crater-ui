import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-bouton-carre': BoutonCarre;
    }
}

@customElement('c-bouton-carre')
export class BoutonCarre extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: fit-content;
                --couleur-fond: var(--couleur-blanc);
                --couleur-texte-base: var(--couleur-neutre);
                --couleur-texte-selectionne: var(--couleur-primaire);
                --couleur-texte-desactive: var(--couleur-neutre-20);
            }

            div {
                background-color: var(--couleur-fond);
            }

            a {
                cursor: pointer;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                gap: calc(var(--dsem) / 2);
                padding: var(--dsem);
                text-decoration: none;
            }

            p {
                white-space: nowrap;
                margin: 0;
                padding: 0;
                color: var(--couleur-texte-base);
            }

            ::slotted(svg) {
                width: 1.5em;
                height: 1.5em;
                fill: none;
                stroke-width: 2;
                stroke: var(--couleur-texte-base);
            }

            .selectionne p {
                color: var(--couleur-texte-selectionne);
            }

            .selectionne ::slotted(svg) {
                stroke: var(--couleur-texte-selectionne);
            }

            .desactive a {
                cursor: default;
            }

            .desactive p {
                color: var(--couleur-texte-desactive);
            }

            .desactive ::slotted(svg) {
                stroke: var(--couleur-texte-desactive);
            }
        `
    ];

    @property()
    libelle?: string;

    @property()
    libelleTooltip: string | undefined;

    @property({ type: Boolean })
    selectionne = false;

    @property({ type: Boolean })
    desactive = false;

    @property()
    href?: string;

    render() {
        return html`
            <div class="${this.selectionne ? 'selectionne' : ''} ${this.desactive ? 'desactive' : ''}">
                <a title="${ifDefined(this.libelleTooltip)}" href="${ifDefined(this.href)}">
                    <slot name="icone"></slot>
                    ${this.libelle ? html`<p class="texte-petit">${this.libelle}</p>` : ''}
                </a>
            </div>
        `;
    }
}
