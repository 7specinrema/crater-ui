import '../../commun/BoiteLienVersPage.js';

import { formaterNombreEnEntierString } from '@lga/commun/build/outils/base';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, TemplateResult, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import ICONE_SUPERFICIE from '../../../ressources/images/ha.svg';
import ICONE_SAU_PRODUCTIVE from '../../../ressources/images/ha-agricoles.svg';
import ICONE_HABITANTS from '../../../ressources/images/habitants.svg';
import { STYLES_CRATER } from '../../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-boite-informations-territoire': BoiteInformationsTerritoire;
    }
}

export interface OptionsBoiteInformationsTerritoire {
    nomTerritoire: string;
    message: TemplateResult;
    population: number;
    superficieHa: number;
    sauProductiveHa: number;
    lienPageTerritoire: string;
}

@customElement('c-boite-informations-territoire')
export class BoiteInformationsTerritoire extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                width: 100%;
            }
            c-boite-lien-vers-page {
                --couleur-fond-boite: transparent;
            }

            section {
                display: grid;
                grid-template-columns: auto auto auto;
                grid-gap: calc(2 * var(--dsem));
                align-items: center;
                justify-items: center;
                padding: calc(2 * var(--dsem));
                max-width: 600px;
                margin: 0 auto;
            }

            p {
                grid-column: 1 / 4;
                color: var(--couleur-neutre);
                margin: 0;
                justify-self: start;
            }

            .indicateur-synthese {
                color: var(--couleur-primaire);
            }

            .indicateur-synthese-unite {
                color: var(--couleur-neutre);
                justify-self: start;
            }

            .icone {
                width: 60px;
                padding: 0.2rem;
                justify-self: end;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                section {
                    padding: 0;
                    grid-gap: calc(1 * var(--dsem));
                }
            }
        `
    ];

    @property({ attribute: false })
    options?: OptionsBoiteInformationsTerritoire;

    render() {
        return html`
            <c-boite-lien-vers-page libelleLien="Présentation du territoire" href="${ifDefined(this.options?.lienPageTerritoire)}">
                <main slot="texte">
                    <section>
                        <p class="texte-moyen">${this.options?.message}</p>
                        <img class="icone" src="${ICONE_HABITANTS}" width="50" height="44" />
                        <div class="indicateur-synthese titre-moyen">${formaterNombreEnEntierString(this.options?.population ?? null)}</div>
                        <div class="indicateur-synthese-unite texte-moyen">habitants</div>
                        <img class="icone" src="${ICONE_SUPERFICIE}" width="48" height="48" />
                        <div class="indicateur-synthese titre-moyen">${formaterNombreEnEntierString(this.options?.superficieHa ?? null)}</div>
                        <div class="indicateur-synthese-unite texte-moyen"><abbr title="hectares">ha</abbr></div>
                        <img class="icone" src="${ICONE_SAU_PRODUCTIVE}" width="51" height="51" />
                        <div class="indicateur-synthese titre-moyen" data-cy="synthese-valeur-sauProductiveHa">
                            ${formaterNombreEnEntierString(this.options?.sauProductiveHa ?? null)}
                        </div>
                        <div class="indicateur-synthese-unite texte-moyen"><abbr title="hectares">ha</abbr> productifs</div>
                    </section>
                </main>
            </c-boite-lien-vers-page>
        `;
    }
}
