import './BarreOngletsItem.js';

import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { EvenementSelectionner } from '../evenements/EvenementSelectionner';

declare global {
    interface HTMLElementTagNameMap {
        'c-barre-onglets': BarreOnglets;
    }
}

export interface OngletItem {
    id: string;
    libelle: string;
    sousLibelle?: string;
    href?: string;
}

export interface OptionsBarreOnglets {
    onglets: OngletItem[];
    idOngletSelectionne?: string;
    theme?: string;
}

@customElement('c-barre-onglets')
export class BarreOnglets extends LitElement {
    static styles = [
        css`
            :host {
                display: block;
                width: 100%;
            }

            div {
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: start;
            }
        `
    ];

    @property({ attribute: false })
    options?: OptionsBarreOnglets;

    render() {
        return html` <div>
            ${this.options?.onglets.map(
                (i) =>
                    html`
                        <c-barre-onglets-item
                            id=${i.id}
                            .libelle=${i.libelle}
                            sousLibelle=${ifDefined(i.sousLibelle)}
                            href=${ifDefined(i.href)}
                            ?selectionne=${i.id === this.options?.idOngletSelectionne}
                            theme=${ifDefined(this.options?.theme)}
                            @click=${() => this.emettreEvtSelectionner(i.id)}
                        >
                        </c-barre-onglets-item>
                    `
            )}
        </div>`;
    }

    private emettreEvtSelectionner(idItemSelectionne: string) {
        this.dispatchEvent(new EvenementSelectionner(idItemSelectionne));
    }
}
