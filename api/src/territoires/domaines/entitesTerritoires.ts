import { Entite } from '../../communs/servicesUtils';

export const CATEGORIE_PAYS = 'PAYS';
export const CATEGORIE_REGION = 'REGION';
export const CATEGORIE_DEPARTEMENT = 'DEPARTEMENT';
export const CATEGORIE_REGROUPEMENT_COMMUNES = 'REGROUPEMENT_COMMUNES';
export const CATEGORIE_EPCI = 'EPCI';
export const CATEGORIE_COMMUNE = 'COMMUNE';

enum LibelleCategorieTerritoire {
    Pays = 'Pays',
    Region = 'Région',
    Departement = 'Département',
    RegroupementCommunes = 'Regroupement de communes',
    EPCI = 'Intercommunalité',
    Commune = 'Commune'
}

export abstract class Territoire implements Entite {
    public idParcel: string | null = null;

    constructor(public readonly id: string, public readonly nom: string) {
        this.id = id;
        this.nom = nom;
    }

    abstract get libelle(): string;
    abstract get libelleSecondaire(): string;
    abstract get categorie(): string;
}

export class Pays extends Territoire {
    get categorie(): string {
        return CATEGORIE_PAYS;
    }

    get libelle(): string {
        return this.nom;
    }
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.Pays;
    }
}

export class Region extends Territoire {
    constructor(id: string, nom: string, public readonly idPays: string) {
        super(id, nom);
    }

    get categorie(): string {
        return CATEGORIE_REGION;
    }

    get libelle(): string {
        return this.nom;
    }
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.Region;
    }
}

export class Departement extends Territoire {
    constructor(id: string, nom: string, public readonly idRegion: string, public readonly idPays: string) {
        super(id, nom);
    }

    get categorie(): string {
        return CATEGORIE_DEPARTEMENT;
    }
    get libelle(): string {
        return this.nom;
    }
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.Departement;
    }
}

export class RegroupementCommunes extends Territoire {
    constructor(
        id: string,
        nom: string,
        public readonly sousCategorie: string,
        public readonly idDepartement: string,
        public readonly idRegion: string,
        public readonly idPays: string
    ) {
        super(id, nom);
    }

    get categorie(): string {
        return CATEGORIE_REGROUPEMENT_COMMUNES;
    }
    get libelle(): string {
        return this.nom;
    }
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.RegroupementCommunes;
    }
}

export class Epci extends RegroupementCommunes {
    get categorie(): string {
        return CATEGORIE_EPCI;
    }
    get libelle(): string {
        return this.nom;
    }
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.EPCI;
    }
}

export class Commune extends Territoire {
    private _codesPostaux: Array<string> = [];

    constructor(
        id: string,
        nom: string,
        public readonly idEpci: string,
        public readonly idDepartement: string,
        public readonly idRegion: string,
        public readonly idPays: string,
        codesPostaux: string[]
    ) {
        super(id, nom);
        if (codesPostaux) {
            this.ajouterCodePostal(...codesPostaux);
        }
    }

    get categorie(): string {
        return CATEGORIE_COMMUNE;
    }
    ajouterCodePostal(...codePostal: string[]): void {
        codePostal.forEach((c) => this._codesPostaux.push(c));
        this._codesPostaux.sort(function (s1, s2) {
            return s1.localeCompare(s2);
        });
    }

    get codesPostaux(): string {
        return this._codesPostaux.join(', ');
    }

    contientUnCodePostalCommencantPar(debutCodePostal: string): boolean {
        const strings = this._codesPostaux.filter(function (cp: string): boolean {
            return cp.startsWith(debutCodePostal);
        });
        return strings.length > 0;
    }
    get libelle(): string {
        return `${this.nom} (${this.codesPostaux})`;
    }
    get libelleSecondaire(): string {
        return LibelleCategorieTerritoire.Commune;
    }
}
