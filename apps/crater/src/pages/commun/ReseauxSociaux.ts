import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { ICONES_SVG } from '../../domaines/systeme-alimentaire/configuration/icones_svg';

declare global {
    interface HTMLElementTagNameMap {
        'c-reseaux-sociaux': ReseauxSociaux;
    }
}

@customElement('c-reseaux-sociaux')
export class ReseauxSociaux extends LitElement {
    static styles = [
        css`
            :host {
                display: block;
                width: 100%;
            }
            a {
                text-decoration: none;
            }
            svg {
                stroke: var(--couleur, var(--couleur-blanc));
            }

            path {
                stroke: var(--couleur, var(--couleur-blanc));
            }
            .dessin-remplissage {
                fill: var(--couleur, var(--couleur-blanc));
            }
        `
    ];

    render() {
        return html`
            <div>
                <a href="https://www.facebook.com/lesgreniersdabondance" target="_blank">${unsafeSVG(ICONES_SVG.facebook)}</a>
                <a href="https://twitter.com/LGA_resilience" target="_blank">${unsafeSVG(ICONES_SVG.twitter)}</a>
                <a href="https://www.linkedin.com/company/les-greniers-d-abondance" target="_blank">${unsafeSVG(ICONES_SVG.linkedin)}</a>
                <a href="https://www.instagram.com/lesgreniersdabondance/" target="_blank">${unsafeSVG(ICONES_SVG.instagram)}</a>
                <a href="https://framapiaf.org/@lga" target="_blank">${unsafeSVG(ICONES_SVG.mastodon)}</a>
                <a href="https://video.tedomum.net/accounts/lga/video-channels" target="_blank">${unsafeSVG(ICONES_SVG.peertube)}</a>
            </div>
        `;
    }
}
