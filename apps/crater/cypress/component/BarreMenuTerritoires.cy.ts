import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../src/pages/commun/BarreMenuTerritoires.js';

import { OngletItem } from '@lga/design-system/build/composants/BarreOnglets.js';
import { html } from 'lit';

import { OptionsBarreMenuTerritoires } from '../../src/pages/commun/BarreMenuTerritoires';

describe('Test BarreMenuTerritoire', () => {
    const onglets: OngletItem[] = [
        {
            id: 'commune',
            libelle: 'Commune',
            sousLibelle: 'Nom commune'
        },
        {
            id: 'epci',
            libelle: 'EPCI',
            sousLibelle: 'Nom EPCI'
        }
    ];

    it('Test affichage desktop', () => {
        // given
        cy.viewport(1400, 500);
        const options: OptionsBarreMenuTerritoires = {
            onglets: onglets,
            idOngletSelectionne: 'commune'
        };
        cy.mount<'c-barre-menu-territoires'>(
            html`<c-barre-menu-territoires id="barre-menu-territoires" .options="${options}"></c-barre-menu-territoires>`
        );
        // then
        cy.get('#barre-menu-territoires').find('c-champ-recherche-territoire').invoke('width').should('be.at.least', 250);
    });

    it('Test affichage mobile', () => {
        // given
        cy.viewport(1200, 500);
        const options: OptionsBarreMenuTerritoires = {
            onglets: onglets,
            idOngletSelectionne: 'epci'
        };
        cy.mount<'c-barre-menu-territoires'>(html`
            <c-barre-menu-territoires id="barre-menu-territoires" .options="${options}"> </c-barre-menu-territoires>
        `);
        // then
        cy.get('#barre-menu-territoires').find('c-champ-recherche-territoire').invoke('width').should('be.lessThan', 450);
        cy.get('#barre-menu-territoires').find('c-barre-onglets-item').should('have.length', 2);
    });
});
