export type DonneesPage =
    | DonneesPageSimple
    | DonneesPageDiagnostic
    | DonneesPageDiagnosticMaillon
    | DonneesPageDiagnosticIndicateur
    | DonneesPageCarte;

export type DonneesPageSimple = {
    idElementCibleScroll?: string;
};

export type DonneesPageDiagnostic = DonneesPageSimple & {
    idTerritoirePrincipal: string;
    chapitre?: string;
    nomTerritoirePrincipal?: string;
    idTerritoireActif?: string;
    nomTerritoireActif?: string;
    idEchelleTerritoriale?: string;
    metaDescription?: string;
};

export type DonneesPageDiagnosticMaillon = DonneesPageDiagnostic & {
    idMaillon: string;
    nomMaillon?: string;
};
export type DonneesPageDiagnosticIndicateur = DonneesPageDiagnostic & {
    idIndicateur: string;
    nomIndicateur?: string;
};

export type DonneesPageCarte = DonneesPageSimple & {
    idIndicateur: string;
    nomIndicateur?: string;
    idEchelleTerritoriale?: string;
    libelleEchelleTerritoriale?: string;
    idTerritoirePrincipal?: string;
};

export function majDonneesPage<D extends DonneesPage>(donneesInitiales: D, donneesMaj: Partial<D>) {
    return { ...donneesInitiales, ...donneesMaj };
}
