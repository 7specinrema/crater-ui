import { LienFilAriane } from '@lga/design-system/build/composants/FilAriane';
import { MenuAccordeonItem } from '@lga/design-system/build/composants/MenuAccordeon';
import { css } from 'lit';

import { PAGES_AIDE, PAGES_METHODOLOGIE, PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';

export const STYLES_METHODOLOGIE = css`
    table {
        color: var(--couleur-neutre);
        margin-bottom: 1rem;
        border-spacing: inherit;
    }

    th {
        text-align: left;
        border-bottom: 3px solid var(--couleur-accent);
        color: var(--couleur-primaire);
    }

    tr:nth-child(even) {
        background-color: var(--couleur-neutre-20);
    }

    th:first-child {
        width: 30rem;
        max-width: 30rem;
    }

    th:nth-child(n + 2) {
        width: max(var(--largeur-maximum-contenu), 30rem);
        background-color: transparent;
    }

    ol li {
        margin-bottom: 1rem;
    }

    blockquote {
        color: var(--couleur-neutre);
        border-left: 4px solid var(--couleur-accent);
        padding-left: 5px;
        padding-top: 5px;
        padding-bottom: 5px;
    }

    blockquote p {
        margin: 0;
    }

    q {
        font-style: italic;
    }
`;

export const ITEMS_MENU_METHODOLOGIE: MenuAccordeonItem[] = [
    {
        id: 'methodologie',
        libelle: 'Méthodologie',
        sousItems: [
            {
                id: PAGES_METHODOLOGIE.presentationGenerale.getId(),
                libelle: PAGES_METHODOLOGIE.presentationGenerale.getTitreCourt(),
                href: PAGES_METHODOLOGIE.presentationGenerale.getUrl()
            },
            {
                id: PAGES_METHODOLOGIE.terresAgricoles.getId(),
                libelle: PAGES_METHODOLOGIE.terresAgricoles.getTitreCourt(),
                href: PAGES_METHODOLOGIE.terresAgricoles.getUrl()
            },
            {
                id: PAGES_METHODOLOGIE.agriculteursExploitations.getId(),
                libelle: PAGES_METHODOLOGIE.agriculteursExploitations.getTitreCourt(),
                href: PAGES_METHODOLOGIE.agriculteursExploitations.getUrl()
            },
            {
                id: PAGES_METHODOLOGIE.intrants.getId(),
                libelle: PAGES_METHODOLOGIE.intrants.getTitreCourt(),
                href: PAGES_METHODOLOGIE.intrants.getUrl()
            },
            {
                id: PAGES_METHODOLOGIE.production.getId(),
                libelle: PAGES_METHODOLOGIE.production.getTitreCourt(),
                href: PAGES_METHODOLOGIE.production.getUrl()
            },
            {
                id: PAGES_METHODOLOGIE.transformationDistribution.getId(),
                libelle: PAGES_METHODOLOGIE.transformationDistribution.getTitreCourt(),
                href: PAGES_METHODOLOGIE.transformationDistribution.getUrl()
            },
            {
                id: PAGES_METHODOLOGIE.consommation.getId(),
                libelle: PAGES_METHODOLOGIE.consommation.getTitreCourt(),
                href: PAGES_METHODOLOGIE.consommation.getUrl()
            },
            {
                id: PAGES_METHODOLOGIE.sourcesDonnees.getId(),
                libelle: PAGES_METHODOLOGIE.sourcesDonnees.getTitreCourt(),
                href: PAGES_METHODOLOGIE.sourcesDonnees.getUrl()
            },
            {
                id: PAGES_METHODOLOGIE.licenceUtilisation.getId(),
                libelle: PAGES_METHODOLOGIE.licenceUtilisation.getTitreCourt(),
                href: PAGES_METHODOLOGIE.licenceUtilisation.getUrl()
            }
        ]
    }
];

export function construireLiensFilArianePagesMethodologie(libellePage: string, hrefPage: string): LienFilAriane[] {
    return [
        {
            libelle: `Aide`,
            href: PAGES_PRINCIPALES.aide.getUrl()
        },
        {
            libelle: `Méthodologie`,
            href: PAGES_AIDE.methodologie.getUrl()
        },
        {
            libelle: libellePage,
            href: hrefPage
        }
    ];
}
