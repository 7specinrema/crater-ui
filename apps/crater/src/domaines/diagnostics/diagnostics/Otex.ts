import { sommeArray } from '@lga/commun/src/outils/base';

interface OtexEntite {
    libelle: string;
    couleur?: string;
    otex5Postes?: OtexEntite;
}

export interface NbCommunesOtex {
    otex: OtexEntite;
    valeur: number;
}

interface NbCommunesParOtex {
    grandesCultures: number;
    maraichageHorticulture: number;
    viticulture: number;
    fruits: number;
    bovinLait: number;
    bovinViande: number;
    bovinMixte: number;
    ovinsCaprinsAutresHerbivores: number;
    porcinsVolailles: number;
    polyculturePolyelevage: number;
    nonClassees: number;
    sansExploitations: number;
    nonRenseigne: number;
}

const Otex5Postes = {
    grandesCultures: {
        libelle: 'Grandes Cultures'
    },
    maraichageHorticultureViticultureFruits: {
        libelle: 'Maraîchage Horticulture, Viticulture ou Fruits'
    },
    elevage: {
        libelle: 'Élevage'
    },
    polyculturePolyelevage: {
        libelle: 'Polyculture Polyélevage'
    },
    nonDefinie: {
        libelle: 'Non définie'
    }
};

const Otex12Postes = {
    grandesCultures: {
        libelle: 'Grandes Cultures',
        couleur: 'rgb(219, 246, 92)',
        otex5Postes: Otex5Postes.grandesCultures
    },
    maraichageHorticulture: {
        libelle: 'Maraîchage Horticulture',
        couleur: 'rgb(91, 126, 113)',
        otex5Postes: Otex5Postes.maraichageHorticultureViticultureFruits
    },
    viticulture: {
        libelle: 'Viticulture',
        couleur: 'rgb(127, 74, 91)',
        otex5Postes: Otex5Postes.maraichageHorticultureViticultureFruits
    },
    fruits: {
        libelle: 'Fruits',
        couleur: 'rgb(80, 168, 92)',
        otex5Postes: Otex5Postes.maraichageHorticultureViticultureFruits
    },
    bovinLait: {
        libelle: 'Bovin lait',
        couleur: 'rgb(121, 69, 27)',
        otex5Postes: Otex5Postes.elevage
    },
    bovinViande: {
        libelle: 'Bovin viande',
        couleur: 'rgb(165, 33, 33)',
        otex5Postes: Otex5Postes.elevage
    },
    bovinMixte: {
        libelle: 'Bovin mixte',
        couleur: 'rgb(65, 44, 38)',
        otex5Postes: Otex5Postes.elevage
    },
    ovinsCaprinsAutresHerbivores: {
        libelle: 'Ovins Caprins et autres Herbivores',
        couleur: 'rgb(188, 138, 134)',
        otex5Postes: Otex5Postes.elevage
    },
    porcinsVolailles: {
        libelle: 'Porcins Volailles',
        couleur: 'rgb(236, 95, 71)',
        otex5Postes: Otex5Postes.elevage
    },
    polyculturePolyelevage: {
        libelle: 'Polyculture Polyélevage',
        couleur: 'rgb(168, 106, 66)',
        otex5Postes: Otex5Postes.polyculturePolyelevage
    },
    nonClassees: {
        libelle: 'Non Classées',
        couleur: 'rgb(213, 199, 176)',
        otex5Postes: Otex5Postes.nonDefinie
    },
    sansExploitations: {
        libelle: 'Sans Exploitations',
        couleur: 'rgb(255, 255, 255)',
        otex5Postes: Otex5Postes.nonDefinie
    },
    nonRenseigne: {
        libelle: 'Non renseignées',
        couleur: 'rgb(100, 100, 100)',
        otex5Postes: Otex5Postes.nonDefinie
    }
};

export class Otex {
    public readonly nbCommunesOtex12Postes: NbCommunesOtex[] = [];
    public readonly nbCommunesOtex5Postes: NbCommunesOtex[] = [];

    constructor(nbCommunesParOtex?: NbCommunesParOtex) {
        if (nbCommunesParOtex) {
            this.nbCommunesOtex12Postes = [
                {
                    otex: Otex12Postes.grandesCultures,
                    valeur: nbCommunesParOtex.grandesCultures
                },
                {
                    otex: Otex12Postes.maraichageHorticulture,
                    valeur: nbCommunesParOtex.maraichageHorticulture
                },
                {
                    otex: Otex12Postes.viticulture,
                    valeur: nbCommunesParOtex.viticulture
                },
                {
                    otex: Otex12Postes.fruits,
                    valeur: nbCommunesParOtex.fruits
                },
                {
                    otex: Otex12Postes.bovinLait,
                    valeur: nbCommunesParOtex.bovinLait
                },
                {
                    otex: Otex12Postes.bovinViande,
                    valeur: nbCommunesParOtex.bovinViande
                },
                {
                    otex: Otex12Postes.bovinMixte,
                    valeur: nbCommunesParOtex.bovinMixte
                },
                {
                    otex: Otex12Postes.ovinsCaprinsAutresHerbivores,
                    valeur: nbCommunesParOtex.ovinsCaprinsAutresHerbivores
                },
                {
                    otex: Otex12Postes.porcinsVolailles,
                    valeur: nbCommunesParOtex.porcinsVolailles
                },
                {
                    otex: Otex12Postes.polyculturePolyelevage,
                    valeur: nbCommunesParOtex.polyculturePolyelevage
                },
                {
                    otex: Otex12Postes.nonClassees,
                    valeur: nbCommunesParOtex.nonClassees
                },
                {
                    otex: Otex12Postes.sansExploitations,
                    valeur: nbCommunesParOtex.sansExploitations
                },
                {
                    otex: Otex12Postes.nonRenseigne,
                    valeur: nbCommunesParOtex.nonRenseigne
                }
            ];
        }
        this.nbCommunesOtex5Postes = Object.values(Otex5Postes).map((otex5Postes) => {
            return {
                otex: otex5Postes,
                valeur: sommeArray(this.nbCommunesOtex12Postes.filter((i) => i.otex.otex5Postes == otex5Postes).map((i) => i.valeur))!
            };
        });
    }

    public get nbCommunesOtexMajoritaire5Postes(): NbCommunesOtex {
        return this.otexMajoritaire(this.nbCommunesOtex5Postes);
    }

    private otexMajoritaire(nbCommunesOtex: NbCommunesOtex[]): NbCommunesOtex {
        return nbCommunesOtex.reduce(function (otex1, otex2) {
            return otex1.valeur > otex2.valeur ? otex1 : otex2;
        });
    }
}
