import { DonneesPage, DonneesPageSimple } from './donnees-pages';
import { ModelePage, OptionsModelePage } from './ModelePage';

export interface OptionsModelePageStatique {
    url: string;
    titreCourt: string;
    urlCanonique?: string;
    titreLong?: string;
    metaDescription?: string;
}

export class ModelePageStatique extends ModelePage<DonneesPage> {
    constructor(optionsModelePageStatique: OptionsModelePageStatique) {
        const optionsModelePage: OptionsModelePage<DonneesPage> = {
            url: () => optionsModelePageStatique.url,
            titreCourt: optionsModelePageStatique.titreCourt,
            urlCanonique: optionsModelePageStatique.urlCanonique ? () => optionsModelePageStatique.urlCanonique! : undefined,
            titreLong: optionsModelePageStatique.titreLong ? () => optionsModelePageStatique.titreLong! : undefined,
            metaDescription: optionsModelePageStatique.metaDescription ? () => optionsModelePageStatique.metaDescription! : undefined
        };
        super(optionsModelePage);
    }

    getId(donneesPageSimple?: DonneesPageSimple): string {
        return super.getId(donneesPageSimple ?? ({} as DonneesPage));
    }
    getUrl(donneesPageSimple?: DonneesPageSimple): string {
        return super.getUrl(donneesPageSimple ?? ({} as DonneesPage));
    }
    getTitreLong(donneesPageSimple?: DonneesPageSimple): string {
        return super.getTitreLong(donneesPageSimple ?? ({} as DonneesPage));
    }
    getUrlCanonique(donneesPageSimple?: DonneesPageSimple): string {
        return super.getUrlCanonique(donneesPageSimple ?? ({} as DonneesPage));
    }
    getMetaDescription(donneesPageSimple?: DonneesPageSimple): string | undefined {
        return super.getMetaDescription(donneesPageSimple ?? ({} as DonneesPage));
    }
}
