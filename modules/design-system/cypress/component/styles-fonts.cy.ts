import '../../public/theme-defaut/theme-defaut.css';

import { html } from 'lit';

import { STYLES_DESIGN_SYSTEM } from '../../src/styles/styles-design-system';

describe('Test styles fonts', () => {
    it('Vérifier les polices de caractères', () => {
        cy.mount<'div'>(html`
            <style>
                ${STYLES_DESIGN_SYSTEM}
            </style>
            <div>
                <p class="texte-petit">Texte petit (Poppins 600)</p>
                <p class="texte-petit-majuscule">Texte petit majuscule (Poppins 600)</p>
                <p class="texte-moyen">Texte moyen (Poppins 400)</p>
                <p class="texte-titre">Texte titre (Poppins 600)</p>
                <p class="texte-alternatif">Texte alternatif (Montserrat 700)</p>
                <p class="titre-petit">Titre petit (Montserrat 700)</p>
                <p class="titre-moyen">Titre moyen (Montserrat 700)</p>
                <p class="titre-large">Titre large (Montserrat 700)</p>
            </div>
        `);

        cy.get('.texte-petit') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-texte-gras/);
        cy.get('.texte-petit-majuscule') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-texte-gras/);
        cy.get('.texte-moyen') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-texte/);
        cy.get('.texte-titre') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-texte-gras/);
        cy.get('.texte-alternatif') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-texte/);
        cy.get('.titre-petit') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-titre/);
        cy.get('.titre-moyen') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-titre/);
        cy.get('.titre-large') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-titre/);
    });
});
