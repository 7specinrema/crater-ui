import '@lga/design-system/build/composants/BoiteAvecLien.js';

import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-boite-lien-vers-page': BoiteLienVersPage;
    }
}

@customElement('c-boite-lien-vers-page')
export class BoiteLienVersPage extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100%;
                height: 100%;
            }

            #contenu {
                display: flex;
                padding: var(--dsem);
            }

            #illustration {
                display: flex;
                justify-content: center;
                align-items: center;
            }

            #illustration ::slotted(img) {
                width: 100%;
            }

            #contenu-texte {
                width: 100%;
                padding: var(--dsem);
            }

            #titre {
                color: var(--couleur-primaire-sombre);
            }

            #texte {
                color: var(--couleur-neutre);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                #illustration {
                    display: none;
                }
            }
        `
    ];

    @property()
    titre? = '';

    @property()
    libelleLien = '';

    @property()
    href = '';

    @property({ type: Boolean })
    encadrement = false;

    render() {
        return html`
            <c-boite-avec-lien libelleLien=${this.libelleLien} href=${this.href} ?encadrement=${this.encadrement}>
                <div slot="contenu" id="contenu">
                    <div id="illustration"><slot name="illustration"></slot></div>
                    <div id="contenu-texte">
                        ${this.titre ? html`<div id="titre" class="texte-titre">${this.titre}</div>` : ''}
                        <slot id="texte" class="texte-moyen" name="texte"></slot>
                    </div>
                </div>
            </c-boite-avec-lien>
        `;
    }
}
