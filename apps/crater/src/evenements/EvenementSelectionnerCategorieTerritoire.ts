import { EvenementPersonnalisable } from '@lga/design-system/build/evenements/EvenementPersonnalisable';

export class EvenementSelectionnerCategorieTerritoire extends EvenementPersonnalisable<{ codeCategorieTerritoire: string }> {
    static ID = 'selectionnerCategorieTerritoire';
    constructor(codeCategorieTerritoire: string) {
        super(EvenementSelectionnerCategorieTerritoire.ID, { codeCategorieTerritoire: codeCategorieTerritoire });
    }
}
