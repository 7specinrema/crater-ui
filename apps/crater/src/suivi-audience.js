window._paq = window._paq || [];

let urlPrecedante = '';

const ID_SITE_MATOMO_PROD = '7';
const ID_SITE_MATOMO_DEV = '4';

initaliserSuiviAudience();

function initaliserSuiviAudience() {
    if (import.meta.env.VITE_MATOMO_SITE_ID === ID_SITE_MATOMO_DEV || import.meta.env.VITE_MATOMO_SITE_ID === ID_SITE_MATOMO_PROD) {
        (function () {
            const baseUrlMatomo = 'https://matomo.resiliencealimentaire.org/';
            window._paq.push(['setTrackerUrl', baseUrlMatomo + 'matomo.php']);
            window._paq.push(['setSiteId', `${import.meta.env.VITE_MATOMO_SITE_ID}`]);
            // const scriptMatomo = document.getElementById('script-matomo');
            const scriptMatomo = document.createElement('script');
            scriptMatomo.async = true;
            scriptMatomo.src = baseUrlMatomo + 'matomo.js';
            document.querySelector('head').appendChild(scriptMatomo);
        })();
    }
}

export function enregistrerSuiviPage(url, titre) {
    window._paq.push(['setCustomUrl', url]);
    window._paq.push(['setDocumentTitle', titre]);
    if (urlPrecedante) {
        window._paq.push(['setReferrerUrl', urlPrecedante]);
    }
    urlPrecedante = url;
    window._paq.push(['trackPageView']);
    window._paq.push(['enableLinkTracking']);
    window._paq.push(['enableHeartBeatTimer']);
}
