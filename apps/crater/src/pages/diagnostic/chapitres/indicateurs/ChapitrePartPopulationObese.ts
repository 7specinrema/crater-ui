import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-population-obese': ChapitrePartPopulationObese;
    }
}

@customElement('c-chapitre-part-population-obese')
export class ChapitrePartPopulationObese extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.partPopulationObese}"
                .options=${<OptionsIndicateurDetaille>{
                    indicateur: SA.getIndicateur(IDS_INDICATEURS.partPopulationObese),
                    donneesPage: this.donneesPage
                }}
                contenuAdhoc
            >
                <p slot="contenu">
                    En France, l'obésité touche environ 8,5 millions de personnes ce qui représente 17% de le population. Ce taux a triplé en 30 ans,
                    il était de 5,85% en 1992.
                </p>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }
}
