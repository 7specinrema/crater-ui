import { ConsommationParGroupeCulture, IndicateurSynthese, Rapport } from '../../../domaines/diagnostics';
import { construireUrlPageCarteTerritoire } from '../../../domaines/pages/pages-utils';
import { DonneesTreemapCultures, DonneesTreemapDetailCultures } from '../charts/ChartTreemapCultures';
import { OptionsSyntheseMaillon } from './SyntheseMaillon';

export function creerDonneesTreemapConsommation(consommationParGroupeCultures: ConsommationParGroupeCulture[]) {
    return consommationParGroupeCultures.map(
        (b) =>
            <DonneesTreemapCultures>{
                code: b!.groupeCulture.code,
                labelCourt: b!.groupeCulture.nomCourt,
                labelLong: b!.nomGroupeCulture,
                couleur: b!.groupeCulture.couleur,
                sauHa: b!.consommationHa,
                detailCultures: b!.consommationParCulture.map(
                    (s) =>
                        <DonneesTreemapDetailCultures>{
                            codeCulture: s.codeCulture,
                            nomCulture: s.nomCulture,
                            sauHa: s.consommationHa
                        }
                )
            }
    );
}

export function creerOptionsSyntheseMaillon(
    rapport: Rapport,
    indicateurSynthese: IndicateurSynthese,
    idMaillon: string,
    idIndicateurLienCarte?: string
): OptionsSyntheseMaillon {
    const lienCarte =
        rapport.echelleEstDisponibleDansCarte() && idIndicateurLienCarte
            ? construireUrlPageCarteTerritoire(idIndicateurLienCarte, rapport.getTerritoireActif()?.id ?? '')
            : undefined;

    return {
        note: indicateurSynthese.note,
        messageSynthese: indicateurSynthese.messageSynthese,
        idMaillon: idMaillon,
        lienCarte: lienCarte
    };
}
