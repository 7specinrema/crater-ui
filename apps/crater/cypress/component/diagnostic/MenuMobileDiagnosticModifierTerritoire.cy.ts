import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/composants/MenuMobileDiagnosticModifierTerritoire.js';

import { OptionsRadioBouton } from '@lga/design-system/build/composants/RadioBouton';
import { html } from 'lit';

describe('Test MenuMobileDiagnosticModifierTerritoire', () => {
    it('Test affichage nominal', () => {
        // given
        cy.viewport(500, 500);
        const optionsBoutonsTerritoires: OptionsRadioBouton = {
            boutons: [
                {
                    id: 'id1',
                    libelle: 'libelle1',
                    sousLibelle: 'libelleSecondaire1'
                },
                {
                    id: 'id2',
                    libelle: 'libelle2',
                    sousLibelle: 'libelleSecondaire2'
                }
            ],
            idBoutonActif: 'id2'
        };

        cy.mount<'c-menu-mobile-diagnostic-modifier-territoire'>(html`
            <c-menu-mobile-diagnostic-modifier-territoire .optionsEchellesTerritoriales="${optionsBoutonsTerritoires}">
            </c-menu-mobile-diagnostic-modifier-territoire>
        `);
        // then
        cy.get('c-champ-recherche-territoire').should('be.visible');
        cy.get('c-radio-bouton').should('be.visible');
    });
});
