import { formaterNombreEnEntierHtml, fusionner } from '@lga/commun/build/outils/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { ApexOptions } from 'apexcharts';
import ApexCharts from 'apexcharts';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

import { APEX_OPTIONS_GLOBALES } from './chart-config';

export interface OptionsDonnesTreemapCultures {
    donnees: DonneesTreemapCultures[];
    hauteur: number;
    activerFillPattern?: boolean;
}

export interface DonneesTreemapCultures {
    code: string;
    labelCourt: string;
    labelLong: string;
    couleur: string;
    sauHa: number;
    detailCultures: DonneesTreemapDetailCultures[];
}

export interface DonneesTreemapDetailCultures {
    codeCulture: string;
    nomCulture: string;
    sauHa: number;
}

const FILL_PATTERN = {
    type: 'pattern',
    pattern: {
        style: 'verticalLines',
        width: 6,
        height: 6,
        strokeWidth: 8
    }
};

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-treemap-cultures': ChartTreemapCultures;
    }
}

@customElement('c-chart-treemap-cultures')
export class ChartTreemapCultures extends LitElement {
    private chart?: ApexCharts;

    @property({ attribute: false })
    options?: OptionsDonnesTreemapCultures;

    @query('figure')
    private figure?: HTMLElement;

    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            figure {
                margin: auto;
                width: 250px;
            }

            .tooltip h1 {
                background-color: var(--couleur-neutre-20);
                color: var(--couleur-neutre);
                text-transform: none;
                padding: 0.3rem 0.5rem;
                margin: 0;
            }

            .tooltip .dot {
                position: relative;
                top: 2px;
                height: 1rem;
                width: 1rem;
                background-color: #bbb;
                border-radius: 50%;
                display: inline-block;
                margin-right: 0.3rem;
            }

            .tooltip ul {
                padding: 0 0.5rem;
                margin: 0.2rem 0;
            }

            .tooltip li {
                list-style: none;
            }

            rect {
                stroke-width: 1px;
            }
            text {
                fill: var(--couleur-neutre);
                font-size: 12px;
            }
        `
    ];

    render() {
        return html`
            <div>
                <figure></figure>
            </div>
        `;
    }

    async updated() {
        if (!this.options) return;

        if (this.chart) {
            this.chart?.updateOptions(this.calculerApexOptions(this.options), true, true);
        } else {
            this.chart = new ApexCharts(this.figure, this.calculerApexOptions(this.options));
            this.chart?.render();
            // Le premier render d'apex ne fonctionne pas dans lit (diagramme vide). Contournement : on attend la fin du render, et on fait un update
            this.chart?.updateOptions(this.calculerApexOptions(this.options), true, true);
        }
    }

    private calculerApexOptions({ donnees, hauteur, activerFillPattern = false }: OptionsDonnesTreemapCultures): ApexOptions {
        const donneesNonNulles = donnees.filter((d) => d.sauHa !== 0);
        const donneesApex = donneesNonNulles.map((d) => {
            return { x: d.labelCourt, y: d.sauHa };
        });
        const couleursApex = donneesNonNulles.map((d) => d.couleur);

        const apexOptionsLocales: ApexOptions = {
            series: [{ data: donneesApex }],
            legend: {
                show: false
            },
            chart: {
                height: hauteur,
                type: 'treemap',
                animations: {
                    enabled: false
                }
            },
            colors: couleursApex,
            dataLabels: {
                enabled: true
            },
            plotOptions: {
                treemap: {
                    distributed: true,
                    enableShades: false
                }
            },
            tooltip: {
                custom: (contexteApex: { dataPointIndex: number }) => {
                    return ChartTreemapCultures.calculerHtmlTooltip(donneesNonNulles, contexteApex.dataPointIndex);
                }
            },
            fill: activerFillPattern ? FILL_PATTERN : {}
        };

        return <ApexOptions>fusionner(APEX_OPTIONS_GLOBALES, apexOptionsLocales);
    }

    private static calculerHtmlTooltip(donnees: DonneesTreemapCultures[], dataPointIndex: number) {
        return `
<div class="tooltip">
  <h1 class="texte-titre"><span class="dot" style="background-color: ${donnees[dataPointIndex].couleur}"></span>${
            donnees[dataPointIndex].labelLong
        } : ${formaterNombreEnEntierHtml(donnees[dataPointIndex].sauHa)} ha</h1>
  <ul>${donnees[dataPointIndex].detailCultures
      .map((dc) => {
          return `
     <li class="texte-moyen">${dc.nomCulture} : ${formaterNombreEnEntierHtml(dc.sauHa)} ha</li>
      `;
      })
      .join('\n')}
  </ul>
</div>`;
    }
}
