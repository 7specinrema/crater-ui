import { EvenementPersonnalisable } from '@lga/design-system/build/evenements/EvenementPersonnalisable';

import { DonneesPage } from '../domaines/pages/donnees-pages';

export class EvenementMajDonneesPage<D extends DonneesPage> extends EvenementPersonnalisable<{ donneesPage: D }> {
    static ID = 'majDonneesPage';
    constructor(donneesPage: D) {
        super(EvenementMajDonneesPage.ID, { donneesPage: donneesPage });
    }
}
