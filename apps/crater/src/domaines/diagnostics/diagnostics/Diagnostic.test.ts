import { describe, expect, it } from 'vitest';

import { CategorieTerritoire, Territoire } from '../../territoires';
import { AgriculteursExploitations, ClassesAges, ClassesSuperficies } from './agriculteurs-exploitations';
import { Consommation } from './consommation';
import { Diagnostic } from './Diagnostic';
import { IndicateursPesticides, IndicateursPesticidesParAnnees, Intrants } from './intrants';
import { Otex } from './Otex';
import { IndicateurHvn, Production } from './production';
import { SurfaceAgricoleUtile } from './SurfaceAgricoleUtile';
import { PolitiqueAmenagement, TerresAgricoles } from './terres-agricoles';
import { TransformationDistribution } from './transformation-distribution';

describe('Tests de la classe Diagnostic', () => {
    it('Valider le calcul des champs dynamiques', () => {
        const diagnostic = creerDiagnostic();

        expect(diagnostic.densitePopulationHabParKM2).toEqual(200000);
        expect(diagnostic.pourcentageSauTotaleSurSuperficie).toEqual(20);
    });

    it('Valider le calcul des champs utilisants le diagnostic pays', () => {
        const diagnostic = creerDiagnostic();
        diagnostic.setDiagnosticPays(creerDiagnosticPays());
        expect(diagnostic.rapportDensitePopulationHabParM2SurPays).toEqual(0.1);
        expect(diagnostic.production.rapportPourcentageSauBioSurPays).toEqual(0.5);
    });
});

function creerDiagnostic() {
    const surfaceAgricoleUtile = new SurfaceAgricoleUtile(1, 0, 0, 0, []);
    const territoire = new Territoire('C-1', 'NomCommune', CategorieTerritoire.Commune);
    const otex = new Otex({
        grandesCultures: 1,
        maraichageHorticulture: 2,
        viticulture: 0,
        fruits: 0,
        bovinLait: 0,
        bovinViande: 0,
        bovinMixte: 0,
        ovinsCaprinsAutresHerbivores: 0,
        porcinsVolailles: 0,
        polyculturePolyelevage: 0,
        nonClassees: 0,
        sansExploitations: 0,
        nonRenseigne: 0
    });
    const diagnostic = new Diagnostic(
        territoire,
        10000,
        5,
        'A',
        1,
        otex,
        surfaceAgricoleUtile,
        new TerresAgricoles(territoire, 0, 0, 0, 0, 0, 0, PolitiqueAmenagement.DONNEES_NON_DISPONIBLES, 0, 0),
        new AgriculteursExploitations(
            territoire,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            new ClassesAges(0, 0, 0, 0),
            new ClassesSuperficies(0, 0, 0, 0, 0),
            new ClassesSuperficies(0, 0, 0, 0, 0)
        ),
        new Intrants(territoire, 0, new IndicateursPesticides(new IndicateursPesticidesParAnnees([]))),
        new Production(territoire, 0, 0, 0, 0, 0, 0, new IndicateurHvn(0, 0, 0, 0), 4, 5),
        new TransformationDistribution(territoire, 0, 0, 0, []),
        new Consommation(territoire, 0, 0, null)
    );

    return diagnostic;
}

function creerDiagnosticPays() {
    const surfaceAgricoleUtile = new SurfaceAgricoleUtile(10, 0, 0, 0, []);
    const territoire = new Territoire('P-FR', 'France', CategorieTerritoire.Pays);
    const otex = new Otex({
        grandesCultures: 100,
        maraichageHorticulture: 200,
        viticulture: 100,
        fruits: 0,
        bovinLait: 0,
        bovinViande: 0,
        bovinMixte: 0,
        ovinsCaprinsAutresHerbivores: 0,
        porcinsVolailles: 0,
        polyculturePolyelevage: 0,
        nonClassees: 0,
        sansExploitations: 0,
        nonRenseigne: 0
    });
    return new Diagnostic(
        territoire,
        100000,
        5,
        'A',
        1,
        otex,
        surfaceAgricoleUtile,
        new TerresAgricoles(territoire, 0, 0, 0, 0, 0, 0, PolitiqueAmenagement.DONNEES_NON_DISPONIBLES, 0, 0),
        new AgriculteursExploitations(
            territoire,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            new ClassesAges(0, 0, 0, 0),
            new ClassesSuperficies(0, 0, 0, 0, 0),
            new ClassesSuperficies(0, 0, 0, 0, 0)
        ),
        new Intrants(territoire, 0, new IndicateursPesticides(new IndicateursPesticidesParAnnees([]))),
        new Production(territoire, 0, 0, 0, 0, 0, 0, new IndicateurHvn(0, 0, 0, 0), 8, 10),
        new TransformationDistribution(territoire, 0, 0, 0, []),
        new Consommation(territoire, 0, 0, null)
    );
}
