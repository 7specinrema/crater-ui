import '../../charts/ChartBarres.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import { OptionsChartBarres } from '../../charts/ChartBarres.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-territoire-dependant-voiture': ChapitrePartTerritoireDependantVoiture;
    }
}

@customElement('c-chapitre-part-territoire-dependant-voiture')
export class ChapitrePartTerritoireDependantVoiture extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.partTerritoireDependantVoiture}"
                .options=${this.calculerOptionsIndicateurDetaillePartTerritoireDependantVoiture(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="3"
            >
                <c-chart-barres
                    .options=${this.calculerOptionsChartPartTerritoireDependantVoiture(this.rapport)}
                    slot="chart-indicateur-detaille"
                ></c-chart-barres>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetaillePartTerritoireDependantVoiture(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partTerritoireDependantVoiture)!,
            message: rapport.getDiagnosticActif()?.transformationDistribution.messagePartTerritoireDependantVoiture,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }

    private calculerOptionsChartPartTerritoireDependantVoiture(rapport: Rapport): OptionsChartBarres {
        return {
            series: new SerieMultiple(
                [new Serie(rapport.getListeDiagnostics()!.map((d) => d!.transformationDistribution.partTerritoireDependantVoiturePourcent))],
                rapport.getListeDiagnostics()!.map((d) => d!.territoire.nom)
            ),
            nomAxeOrdonnees: 'Part du territoire [%]'
        };
    }
}
