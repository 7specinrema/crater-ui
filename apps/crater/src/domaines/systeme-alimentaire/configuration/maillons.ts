import { LienIndicateur } from '../../../pages/diagnostic/chapitres/composants/VoirEgalement';
import { Citation, Maillon } from '../Maillon';
import { SystemeAlimentaire } from '../SystemeAlimentaire';
import { ICONES_SVG } from './icones_svg';
import { IDS_INDICATEURS } from './indicateurs';
import { IDS_OUTILS_EXTERNES } from './outils_externes';
import { IDS_VOIES_RESILIENCE } from './voies_resilience';

export const IDS_MAILLONS = {
    terresAgricoles: 'terres-agricoles' as const,
    agriculteursExploitations: 'agriculteurs-exploitations' as const,
    intrants: 'intrants' as const,
    production: 'production' as const,
    transformationDistribution: 'transformation-distribution' as const,
    consommation: 'consommation' as const
};

interface DonneesMaillon {
    id: string;
    nom: string;
    icone: string;
    description: string;
    citations: Citation[];
    idsVoiesResilience: string[];
    idsOutilsExternes: string[];
    indicateursAutresMaillons: LienIndicateur[];
    lienVersCarteDesactive?: boolean;
    informationComplementaire?: string;
}

export function creerMaillons(systemeAlimentaire: SystemeAlimentaire): Maillon[] {
    const donneesMaillons: DonneesMaillon[] = [
        {
            id: IDS_MAILLONS.terresAgricoles,
            nom: 'Terres agricoles',
            description: `
            Depuis des décennies, les constructions humaines s’étendent à un rythme soutenu, <strong>détruisant de façon souvent irréversible</strong> des terres agricoles fertiles situées à proximité de nos lieux de vie.
            <br>Face aux incertitudes qui pèsent sur notre capacité à maintenir des rendements élevés – du fait du changement climatique, de l’épuisement des ressources et de l’effondrement de la biodiversité – conserver un maximum de surfaces agricoles productives constitue un élément de résilience de premier plan.
            <br>Il est donc impératif de protéger les terres agricoles en se fixant un objectif de <strong>zéro artificialisation brute</strong>.
            `,
            citations: [
                {
                    texte: `L'équivalent d'un département moyen est artificialisé tous les 10 ans.`,
                    source: `<c-lien href="https://www.lemonde.fr/planete/article/2019/03/13/l-artificialisation-des-sols-progresse-meme-sans-pression-demographique-et-economique_5435447_3244.html">Le Monde, 2015</c-lien>`
                },
                {
                    texte: `Et ce n’est pas simplement dû à la croissance démographique puisque les sols artificialisés progressent trois fois plus vite que la population française.`,
                    source: `d’après Commissariat Général au Développement Durable (2018b) Biodiversité. Les chiffres clés édition 2018`
                }
            ],
            icone: ICONES_SVG.terresAgricoles,
            idsVoiesResilience: [IDS_VOIES_RESILIENCE.VRL_2],
            idsOutilsExternes: [
                IDS_OUTILS_EXTERNES.tableauDeBord,
                IDS_OUTILS_EXTERNES.parcelPourTerresAgricoles,
                IDS_OUTILS_EXTERNES.cartofriches,
                IDS_OUTILS_EXTERNES.geoportail
            ],
            indicateursAutresMaillons: [
                {
                    question: 'Quels agriculteurs cultivent ces terres ?',
                    idIndicateur: IDS_INDICATEURS.partActifsAgricoles
                },
                {
                    question: 'Quelles productions sur mon territoire ? Quelle adéquation théorique avec les consommations ?',
                    idIndicateur: IDS_INDICATEURS.adequationTheoriqueProductionConsommation
                },
                {
                    question: 'Quels besoins en surfaces agricoles pour nourrir les animaux d’élevage ?',
                    idIndicateur: IDS_INDICATEURS.partAlimentationAnimaleDansConsommation
                }
            ]
        },
        {
            id: IDS_MAILLONS.agriculteursExploitations,
            nom: 'Agriculteurs & Exploitations',
            description: `
            En déclin depuis des décennies, la profession agricole est <strong>vieillissante et peine à se renouveler</strong>. Pourtant, des agriculteurs en nombre suffisant sont essentiels pour :
            <ul>
                <li>lutter contre l’agrandissement et l’endettement croissants des exploitations agricoles ;</li>
                <li>lutter contre l’homogénéisation et à la rigidification des systèmes de production agricole ;</li>
                <li>améliorer leurs conditions de vie : lutter contre l’isolement social, diminuer leur charge de travail, augmenter leurs revenus ;</li>
                <li>faire face aux besoins plus élevés en main-d’œuvre des systèmes agroécologiques et limiter l’usage et la dépendance aux machines.</li>
            </ul>    
            Il est donc impératif de renouveler et même d’augmenter le nombre d’agriculteurs.
            `,
            citations: [
                {
                    texte: `La France comptera <strong>un quart d’agriculteurs en moins</strong> d’ici une dizaine d’années si rien n’est fait pour freiner la tendance.`,
                    source: `d’après Agreste (2019) GraphAgri 2019`
                }
            ],
            icone: ICONES_SVG.agriculteursExploitations,
            idsVoiesResilience: [IDS_VOIES_RESILIENCE.VRL_1],
            idsOutilsExternes: [IDS_OUTILS_EXTERNES.tableauDeBord, IDS_OUTILS_EXTERNES.parcelPourAgriculteursEtExploitations],
            indicateursAutresMaillons: []
        },
        {
            id: IDS_MAILLONS.intrants,
            nom: 'Intrants',
            informationComplementaire: '[Pesticides uniquement]',
            description: `
            Les systèmes agricoles sont dépendants d’intrants agricoles tels que l’eau, les semences, les engrais, les pesticides, les combustibles, le matériel agricole, l’alimentation animale… Ces intrants sont en général importés depuis l’extérieur des territoires et proposés par un petit nombre de fournisseurs, pour la plupart des multinationales.
            <br>Ce manque d’autonomie constitue une vulnérabilité dans un contexte de contraintes économiques, géopolitiques et énergétiques (disponibilité et prix des engrais, pesticides, combustibles…) et de changement climatique (disponibilité de la ressource eau).
            <br>Il est donc impératif de réduire ces dépendances en favorisant l'<strong>autonomie en ressources matérielles et énergétiques</strong>.
            `,
            citations: [
                {
                    texte: `En moins d’un siècle, les fermes sont passées d’une situation d’autonomie énergétique (les animaux de trait étaient alimentés par les cultures et les prairies) à une dépendance quasi-totale aux énergies fossiles.`,
                    source: `Les Greniers d’Abondance (2020), Vers la résilience alimentaire (page 70)`
                }
            ],
            icone: ICONES_SVG.intrants,
            idsVoiesResilience: [IDS_VOIES_RESILIENCE.VRL_3, IDS_VOIES_RESILIENCE.VRL_4, IDS_VOIES_RESILIENCE.VRL_5],
            idsOutilsExternes: [IDS_OUTILS_EXTERNES.tableauDeBord],
            indicateursAutresMaillons: [
                {
                    question: 'Les pratiques agricoles sont-elles sobres en intrants ?',
                    idIndicateur: IDS_INDICATEURS.partSAUBio
                },
                {
                    question: 'Quels besoins en surfaces agricoles pour nourrir les animaux d’élevage ?',
                    idIndicateur: IDS_INDICATEURS.partAlimentationAnimaleDansConsommation
                }
            ],
            lienVersCarteDesactive: true
        },
        {
            id: IDS_MAILLONS.production,
            nom: 'Production',
            description: `
            Les paysages des campagnes françaises se sont fortement appauvris et uniformisés durant le XXe siècle du fait de l’intensification et la spécialisation de l’agriculture. Ces changements de pratiques sont à l’origine d’une <strong>dégradation profonde de l’environnement</strong> (pollution de l'air, des sols et de l'eau). La spécialisation conduit également à une inadéquation entre les productions d’un bassin de vie et les besoins de ses habitants et par voie de conséquence à une dépendance totale au pétrole pour le transport de marchandises.
            <br>Il est donc impératif d’adopter massivement des pratiques agroécologiques et d’évoluer vers une agriculture nourricière en réintroduisant de la diversité à tous les niveaux (types de cultures, cultures, et variétés culturales), et en favorisant l’autonomie des territoires et la sobriété dans l’usage des ressources.            
            `,
            citations: [
                {
                    texte: `Un tiers des oiseaux à disparu des milieux agricoles en trente ans et deux tiers des insectes ont déserté les prairies allemandes en seulement dix ans.`,
                    source: `Commissariat Général au Développement Durable (2018) Biodiversité. Les chiffres clés édition 2018
                    <br>et
                    <br>Seibold S. et al. (2019) Arthropod decline in grasslands and forests is associated with landscape-level drivers. Nature 574:671–674
                    `
                }
            ],
            icone: ICONES_SVG.production,
            idsVoiesResilience: [IDS_VOIES_RESILIENCE.VRL_6, IDS_VOIES_RESILIENCE.VRL_7],
            idsOutilsExternes: [
                IDS_OUTILS_EXTERNES.tableauDeBord,
                IDS_OUTILS_EXTERNES.parcelPourProduction,
                IDS_OUTILS_EXTERNES.osae,
                IDS_OUTILS_EXTERNES.territoiresBio
            ],
            indicateursAutresMaillons: [
                {
                    question: 'Quelle intensité d’usage en pesticides ?',
                    idIndicateur: IDS_INDICATEURS.qsaNodu
                },
                {
                    question: `Quels besoins en surfaces agricoles pour nourrir les animaux d’élevage ?`,
                    idIndicateur: IDS_INDICATEURS.partAlimentationAnimaleDansConsommation
                }
            ]
        },
        {
            id: IDS_MAILLONS.transformationDistribution,
            nom: 'Transformation & Distribution',
            description: `
            Au cours de la seconde moitié du XXe siècle, les activités de transformation et de distribution se sont concentrées à la fois économiquement et géographiquement. L’essentiel de la transformation et de la distribution repose désormais sur de <strong>grosses unités, distantes des fermes et des consommateurs</strong> entraînant un bouleversement de la logistique alimentaire et une <strong>dépendance totale au pétrole et aux infrastructures pour le transport des marchandises</strong>.
            <br>Afin de limiter la dépendance du système alimentaire aux transports et aux énergies fossiles et la concentration du pouvoir dans la chaîne agroalimentaire, il est donc essentiel de développer des filières territoriales.
            <br>Cela passe par la relocalisation d’unités de stockage, de transformation et de distribution, et la simplification de la logistique.
            `,
            citations: [
                {
                    texte: `En France, les déplacements des consommateurs pour leurs achats alimentaires sont réalisés à 90% en voiture et s’élèvent à plus de 60 kilomètres par semaine pour un foyer moyen.`,
                    source: `d’après Barbier C. et al. (2019) L’empreinte énergétique et carbone de l’alimentation en France. Club Ingénierie Prospective Énergie et Environnement, Paris.`
                }
            ],
            icone: ICONES_SVG.transformationDistribution,
            idsVoiesResilience: [IDS_VOIES_RESILIENCE.VRL_8, IDS_VOIES_RESILIENCE.VRL_9],
            idsOutilsExternes: [
                IDS_OUTILS_EXTERNES.tableauDeBord,
                IDS_OUTILS_EXTERNES.etudeIDDRI,
                IDS_OUTILS_EXTERNES.ObSAT,
                IDS_OUTILS_EXTERNES.transiscope
            ],
            indicateursAutresMaillons: []
        },
        {
            id: IDS_MAILLONS.consommation,
            nom: 'Consommation',
            informationComplementaire: "[A l'échelle 🇫🇷]",
            description: `
            L’élevage intensif des animaux utilisés pour couvrir notre surconsommation de protéines est responsable d’une part significative de nos émissions de gaz à effet de serre et mobilise la plus grande partie des productions végétales - qui doivent être beaucoup plus importantes que si elles étaient directement consommées par les humains -. D’autre part, environ <strong>un quart des Français</strong> se restreignent sur la quantité de ce qu’ils mangent pour des raisons financières alors que la malnutrition cause surpoids et obésité, et est devenue l’une des premières causes de mortalité. 
            <br>Pour réduire ces impacts environnementaux et sanitaires tout en garantissant la couverture des besoins alimentaires de la population, il est indispensable d’évoluer vers des régimes alimentaires plus sains et moins carnés, de réduire les gaspillages et de lutter contre la précarité alimentaire.
            `,
            citations: [
                {
                    texte: `Pour faire 1 calorie de poulet en système intensif, il faut 4 calories de blé et d'autres aliments dont plus de 80 % pourraient être consommés par les humains.`,
                    source: `d’après Laisse et al. (2018) : <c-lien href="https://doi.org/10.20870/productions-animales.2018.31.3.2355">L’efficience nette de conversion des aliments par les animaux d’élevage</c-lien>.`
                }
            ],
            icone: ICONES_SVG.consommation,
            idsVoiesResilience: [IDS_VOIES_RESILIENCE.VRL_10],
            idsOutilsExternes: [IDS_OUTILS_EXTERNES.tableauDeBord, IDS_OUTILS_EXTERNES.parcelPourConsommation],
            indicateursAutresMaillons: [
                {
                    question: 'Est-ce que la production est suffisante pour couvrir la consommation de mon territoire ?',
                    idIndicateur: IDS_INDICATEURS.adequationTheoriqueProductionConsommation
                },
                {
                    question: 'Quelle est la dépendance des consommateurs au pétrole pour faire leurs courses ?',
                    idIndicateur: IDS_INDICATEURS.partPopulationDependanteVoiture
                }
            ],
            lienVersCarteDesactive: true
        }
    ];

    return donneesMaillons.map(
        (m) =>
            new Maillon(
                m.id,
                m.nom,
                m.icone,
                m.description,
                m.citations,
                m.idsVoiesResilience.map((l) => systemeAlimentaire.getVoieResilience(l)!),
                m.idsOutilsExternes.map((l) => systemeAlimentaire.getOutilExterne(l)!),
                m.indicateursAutresMaillons,
                m.lienVersCarteDesactive,
                m.informationComplementaire
            )
    );
}
