import '../../charts/ChartBarres.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import { OptionsChartBarres } from '../../charts/ChartBarres.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-sau-bio': ChapitrePartSAUBio;
    }
}

@customElement('c-chapitre-part-sau-bio')
export class ChapitrePartSAUBio extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.partSAUBio}"
                .options=${this.calculerOptionsIndicateurDetaillePartSauBio(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="2"
            >
                <c-chart-barres .options=${this.calculerOptionsChartPartSauBio(this.rapport)} slot="chart-indicateur-detaille"></c-chart-barres>
            </c-indicateur-detaille> `;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetaillePartSauBio(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partSAUBio)!,
            message: rapport.getDiagnosticActif()!.production.messageSauBio,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }

    private calculerOptionsChartPartSauBio(rapport: Rapport): OptionsChartBarres {
        return {
            series: new SerieMultiple(
                [new Serie(rapport.getListeDiagnostics()!.map((d) => d!.production.partSauBioPourcent))],
                rapport.getListeDiagnostics()!.map((d) => d!.territoire.nom)
            ),
            nomAxeOrdonnees: 'Part de la SAU Bio sur la SAU Totale (%)'
        };
    }
}
