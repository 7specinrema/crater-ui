import { formaterNombreEnEntierString, formaterNombreEnNDecimalesString } from '@lga/commun/build/outils/base';

import { Territoire } from '../../../territoires';
import { IndicateurSynthese, Note } from '../IndicateurSynthese';
import { MessageHvn } from './MessageHvn';
import { calculerMessageSyntheseProduction } from './messages-production';

export class Production implements IndicateurSynthese {
    public productionPays?: Production;

    constructor(
        private territoire: Territoire,
        public readonly note: Note,
        public readonly tauxAdequationBrutPourcent: number,
        public readonly tauxAdequationMoyenPonderePourcent: number,
        public readonly noteTauxAdequationMoyenPondere: number | null,
        public readonly notePartSauBio: number | null,
        public readonly noteHvn: number | null,
        public readonly indicateurHvn: IndicateurHvn,
        public readonly sauBioHa: number | null,
        public readonly partSauBioPourcent: number | null
    ) {}

    setProductionPays(productionPays: Production) {
        this.productionPays = productionPays;
    }

    get messageSynthese() {
        return calculerMessageSyntheseProduction(
            this.tauxAdequationBrutPourcent,
            this.tauxAdequationMoyenPonderePourcent,
            this.notePartSauBio,
            this.noteHvn
        );
    }

    get messageHvn() {
        return MessageHvn.construireMessage(this.territoire.nom, this.indicateurHvn.indiceTotal);
    }

    get rapportPourcentageSauBioSurPays() {
        return this.partSauBioPourcent! / this.productionPays!.partSauBioPourcent!;
    }

    get messageSauBio() {
        let message;
        if (this.sauBioHa !== null) {
            message =
                `Pour le territoire <em>${this.territoire.nom}</em>, la surface agricole biologique est de ` +
                formaterNombreEnEntierString(this.sauBioHa) +
                ` ha ce qui représente ` +
                formaterNombreEnEntierString(this.partSauBioPourcent) +
                ` % de sa surface agricole utile productive soit ` +
                formaterNombreEnNDecimalesString(this.rapportPourcentageSauBioSurPays, 1) +
                ` fois la moyenne nationale.`;
        } else {
            message = `Pour le territoire <em>${this.territoire.nom}</em>, la surface agricole biologique est inconnue.`;
        }
        return message;
    }
}

export class IndicateurHvn {
    constructor(
        public readonly indice1: number | null,
        public readonly indice2: number | null,
        public readonly indice3: number | null,
        public readonly indiceTotal: number | null
    ) {}
}
