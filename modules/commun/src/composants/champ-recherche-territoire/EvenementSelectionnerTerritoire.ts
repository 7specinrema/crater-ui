import { EvenementPersonnalisable } from '@lga/design-system/build/evenements/EvenementPersonnalisable.js';

export class EvenementSelectionnerTerritoire extends EvenementPersonnalisable<{ idTerritoireCrater: string; idTerritoireParcel: string }> {
    static ID = 'selectionnerTerritoire';
    constructor(idTerritoireCrater: string, idTerritoireParcel: string) {
        super(EvenementSelectionnerTerritoire.ID, { idTerritoireCrater: idTerritoireCrater, idTerritoireParcel: idTerritoireParcel });
    }
}
