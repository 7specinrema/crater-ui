import { EvenementPersonnalisable } from '@lga/design-system/build/evenements/EvenementPersonnalisable';

export class EvenementDeplacerCarte extends EvenementPersonnalisable<{ idsDepartementsVisibles: string[]; zoom: number }> {
    static ID = 'deplacerCarte';
    constructor(idsDepartementsVisibles: string[], zoom: number) {
        super(EvenementDeplacerCarte.ID, { idsDepartementsVisibles: idsDepartementsVisibles, zoom: zoom });
    }
}
