import '../commun/template/TemplatePageSansSommaire.js';
import '@lga/design-system/build/composants/Lien.js';

import { getApiBaseUrl } from '@lga/commun/build/env/config-baseurl';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement, unsafeCSS } from 'lit';
import { customElement, query } from 'lit/decorators.js';
import SwaggerUI from 'swagger-ui';
import swaggerUiRawCss from 'swagger-ui/dist/swagger-ui.css';

import { PAGES_METHODOLOGIE, PAGES_PRINCIPALES } from '../../domaines/pages/configuration/declaration-pages.js';
import { STYLES_CRATER } from '../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-api': PageApi;
    }
}

@customElement('c-page-api')
export class PageApi extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, unsafeCSS(swaggerUiRawCss)];

    @query('#swagger-ui')
    private swaggerUiElement?: HTMLElement;

    render() {
        return html`
            <c-template-page-sans-sommaire idItemActifMenuPrincipal=${PAGES_PRINCIPALES.api.getId()}>
                <section slot="contenu">
                    <h1>Utilisez les données de CRATer dans votre application</h1>
                    <p>
                        L'ensemble des données et des résultats du calculateur CRATer sont disponibles sous forme d'APIs, dont la documentation
                        technique est décrite ci-dessous.
                    </p>
                    <p>
                        L'API est susceptible d'évoluer, sans garantie de compatibilité ascendante entre les versions. Si vous utilisez cette API dans
                        vos applications, merci de nous l'indiquer via le
                        <c-lien href=${PAGES_PRINCIPALES.contact.getUrl()}>formulaire de contact</c-lien> afin que nous puissions vous tenir informé
                        des impacts éventuels. Merci également de prendre connaissance des
                        <c-lien href=${PAGES_METHODOLOGIE.licenceUtilisation.getUrl()}>conditions d'utilisation des données</c-lien>.
                    </p>

                    <div id="swagger-ui"></div>
                </section>
            </c-template-page-sans-sommaire>
        `;
    }

    protected firstUpdated(): void {
        SwaggerUI({
            url: getApiBaseUrl() + 'crater/api/docs',
            domNode: this.swaggerUiElement
        });
    }
}
