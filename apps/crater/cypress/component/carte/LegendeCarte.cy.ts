import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/carte/composants/widget-carte/LegendeCarte';

import { html } from 'lit';

describe('Légende carte', () => {
    it('Test affichage desktop + mobile', () => {
        cy.viewport(320, 500);
        cy.mount<'c-legende-carte'>(
            html`<c-legende-carte
                .intervalles=${[
                    { couleur: 'red', libelleBorneInf: 0 },
                    { couleur: 'blue', libelleBorneInf: 400 },
                    { couleur: 'green', libelleBorneInf: 800 }
                ]}
            ></c-legende-carte>`
        );
        cy.get('c-legende-carte').find('.rond.debut').should('have.css', 'background-color', 'rgb(255, 0, 0)');
        cy.get('c-legende-carte').find('.rond.fin').should('have.css', 'background-color', 'rgb(0, 128, 0)');
    });
});
