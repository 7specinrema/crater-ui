import '../../charts/ChartBarres.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import { OptionsChartBarres } from '../../charts/ChartBarres.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-logements-vacants': ChapitrePartLogementsVacants;
    }
}

@customElement('c-chapitre-part-logements-vacants')
export class ChapitrePartLogementsVacants extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html` <c-indicateur-detaille
                id="indicateur-detaille-${IDS_INDICATEURS.partLogementsVacants}"
                .options=${this.calculerOptionsIndicateurDetaillePartLogementsVacants(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="2"
            >
                <c-chart-barres
                    .options=${this.calculerOptionsChartPartLogementsVacants(this.rapport)}
                    slot="chart-indicateur-detaille"
                ></c-chart-barres>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetaillePartLogementsVacants(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partLogementsVacants)!,
            message: rapport.getDiagnosticActif()!.terresAgricoles.messagePartLogementsVacants,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }

    private calculerOptionsChartPartLogementsVacants(rapport: Rapport): OptionsChartBarres {
        const labels = rapport.getListeDiagnostics()!.map((d) => d!.territoire.nom);
        return {
            series: new SerieMultiple(
                [
                    new Serie(
                        rapport.getListeDiagnostics().map((d) => d!.terresAgricoles.partLogementsVacants2013Pourcent),
                        '2013'
                    ),
                    new Serie(
                        rapport.getListeDiagnostics().map((d) => d!.terresAgricoles.partLogementsVacants2018Pourcent),
                        '2018'
                    )
                ],
                labels
            ),

            nomAxeOrdonnees: 'Part de logements vacants (%)'
        };
    }
}
