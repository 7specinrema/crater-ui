import '../../src/composants/champ-recherche-territoire/ChampRechercheTerritoire.js';
import '../../../design-system/public/theme-defaut/theme-defaut.css';

import { html } from 'lit';

import { EvenementReinitialiserTerritoire } from '../../src/composants/champ-recherche-territoire/EvenementReinitialiserTerritoire';
import { EvenementSelectionnerTerritoire } from '../../src/composants/champ-recherche-territoire/EvenementSelectionnerTerritoire';

describe('Test composant ChampRechercheTerritoire', () => {
    const RESPONSE_LISTE_SUGGESTIONS = {
        statusCode: 200,
        body: [
            {
                id: 'E-1',
                idParcel: '5',
                nom: 'EPCI-1',
                categorie: 'EPCI',
                libellePrincipal: 'EPCI1',
                libelleSecondaire: 'Intercommunalité de Commune1',
                estSuggere: true
            },
            { id: 'C-1', idParcel: '1', nom: 'Commune1', categorie: 'COMMUNE', libellePrincipal: 'Commune1 (00001)', libelleSecondaire: 'Commune' },
            { id: 'C-2', idParcel: '2', nom: 'Commune2', categorie: 'COMMUNE', libellePrincipal: 'Commune2 (00002)', libelleSecondaire: 'Commune' },
            { id: 'C-3', idParcel: '3', nom: 'Commune3', categorie: 'COMMUNE', libellePrincipal: 'Commune3 (00003)', libelleSecondaire: 'Commune' }
        ]
    };
    const RESPONSE_AUCUNE_SUGGESTION = {
        statusCode: 200,
        body: []
    };
    const RESPONSE_ERREUR_RESEAU = {
        statusCode: 404,
        body: 'Not Found'
    };

    it('Selectionner un element dans la liste de suggestions', () => {
        // given
        cy.intercept('GET', 'territoires?critere=*', RESPONSE_LISTE_SUGGESTIONS);
        cy.document()
            .then((doc) => {
                doc.addEventListener(EvenementSelectionnerTerritoire.ID, cy.stub().as('selectionnerTerritoire'));
            })
            .mount<'c-champ-recherche-territoire'>(
                html`<c-champ-recherche-territoire apiBaseUrl="http://serveurbidon"></c-champ-recherche-territoire>`
            );

        // when
        cy.get('c-champ-recherche-territoire').find('input').type('Com', { force: true });
        cy.get('c-champ-recherche-territoire').find('menu article').eq(2).click();

        // then
        cy.get('@selectionnerTerritoire').should('have.been.calledOnce');
        cy.get('c-champ-recherche-territoire').find('input').click().should('have.value', 'Commune2');
        cy.get('c-champ-recherche-territoire').find('menu article').should('not.exist');
    });

    it('Vider le champ input (click sur croix)', () => {
        // given
        cy.intercept('GET', 'territoires?critere=*', RESPONSE_LISTE_SUGGESTIONS);
        cy.document()
            .then((doc) => {
                doc.addEventListener(EvenementReinitialiserTerritoire.ID, cy.stub().as('reinitialiserTerritoire'));
            })
            .mount<'c-champ-recherche-territoire'>(
                html`<c-champ-recherche-territoire apiBaseUrl="http://serveurbidon"></c-champ-recherche-territoire>`
            );

        // when
        cy.get('c-champ-recherche-territoire').find('input').type('Com', { force: true });
        cy.get('c-champ-recherche-territoire').find('menu article').should('be.visible');
        cy.get('c-champ-recherche-territoire').find('#icone-croix').click();

        // then
        cy.get('@reinitialiserTerritoire').should('have.been.calledOnce');
        cy.get('c-champ-recherche-territoire').find('input').should('have.value', '');
        cy.get('c-champ-recherche-territoire').find('menu article').should('not.exist');
    });

    it('Aucun territoire trouvé', () => {
        cy.intercept('GET', 'territoires?critere=*', RESPONSE_AUCUNE_SUGGESTION);
        cy.mount<'c-champ-recherche-territoire'>(
            html`<c-champ-recherche-territoire
                apiBaseUrl="http://serveurbidon"
                lienFormulaireDemandeAjoutTerritoire="http://lienformulaire"
            ></c-champ-recherche-territoire>`
        );
        // when
        cy.get('c-champ-recherche-territoire').find('input').type('Commune inconnue', { force: true });
        // then
        cy.get('c-champ-recherche-territoire').find('c-lien').should('have.attr', 'href', 'http://lienformulaire');
        // when
    });

    it('Erreur réseau', () => {
        cy.intercept('GET', 'territoires?critere=*', RESPONSE_ERREUR_RESEAU);
        cy.mount<'c-champ-recherche-territoire'>(
            html`<c-champ-recherche-territoire
                apiBaseUrl="http://serveurbidon"
                lienFormulaireDemandeAjoutTerritoire="http://lienformulaire"
            ></c-champ-recherche-territoire>`
        );
        // when
        cy.get('c-champ-recherche-territoire').find('input').type('C', { force: true });
        // then
        cy.get('c-champ-recherche-territoire').find('menu').contains('Problème technique');
        // when
    });
});
