export class GroupeCulture {
    static readonly AutresCultures = new GroupeCulture('DVC', 'Autres cultures', '#9195d4', false, 1);
    static readonly FuitsEtLegumes = new GroupeCulture('FLC', 'Fruits et légumes', '#74a48b', false, 2);
    static readonly Oleoproteagineux = new GroupeCulture('OLP', 'Oléoprotéagineux', '#a38566', true, 3);
    static readonly Cereales = new GroupeCulture('CER', 'Céréales', '#ffba66', true, 4);
    static readonly Fourrages = new GroupeCulture('FOU', 'Fourrages', '#ce7676', true, 5);

    private constructor(
        public readonly code: string,
        public readonly nom: string,
        public readonly couleur: string,
        private readonly alimentationAnimale: boolean,
        private readonly ordre: number
    ) {}

    get nomCourt(): string {
        return this.code.slice(0, 2);
    }

    public comparer(groupeCulture: GroupeCulture): number {
        return groupeCulture.ordre - this.ordre;
    }

    static fromString(code: string): GroupeCulture {
        const groupeCulture = this.tous.find((g) => g.code === code);
        if (groupeCulture) return groupeCulture;
        else throw new RangeError(`Valeur incorrecte : Le code de culture ${code} ne correspondent à aucun des codes de l'énumération`);
    }

    static get tous(): GroupeCulture[] {
        return Object.values(GroupeCulture)
            .filter((c) => c instanceof GroupeCulture)
            .sort((a, b) => a.comparer(b));
    }

    static get groupesAlimentationAnimale(): GroupeCulture[] {
        return this.tous.filter((g) => g.alimentationAnimale);
    }

    static get codes(): string[] {
        return this.tous.map((gc) => gc.code);
    }
    static get couleurs(): string[] {
        return this.tous.map((gc) => gc.couleur);
    }
}
