import '../../commun/template/TemplatePageAvecSommaire.js';
import '@lga/design-system/build/composants/Lien.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_METHODOLOGIE, PAGES_PRINCIPALES } from '../../../domaines/pages/configuration/declaration-pages';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { construireLiensFilArianePagesMethodologie, ITEMS_MENU_METHODOLOGIE, STYLES_METHODOLOGIE } from './methodologie-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-methodologie-licence-utilisation': PageMethodologieLicenceUtilisation;
    }
}

@customElement('c-page-methodologie-licence-utilisation')
export class PageMethodologieLicenceUtilisation extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_METHODOLOGIE];

    render() {
        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${PAGES_METHODOLOGIE.licenceUtilisation.getId()}"
                .itemsMenuSommaire="${ITEMS_MENU_METHODOLOGIE}"
                .liensFilAriane="${construireLiensFilArianePagesMethodologie(
                    PAGES_METHODOLOGIE.licenceUtilisation.getTitreCourt(),
                    PAGES_METHODOLOGIE.licenceUtilisation.getUrl()
                )}"
                idElementCibleScroll=${this.idElementCibleScroll}
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    @property()
    idElementCibleScroll = '';

    private renderCoeurPage() {
        return html`
            <section class="texte-moyen">
                <h1>Licence d’utilisation du contenu et des données CRATer</h1>
                <p>
                    Sauf mention contraire, les textes et graphiques sont publiés par l'association Les Greniers d'Abondance sous la
                    <c-lien href="https://creativecommons.org/licenses/by-nc-sa/4.0/">licence CC BY-NC-SA 4.0</c-lien>
                    (Creative Commons, Attribution, Pas d’utilisation commerciale, Partage dans les mêmes conditions).
                </p>
                <p>
                    Les données accessibles dans l’application ou via l’API CRATer sont soumises aux conditions d’utilisations décrites ci-dessous :
                </p>
                <ol>
                    <li>
                        Les données concernant les besoins en surfaces agricoles par culture proviennent de l'application PARCEL et ne peuvent être
                        utilisées sans l’accord des porteurs du projet (voir
                        <c-lien href="http://www.parcel-app.org/partenaires-du-projet">http://www.parcel-app.org/partenaires-du-projet</c-lien>).
                    </li>
                    <li>
                        Les données concernant les indicateurs HVN sont fournies par SOLAGRO, et sont soumises aux règles d’utilisation suivantes :
                        <ul>
                            <li>
                                toute rediffusion ou ré-utilisation nécessite un accord préalable de Solagro. La demande est à adresser par message
                                électronique à
                                <c-lien href="mailto:solagro@solagro.asso.fr">solagro@solagro.asso.fr</c-lien>
                            </li>
                            <li>
                                toute réutilisation doit mentionner la paternité de l’information en faisant référence au site web de
                                <c-lien href="https://www.solagro.org">Solagro</c-lien>, ainsi qu’aux rapports décrivant la méthodologie de calcul des
                                indicateurs HVN précisés dans cet
                                <c-lien href="https://solagro.org/nos-domaines-d-intervention/agroecologie/haute-valeur-naturelle">article</c-lien>.
                            </li>
                        </ul>
                    </li>
                    <li>
                        Toutes les autres données, y compris celles calculées par CRATer sont soumises à la “Licence ouverte” définie par ETALAB.
                        Cette licence autorise l’utilisation et la redistribution sans accord préalable, mais sous réserve de mentionner la paternité
                        de la donnée. Voir
                        <c-lien href="https://www.etalab.gouv.fr/licence-ouverte-open-licence"
                            >https://www.etalab.gouv.fr/licence-ouverte-open-licence</c-lien
                        >
                        pour le détail de ces conditions d’usage.
                    </li>
                    <li>
                        Le format des données exposées va évoluer au fil des versions, sans garantie de compatibilité ascendante. Cela est valable en
                        particulier pour l'API CRATer. Si vous utilisez cette API dans vos applications, merci de nous l'indiquer via le
                        <c-lien href=${PAGES_PRINCIPALES.contact.getUrl()}>formulaire de contact</c-lien> afin que nous vous tenions informé de ces
                        différentes évolutions et des impacts éventuels dans vos applications.
                    </li>
                    <li>
                        Citer l'application comme suit :
                        <q
                            >Les Greniers d'Abondance, Calculateur pour la Résilience Alimentaire des Territoires (CRATer). [Application disponible en
                            ligne :
                            <c-lien href="https://crater.resiliencealimentaire.org">https://crater.resiliencealimentaire.org</c-lien>
                            (consultée le XX/XX/20XX)]</q
                        >.
                    </li>
                </ol>
            </section>
        `;
    }
}
