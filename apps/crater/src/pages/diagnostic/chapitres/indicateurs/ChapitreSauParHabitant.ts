import '../../charts/ChartBarres.js';
import '../composants/IndicateurDetaille.js';
import '../../../commun/PageSablier.js';

import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import {
    SEUIL_SAU_PAR_HAB_M2_REGIME_ACTUEL,
    SEUIL_SAU_PAR_HAB_M2_REGIME_MOINS_CARNE,
    SEUIL_SAU_PAR_HAB_M2_REGIME_TRES_VEGETAL
} from '../../../../domaines/diagnostics/index.js';
import { EtatRapport, Rapport } from '../../../../domaines/diagnostics/Rapport.js';
import { DonneesPageDiagnostic } from '../../../../domaines/pages/donnees-pages.js';
import { IDS_INDICATEURS } from '../../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { SA } from '../../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import { OptionsChartBarres } from '../../charts/ChartBarres.js';
import { OptionsIndicateurDetaille } from '../composants/IndicateurDetaille.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-sau-par-habitant': ChapitreSauParHabitant;
    }
}

@customElement('c-chapitre-sau-par-habitant')
export class ChapitreSauParHabitant extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-indicateur-detaille
                .options=${this.calculerOptionsIndicateurDetailleSauParHabitant(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="5"
            >
                <c-chart-barres .options=${this.calculerOptionsChartSauParHabitant(this.rapport)} slot="chart-indicateur-detaille"></c-chart-barres>
            </c-indicateur-detaille>`;
        } else {
            return html`<c-page-sablier></c-page-sablier>`;
        }
    }

    private calculerOptionsIndicateurDetailleSauParHabitant(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurDetaille {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.sauParHabitant)!,
            message: rapport.getDiagnosticActif()!.terresAgricoles.messageSauParHabitant,
            donneesPage: donneesPage,
            echelleEstDisponibleDansCarte: rapport.echelleEstDisponibleDansCarte()
        };
    }

    private calculerOptionsChartSauParHabitant(rapport: Rapport): OptionsChartBarres {
        const labels = rapport.getListeDiagnostics()!.map((d) => d!.territoire.nom);
        return {
            series: new SerieMultiple([new Serie(rapport.getListeDiagnostics()!.map((d) => d!.terresAgricoles.sauParHabitantM2))], labels),
            nomAxeOrdonnees: 'Surface Agricole par habitant (m²)',
            annotations: {
                yaxis: [
                    {
                        y: SEUIL_SAU_PAR_HAB_M2_REGIME_TRES_VEGETAL,
                        borderColor: 'var(--couleur-neutre-80)',
                        strokeDashArray: 5,
                        label: {
                            style: {
                                color: 'var(--couleur-blanc)',
                                background: 'var(--couleur-neutre-40)'
                            },
                            text: 'Surface nécessaire pour un régime alimentaire très végétal'
                        }
                    },
                    {
                        y: SEUIL_SAU_PAR_HAB_M2_REGIME_MOINS_CARNE,
                        borderColor: 'var(--couleur-neutre-80)',
                        strokeDashArray: 5,
                        label: {
                            style: {
                                color: 'var(--couleur-blanc)',
                                background: 'var(--couleur-neutre-40)'
                            },
                            text: 'Surface nécessaire pour un régime plus végétal'
                        }
                    },
                    {
                        y: SEUIL_SAU_PAR_HAB_M2_REGIME_ACTUEL,
                        borderColor: 'var(--couleur-neutre-80)',
                        strokeDashArray: 5,
                        label: {
                            style: {
                                color: 'var(--couleur-blanc)',
                                background: 'var(--couleur-neutre-40)'
                            },
                            text: 'Surface nécessaire pour le régime alimentaire actuel'
                        }
                    }
                ]
            }
        };
    }
}
