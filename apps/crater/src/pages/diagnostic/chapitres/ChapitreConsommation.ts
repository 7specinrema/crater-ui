import '../../commun/TimeLine.js';
import '../charts/ChartTreemapConsommation';
import './composants/CommentAgir.js';
import './composants/IndicateurDetaille.js';
import './composants/IndicateurResume.js';
import './composants/PourquoiCestImportant.js';
import './composants/VoirEgalement.js';
import './SyntheseMaillon.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import { formaterNombreEnEntierString, QualificatifEvolution } from '@lga/commun/build/outils/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { Rapport } from '../../../domaines/diagnostics';
import { DonneesPageDiagnostic } from '../../../domaines/pages/donnees-pages';
import { construireUrlPageDiagnosticMaillon } from '../../../domaines/pages/pages-utils';
import { IDS_INDICATEURS } from '../../../domaines/systeme-alimentaire/configuration/indicateurs.js';
import { IDS_MAILLONS } from '../../../domaines/systeme-alimentaire/configuration/maillons';
import { SA } from '../../../domaines/systeme-alimentaire/configuration/systeme-alimentaire';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import { creerOptionsSyntheseMaillon } from './chapitres-outils';
import { OptionsIndicateurResume } from './composants/IndicateurResume';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-consommation': ChapitreConsommation;
    }
}

@customElement('c-chapitre-consommation')
export class ChapitreConsommation extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_CHAPITRES_DIAGNOSTIC];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly maillon = SA.getMaillon(IDS_MAILLONS.consommation);

    render() {
        if (!this.rapport) return;

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.maillon?.nom}</h1>

                <c-synthese-maillon
                    .options=${creerOptionsSyntheseMaillon(this.rapport, this.rapport.getDiagnosticActif()!.consommation, IDS_MAILLONS.consommation)}
                >
                </c-synthese-maillon>

                <h2 id="pourquoi-cest-important">Pourquoi c'est important ?</h2>
                <c-pourquoi-cest-important .maillon=${this.maillon}></c-pourquoi-cest-important>

                <h2 id="etat-lieux">Quel est l'état des lieux ?</h2>
                <div class="indicateurs-resumes">
                    <p>Quelle surface est nécessaire pour produire l'alimentation d'origine animale ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-indicateurs-resumes">
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumePartAlimentationAnimaleDansConsommation(this.rapport, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.partAlimentationAnimaleDansConsommation}"
                        ></c-indicateur-resume>
                    </div>
                    <p>Est-ce que notre alimentation est financièrement accessible pour tous ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-indicateurs-resumes">
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumeTauxPauvrete(this.rapport, this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.tauxPauvrete}"
                        ></c-indicateur-resume>
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumeAideAlimentaire(this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.aideAlimentaire}"
                        ></c-indicateur-resume>
                    </div>
                    <p>Quel est l'impact de notre alimentation sur la santé ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-indicateurs-resumes">
                        <c-indicateur-resume
                            .options=${this.calculerOptionsIndicateurResumePartPopulationObese(this.donneesPage)}
                            id="indicateur-resume-${IDS_INDICATEURS.partPopulationObese}"
                        ></c-indicateur-resume>
                    </div>
                </div>

                <c-voir-egalement .liensIndicateurs=${this.maillon!.indicateursAutresMaillons} .donneesPage=${this.donneesPage}></c-voir-egalement>

                <h2 id="leviers-action">Comment ma collectivité peut-elle s&#39;améliorer ?</h2>
                <c-comment-agir
                    .maillon=${this.maillon}
                    idTerritoireParcel=${ifDefined(this.rapport.getDiagnosticActif()?.idTerritoireParcel || undefined)}
                ></c-comment-agir>

                <c-encart-pages-precedente-suivante
                    libellePagePrecedente=${ifDefined(SA.getMaillon(IDS_MAILLONS.transformationDistribution)?.nom)}
                    hrefPagePrecedente=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.transformationDistribution,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsIndicateurResumePartAlimentationAnimaleDansConsommation(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partAlimentationAnimaleDansConsommation)!,
            chiffresCles: [
                {
                    chiffre: `${formaterNombreEnEntierString(
                        rapport.getDiagnosticActif()!.consommation!.indicateurConsommation!.pourcentageConsommationAnimauxSurConsommationTotale!
                    )} %`,
                    libelleApresChiffre: `des surfaces sont utilisées pour la production d’aliments à destination des animaux d'élevage`
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsIndicateurResumeTauxPauvrete(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.tauxPauvrete)!,
            chiffresCles: [
                {
                    chiffre: formaterNombreEnEntierString(rapport.getDiagnosticActif()!.consommation.tauxPauvrete!) + ' %',
                    libelleApresChiffre: 'de la population vit sous le seuil de pauvreté'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsIndicateurResumeAideAlimentaire(donneesPage: DonneesPageDiagnostic | undefined): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.aideAlimentaire)!,
            chiffresCles: [
                {
                    chiffre: '> 5 millions',
                    libelleApresChiffre: 'de personnes',
                    evolutionQualificatif: QualificatifEvolution.declin,
                    evolutionDescription: 'x2 en 10 ans'
                },
                {
                    libelleAvantChiffre: 'soit',
                    chiffre: '1 Français sur 10'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsIndicateurResumePartPopulationObese(donneesPage: DonneesPageDiagnostic | undefined): OptionsIndicateurResume {
        return {
            indicateur: SA.getIndicateur(IDS_INDICATEURS.partPopulationObese)!,
            chiffresCles: [
                {
                    chiffre: '17 %',
                    libelleApresChiffre: 'des plus de 18 ans',
                    evolutionQualificatif: QualificatifEvolution.declin,
                    evolutionDescription: 'x3 depuis 1992'
                }
            ],
            donneesPage: donneesPage
        };
    }
}
